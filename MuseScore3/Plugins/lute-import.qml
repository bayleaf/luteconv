
//=============================================================================
//  MuseScore
//  Imports a variety of lute tablature file formats into MuseScore using luteconv
//
//  Copyright (C) 2020-2025 Paul Overell
//
//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License version 2.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software
//  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
//=============================================================================

import QtQuick 2.1
import QtQuick.Dialogs 1.0
import MuseScore 3.0
import FileIO 3.0

MuseScore {
	menuPath: "Plugins.lute-import";
    version:  "3.0";
    description: "Imports a variety of lute tablature file formats into MuseScore using luteconv";
    requiresScore: false;

    QProcess {
    	id: proc;
    }

    onRun: {
    	fileDialog.open();
    }

    FileIO {
        id: myFileSrc;
        onError: console.log(msg + "  Filename = " + myFileSrc.source);
    }

    FileIO {
        id: myTemp;
        source: tempPath()
        onError: console.log(msg);
    }

	function basename(str)
	{
    	return (str.slice(str.lastIndexOf("/")+1))
	}
	
    FileDialog {
        id: fileDialog
        title: qsTr("Please choose a lute tablature file");
        nameFilters: [ "Lute tablature files (*.abc *.frt *.ft2 *.ft3 *.jtxml *.jtz *.lsml *.mei *.mid *.mnx *.musicxml *.mxl *.tab *.tc)", "All files (*)" ];
        onAccepted: {
            var filename = fileDialog.fileUrl;
            console.log("You chose: " + filename);

            if (filename) {
                myFileSrc.source = filename;
                
                // lose initial part of url file://
            	var srcFile = myFileSrc.source.replace(/^(file:\/{2})/,"");
        		// unescape html codes like '%23' for '#'
        		srcFile = decodeURIComponent(srcFile);
        		var dstFile = myTemp.source + "/" + basename(srcFile) + ".musicxml";
        
        		var cmd = "luteconv -V -d musicxml " + srcFile + " " + dstFile;
        		console.log(cmd);
        		proc.start(cmd);
        		var val = proc.waitForFinished(30000);
        		console.log("luteconv finished");
            	if (val)
                	console.log(proc.readAllStandardOutput());
                	
                // load musicxml
            	readScore(dstFile);
            	Qt.quit();
            }
        }
        onRejected: {
	        console.log("Canceled");
        	Qt.quit();
    	}
    }
}

