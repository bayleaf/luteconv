#include <gtest/gtest.h>
#include <converter.h>

#include <dirent.h>
#include <sys/stat.h>
#include <fstream>
#include <regex>

class LuteConvConvertFixture: public ::testing::Test
{
protected:
    void SetUp() override
    {
#define XSTRINGIFY(s) STRINGIFY(s)
#define STRINGIFY(s) #s
        m_binaryDir = XSTRINGIFY(BINARY_DIR);
        m_sourceDir = XSTRINGIFY(SOURCE_DIR);
#undef STRINGIFY
#undef XSTRINGIFY
    
        std::cout << "BIN_DIRECTORY=" << m_binaryDir << '\n';
        std::cout << "SOURCE_DIRECTORY=" << m_sourceDir << '\n';
    }
    
    static void ConvertOneTest(const std::string& originalDir, const std::string& dstDir,
            const std::string& convertedDir, const std::string& filename);

    static void Diff(const std::string& lhs, const std::string& rhs);
    
    std::string m_binaryDir;
    std::string m_sourceDir;
};

TEST_F(LuteConvConvertFixture, FixtureTest)
{

}

TEST_F(LuteConvConvertFixture, ConvertTest)
{
    const std::string originalDir = m_sourceDir + "/examples/original";
    const std::string convertedDir = m_sourceDir + "/examples/converted";
    const std::string dstDir = m_binaryDir + "/converter_test";
    
    mkdir(dstDir.c_str(), S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH);
    
    struct dirent* entry{nullptr};
    DIR* dir = opendir(originalDir.c_str());
    EXPECT_NE(nullptr, dir);
    
    while ((entry = readdir(dir)) != nullptr)
    {
        if (entry->d_type == DT_REG)
        {
            ConvertOneTest(originalDir, dstDir, convertedDir, entry->d_name);
        }
    }
    closedir(dir);
}

void LuteConvConvertFixture::ConvertOneTest(const std::string& originalDir, const std::string& dstDir,
        const std::string& convertedDir, const std::string& filename)
{
    using namespace luteconv;
    
    for (const auto* filetype : {".abc", ".frt", ".lsml", ".ltl", ".mei", ".mnx", ".musicxml", ".tab", ".tc"})
    {
        Options options;
        options.m_srcFilename = (originalDir + "/").append(filename);
        options.m_dstFilename = (dstDir + "/").append(filename).append(filetype);
        options.SetFormatFilename();
        
        // convert original into dstDir
        const Converter converter;
        EXPECT_NO_THROW(converter.Convert(options));
        
        // compare convert with expected
        Diff((convertedDir + "/").append(filename).append(filetype), options.m_dstFilename);
    }
}

void LuteConvConvertFixture::Diff(const std::string& lhs, const std::string& rhs)
{
    std::fstream lhsStream(lhs, std::fstream::in);
    std::fstream rhsStream(rhs, std::fstream::in);
    
    std::string lhsLine;
    std::string rhsLine;
    
    while (!lhsStream.eof() && !rhsStream.eof())
    {
        getline(lhsStream, lhsLine);
        getline(rhsStream, rhsLine);
        
        if (lhsLine != rhsLine)
        {
            // allow dates and our version number to differ
            static const std::regex re{R"((2\d\d\d|luteconv))"};
            std::smatch results;
            if (!std::regex_search(lhsLine, results, re))
            {
                std::cout << "Compare " << lhs << " with " << rhs << '\n';
                EXPECT_EQ(lhsLine, rhsLine);
                break;
            }
        }
    }
    
    EXPECT_EQ(lhsStream.eof(), rhsStream.eof());
}

int main(int argc, char **argv)
{
	::testing::InitGoogleTest(&argc, argv);
	return RUN_ALL_TESTS();
}
