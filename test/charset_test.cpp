#include <gtest/gtest.h>

#include <charset.h>

using namespace luteconv;

class LuteConvCharsetTest: public ::testing::Test
{
};

TEST_F(LuteConvCharsetTest, FixtureTest)
{
    
}

TEST_F(LuteConvCharsetTest, TabTest)
{
    const std::string tabString{R"(abc\AE\=Exyz)"};
    const std::string utf8String{"abc\xc3\x86\xc4\x92xyz"};

    EXPECT_EQ(utf8String, CharSet::TabToUtf8(tabString));
    EXPECT_EQ(tabString, CharSet::Utf8ToTab(utf8String));
}

TEST_F(LuteConvCharsetTest, Iso8859_1Test)
{
    const std::string iso8859_1String{"abc\xA3\xE8xyz"};
    const std::string utf8String{"abc\xc2\xa3\xc3\xa8xyz"};

    EXPECT_EQ(utf8String, CharSet::Iso8859_1ToUtf8(iso8859_1String));
    EXPECT_EQ(iso8859_1String, CharSet::Utf8ToIso8859_1(utf8String));
}

TEST_F(LuteConvCharsetTest, Windows1252Test)
{
    const std::string windows1252String{"abc\xA3\xE8\x8A\x91xyz"};
    const std::string utf8String{"abc\xc2\xa3\xc3\xa8\xc5\xa0\xe2\x80\x98xyz"};

    EXPECT_EQ(utf8String, CharSet::Cp1252ToUtf8(windows1252String));
}

int main(int argc, char **argv)
{
	::testing::InitGoogleTest(&argc, argv);
	return RUN_ALL_TESTS();
}
