#include <gtest/gtest.h>

#include <cstring>
#include <iostream>
#include <sstream>

#include <gentab.h>
#include <parsertab.h>
#include <pitch.h>

using namespace luteconv;

class LuteConvPitchFixture: public ::testing::Test
{
};

TEST_F(LuteConvPitchFixture, FixtureTest)
{
    
}

TEST_F(LuteConvPitchFixture, Midi)
{
    EXPECT_EQ(12, Pitch('C', 0, 0).Midi());
    EXPECT_EQ(59, Pitch('C', -1, 4).Midi());
    EXPECT_EQ(60, Pitch('C', 0, 4).Midi());
    EXPECT_EQ(61, Pitch('C', 1, 4).Midi());
    
    EXPECT_EQ(Pitch(12), Pitch('C', 0, 0));
    EXPECT_EQ(Pitch(59), Pitch('B', 0, 3));
    EXPECT_EQ(Pitch(60), Pitch('C', 0, 4));
    EXPECT_EQ(Pitch(61), Pitch('C', 1, 4));
}

TEST_F(LuteConvPitchFixture, AddSemitones)
{
    const Pitch p('A', 0, 4);
    
    EXPECT_EQ(Pitch('A', 0, 4), p + 0);
    EXPECT_EQ(Pitch('B', -1, 4), p + 1);
    EXPECT_EQ(Pitch('B', 0, 4), p + 2);
    EXPECT_EQ(Pitch('C', 0, 5), p + 3);
    EXPECT_EQ(Pitch('C', 1, 5), p + 4);
    EXPECT_EQ(Pitch('D', 0, 5), p + 5);
    EXPECT_EQ(Pitch('D', 1, 5), p + 6);
    EXPECT_EQ(Pitch('E', 0, 5), p + 7);
    EXPECT_EQ(Pitch('F', 0, 5), p + 8);
    EXPECT_EQ(Pitch('F', 1, 5), p + 9);
    EXPECT_EQ(Pitch('G', 0, 5), p + 10);
    EXPECT_EQ(Pitch('G', 1, 5), p + 11);
    EXPECT_EQ(Pitch('A', 0, 5), p + 12);
    EXPECT_EQ(Pitch('B', -1, 5), p + 13);
}

TEST_F(LuteConvPitchFixture, SetTuningByCourses)
{
    std::vector<Pitch> tuning;
    Pitch::SetTuning(6, tuning);
    EXPECT_EQ(Pitch('G', 0, 4), tuning[0]);
    EXPECT_EQ(Pitch('D', 0, 4), tuning[1]);
    EXPECT_EQ(Pitch('A', 0, 3), tuning[2]);
    EXPECT_EQ(Pitch('F', 0, 3), tuning[3]);
    EXPECT_EQ(Pitch('C', 0, 3), tuning[4]);
    EXPECT_EQ(Pitch('G', 0, 2), tuning[5]);
}

TEST_F(LuteConvPitchFixture, SetTuningByString)
{
    std::vector<Pitch> tuning;
    Pitch::SetTuning("G4 D4 A3 F3 C3 G2", tuning);
    EXPECT_EQ(Pitch('G', 0, 4), tuning[0]);
    EXPECT_EQ(Pitch('D', 0, 4), tuning[1]);
    EXPECT_EQ(Pitch('A', 0, 3), tuning[2]);
    EXPECT_EQ(Pitch('F', 0, 3), tuning[3]);
    EXPECT_EQ(Pitch('C', 0, 3), tuning[4]);
    EXPECT_EQ(Pitch('G', 0, 2), tuning[5]);
}

TEST_F(LuteConvPitchFixture, SetTuningTab)
{
    std::vector<Pitch> tuning;
    ParserTab::SetTuningTab("G4c3f3a2d2g2", tuning);
    EXPECT_EQ(Pitch('G', 0, 4), tuning[0]);
    EXPECT_EQ(Pitch('D', 0, 4), tuning[1]);
    EXPECT_EQ(Pitch('A', 0, 3), tuning[2]);
    EXPECT_EQ(Pitch('F', 0, 3), tuning[3]);
    EXPECT_EQ(Pitch('C', 0, 3), tuning[4]);
    EXPECT_EQ(Pitch('G', 0, 2), tuning[5]);
}

TEST_F(LuteConvPitchFixture, GetTuning)
{
    std::vector<Pitch> tuning;
    Pitch::SetTuning("G4 D4 A3 F3 C3 G2", tuning);
    EXPECT_EQ("G4D4A3F3C3G2", Pitch::GetTuning(tuning));
}

TEST_F(LuteConvPitchFixture, GetTuningTab)
{
    std::vector<Pitch> tuning;
    Pitch::SetTuning("G4 D4 A3 F3 C3 G2", tuning);
    EXPECT_EQ("g4c3f3a2d2g2", GenTab::GetTuningTab(tuning));
}

TEST_F(LuteConvPitchFixture, NegativeOctaves)
{
    EXPECT_EQ(0, Pitch('C', 0, -1).Midi());
    EXPECT_EQ(-11, Pitch('C', 1, -2).Midi());
    EXPECT_EQ(-1, Pitch('B', 0, -2).Midi());
    
    EXPECT_EQ(Pitch('C', 0, -1), Pitch(0));
    EXPECT_EQ(Pitch('C', 1, -2), Pitch(-11));
    EXPECT_EQ(Pitch('B', 0, -2), Pitch(-1));
    
    EXPECT_EQ(std::string("C#-2"), Pitch('C', 1, -2).ToString());
    size_t pos{0};
    EXPECT_EQ(Pitch('C', 1, -2), Pitch::Parse("C#-2", pos));
}

TEST_F(LuteConvPitchFixture, Cflat)
{
    EXPECT_EQ(59, Pitch('C', -1, 4).Midi());
    EXPECT_EQ(Pitch('B', 0, 3).Midi(), Pitch('C', -1, 4).Midi());    
}

TEST_F(LuteConvPitchFixture, Relations)
{
    const Pitch A4{'A', 0, 4};
    const Pitch B4{'B', 0, 4};
    const Pitch C4{'C', 0, 4};
    const Pitch D4{'D', 0, 4};
    const Pitch E4{'E', 0, 4};
    const Pitch F4{'F', 0, 4};
    const Pitch G4{'G', 0, 4};

    const Pitch B3{'B', 0, 3};
    const Pitch D3{'D', 0, 3};
    const Pitch Ds4{'D', 1, 4};
    const Pitch Eb4{'E', -1, 4};
    
    EXPECT_NE(D4, Ds4);
    EXPECT_TRUE(D4 < Ds4);
    
    EXPECT_NE(Ds4, Eb4);
    EXPECT_EQ(Ds4.Midi(), Eb4.Midi());
    EXPECT_TRUE(Ds4 < Eb4);
    EXPECT_FALSE(Eb4 < Ds4);
    
    EXPECT_TRUE(D3 < D4);
    EXPECT_FALSE(D4 < D3);

    EXPECT_TRUE(A4 < B4);
    EXPECT_TRUE(G4 < A4);
    EXPECT_TRUE(F4 < G4);
    EXPECT_TRUE(E4 < F4);
    EXPECT_TRUE(D4 < E4);
    EXPECT_TRUE(C4 < D4);
    EXPECT_TRUE(B3 < C4);
}

int main(int argc, char **argv)
{
	::testing::InitGoogleTest(&argc, argv);
	return RUN_ALL_TESTS();
}
