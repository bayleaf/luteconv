#include <gtest/gtest.h>

#include <rational.h>

using namespace luteconv;

class LuteConvRationalTest: public ::testing::Test
{
};

TEST_F(LuteConvRationalTest, FixtureTest)
{
    
}

TEST_F(LuteConvRationalTest, Construct)
{
    EXPECT_EQ(Rational(2, 3), Rational(12, 18));
    EXPECT_EQ(Rational(2, 3), Rational(-12, -18));
    EXPECT_EQ(Rational(2, 3), -Rational(12, -18));
    EXPECT_EQ(Rational(2, 3), -Rational(-12, 18));
    EXPECT_EQ(Rational(-2, 3), Rational(12, -18));
}

TEST_F(LuteConvRationalTest, Arithmetic)
{
    EXPECT_EQ(Rational(2, 3), Rational(1, 3) + Rational(1, 3));
    EXPECT_EQ(Rational(2, 3), Rational(1) - Rational(1, 3));
    EXPECT_EQ(Rational(2, 3), Rational(2) * Rational(1, 3));
    EXPECT_EQ(Rational(2, 3), Rational(2) / Rational(3));
    
    EXPECT_EQ(Rational(67, 44), Rational(7, 66) + Rational(17, 12));
    EXPECT_EQ(Rational(-41, 168), Rational(17, 120) + Rational(-27, 70)); // Knuth II 4.5.1 ex. 5
    
    EXPECT_EQ(5, Rational(56, 10).Trunc());
    EXPECT_EQ(-5, Rational(-56, 10).Trunc());
}

TEST_F(LuteConvRationalTest, Assignment)
{
    Rational rat;
    
    rat = Rational(1, 3);
    rat += Rational(1, 3);
    EXPECT_EQ(Rational(2, 3), rat);
    
    rat = Rational(1);
    rat -= Rational(1, 3);
    EXPECT_EQ(Rational(2, 3), rat);
    
    rat = Rational(2);
    rat *= Rational(1, 3);
    EXPECT_EQ(Rational(2, 3), rat);
    
    rat = Rational(2);
    rat /= Rational(3);
    EXPECT_EQ(Rational(2, 3), rat);
}

TEST_F(LuteConvRationalTest, Relations)
{
    EXPECT_TRUE(Rational(123, 123) < Rational(124, 123));
    EXPECT_FALSE(Rational(123, 123) < Rational(122, 123));
    
    EXPECT_TRUE(Rational(123, 123) <= Rational(123, 123));
    EXPECT_TRUE(Rational(123, 123) <= Rational(124, 123));
    EXPECT_FALSE(Rational(123, 123) <= Rational(122, 123));
    
    EXPECT_TRUE(Rational(123, 123) == Rational(123, 123));
    EXPECT_FALSE(Rational(123, 123) == Rational(124, 123));
    
    EXPECT_TRUE(Rational(123, 123) != Rational(124, 123));
    EXPECT_FALSE(Rational(123, 123) != Rational(123, 123));
    
    EXPECT_TRUE(Rational(123, 123) >= Rational(123, 123));
    EXPECT_TRUE(Rational(124, 123) >= Rational(123, 123));
    EXPECT_FALSE(Rational(123, 123) >= Rational(124, 123));
    
    EXPECT_TRUE(Rational(124, 123) > Rational(123, 123));
    EXPECT_FALSE(Rational(123, 123) > Rational(124, 123));
}

TEST_F(LuteConvRationalTest, Strings)
{
    EXPECT_EQ(std::string("97/17"), Rational(97, 17).ToString());
}

int main(int argc, char **argv)
{
	::testing::InitGoogleTest(&argc, argv);
	return RUN_ALL_TESTS();
}
