#include "genmei.h"

#include <algorithm>
#include <cstdlib>
#include <ctime>
#include <chrono>
#include <fstream>
#include <iomanip>
#include <set>
#include <sstream>
#include <string>

#include "logger.h"
#include "mei.h"

namespace luteconv
{

using namespace pugi;

void GenMei::Generate(const Options& options, const Piece& piece, std::ostream& dst)
{
    LOGGER << "Generate mei";
    
    m_nextId = 0;
    m_beats = 4;
    
    xml_document doc;
    
    xml_node decl{doc.prepend_child(node_declaration)};
    decl.append_attribute("version") = "1.0";
    decl.append_attribute("standalone") = "no";
    
    // <mei xmlns='http://www.music-encoding.org/ns/mei' meiversion='5.1'>
    xml_node xmlmei{doc.append_child("mei")};
    xmlmei.append_attribute("xmlns") = "http://www.music-encoding.org/ns/mei";
    xmlmei.append_attribute("meiversion") = "5.1";
    
    xml_node xmlmeiHead{xmlmei.append_child("meiHead")};
    const xml_node xmlfileDesc{xmlmeiHead.append_child("fileDesc")};
    FileDesc(xmlfileDesc, piece);
    
    const xml_node xmlencodingDesc{xmlmeiHead.append_child("encodingDesc")};
    EncodingDesc(xmlencodingDesc, options);
    
    xml_node xmlworkDesc{xmlmeiHead.append_child("workList")};
    const xml_node xmlwork{xmlworkDesc.append_child("work")};
    Work(xmlwork, piece);
    
    xml_node xmlmusic{xmlmei.append_child("music")};
    const xml_node xmlBody{xmlmusic.append_child("body")};
    Body(xmlBody, options, piece);
    
    doc.save(dst, "    ");
}

void GenMei::FileDesc(xml_node xmlfileDesc, const Piece& piece)
{
    // titleStmt and pubStmt are mandatory
    xml_node xmltitleStmt{xmlfileDesc.append_child("titleStmt")};
    
    // mandatory
    xmltitleStmt.append_child("title").text().set(piece.m_title);
    
    if (!piece.m_composer.empty())
    {
        xmltitleStmt.append_child("composer").text().set(piece.m_composer);
    }

    // mandatory
    xmlfileDesc.append_child("pubStmt");
}
   
void GenMei::EncodingDesc(xml_node xmlEncodingDesc, const Options& options)
{
    xml_node xmlappInfo{xmlEncodingDesc.append_child("appInfo")};
    
    const std::time_t tt{std::chrono::system_clock::to_time_t(std::chrono::system_clock::now())};
    struct std::tm * ptm{std::gmtime(&tt)};
    std::ostringstream ss;
    ss << std::put_time(ptm,"%F");
    
    // Where we know which application was used to generate the source add an
    // <application> entity.  Fronimo, Fandango and LuteScribe.  For the other formats
    // we don't know what application was used.
    std::string app;
    std::string appVersion;
    if (options.m_srcFormat == FormatFt2)
    {
        app = "Fronimo";
        appVersion = "2";
    }
    else if (options.m_srcFormat == FormatFt3)
    {
        app = "Fronimo";
        appVersion = "3";
    }
    else if (options.m_srcFormat == FormatJtxml || options.m_srcFormat == FormatJtz)
    {
        app = "Fandango";
    }
    else if (options.m_srcFormat == FormatLsml)
    {
        app = "LuteScribe";
    }
    
    if (!app.empty())
    {
        xml_node xmlapplication{xmlappInfo.append_child("application")};
        xmlapplication.append_attribute("isodate") = ss.str();

        if (!appVersion.empty())
        {
            xmlapplication.append_attribute("version") = appVersion;
        }
        xmlapplication.append_child("name").text().set(app);
        xmlapplication.append_child("p").text().set("Typesetting");
    }
    
    xml_node xmlapplication{xmlappInfo.append_child("application")};
    xmlapplication.append_attribute("isodate") = ss.str();
    xmlapplication.append_attribute("version") = options.m_version;
    xmlapplication.append_child("name").text().set("luteconv");
}

void GenMei::Work(xml_node xmlwork, const Piece& piece)
{
    // title and composer
    xmlwork.append_child("title").text().set(piece.m_title);
    
    if (!piece.m_composer.empty())
    {
        xmlwork.append_child("composer").text().set(piece.m_composer);
    }
    
    // credits
    if (!piece.m_credits.empty())
    {
        xml_node xmlnotesStmt{xmlwork.append_child("notesStmt")};
        for (const auto& credit : piece.m_credits)
        {
            xmlnotesStmt.append_child("annot").text().set(credit);
        }
    }
}
   
void GenMei::Body(xml_node xmlbody, const Options& options, const Piece& piece)
{
    xml_node xmlmdiv{xmlbody.append_child("mdiv")};
    xmlmdiv.append_attribute("n") = 1;
    
    xml_node xmlscore{xmlmdiv.append_child("score")};
    xml_node xmlscoreDef{xmlscore.append_child("scoreDef")};
    xml_node xmlstaffGrp{xmlscoreDef.append_child("staffGrp")};
    
    int partNo{1};
    for (const auto& part : piece.m_parts)
    {
        TabType tabType{part.m_tabType};
        const std::set<TabType> tabTypes{ TabFrench, TabGerman, TabItalian, TabSpanish, TabCmn };

        if (tabTypes.find(tabType) == tabTypes.end())
        {
            LOGGER << "MEI does not support " << Options::GetTabType(tabType) << " tablature type. Using french.";
            tabType = TabFrench;
        }

        xml_node xmlstaffDef{xmlstaffGrp.append_child("staffDef")};
        xmlstaffDef.append_attribute("n") = partNo++;
        
        // lines
        if (tabType == TabGerman)
        {
            xmlstaffDef.append_attribute("lines") = part.MaxChord();
        }
        else
        {
            xmlstaffDef.append_attribute("lines") = part.m_staffLines;
        }
        
        // label
        if (!part.m_partName.empty())
        {
            xmlstaffDef.append_child("label").text().set(part.m_partName);
        }
        
        // clef
        Clef clef{ClefNone};
        if (part.m_tabType != TabCmn)
        {
            clef = ClefTab;
        }
        else if (!part.m_bars.empty())
        {
            clef =  part.m_bars.front().m_clef;
        }
        AddClef(xmlstaffDef, clef);

        // keySig
        if (part.m_tabType == TabCmn && !part.m_bars.empty() && part.m_bars.front().m_key != KeyNone)
        {
            AddKeySignature(xmlstaffDef, part.m_bars.front().m_key);
        }
        
        // meterSig
        if (!part.m_bars.empty())
        {
            AddTimeSignature(xmlstaffDef, part.m_bars.front().m_timeSig);
        }

        // notationtype
        if (part.m_tabType != TabCmn)
        {
            xmlstaffDef.append_attribute("notationtype") = Mei::notationtype[tabType];
            
            // <tuning> always plant the full tuning table
            xml_node xmltuning{xmlstaffDef.append_child("tuning")};
        
            // <course n='1' pname='g' oct='4'>
            int course{1};
            for (const auto & tuning : part.m_tuning)
            {
                xml_node xmlcourse{xmltuning.append_child("course")};
                xmlcourse.append_attribute("n") = course;
                const char pname[]{static_cast<char>(tolower(tuning.Step())), '\0'};
                xmlcourse.append_attribute("pname") = pname;
                xmlcourse.append_attribute("oct") = tuning.Octave();
                if (tuning.Alter() == 1)
                {
                    xmlcourse.append_attribute("accid") = "s";
                }
                else if (tuning.Alter() == -1)
                {
                    xmlcourse.append_attribute("accid") = "f";
                }
                ++course;
            }
        }
    }

    // section
    xml_node xmlsection{xmlscore.append_child("section")};
    xmlsection.append_attribute("n") = 1;
    Section(xmlsection, options, piece);
}
    
void GenMei::Section(xml_node xmlsection, const Options& options, const Piece& piece)
{
    // all parts should have the same number of bars, we will cope if they don't
    size_t numBars{0};
    size_t partMostBarsIdx{0};
    for (const auto& part : piece.m_parts)
    {
        const auto partIdx{static_cast<size_t>(std::distance(&piece.m_parts.front(), &part))};

        if (numBars < part.m_bars.size())
        {
            numBars = part.m_bars.size();
            partMostBarsIdx = partIdx;
        }
    }
    
    std::vector<std::map<int, std::string>> slurStarts(piece.m_parts.size());
    std::vector<std::map<int, std::string>> holdStarts(piece.m_parts.size());
    std::vector<Clef> curClef(piece.m_parts.size()); // clef, key, time sig debouncing
    std::vector<Key> curKey(piece.m_parts.size());
    std::vector<TimeSig> curTimeSig(piece.m_parts.size());
    std::vector<uint8_t> tieTerminalPending(piece.m_parts.size(), static_cast<uint8_t>(false)); // can't use vector<bool> need to assign by reference to elements

    // Timewise: each measure contains their parts
    for (size_t barIdx{0}; barIdx < numBars; ++barIdx)
    {
        // all parts share the same volta brackets, repeats and bar sytles
        const Part& mostBarsPart{piece.m_parts.at(partMostBarsIdx)};
        const Bar& mostBarsBar{mostBarsPart.m_bars.at(barIdx)};
        const size_t barNo{barIdx + 1};

        xml_node xmlmeasure{};
        if (mostBarsBar.m_volta > 0)
        {
            xml_node xmlending{xmlsection.append_child("ending")};
            xmlending.append_attribute("n") = mostBarsBar.m_volta;
            xmlending.append_attribute("lendsym") = (mostBarsBar.m_volta == 1 ? "angledown" : "none");
            xmlmeasure = xmlending.append_child("measure");
        }
        else
        {
            xmlmeasure = xmlsection.append_child("measure");
        }
        xmlmeasure.append_attribute("n") = barNo;
        
        // repeat
        switch (mostBarsBar.m_repeat)
        {
        case RepNone:
            break;
        case RepForward:
            xmlmeasure.append_attribute("right") = "rptstart";
            break;
        case RepBackward:
            xmlmeasure.append_attribute("right") = "rptend";
            break;
        case RepJanus:
            xmlmeasure.append_attribute("right") = "rptboth";
            break;
        }
        
        // bar style, if not a repeat
        if (mostBarsBar.m_repeat == RepNone && mostBarsBar.m_barStyle != BarStyleRegular)
        {
            const std::string barStyle{Mei::barStyle[mostBarsBar.m_barStyle]};
            if (!barStyle.empty())
            {
                xmlmeasure.append_attribute("right") = barStyle;
            }
        }
        
        if (mostBarsBar.m_fermata)
        {
            // fermata over a barline
            xml_node xmlfermata{xmlmeasure.append_child("fermata")};
            // can't use an xml:id, have to use a time stamp, time signature numerator + 1
            xmlfermata.append_attribute("tstamp") = m_beats + 1; // right barline
        }
    
        for (const auto& part : piece.m_parts)
        {
            const auto partIdx{static_cast<size_t>(std::distance(&piece.m_parts.front(), &part))};

            const Bar emptyBar;
            const Bar& bar{barIdx < part.m_bars.size() ? part.m_bars.at(barIdx) : emptyBar};
            xml_node xmlstaff{xmlmeasure.append_child("staff")};
            xmlstaff.append_attribute("n") = partIdx + 1;
            xml_node xmllayer{xmlstaff.append_child("layer")};
            xmllayer.append_attribute("n") = 1;
            
            // change of clef, key or time signature
            if (barNo == 1)
            {
                curClef.at(partIdx) = bar.m_clef;
                curKey.at(partIdx) = bar.m_key;
                curTimeSig.at(partIdx) = bar.m_timeSig;
            }
            else
            {
                if (bar.m_clef != ClefNone && bar.m_clef != curClef.at(partIdx))
                {
                    AddClef(xmllayer, bar.m_clef);
                    curClef.at(partIdx) = bar.m_clef;
                }
                if (bar.m_key != KeyNone && bar.m_key != curKey.at(partIdx))
                {
                    AddKeySignature(xmllayer, bar.m_key);
                    curKey.at(partIdx) = bar.m_key;
                }
                if (bar.m_timeSig.m_timeSymbol != TimeSyNone && bar.m_timeSig != curTimeSig.at(partIdx))
                {
                    AddTimeSignature(xmllayer, bar.m_timeSig);
                    curTimeSig.at(partIdx) = bar.m_timeSig;
                }
            }
            
            xml_node xmlbeam{};
            xml_node xmltuplet{};
            int tripletCount{0};
            
            for (const auto & chord : bar.m_chords)
            {
                xml_node xmlNoteHolder{xmllayer};
                
                // beams and tuplets are a problem as they can be nested inside each other
                // FIXME we only allow either a beam or a tuplet, no beamed triplets or tripleted beams
                
                if (chord.m_grid == GridStart && xmltuplet == nullptr)
                {
                    xmlbeam = xmllayer.append_child("beam");
                }
    
                if (chord.m_triplet && xmlbeam == nullptr)
                {
                    xmltuplet = xmllayer.append_child("tuplet");
                    xmltuplet.append_attribute("num") = 3;
                    xmltuplet.append_attribute("numbase") = 2;
                    tripletCount = bar.TripletNotes(chord);
                }
                
                if (chord.m_grid != GridNone && xmlbeam != nullptr)
                {
                    xmlNoteHolder = xmlbeam;
                    if (chord.m_grid == GridEnd)
                    {
                        xmlbeam = xml_node{};
                    }
                }
                else if (tripletCount != 0 && xmltuplet != nullptr)
                {
                    xmlNoteHolder = xmltuplet;
                    --tripletCount;
                    if (tripletCount == 0)
                    {
                        xmltuplet = xml_node{};
                    }
                }
               
                if (part.m_tabType != TabCmn)
                {
                    const xml_node xmltabGrp{xmlNoteHolder.append_child("tabGrp")};
                    AddNotesTab(xmltabGrp, xmlmeasure, part, chord, options, slurStarts.at(partIdx), holdStarts.at(partIdx));
                }
                else if (chord.m_notes.size() > 1)
                {
                    const xml_node xmlchord{xmlNoteHolder.append_child("chord")};
                    AddNotesCmn(xmlchord, chord, tieTerminalPending.at(partIdx));
                }
                else
                {
                    AddNotesCmn(xmlNoteHolder, chord, tieTerminalPending.at(partIdx));
                }
            }
        }
    }
}
                
std::string GenMei::GetDur(NoteType noteType)
{
    switch (noteType)
    {
        case NoteTypeLong:
            return "long";
        case NoteTypeBreve:
            return "breve";
        default:
            return std::to_string(1 << (noteType - NoteTypeWhole));
    }
    return {};
}

void GenMei::AddNotesTab(xml_node xmltabGrp, xml_node xmlmeasure, const Part& part, const Chord& chord,
        const Options& options, std::map<int, std::string>& slurStart, std::map<int, std::string>& holdStart)
{
    // mei has quarter note = 1 flag
    NoteType adjusted{static_cast<NoteType>(chord.m_noteType - 1 + options.m_flags)};
    adjusted = std::max(NoteTypeLong, adjusted);
    adjusted = std::min(NoteType256th, adjusted);
    const std::string dur{GetDur(adjusted)};
    xmltabGrp.append_attribute("dur") = dur;
    
    if (chord.m_dotted)
    {
        xmltabGrp.append_attribute("dots") = 1;
    }
    
    if (chord.m_fermata)
    {
        // fermata on a tabGrp, omit tabDurSym
        const std::string id{MakeId()};
        xmltabGrp.append_attribute("xml:id") = id;
    
        xml_node xmlfermata{xmlmeasure.append_child("fermata")};
        xmlfermata.append_attribute("startid") = "#" + id;
    }
    else if (!chord.m_noFlag)
    {
        xmltabGrp.append_child("tabDurSym");
    }
    
    for (size_t noteIndex{0}; noteIndex < chord.m_notes.size(); ++noteIndex)
    {
        const Note& note{chord.m_notes[noteIndex]};
    
        // if this note is just a dummy place holder to anchor a connector
        // then ignore it.  Its connector will have been put on the note above.
        // Ignore ensemble lines
        if (note.m_fret == Note::Anchor || note.m_fret == Note::Ensemble)
        {
            continue;
        }
    
        xml_node xmlnote{xmltabGrp.append_child("note")};
        xmlnote.append_attribute("tab.course") = note.m_course;
        xmlnote.append_attribute("tab.fret") = note.m_fret;
        
        // MIDI note number is optional but might be useful
        xmlnote.append_attribute("pnum") = part.CalcPitch(note).Midi();
    
        // connector.  Mei does not support dummy notes as anchor points for connectors
        // if this note does not have a connector and
        // the first or next note is a dummy anchor point, then put its connector
        // on this note
        size_t connectorIndex{noteIndex};
        if (note.m_connector.m_endPoint == EndPointNone)
        {
            if (noteIndex == 1 && chord.m_notes[0].m_fret == Note::Anchor)
            {
                connectorIndex = 0;
            }
            else if (noteIndex + 1 < chord.m_notes.size() &&
                chord.m_notes[noteIndex + 1].m_fret == Note::Anchor)
            {
                connectorIndex = noteIndex + 1;
            }
        }
        const Note& connectorNote{chord.m_notes[connectorIndex]};
        
        // need an xml::id if fingering or connector or ornament
        const std::string leftOrnamText{Mei::ornament[note.m_leftOrnament]};
        const std::string rightOrnamText{Mei::ornament[note.m_rightOrnament]};
        
        std::string id;
        if (note.m_leftFingering != FingerNone || note.m_rightFingering != FingerNone ||
                connectorNote.m_connector.m_endPoint != EndPointNone ||
                !leftOrnamText.empty() || !rightOrnamText.empty())
        {
            id = MakeId();
            xmlnote.append_attribute("xml:id") = id;
        }
        
        // fingering.
        // TODO Proposed fing@playingHand and fing@playingFinger didn't make it into MEI 5.1
        // so using fing@type.  Syntactically correct MEI 5.1, but undocumented.  Need a proper
        // extension to <fing> to specify lute fingering.
        // <fing type='right 1' startid='m3.n6'/>
        if (note.m_leftFingering != FingerNone)
        {
            xml_node xmlfing{xmlmeasure.append_child("fing")};
            xmlfing.append_attribute("type") = std::string("left") + ' ' + Mei::fingering[note.m_leftFingering];
            xmlfing.append_attribute("startid") = "#" + id;
        }
        if (note.m_rightFingering != FingerNone)
        {
            xml_node xmlfing{xmlmeasure.append_child("fing")};
            xmlfing.append_attribute("type") = std::string("right") + ' ' + Mei::fingering[note.m_rightFingering];
            xmlfing.append_attribute("startid") = "#" + id;
        }
        
        // ornament
        // <ornam ho="-2" startid='m3.n6'>#<ornam/>
        if (!leftOrnamText.empty())
        {
            xml_node xmlornam{xmlmeasure.append_child("ornam")};
            xmlornam.append_attribute("ho") = -2;
            xmlornam.append_attribute("startid") = "#" + id;
            xmlornam.text().set(leftOrnamText);
        }
        if (!rightOrnamText.empty())
        {
            xml_node xmlornam{xmlmeasure.append_child("ornam")};
            xmlornam.append_attribute("ho") = 2;
            xmlornam.append_attribute("startid") = "#" + id;
            xmlornam.text().set(rightOrnamText);
        }
        
        // start of slur or hold, record the xml::id
        if (connectorNote.m_connector.m_endPoint == EndPointStart)
        {
            if (connectorNote.m_connector.m_function == FunSlur)
            {
                slurStart[connectorNote.m_connector.m_number] = id;
            }
            else
            {
                holdStart[connectorNote.m_connector.m_number] = id;
            }
        }
        else if (connectorNote.m_connector.m_endPoint == EndPointStop)
        {
            if (connectorNote.m_connector.m_function == FunSlur)
            {
                // find start id
                std::string startId;
                auto it{slurStart.find(connectorNote.m_connector.m_number)};
                if (it != slurStart.end())
                {
                    startId = it->second;
                    slurStart.erase(it);
                }
                
                if (!startId.empty())
                {
                    xml_node xmlslur{xmlmeasure.append_child("slur")};
                    xmlslur.append_attribute("startid") = "#" + startId;
                    xmlslur.append_attribute("endid") = "#" + id;
                    if (connectorNote.m_connector.m_curve == CurveUnder)
                    {
                        xmlslur.append_attribute("curvedir") = "below";
                    }
                    else if (connectorNote.m_connector.m_curve == CurveOver)
                    {
                        xmlslur.append_attribute("curvedir") = "above";
                    }
                }        
            }
            else
            {
                // find start id
                std::string startId;
                auto it{holdStart.find(connectorNote.m_connector.m_number)};
                if (it != holdStart.end())
                {
                    startId = it->second;
                    holdStart.erase(it);
                }
                
                if (!startId.empty())
                {
                    xml_node xmlline{xmlmeasure.append_child("line")};
                    xmlline.append_attribute("startid") = "#" + startId;
                    xmlline.append_attribute("endid") = "#" + id;
                }
            }
        }
    }
}

void GenMei::AddNotesCmn(xml_node xmlparent, const Chord& chord, uint8_t& tieTerminalPending)
{
    const std::string dur{GetDur(chord.m_noteType)};
    
    xml_node xmlchild{};
    const bool parentIsChord{std::string(xmlparent.name()) == "chord"};
    
    if (chord.m_notes.empty())
    {
        xmlchild = xmlparent.append_child("rest");
        xmlchild.append_attribute("dur") = dur;
        
        if (chord.m_dotted)
        {
            xmlchild.append_attribute("dots") = 1;
        }
    }
    else
    {
        if (parentIsChord)
        {
            xmlparent.append_attribute("dur") = dur;
            if (chord.m_dotted)
            {
                xmlparent.append_attribute("dots") = 1;
            }
        }

        for (const auto& note : chord.m_notes)
        {
            xmlchild = xmlparent.append_child("note");

            if (!parentIsChord)
            {
                xmlchild.append_attribute("dur") = dur;
                if (chord.m_dotted)
                {
                    xmlchild.append_attribute("dots") = 1;
                }
            }
        
            const Pitch& pitch{note.m_pitch};
            const char pname[]{static_cast<char>(std::tolower(pitch.Step())), '\0'};
            xmlchild.append_attribute("pname") = pname;
            xmlchild.append_attribute("oct") = pitch.Octave();
        
            if (pitch.Alter() != 0)
            {
                xmlchild.append_attribute("accid.ges") = pitch.Alter() > 0 ? "s" : "f";
            }
            
            switch (note.m_accidental)
            {
                case AccFlat:
                    xmlchild.append_attribute("accid") = "f";
                    break;
                case AccNatural:
                    xmlchild.append_attribute("accid") = "n";
                    break;
                case AccSharp:
                    xmlchild.append_attribute("accid") = "s";
                    break;
                case AccNone:
                    break;
            }
        }
    }
    
    // If there is only one note/rest element then add lyrics or ties in child, else in parent
    xml_node xmlLyricTieParent{};
    if (xmlchild != nullptr && chord.m_notes.size() == 1 && std::string(xmlchild.name()) == "note")
    {
        xmlLyricTieParent = xmlchild;
    }
    else if (parentIsChord)
    {
        xmlLyricTieParent = xmlparent;
    }
    else
    {
        return;
    }

    int lyricNum{1};
    for (const auto& lyric : chord.m_lyrics)
    {
        xml_node xmlverse{xmlLyricTieParent.append_child("verse")};
        xmlverse.append_attribute("n") = lyricNum++;
        
        xml_node xmlsyl{xmlverse.append_child("syl")};
        xmlsyl.text().set(lyric.m_text);

        if (lyric.m_syllabic == SylBegin)
        {
            xmlsyl.append_attribute("wordpos") = "i";
            xmlsyl.append_attribute("con") = "d";
        }
        else if (lyric.m_syllabic == SylMiddle)
        {
            xmlsyl.append_attribute("wordpos") = "m";
            xmlsyl.append_attribute("con") = "d";
        }
        else if (lyric.m_syllabic == SylEnd)
        {
            xmlsyl.append_attribute("wordpos") = "t";
            xmlsyl.append_attribute("con") = "s";
        }
        else
        {
            xmlsyl.append_attribute("wordpos") = "s";
            xmlsyl.append_attribute("con") = "s";
        }
    }
    
    if (chord.m_tie)
    {
        xmlLyricTieParent.append_attribute("tie") = "i";
        tieTerminalPending = static_cast<uint8_t>(true);
    }
    else if (static_cast<bool>(tieTerminalPending))
    {
        xmlLyricTieParent.append_attribute("tie") = "t";
        tieTerminalPending = static_cast<uint8_t>(false);
    }
}

std::string GenMei::MakeId()
{
    return "id" + std::to_string(m_nextId++); 
}

void GenMei::AddClef(xml_node xmlparent, Clef clef)
{
    if (clef == ClefNone)
    {
        return;
    }
    
    xml_node xmlclef{xmlparent.append_child("clef")};
    switch (clef)
    {
        case ClefTab:
            xmlclef.append_attribute("visible") = "false";
            xmlclef.append_attribute("shape") = "TAB";
            xmlclef.append_attribute("line") = "5";
            break;
        case ClefF:
            xmlclef.append_attribute("shape") = "F";
            xmlclef.append_attribute("line") = "4";
            break;
        case ClefG:
            xmlclef.append_attribute("shape") = "G";
            xmlclef.append_attribute("line") = "2";
            break;
        case ClefG8a:
            xmlclef.append_attribute("shape") = "G";
            xmlclef.append_attribute("line") = "2";
            xmlclef.append_attribute("dis") = "8";
            xmlclef.append_attribute("dis.place") = "above";
            break;
        case ClefG8b:
            xmlclef.append_attribute("shape") = "G";
            xmlclef.append_attribute("line") = "2";
            xmlclef.append_attribute("dis") = "8";
            xmlclef.append_attribute("dis.place") = "below";
            break;
        case ClefNone:
            break;
    }
}

void GenMei::AddKeySignature(xml_node xmlparent, Key key)
{
    if (key == KeyNone)
    {
        return;
    }
    xml_node xmlkeySig{xmlparent.append_child("keySig")};
    const std::string keySig{std::to_string(std::abs(key)) + (key > 0 ? "s" : key < 0 ? "f" : "")};
    xmlkeySig.append_attribute("sig") = keySig;
}

void GenMei::AddTimeSignature(xml_node xmlparent, const TimeSig& timeSig)
{
    if (timeSig.m_timeSymbol == TimeSyNone)
    {
        return;
    }
    
    xml_node xmlmeterSig{xmlparent.append_child("meterSig")};
    
    switch (timeSig.m_timeSymbol)
    {
    case TimeSyCommon:
        xmlmeterSig.append_attribute("sym") = "common";
        xmlmeterSig.append_attribute("count") = timeSig.m_beats;
        xmlmeterSig.append_attribute("unit") = timeSig.m_beatType;
        break;
    case TimeSyCut:
        xmlmeterSig.append_attribute("sym") = "cut";
        xmlmeterSig.append_attribute("count") = timeSig.m_beats;
        xmlmeterSig.append_attribute("unit") = timeSig.m_beatType;
        break;
    case TimeSySingleNumber:
        xmlmeterSig.append_attribute("form") = "num";
        xmlmeterSig.append_attribute("count") = timeSig.m_beats;
        xmlmeterSig.append_attribute("unit") = timeSig.m_beatType;
        break;
    case TimeSyNormal:
        xmlmeterSig.append_attribute("count") = timeSig.m_beats;
        xmlmeterSig.append_attribute("unit") = timeSig.m_beatType;
        break;
    default:
        break;
    }
    
    m_beats = timeSig.m_beats;
}

} // namespace luteconv
