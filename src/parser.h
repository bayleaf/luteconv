#ifndef _PARSER_H_
#define _PARSER_H_

#include <istream>

#include "options.h"
#include "piece.h"

namespace luteconv
{

/**
 * Parse file
 * 
 */
class Parser
{
public:

    /**
     * Constructor
    */
    Parser() = default;

    /**
     * Destructor
     */
    virtual ~Parser() = default;
    
    /**
     * Parse file
     *
     * @param[in] options
     * @param[out] piece destination
     */
    virtual void Parse(const Options& options, Piece& piece);
    
    /**
     * Parse file
     *
     * @param[in] src stream
     * @param[in] options
     * @param[out] piece destination
     */
    virtual void Parse(std::istream& srcFile, const Options& options, Piece& piece) = 0;
};

} // namespace luteconv

#endif // _PARSER_H_
