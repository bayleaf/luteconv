#include "gunzipper.h"

#include <iostream>
#include <fstream>
#include <sstream>
#include <stdexcept>
#include <utility>

extern "C" {
#include <mini_gzip.h>
}

#include "logger.h"

namespace luteconv
{

void Gunzipper::Gunzip(std::istream& srcFile, std::vector<uint8_t>& outImage)
{
    // read the source file into memory
    
    const size_t srcImageInc{10000};
    std::vector<uint8_t> srcImage(srcImageInc);
    size_t srcSize{0};
    
    for (;;)
    {
        const bool result{srcFile.read(reinterpret_cast<char*>(srcImage.data() + srcSize),
                static_cast<std::streamsize>(srcImage.size() - srcSize))};
        srcSize += static_cast<size_t>(srcFile.gcount());
        if (!result)
        {
            break;
        }
        srcImage.resize(srcImage.size() + srcImageInc);
    }
    srcImage.resize(srcSize);
    
    mini_gzip miniGzip{};
    mini_gz_init(&miniGzip);
    
    int res{mini_gz_start(&miniGzip, srcImage.data(), srcImage.size())};
    if (res < 0)
    {
        LOGGER << "source file is not gzipped, error: " << res;
        outImage = std::move(srcImage);
        return;
    }
    
    // last 4 bytes of the gzip file contain the uncompressed data size, little endian
    size_t uncompSize{0};
    if (srcSize > 4)
    {
        for (size_t i{srcSize - 1}; i >= srcSize - 4; --i)
        {
            uncompSize = (uncompSize << 8) | srcImage[i];
        }
    }
    
    outImage.resize(uncompSize);
    res = mini_gz_unpack(&miniGzip, outImage.data(), outImage.size());
    if (res < 0)
    {
        throw std::runtime_error("Error: mini_gz_unpack " + std::to_string(res));
    }

    outImage.resize(res);
}

} // namespace luteconv
