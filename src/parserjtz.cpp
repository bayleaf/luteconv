#include "parserjtz.h"

#include <vector>
#include <string>

#include "parserjtxml.h"
#include "unzipper.h"
#include "logger.h"

namespace luteconv
{

void ParserJtz::Parse(std::istream& srcFile, const Options& options, Piece& piece)
{
    LOGGER << "Parse jtz";
    
    std::vector<char> image;
    std::string zipFilename;
    Unzipper::Unzip(srcFile, image, zipFilename);
    ParserJtxml::Parse(zipFilename, image.data(), image.size(), options, piece);
}

} // namespace luteconv
