#ifndef _GENERATOR_H_
#define _GENERATOR_H_

#include <ostream>

#include "options.h"
#include "piece.h"


namespace luteconv
{

/**
 * Generate
 */
class Generator
{
public:
    /**
     * Constructor
     */
    Generator() = default;

    /**
     * Destructor
     */
    virtual ~Generator() = default;
    
    /**
     * Generate to destination file in options
     * 
     * @param[in] options
     * @param[in] piece
     */
    virtual void Generate(const Options& options, const Piece& piece);
    
    /**
     * Generate to destination stream
     * 
     * @param[in] options
     * @param[in] piece
     * @param[out] dst destination
     */
    virtual void Generate(const Options& options, const Piece& piece, std::ostream& dst) = 0;
    
    /**
     * Find the first tab part in the piece.  Throws if no tab part found.
     * 
     * @param[in] piece
     * @return index of tab part
     */
    static size_t FindTabPart(const Piece& piece);

};

} // namespace luteconv

#endif // _GENERATOR_H_

