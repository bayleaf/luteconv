#include "parsermid.h"

#include <algorithm>
#include <iostream>
#include <sstream>
#include <stdexcept>
#include <string>

#include "MidiFile.h"

#include "pitch.h"
#include "platform.h"
#include "logger.h"

namespace luteconv
{

using namespace smf;

void ParserMid::Parse(std::istream& srcFile, const Options& options, Piece& piece)
{
    LOGGER << "Parse mid";
    
    Part& part{piece.m_parts.at(0)}; // only 1 part
    
    MidiFile midiFile;
    midiFile.read(srcFile);

    if (!midiFile.status())
    {
        throw std::runtime_error("Error: Can't open " + options.m_srcFilename);
    }
    
    if (options.m_index != 0)
    {
        // TODO Can midi have more than one section?
        throw std::invalid_argument("Error: Can't find section, index=" + std::to_string(options.m_index));
    }
    
    // Use the filename as the title, overwritten by track name if available
    const size_t slash = options.m_srcFilename.find_last_of(pathSeparator);
    if (slash == std::string::npos)
    {
        piece.m_title = options.m_srcFilename;
    }
    else
    {
        piece.m_title = options.m_srcFilename.substr(slash + 1);
    }
    
    const int tpq{midiFile.getTPQ()};  // ticks per quarter note
    LOGGER << "tpq=" << tpq;
    
    midiFile.joinTracks();
    
    int barStart{0};
    int barDuration{4 * tpq};
    Bar bar;
    int chordStart{0};
    std::vector<int> chordMidiPitches;

    for (int event = 0; event < midiFile[0].size(); event++)
    {
        MidiEvent* mev{&midiFile[0][event]};
        
        if (chordStart != mev->tick && !chordMidiPitches.empty())
        {
            // finished chord.  If this is the first chord in the bar, but it is not at the bar
            // start then insert a rest first
            if (bar.m_chords.empty() && chordStart != barStart)
            {
                Chord chord;
                chord.m_stamp = barStart;
                bar.m_chords.push_back(chord);
            }
            bar.m_chords.emplace_back();
            bar.m_chords.back().m_stamp = chordStart;
            for (const auto pitchMidi : chordMidiPitches)
            {
                bar.m_chords.back().m_notes.emplace_back();
                bar.m_chords.back().m_notes.back().m_pitch = Pitch(pitchMidi);
            }
            chordMidiPitches.clear();
        }
        
        if ((event == midiFile[0].size() - 1) || mev->tick >= barStart + barDuration)
        {
            do
            {
                // if the bar is empty add a rest
                if (bar.m_chords.empty())
                {
                    Chord chord;
                    chord.m_stamp = barStart;
                    bar.m_chords.push_back(chord);
                }

                bar.FixNoteTypes(tpq, barStart + barDuration);
                part.m_bars.push_back(bar);
                bar.Clear();
                barStart += barDuration;
            }
            while (mev->tick >= barStart + barDuration);
        }
        
        // MIDI messages.
        //
        // Don't use the note on to note off time to estimate a note's duration because
        // for tablature the 'rule of holds' may mean that a note may sound beyond its rhythm value.
        // Only use successive note ons to calculate rhythm.

        if (mev->isNoteOn())
        {
            // first note is a chord?
            if (chordMidiPitches.empty())
            {
                chordStart = mev->tick;
            }
            chordMidiPitches.push_back(mev->getP1());
        }
        else if (mev->isTimeSignature())
        {
            bar.m_timeSig.m_beats = mev->getP3();
            bar.m_timeSig.m_beatType = 1 << static_cast<int>((*mev)[4]);
            
            if (bar.m_timeSig.m_beats == 4 && bar.m_timeSig.m_beatType == 4)
            {
                bar.m_timeSig.m_timeSymbol = TimeSyCommon;
            }
            else if (bar.m_timeSig.m_beats == 2 && bar.m_timeSig.m_beatType == 2)
            {
                bar.m_timeSig.m_timeSymbol = TimeSyCut;
            }
            else if (bar.m_timeSig.m_beats == 3)
            {
                bar.m_timeSig.m_timeSymbol = TimeSySingleNumber;
            }
            else
            {
                bar.m_timeSig.m_timeSymbol = TimeSyNormal;
            }
            
            barDuration = bar.m_timeSig.m_beats * 4 * tpq / bar.m_timeSig.m_beatType;
            
            LOGGER << "tick=" << mev->tick << " time signature=" << bar.m_timeSig.m_beats << "/" << bar.m_timeSig.m_beatType;;
        }
        else if (mev->isTrackName())
        {
            // MIDI does not specify a character set, I assume UTF-8
            piece.m_title = mev->getMetaContent();
            LOGGER << "title=" << piece.m_title;
        }
        else if (mev->isCopyright())
        {
            // MIDI does not specify a character set, I assume UTF-8
            piece.m_copyright = mev->getMetaContent();
            piece.m_copyrightEnabled = true;
            LOGGER << "copyright=" << piece.m_copyright;
        }
    }

    // remove any trailing empty bars, or bars with just a rest
    while (!part.m_bars.empty() && (part.m_bars.back().m_chords.empty() ||
            (part.m_bars.back().m_chords.size() == 1 && part.m_bars.back().m_chords[0].m_notes.empty())))
    {
        part.m_bars.pop_back();
    }
    
    if (!part.m_bars.empty())
    {
        // final bar
        part.m_bars.back().m_barStyle = BarStyleLightHeavy;
    }
    part.m_pitched = true;
}

} // namespace luteconv
