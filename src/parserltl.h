#ifndef _PARSERLTL_H_
#define _PARSERLTL_H_

#include <string_view>

#include "parser.h"

namespace luteconv
{

/**
 * Parse Lute Tablature with Ligatures font .ltl
 * 
 */
class ParserLtl: public Parser
{
public:

    /**
     * Constructor
    */
    ParserLtl() = default;

    /**
     * Destructor
     */
    ~ParserLtl() override = default;
    
    /**
     * Parse Lute Tablature with Ligatures font .ltl
     *
     * @param[in] src .ltl stream
     * @param[in] options
     * @param[out] piece destination
     */
    void Parse(std::istream& srcFile, const Options& options, Piece& piece) override;
    
private:
    static bool ChordFunc(std::string_view line, size_t idx);
    
    Chord m_previousChord;
};

} // namespace luteconv

#endif // _PARSERLTL_H_
