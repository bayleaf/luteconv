#include "genmid.h"

#include <algorithm>
#include <fstream>
#include <iomanip>
#include <iostream>
#include <string>
#include <vector>

#include "MidiFile.h"

#include "logger.h"

namespace luteconv
{

using namespace smf;

void GenMid::Generate(const Options& options, const Piece& piece, std::ostream& dst)
{
    LOGGER << "Generate mid";
    
    // find the first tablature part
    const Part& part{piece.m_parts.at(FindTabPart(piece))};
    
    MidiFile midiFile;
    const int midiTrack{0};
    const int midiChannel{0};
    const int midiVelocity{90}; // copied from Verovio
    // set our own tpq, the default 120 is too coarse for 4 flag notes
    const int tpq{Bar::DivisionsDefault};
    midiFile.setTPQ(tpq);
    int tick{0}; // in ticks
    
    // track name.  MIDI standard does not specify a character set, I use UTF-8
    if (!piece.m_title.empty())
    {
        if (!piece.m_composer.empty())
        {
            midiFile.addTrackName(midiTrack, tick, piece.m_title + " " + piece.m_composer);
        }
        else
        {
            midiFile.addTrackName(midiTrack, tick, piece.m_title);
        }
    }
    
    midiFile.addPatchChange(midiTrack, tick, midiChannel, options.m_midiPatch);
    midiFile.addTempo(midiTrack, tick, options.m_midiTempo);
    
    // Tablature 'rule of holds'.  A note on a course is held until the next note
    // on that course is required, or until a default hold duration is reached.
    
    std::vector<std::pair<int, int>> heldNotes(part.m_tuning.size()); // stopTime, pitch
    
    // TODO optimize the default hold duration
    NoteType adjusted{static_cast<NoteType>(NoteTypeWhole + options.m_flags)};
    adjusted = std::max(NoteTypeWhole, adjusted);
    adjusted = std::min(NoteType256th, adjusted);
    const int defaultHoldDur{Bar::Duration(adjusted, false, false, tpq)}; // in ticks
    
    for (const auto & bar : part.m_bars)
    {
        if (bar.m_timeSig.m_timeSymbol != TimeSyNone)
        {
            LOGGER << "time signature=" << bar.m_timeSig.m_beats << "/" << bar.m_timeSig.m_beatType;
            midiFile.addTimeSignature(midiTrack, tick,
                    bar.m_timeSig.m_beats, bar.m_timeSig.m_beatType);
        }
        
        int tripletCount{0};
        for (const auto & chord : bar.m_chords)
        {
            if (chord.m_triplet)
            {
                tripletCount = bar.TripletNotes(chord);
            }

            NoteType adjusted{static_cast<NoteType>(chord.m_noteType + options.m_flags)};
            adjusted = std::max(NoteTypeWhole, adjusted);
            adjusted = std::min(NoteType256th, adjusted);
            const int duration{Bar::Duration(adjusted, chord.m_dotted, tripletCount > 0, tpq)}; // in ticks
            
            if (tripletCount > 0)
            {
                --tripletCount;
            }
            
            // End all previously held notes that have reached their stop time.
            // If this is a rest then stop all held notes
            const bool rest{chord.m_notes.empty()};
            for (auto & held : heldNotes)
            {
                const int stopTime{rest ? std::min(held.first, tick) : held.first};
                if (stopTime > 0 && stopTime <= tick)
                {
                    midiFile.addNoteOff(midiTrack, stopTime, midiChannel, held.second);
                    held.first = 0;
                    held.second = 0;
                }
            }

            for (const auto & note : chord.m_notes)
            {
                if (note.m_fret == Note::Anchor || note.m_fret == Note::Ensemble)
                {
                    continue;   // ignore dummy note, connector anchor and ensemble lines
                }
                
                const int pitch{part.CalcPitch(note).Midi()};
                
                // if this pitch is already sounding, on any course, end it now
                for (auto & held : heldNotes)
                {
                    if (held.first != 0 && held.second == pitch)
                    {
                        midiFile.addNoteOff(midiTrack, tick, midiChannel, held.second);
                        held.first = 0;
                        held.second = 0;
                    }
                }
                
                auto & held{heldNotes.at(note.m_course - 1)};

                // if a previously held note on this course is still sounding, end it now.
                if (held.first != 0)
                {
                    midiFile.addNoteOff(midiTrack, tick, midiChannel, held.second);
                }

                // hold this note until the greater of its rhythm sign and the default hold duration.
                held.first = tick + std::max(defaultHoldDur, duration);
                held.second = pitch;
                
                // start this note
                midiFile.addNoteOn(midiTrack, tick, midiChannel, pitch, midiVelocity);
            }
            
            tick += duration;
        }
    }

    // stop any remaining held notes
    for (const auto & held : heldNotes)
    {
        if (held.first > 0)
        {
            midiFile.addNoteOff(midiTrack, held.first, midiChannel, held.second);
        }
    }
    
    midiFile.sortTracks(); 

    if (!midiFile.write(dst))
    {
        throw std::runtime_error("Error: Can't write MIDI file");
    }
}

} // namespace luteconv
