#include "gentab.h"

#include <algorithm>
#include <fstream>
#include <chrono>
#include <iomanip>
#include <set>

#include "charset.h"
#include "logger.h"

namespace luteconv
{

GenTab::GenTab()
: m_clefGTuning{Pitch::SetTuning("F3 G3 A3 B3 C4 D4 E4 F4 G4 A4 B4 C5 D5 E5 F5 G5 A5 B5 C6 D6")},
  m_clefFTuning{Pitch::SetTuning("A1 B1 C2 D2 E2 F2 G2 A2 B2 C3 D3 E3 F3 G3 A3 B3 C4 D4 E4 F4")}
{
    
}

void GenTab::Generate(const Options& options, const Piece& piece, std::ostream& dst)
{
    LOGGER << "Generate tab";
    
    // find the first tablature part, and 1 or 2 voices
    const size_t partTabIdx{FindTabPart(piece)};
    const size_t numParts = (partTabIdx >= 2) ? 3 : (partTabIdx >= 1) ? 2 : 1;
    
    const Part emptyPart{};
    const Part& partTab{piece.m_parts.at(partTabIdx)};

    const Part& partVoice1{(numParts == 3) ? piece.m_parts.at(partTabIdx - 2)
            : (numParts == 2) ? piece.m_parts.at(partTabIdx - 1)
            : emptyPart};

    const Part& partVoice2{(numParts == 3) ? piece.m_parts.at(partTabIdx - 1)
                : emptyPart};
    
    TabType tabType{partTab.m_tabType};
    const std::set<TabType> tabTypes{ TabFrench, TabItalian, TabSpanish };

    if (tabTypes.find(partTab.m_tabType) == tabTypes.end())
    {
        LOGGER << "tab does not support " << Options::GetTabType(partTab.m_tabType) << " tablature type. Using french.";
        tabType = TabFrench;
    }

    const std::time_t tt{std::chrono::system_clock::to_time_t(std::chrono::system_clock::now())};
    struct std::tm * ptm{std::gmtime(&tt)};
    
    // headers
    dst << "% Converted to .tab by luteconv " << options.m_version << '\n'
        << "% encoding-date " << std::put_time(ptm,"%F") << '\n';
    
    std::vector<std::string> headers;
    GetHeaders(piece, partTab, tabType, numParts, headers);
    for (const auto & header : headers)
    {
        dst << header << '\n';
    }

    dst << '\n';
     
    // body
    int staveNum{1};
    size_t barNum{1};
    int chordCount{0};
    std::vector<std::string> repForward;
    const Bar& emptyBar{};
    Clef curClef{ClefG};
    Key curKey{KeyC};
    TimeSig curTimeSig{};
    
    dst << "% Stave " << staveNum << '\n';
    
    // all parts should have the same number of bars, we will cope if they don't
    const size_t numBars{std::max(std::max(partTab.m_bars.size(), partVoice1.m_bars.size()), partVoice2.m_bars.size())};
    
    for (size_t barIdx{0}; barIdx < numBars; ++barIdx)
    {
        const Bar& barTab{barIdx < partTab.m_bars.size() ? partTab.m_bars.at(barIdx) : emptyBar};
        const Bar& barVoice1{barIdx < partVoice1.m_bars.size() ? partVoice1.m_bars.at(barIdx) : emptyBar};
        const Bar& barVoice2{barIdx < partVoice2.m_bars.size() ? partVoice2.m_bars.at(barIdx) : emptyBar};
        
        if (chordCount == 0)
        {
            // first bar on line
            dst <<  "% Bar " << barNum << '\n';
            if (!repForward.empty())
            {
                for (const auto& rep : repForward)
                {
                    dst << rep << '\n';
                }
                repForward.clear();
            }
            else if (!options.m_barSupp)
            {
                dst << "b" << '\n';
            }
        }
        
        // prima, seconda ... volta
        if (barTab.m_volta > 0)
        {
            dst << "A" << barTab.m_volta << '\n';
        }
        
        if (numParts >= 2) // don't want clef or key for just tablature
        {
            // clef - all voice parts share the same clef
            if (chordCount == 0 || (barVoice1.m_clef != ClefNone && barVoice1.m_clef != curClef))
            {
                if (barVoice1.m_clef != ClefNone)
                {
                    curClef = barVoice1.m_clef;
                }
                if (curClef != ClefNone)
                {
                    dst << GetClef(curClef) << '\n';
                }
            }
            
            // key signature - all voice parts share the same key signature
            if (chordCount == 0 || (barVoice1.m_key != KeyNone && barVoice1.m_key != curKey))
            {
                if (barVoice1.m_key != KeyNone)
                {
                    curKey = barVoice1.m_key;
                }
                if (curKey != KeyNone && curKey != KeyC)
                {
                    dst << GetKeySignature(curKey) << '\n';
                }
            }
        }
        
        // time signature - all parts share the same time signature
        if (barTab.m_timeSig.m_timeSymbol != TimeSyNone && barTab.m_timeSig != curTimeSig)
        {
            if (barTab.m_timeSig.m_timeSymbol != TimeSyNone)
            {
                curTimeSig = barTab.m_timeSig;
            }
            if (curTimeSig.m_timeSymbol != TimeSyNone)
            {
                dst << GetTimeSignature(curTimeSig) << '\n';
            }
        }

        
        // chords
        // get duration of bar for all parts
        int durBarTab{0};
        int durBarVoice1{0};
        int durBarVoice2{0};
        for (const auto& chord : barTab.m_chords)
        {
            durBarTab += Bar::Duration(chord.m_noteType, chord.m_dotted, false); // don't need to adjust - tabFactor does the job
        }
        
        for (const auto& chord : barVoice1.m_chords)
        {
            durBarVoice1 += Bar::Duration(chord.m_noteType, chord.m_dotted, false);
        }

        for (const auto& chord : barVoice2.m_chords)
        {
            durBarVoice2 += Bar::Duration(chord.m_noteType, chord.m_dotted, false);
        }
        
        const int durCmn{std::max(durBarVoice1, durBarVoice2)};
        const int tabFactor{durCmn == 0 || durBarTab == 0 || durCmn < durBarTab ? 1 : durCmn / durBarTab}; // to equalize tab and CMN bar duration

        size_t idxChordTab{0};
        size_t idxChordVoice1{0};
        size_t idxChordVoice2{0};
        int stampChordTab{0};
        int stampChordVoice1{0};
        int stampChordVoice2{0};
        
        NoteType gridNoteType{NoteTypeLong};
        while (idxChordTab < barTab.m_chords.size() || idxChordVoice1 < barVoice1.m_chords.size() || idxChordVoice2 < barVoice2.m_chords.size())
        {
            int durChordTab{0};
            int durChordVoice1{0};
            int durChordVoice2{0};
            
            if (idxChordTab < barTab.m_chords.size())
            {
                durChordTab = Bar::Duration(barTab.m_chords.at(idxChordTab).m_noteType,
                        barTab.m_chords.at(idxChordTab).m_dotted, false) * tabFactor;
            }
            else
            {
                stampChordTab = std::numeric_limits<int>::max();
            }
            if (idxChordVoice1 < barVoice1.m_chords.size())
            {
                durChordVoice1 = Bar::Duration(barVoice1.m_chords.at(idxChordVoice1).m_noteType,
                        barVoice1.m_chords.at(idxChordVoice1).m_dotted, false);
            }
            else
            {
                stampChordVoice1 = std::numeric_limits<int>::max();
            }

            if (idxChordVoice2 < barVoice2.m_chords.size())
            {
                durChordVoice2 = Bar::Duration(barVoice2.m_chords.at(idxChordVoice2).m_noteType,
                        barVoice2.m_chords.at(idxChordVoice2).m_dotted, false);
            }
            else
            {
                stampChordVoice2 = std::numeric_limits<int>::max();
            }

            const int stamp{std::min(std::min(stampChordTab, stampChordVoice1), stampChordVoice2)};
            
            if (stampChordTab == stamp)
            {
                // want tablature
                
                // flags
                std::string line{GetFlagInfo(options, barTab.m_chords, barTab.m_chords.at(idxChordTab), gridNoteType)};
                
                // notes
                std::vector<std::string> vert;
                GetCourses(partTab, tabType, barTab.m_chords.at(idxChordTab), line, vert);
                
                for (const auto & s : vert)
                {    
                    line += s;
                }
                
                // remove trailing spaces
                // TODO why is this necessary?
                while (!line.empty() && line.back() == ' ')
                {
                    line.pop_back();
                }
                
                dst << line;

                stampChordTab += durChordTab;
                ++idxChordTab;
            }
            if (numParts == 2)
            {
                if (stampChordVoice1 == stamp)
                {
                    // want voice 1
                    dst << "M" << GetCmnNote(options, barVoice1.m_chords.at(idxChordVoice1), curClef);
                    if (!barVoice1.m_chords.at(idxChordVoice1).m_lyrics.empty())
                    {
                        dst << "T" << GetLyrics(barVoice1.m_chords.at(idxChordVoice1));
                    }
                    stampChordVoice1 += durChordVoice1;
                    ++idxChordVoice1;
                }
            }
            else if (numParts == 3)
            {
                // Order is: tab voice 2 voice 1.  Voice 2 can be introduced with
                // either 'M' or '\t', but for voice 1 it must be '\t' - not documented
                if (stampChordVoice1 == stamp && stampChordVoice2 != stamp)
                {
                    // want voice 1, don't want voice 2
                    dst << "M000\t" << GetCmnNote(options, barVoice1.m_chords.at(idxChordVoice1), curClef);
                    if (!barVoice1.m_chords.at(idxChordVoice1).m_lyrics.empty())
                    {
                        dst << "T^" << GetLyrics(barVoice1.m_chords.at(idxChordVoice1));
                    }
                    stampChordVoice1 += durChordVoice1;
                    ++idxChordVoice1;
                }
                else if (stampChordVoice1 != stamp && stampChordVoice2 == stamp)
                {
                    // don't want voice 1, want voice 2
                    dst << "M" << GetCmnNote(options, barVoice2.m_chords.at(idxChordVoice2), curClef) << "\t000";
                    if (!barVoice2.m_chords.at(idxChordVoice2).m_lyrics.empty())
                    {
                        dst << "T" << GetLyrics(barVoice2.m_chords.at(idxChordVoice2)) << "^";
                    }
                     stampChordVoice2 += durChordVoice2;
                    ++idxChordVoice2;
                }
                else if (stampChordVoice1 == stamp && stampChordVoice2 == stamp)
                {
                    // want voice 1, want voice 2
                    dst << "M" << GetCmnNote(options, barVoice2.m_chords.at(idxChordVoice2), curClef)
                        << "\t" << GetCmnNote(options, barVoice1.m_chords.at(idxChordVoice1), curClef);
                    if (!barVoice1.m_chords.at(idxChordVoice1).m_lyrics.empty() || !barVoice2.m_chords.at(idxChordVoice2).m_lyrics.empty())
                    {
                        dst << "T" << GetLyrics(barVoice2.m_chords.at(idxChordVoice2))
                                << "^" << GetLyrics(barVoice1.m_chords.at(idxChordVoice1));
                    }
                    stampChordVoice1 += durChordVoice1;
                    ++idxChordVoice1;
                    stampChordVoice2 += durChordVoice2;
                    ++idxChordVoice2;
                }
            }
            ++chordCount;
            dst << '\n';
        }

        // end of current bar
        ++barNum;
        dst <<  "% Bar " << barNum << '\n';
        
        // Tab doesn't automatically add stave endings.  Use herustic:
        // count chords, when the threshold is reached end the stave at the end of
        // the current bar.  Except for last bar.
        
        // For multi-part music use a smaller default as lyrics take up more space
        const int wrapThreshold{(numParts >= 2 && options.m_wrapThreshold == options.m_wrapDefault)
            ? options.m_wrapDefault / 2 : options.m_wrapThreshold};
        
        const bool lineBreak{barNum < numBars && chordCount > wrapThreshold};
        std::vector<std::string> barLines;
        GetBarLines(barTab, lineBreak, repForward, barLines);
        for (const auto& barLine : barLines)
        {
            dst << barLine << '\n';
        }
        
        if (lineBreak)
        {
            dst << '\n';
            ++staveNum;
            chordCount = 0;
            dst << "% Stave " << staveNum << '\n';
        }
    }
    
    dst << "e" << '\n'; // end of piece
}
    
std::string GenTab::GetLyrics(const Chord& chord)
{
    std::string result;
    for (const auto& lyric : chord.m_lyrics)
    {
        if (!result.empty())
        {
            result += '\t';
        }
        result += CharSet::Utf8ToTab(lyric.m_text);
        
        if (lyric.m_syllabic == SylBegin || lyric.m_syllabic == SylMiddle)
        {
            result += '-';
        }
    }
    return result;
}

std::string GenTab::GetCmnNote(const Options& options, const Chord& chord, Clef clef)
{
    std::string result;
    
    result = GetRhythm(options, chord);
    
    if (chord.m_notes.empty())
    {
        result += "R";
        result += (chord.m_dotted) ? '.' : '0';
        if (chord.m_tie)
        {
            result += '-';
        }
    }
    else
    {
        // We can only use the first note of the chord
        const Note& note{chord.m_notes.at(0)};
        
        // Pitch.  tab used same note letters regardless of the clef
        const int octaveShift{(clef == ClefG8a) ? -1 : (clef == ClefG8b) ? 1 : 0};
        const Pitch basePitch{note.m_pitch.Step(), 0, note.m_pitch.Octave() + octaveShift}; // without accidentals octave shited
        const std::vector<Pitch>& tuning{(clef == ClefF) ? m_clefFTuning : m_clefGTuning};
        const auto it{std::find(tuning.begin(), tuning.end(), basePitch)};
        const ptrdiff_t index{std::distance(tuning.begin(), it)};
        const std::string_view cmnPitches{"?@ABCDEFGabcdefg1234"};
        if (index < static_cast<ptrdiff_t>(cmnPitches.size()))
        {
            result += cmnPitches[index];
        }
        else
        {
            LOGGER << "note " << note.m_pitch.ToString() << " is out of range";
            result += '?';
        }
        
        // Accidental and dot
        // + is for sharp, . for dotted note
        //  - for flat, n for natural, or 0 for neither
        // v for dot and flat, ^ for sharp and dot
        // N for dot and natural
        switch (note.m_accidental)
        {
            case AccNone:
                result += chord.m_dotted ? '.' : '0';
                break;
            case AccFlat:
                result += chord.m_dotted ? 'v' : '-';
                break;
            case AccNatural:
                result += chord.m_dotted ? 'N' : 'n';
                break;
            case AccSharp:
                result += chord.m_dotted ? '^' : '+';
                break;
        }
        
        // grid and tie
        // a # following the three characters starts a
        // beamed set, an x following the three characters
        // continues it. A - following the three characters 
        // is a tie to the next note. = is a tie at the end of
        // a beamed set.
        if (chord.m_grid == GridStart)
        {
            result += '#';
        }
        else if (chord.m_grid == GridMid || chord.m_grid == GridEnd)
        {
            if (chord.m_tie)
            {
                result += '=';
            }
            else
            {
                result += 'x';
            }
        }
        else if (chord.m_tie)
        {
            result += '-';
        }
    }
    
    return result;
}

void GenTab::GetBarLines(const Bar& bar, bool lineBreak, std::vector<std::string>& repForward,
        std::vector<std::string>& barLines)
{
    std::string barStyle;
    std::string repStyle;
    if (bar.m_repeat == RepBackward || bar.m_repeat == RepJanus)
    {
        repStyle = ".";
    }
    
    switch (bar.m_barStyle)
    {
    case BarStyleHeavy:
        barStyle = "B";
        break;
    case BarStyleLightLight:
        barStyle = "bb";
        break;
    case BarStyleLightHeavy:
        barLines.emplace_back(repStyle + "b"); // can't have bB on same line
        barStyle = "B";
        repStyle.clear();
        break;
    case BarStyleHeavyLight:
        if (!repStyle.empty())
        {
            barLines.emplace_back(repStyle); // can't have .B on same line
        }
        barLines.emplace_back("B"); // can't have Bb on same line
        barStyle = "b";
        repStyle.clear();
        break;
    case BarStyleHeavyHeavy:
        if (!repStyle.empty())
        {
            barLines.emplace_back(repStyle); // can't have .B on same line
        }
        barLines.emplace_back("B"); // can't have BB on same line
        barStyle = "B";
        repStyle.clear();
        break;
    default:
        barStyle = "b";
    }
    
    switch (bar.m_repeat)
    {
    case RepNone:
        break;
    case RepForward:
        if (barStyle == "B")
        {
            barLines.emplace_back("B");
            barStyle = ".";
        }
        else
        {
            barStyle += ".";
        }
        break;
    case RepBackward:
        break;
    case RepJanus:
        if (lineBreak)
        {
            // backward repeat here, forward repeat in next bar, next line
            switch (bar.m_barStyle)
            {
            case BarStyleHeavy:
                repForward = {"B", "."};
                break;
            case BarStyleLightLight:
                repForward = {"bb."};
                break;
            case BarStyleLightHeavy:
                [[fallthrough]];
            case BarStyleHeavyLight:
                repForward = {"B", "b."};
                break;
            case BarStyleHeavyHeavy:
                repForward = {"B", "B", "."};
                break;
            default:
                repForward = {"b."};
            }
        }
        else
        {
            if (barStyle == "B")
            {
                barLines.emplace_back("B");
                barStyle = ".";
            }
            else
            {
                barStyle += ".";
            }
        }
    }
    
    if (bar.m_fermata)
    {
        barLines.emplace_back("Y" + repStyle + barStyle);
    }
    else
    {
        barLines.emplace_back(repStyle + barStyle);
    }
}

void GenTab::GetHeaders(const Piece& piece, const Part& partTab, TabType tabType, size_t numParts, std::vector<std::string>& headers)
{
    headers.emplace_back("-C");
    headers.emplace_back("-highlightparen");
    headers.emplace_back("-tuning " + GetTuningTab(partTab.m_tuning));
    if (numParts == 3)
    {
        headers.emplace_back("-twostaff");
    }
    
    switch (partTab.m_staffLines)
    {
        case 4:
            headers.emplace_back("-4");
            break;
        case 5:
            headers.emplace_back("-5");
            break;
        case 6:
            break;
        case 7:
            headers.emplace_back("-7");
            break;
        default:
            break;
    }

    if (piece.m_copyrightEnabled)
    {
        headers.emplace_back("-G");
    }

    switch (tabType)
    {
    case TabFrench:
    {
        // letter frets
        if (partTab.m_tuning.size() >= Course11)
        {
            headers.emplace_back("-b"); // baroque font
        }
        else
        {
            headers.emplace_back("$flagstyle=thin");
            headers.emplace_back("$charstyle=robinson");
        }
        break;
    }
    case TabItalian:
    {
        // 7+ course italian tablature, extra space after flags for diapasons
        if (partTab.m_tuning.size() > Course6)
        {
            headers.emplace_back("-s");
        }
        
        // numeric frets
        headers.emplace_back("$numstyle=italian");
        headers.emplace_back("$line=o");
        break;
    }
    case TabSpanish:
    {
        headers.emplace_back("-milan");
        
        // numeric frets
        headers.emplace_back("$numstyle=italian");
        headers.emplace_back("$line=o");
        break;
    }
    default:
        break;
    }
    
    if (!piece.m_copyright.empty())
    {
        headers.emplace_back("$scribe=" + CharSet::Utf8ToTab(piece.m_copyright));
    }
    
    // header text
    if (!piece.m_title.empty() && !piece.m_composer.empty())
    {
        headers.emplace_back("{" + CharSet::Utf8ToTab(piece.m_title) + "/" + CharSet::Utf8ToTab(piece.m_composer) + "}");
    }
    else if (!piece.m_title.empty() && piece.m_composer.empty())
    {
        headers.emplace_back("{" + CharSet::Utf8ToTab(piece.m_title )+ "}");
    }
    else if (piece.m_title.empty() && !piece.m_composer.empty())
    {
        headers.emplace_back("{/" + CharSet::Utf8ToTab(piece.m_composer) + "}");
    }
        
    // Credits can be very long, tab doesn't wrap so we have to do it
    for (const auto & credit : piece.m_credits)
    {
        const std::string tabCredit{CharSet::Utf8ToTab(credit)};
        size_t j{0};
        const size_t maxCredit{70};
        
        for (size_t i{0}; i < tabCredit.size(); i = j)
        {
            j = std::min(i + maxCredit, tabCredit.size());
            const size_t k{j};
            
            if (j != tabCredit.size())
            {
                // break at a space
                while (j > i && tabCredit.at(j - 1) != ' ')
                {
                    --j;
                }
                
                if (j == i)
                {
                    // no space found
                    j = k;
                }
            }
            headers.emplace_back("{" + tabCredit.substr(i, j - i) + "}");
        }
    }
}

void GenTab::GetCourses(const Part& partTab, TabType tabType, const Chord& chord, std::string& flag, std::vector<std::string>& courses)
{
    courses.clear();
    courses.reserve(20);
    int vertOffset{0};
    
    for (size_t noteIdx{0}; noteIdx < chord.m_notes.size(); ++noteIdx)
    {
        // Italian tablature walk notes in reverse course order, all others in order.
        const size_t idx{tabType == TabItalian ? chord.m_notes.size() - noteIdx - 1 : noteIdx};
        const Note& note{chord.m_notes.at(idx)};

        int vertIndex{0};
        if (tabType == TabItalian)
        {
            if (note.m_course > partTab.m_staffLines)
            {
                vertIndex = 0;
            }
            else
            {
                // upside down
                const int flip{partTab.m_staffLines - note.m_course + 1};
                
                vertIndex = flip - 1 - vertOffset;
            
                if (partTab.m_tuning.size() > Course6)
                {
                    ++vertIndex; // extra space after flags for 7+ course italian
                }
            }
        }
        else
        {
            vertIndex = std::min(note.m_course, static_cast<int>(Course7)) - 1 - vertOffset;
        }
         
        while (vertIndex >= static_cast<int>(courses.size()))
        {
            courses.emplace_back(" "); // reserve unused strings
        }
         
        // TabCode can put an ensemble line where fingering needs to go in Tab,
        // in which case ignore the ensemble line.
        if (note.m_fret == Note::Ensemble && courses.at(vertIndex) != " ")
        {
            LOGGER << "Ensemble line already occupied, ignored";
            continue;
        }
         
        const std::string rightFingering{GetRightFingering(note)};
        const std::string leftOrnament{GetLeftOrnament(note)};
        const std::string rightConnector{GetRightConnector(note.m_connector)};
         
        // Ornaments #*- on first course may clash with flags, put default - as 2nd character
        if (!leftOrnament.empty() && note.m_course == 1 && flag.size() == 1)
        {
            flag += "-";
        }
        
        // courses >= 7 all accumulate in the same element, diapason chord
        if (courses.at(vertIndex) == " ")
        {
            courses.at(vertIndex).clear();
        }
        courses.at(vertIndex).append(leftOrnament) // before the letter
                            .append(GetLeftFingering(note)) // before the letter
                            .append(GetLeftConnector(note.m_connector)) // before the letter
                            .append(GetFret(note, tabType)) // fret letter
                            .append(GetRightOrnament(note)) // after the letter
                            .append(rightFingering)  // below the letter
                            .append(rightConnector) // after or below the letter
                            ;
         
        // right fingering pushes the vertical position out of place, compensate
        vertOffset += static_cast<int>(rightFingering.size());
         
        // a south connector pushes the vertical position out of place, compensate.
        // a place holder has no height.
        if (Bearing(note.m_connector.m_compass) == South && note.m_fret != Note::Anchor)
        {
            vertOffset += static_cast<int>(rightConnector.size());
        }
    }
}

std::string GenTab::GetClef(Clef clef)
{
    switch (clef)
    {
        case ClefF:
            return "MF";
        case ClefG:
            return "MG";
        case ClefG8a:
            return "MG8a";
        case ClefG8b:
            return "MG8b";
        default:
            break;
    }
    return "";
}

std::string GenTab::GetKeySignature(Key key)
{
    // key signature kg is key of G, also d, a, f, c, b, e
    switch (key)
    {
        case KeyA:
            return "ka";
        case KeyBb:
            return "kb";
        case KeyC:
            return "kc";
        case KeyD:
            return "kd";
        case KeyEb:
            return "ke";
        case KeyF:
            return "kf";
        case KeyG:
            return "kg";
        default:
            break;
    }
    return "";
}

std::string GenTab::GetTimeSignature(const TimeSig & timeSig)
{
    switch (timeSig.m_timeSymbol)
    {
    case TimeSyNone:
        return "";
    case TimeSyCommon:
        return "C";
    case TimeSyCut:
        return "c";
    case TimeSySingleNumber:
        return "S" + std::to_string(timeSig.m_beats);
    case TimeSyNote:
        return "";
    case TimeSyDottedNote:
        return "";
    case TimeSyNormal:
        return "S" + std::to_string(timeSig.m_beats) + "-" + std::to_string(timeSig.m_beatType);
    }
    return "";
}

std::string GenTab::GetRhythm(const Options& options, const Chord & chord)
{
    // TAB thinks 0 flags = crotchet
    NoteType adjusted{static_cast<NoteType>(chord.m_noteType + options.m_flags)};
    adjusted = std::max(NoteTypeLong, adjusted);
    adjusted = std::min(NoteType128th, adjusted);
    
    if (adjusted >= NoteTypeQuarter)
    {
        return std::to_string(adjusted - NoteTypeQuarter);
    }
    if (adjusted == NoteTypeHalf)
    {
        return "w";
    }
    if (adjusted == NoteTypeWhole)
    {
        return "W";
    }
    if (adjusted == NoteTypeBreve)
    {
        return "B";
    }
    if (adjusted == NoteTypeLong)
    {
        return "L";
    }
    
    return "0";
}

std::string GenTab::GetFlagInfo(const Options& options, const std::vector<Chord> & chords, const Chord & our, NoteType& gridNoteType)
{
    std::string result;
    std::string ourFlag;
    
    const long int ourIndex{std::distance(chords.data(), &our)};
    const long int following{ourIndex + 1};
    const long int previous{ourIndex - 1};
    
    if (our.m_fermata)
    {
        return "Y-";
    }
    
    if (our.m_triplet)
    {
        result = 't';
    }
    
    ourFlag = GetRhythm(options, our);
    
    // first flag in grid, with following
    if (following < static_cast<int>(chords.size()) && (our.m_grid == GridStart) && (chords.at(following).m_grid != GridStart))
    {
        if (our.m_dotted)
        {
            result += ourFlag + "*";
            gridNoteType = our.m_noteType;
        }
        else if (our.m_noteType == chords.at(following).m_noteType + 1)
        {
            result += ourFlag + "|";
            gridNoteType = chords.at(following).m_noteType;
        }
        else
        {
            result += "#" + ourFlag;
            gridNoteType = our.m_noteType;
        }
    }
    else if ((previous >= 0) && (our.m_grid == GridMid || our.m_grid == GridEnd))
    {
        // middle or end of grid
        if (our.m_noteType == gridNoteType + 1)
        {
            result += "x|";
        }
        else if (our.m_noteType == gridNoteType)
        {
            result += "x";

            if (our.m_dotted)
            {
                result += "*";
            }
        }
        else
        {
            // can't deal with this grid, if mid grid start another
            if (our.m_grid == GridMid)
            {
                result += "#" + ourFlag;
                gridNoteType = our.m_noteType;
            }
            else
            {
                result += ourFlag;
            }
        }
    }
    else if (our.m_noFlag)
    {
        // no flag
        result += "x";
    }
    else
    {
        result += ourFlag;
        // dotted
        if (our.m_dotted)
        {
            result += ".";
        }
    }
    
    return result;
}

std::string GenTab::GetRightFingering(const Note & note)
{
    switch (note.m_rightFingering)
    {
    case FingerNone:
        return "";
    case FingerFirst:
        return ".";
    case FingerSecond:
        return ":";
    case FingerThird:
        return ";";
    case FingerForth:
        return "";
    case FingerThumb:
        return "|";
    }
    return "";
}
    
std::string GenTab::GetLeftFingering(const Note & note)
{
    switch (note.m_leftFingering)
    {
    case FingerNone:
        return "";
    case FingerFirst:
        return "\\1";
    case FingerSecond:
        return "\\2";
    case FingerThird:
        return "\\3";
    case FingerForth:
        return "\\4";
    case FingerThumb:
        return "";
    }
    return "";
}

std::string GenTab::GetLeftOrnament(const Note & note)
{
    switch (note.m_leftOrnament)
    {
    case OrnNone:
        return "";
    case OrnHash:
        return "#";
    case OrnPlus:
        return "+";
    case OrnCross:
        return "x";
    case OrnDot:
        return "*";
    case OrnDotDot:
        return "%";
    case OrnBrackets:
        return "Q";
    case OrnComma:
        return ",";
    case OrnCommaRaised:
        return "'";
    case OrnColon:
        return "=";
    case OrnColonColon:
        return "$";
    case OrnAsterisk:
        return "@";
    case OrnSmiley:
        return "_";
    case OrnHat:
        return "^";
    case OrnDash:
        return "<";
        
    case OrnSize:
        return "";
    }
    return "";
}

std::string GenTab::GetRightOrnament(const Note & note)
{
    switch (note.m_rightOrnament)
    {
    case OrnNone:
        return "";
    case OrnHash:
        return "&#";
    case OrnPlus:
        return "&+";
    case OrnCross:
        return "&x";
    case OrnDot:
        return "&*";
    case OrnDotDot:
        return "&%";
    case OrnBrackets:
        return "";
    case OrnComma:
        return "&,";
    case OrnCommaRaised:
        return "&'";
    case OrnColon:
        return "&=";
    case OrnColonColon:
        return "&$";
    case OrnAsterisk:
        return "&@";
    case OrnSmiley:
        return "&_";
    case OrnHat:
        return "&^";
    case OrnDash:
        return "&<";
        
    case OrnSize:
        return "";
    }
    return "";
}

std::string GenTab::GetFret(const Note & note, TabType tabType)
{
    if (note.m_fret == Note::Ensemble)
    {
        return "-";
    }
    
    if (note.m_fret == Note::Anchor)
    {
        return (Bearing(note.m_connector.m_compass) == West) ? " " : ""; // not a note, anchor for connector
    }
    
    if (tabType == TabFrench)
    {
        // a..p, excluding j
        // Tab support a..p but JHR uses q (a '/. sign for Kapsberger)
        // allow q as a fret
        const std::string letter{static_cast<char>('a' + note.m_fret + (note.m_fret > 8 ? 1 : 0))};
        if (note.m_course >= Course17)
        {
            return ("N" + std::to_string(note.m_course - Course7)); // 2 digits ledger numbering
        }
        if (note.m_course >= Course11)
        {
            return std::to_string(note.m_course - Course7); // 1 digit ledger numbering
        }
        if (note.m_course >= Course8)
        {
            return std::string(note.m_course - Course7, '/') + letter;
        }

        return letter;
    }

    const std::string number{(note.m_fret <= 9)
            ? std::to_string(note.m_fret)
            : "N" + std::to_string(note.m_fret / 10) + std::to_string(note.m_fret % 10)};
        
    if (note.m_course >= Course8)
    {
        return std::string(note.m_course - Course7, '/') + number;
    }
    return number;
}

std::string GenTab::GetLeftConnector(const Connector & connector)
{
    // only west facing connectors go before the note
    if (Bearing(connector.m_compass) == West)
    {
        return GetConnector(connector);
    }

    return "";
}

std::string GenTab::GetRightConnector(const Connector & connector)
{
    // only south and east facing connectors go after the note
    if (Bearing(connector.m_compass) != West)
    {
        return GetConnector(connector);
    }

    return "";
}

std::string GenTab::GetConnector(const Connector & connector)
{
    std::string result;
    
    if (connector.m_endPoint == EndPointNone || connector.m_number > 2)
    {
        return result;
    }
    
    if (Bearing(connector.m_compass) == West)
    {
        result = "\"";
    }
    else if (Bearing(connector.m_compass) == East)
    {
        result = "&";
    }
    
    if (connector.m_function == FunSlur)
    {
        if (connector.m_endPoint == EndPointStart)
        {
            result += (connector.m_number == 1) ? "[" : "U";
        }
        else
        {
            if (connector.m_number == 1)
            {
                result += "]";
                if (connector.m_curve == CurveOver)
                {
                    result += "v"; // only ] supports an over curve
                }
            }
            else
            {
                result += "X";
            }
        }
    }
    else
    {
        if (connector.m_endPoint == EndPointStart)
        {
            result += (connector.m_number == 1) ? "(" : "{";
        }
        else
        {
            result += (connector.m_number == 1) ? ")" : "}";
        }
    }
    
    return result;
}

Compass GenTab::Bearing(const Compass compass)
{
    // tab only suppports east, south and west - remap
    switch (compass)
    {
    case North:
    case NorthWest:
    case West:
    case SouthWest:
        return West;
    case South:
        return South;
    case SouthEast:
    case East:
    case NorthEast:
        return East;
    }
    
    return South; // silly compiler - can't get here but complains if missing
}

std::string GenTab::GetTuningTab(const std::vector<Pitch>& tuning)
{
    std::string result;

    for (auto p = tuning.rbegin(); p != tuning.rend(); ++p )
    {
        // step
        result += static_cast<char>(std::tolower(p->Step()));
        
        // alter
        if (p->Alter() > 0)
        {
            result += std::string(p->Alter(), '+');
        }
        if (p->Alter() < 0)
        {
            result += std::string(-(p->Alter()), '-');
        }
        
        // octave
        // Tab octaves decrease with increasing pitch, and run A..G
        int octave = 6 - p->Octave();
        if (p->Step() == 'A' || p->Step() == 'B')
        {
            --octave;
        }
        result += std::to_string(octave);
    }
    
    return result;
}

} // namespace luteconv
