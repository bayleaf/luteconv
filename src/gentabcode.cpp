#include "gentabcode.h"

#include <algorithm>
#include <fstream>
#include <chrono>
#include <iomanip>
#include <set>

#include "logger.h"

namespace luteconv
{

void GenTabCode::Generate(const Options& options, const Piece& piece, std::ostream& dst)
{
    LOGGER << "Generate tc";
    
    // find the first tablature part
    const Part& part{piece.m_parts.at(FindTabPart(piece))};
    
    TabType tabType{part.m_tabType};
    const std::set<TabType> tabTypes{ TabFrench, TabGerman, TabItalian, TabSpanish };

    if (tabTypes.find(part.m_tabType) == tabTypes.end())
    {
        LOGGER << "TabCode does not support " << Options::GetTabType(part.m_tabType) << " tablature type. Using french.";
        tabType = TabFrench;
    }

    dst << GetRules(piece, part, tabType);
    
    const std::time_t tt{std::chrono::system_clock::to_time_t(std::chrono::system_clock::now())};
    struct std::tm * ptm{std::gmtime(&tt)};
    
    // TabCode has no syntax for composer etc that I know of.  Just put everything in comments.
    dst << "{ Converted to TabCode .tc by luteconv " << options.m_version << " }" << '\n'
        << "{ encoding-date " << std::put_time(ptm,"%F") << " }" << '\n';

    if (!piece.m_title.empty())
    {
        dst << "{ " << piece.m_title << " }" << '\n';
    }

    if (!piece.m_composer.empty())
    {
        dst << "{ " << piece.m_composer << " }" << '\n';
    }

    if (!piece.m_copyright.empty())
    {
        dst << "{ " << piece.m_copyright << " }" << '\n';
    }
    
    for (const auto & credit : piece.m_credits)
    {
        dst << "{ " << credit << " }" << '\n';
    }

    dst << '\n';
     
    // body
    int staveNum{1};
    int barNum{1};
    int chordCount{0};
    std::string repForward;
    
    dst << "{ Stave " << staveNum << " }" << '\n';
    
    for (const auto & bar : part.m_bars)
    {
        if (chordCount == 0)
        {
            // first bar on line
            dst <<  "{ Bar " << barNum << " }" << '\n';
            if (!repForward.empty())
            {
                dst << repForward << '\n';
                repForward.clear();
            }
            else if (!options.m_barSupp)
            {
                dst << "|" << '\n';
            }
        }
        
        // prima, seconda ... volta
        if (bar.m_volta > 0)
        {
            dst << "(T+:\\" << bar.m_volta << ")" << '\n';
        }
        
        // time signature
        const std::string timeSignature{GetTimeSignature(bar)};
        if (!timeSignature.empty())
        {
            dst << timeSignature << '\n';
        }
        
        // chords
        int lhsNumBeams{0};
        for (const auto & chord : bar.m_chords)
        {
            ++chordCount;
            
            // flags
            std::string tabWord{GetFlagInfo(options, bar.m_chords, chord, lhsNumBeams)};
            
            // notes. TabCode uses fret/string pairs so ordering in a tabword shouldn't matter,
            // but assume that it does.
            std::vector<std::string> vert(part.m_tuning.size());
            
            int lastString{0};
            for (const auto & note : chord.m_notes)
            {
                if (note.m_fret == Note::Ensemble && lastString > 0)
                {
                    vert[lastString - 1] += "(E)";
                }
                else if (note.m_course <= Course7 - 1)
                {
                    lastString = note.m_course;
                    vert[lastString - 1] = GetFret(note)
                            + std::to_string(note.m_course)
                            + GetLeftOrnament(note)
                            + GetRightOrnament(note)
                            + GetLeftFingering(note)
                            + GetRightFingering(note)
                            + GetConnector(note);
                }
                else
                {
                    // diapasons don't have fingering or ornaments - is that right?
                    lastString = note.m_course;
                    vert[lastString - 1] = "X" + GetFret(note);
                }
            }
            
            if (chord.m_triplet)
            {
                tabWord += '3';
            }
            
            for (const auto & s : vert)
            {
                tabWord += s;
            }
            
            dst << tabWord << '\n';
        }
        
        // end of current bar
        ++barNum;
        dst <<  "{ Bar " << barNum << " }" << '\n';
        
        // Stave ending Use herustic:
        // count chords, when the threshold is reached end the stave at the end of
        // the current bar.  Except for last bar.
        const bool lineBreak{barNum < static_cast<int>(part.m_bars.size()) && chordCount > options.m_wrapThreshold};
        
        std::string barStyle;
        switch (bar.m_barStyle)
        {
        case BarStyleLightLight:
            barStyle = "||";
            break;
        default:
            barStyle = "|";
        }
        
        switch (bar.m_repeat)
        {
        case RepNone:
            break;
        case RepForward:
            barStyle += ":";
            break;
        case RepBackward:
            barStyle = std::string(":").append(barStyle);
            break;
        case RepJanus:
            if (lineBreak)
            {
                // backward repeat here, forward repeat in next bar, next line
                repForward = barStyle + ":";
                barStyle = std::string(":").append(barStyle);
            }
            else
            {
                barStyle = std::string(":").append(barStyle).append(":");
            }
        }
        
        dst << barStyle << '\n';
        
        if (lineBreak)
        {
            dst << "{^}" << '\n';
            ++staveNum;
            chordCount = 0;
            dst << "{ Stave " << staveNum << " }" << '\n';
        }
    }
}
    
std::string GenTabCode::GetTimeSignature(const Bar & bar)
{
    switch (bar.m_timeSig.m_timeSymbol)
    {
    case TimeSyNone:
        return "";
    case TimeSyCommon:
        return "M(C)";
    case TimeSyCut:
        return "M(C/)";
    case TimeSySingleNumber:
        return "M(" + std::to_string(bar.m_timeSig.m_beats) + ")";
    case TimeSyNote:
        return "";
    case TimeSyDottedNote:
        return "";
    case TimeSyNormal:
        return "M(" + std::to_string(bar.m_timeSig.m_beats) + "/" + std::to_string(bar.m_timeSig.m_beatType) + ")";
    }
    return "";
}

std::string GenTabCode::GetFlagInfo(const Options& options, const std::vector<Chord>& chords, const Chord& our, int& lhsNumBeams)
{
    if (our.m_fermata)
    {
        return "F";
    }
    
    if (our.m_noFlag)
    {
        return "";
    }
    
    const NoteType adjusted{static_cast<NoteType>(Adjust(our, options))};
    const std::string flags{"BWHQESTYZ"};
    std::string ourFlags;
    
    const long int ourIndex{std::distance(chords.data(), &our)};
    const int rhsNumBeams{(ourIndex + 1 < static_cast<int>(chords.size()) && chords[ourIndex + 1].m_grid != GridNone && chords[ourIndex + 1].m_grid != GridStart)
                        ? std::max(static_cast<int>(Adjust(chords[ourIndex + 1], options)) - static_cast<int>(NoteTypeQuarter) + 1, 0)
                        : 0
                        };
    
    const int numBeams{(our.m_grid != GridNone)
                        ? std::max(adjusted - static_cast<int>(NoteTypeQuarter) + 1, 0)
                        : 0
                        };
    
    // decrease beams
    if (numBeams < lhsNumBeams)
    {
        ourFlags = std::string(lhsNumBeams - numBeams, ']');
        lhsNumBeams = numBeams;
    }
    
    // increase beams
    if (numBeams > lhsNumBeams)
    {
        ourFlags += std::string(numBeams - lhsNumBeams, '[');
        lhsNumBeams = numBeams;
    }
    
    // decrease beams if we've got a hook
    if (numBeams > rhsNumBeams)
    {
        ourFlags += std::string(numBeams - rhsNumBeams, ']');
        lhsNumBeams -= (numBeams - rhsNumBeams);
    }
    
    // no beams
    if (numBeams == 0)
    {
        const ptrdiff_t flagsIdx = static_cast<ptrdiff_t>(adjusted) - static_cast<ptrdiff_t>(NoteTypeBreve);
        if (flagsIdx <= 0)
        {

            ourFlags = flags.front();
            LOGGER << "error NoteType " << adjusted << " too long clamped to " << ourFlags;
        }
        else if (flagsIdx >= static_cast<ptrdiff_t>(flags.size()))
        {
            ourFlags = flags.back();
            LOGGER << "error NoteType " << adjusted << " too short clamped to " << ourFlags;
        }
        else
        {
            ourFlags = flags[flagsIdx];
        }
    }
    
    // dotted
    if (our.m_dotted)
    {
        ourFlags += ".";
    }
    
    return ourFlags;
}

std::string GenTabCode::GetRightFingering(const Note & note)
{
    switch (note.m_rightFingering)
    {
    case FingerNone:
        return "";
    case FingerFirst:
        return ".";
    case FingerSecond:
        return ":";
    case FingerThird:
        return "(Fr...:7)";
    case FingerForth:
        return "";
    case FingerThumb:
        return "!";
    }
    return "";
}
    
std::string GenTabCode::GetLeftFingering(const Note & note)
{
    switch (note.m_leftFingering)
    {
    case FingerNone:
        return "";
    case FingerFirst:
        return "(Fl1:4)";
    case FingerSecond:
        return "(Fl2:4)";
    case FingerThird:
        return "(Fl3:4)";
    case FingerForth:
        return "(Fl4:4)";
    case FingerThumb:
        return "";
    }
    return "";
}

std::string GenTabCode::GetLeftOrnament(const Note & note)
{
    switch (note.m_leftOrnament)
    {
    case OrnNone:
        return "";
    case OrnHash:
        return "(Oe:4)";
    case OrnPlus:
        return "";
    case OrnCross:
        return "(Of:4)";
    case OrnDot:
        return "";
    case OrnDotDot:
        return "";
    case OrnBrackets:
        return "";
    case OrnComma:
        return "(Oa1:6)";
    case OrnCommaRaised:
        return "(Oa1:4)";
    case OrnColon:
        return "";
    case OrnColonColon:
        return "";
    case OrnAsterisk:
        return "";
    case OrnSmiley:
        return "(Oc1:7)";
    case OrnHat:
        return "";
    case OrnDash:
        return "";
        
    case OrnSize:
        return "";
    }
    return "";
}

std::string GenTabCode::GetRightOrnament(const Note & note)
{
    switch (note.m_rightOrnament)
    {
    case OrnNone:
        return "";
    case OrnHash:
        return "(Oe:5)";
    case OrnPlus:
        return "";
    case OrnCross:
        return "(Of:5)";
    case OrnDot:
        return "";
    case OrnDotDot:
        return "";
    case OrnBrackets:
        return "";
    case OrnComma:
        return "(Oa1:8)";
    case OrnCommaRaised:
        return "(Oa1:5)";
    case OrnColon:
        return "";
    case OrnColonColon:
        return "";
    case OrnAsterisk:
        return "";
    case OrnSmiley:
        return "(Oc1:8)";
    case OrnHat:
        return "";
    case OrnDash:
        return "";
        
    case OrnSize:
        return "";
    }
    return "";
}

std::string GenTabCode::GetFret(const Note & note)
{
    if (note.m_fret == Note::Anchor)
    {
        return "-"; // anchor for connector
    }
    
    // a..p, excluding j
    std::string letter{static_cast<char>('a' + note.m_fret + (note.m_fret > 8 ? 1 : 0))};
    if (note.m_course >= Course11)
    {
        return std::to_string(note.m_course - Course7);
    }

    if (note.m_course >= Course8)
    {
        return letter + std::string(note.m_course - Course7, '/');
    }

    return letter;

}

std::string GenTabCode::GetConnector(const Note & note)
{
    if (note.m_connector.m_endPoint == EndPointNone)
    {
        return "";
    }
    
    std::string result("(C");
    
    if (note.m_connector.m_endPoint == EndPointStart)
    {
        ++m_lineId; // every line has a unique line numner
        result += std::to_string(m_lineId);
        result += ":";
        if (note.m_connector.m_curve == CurveOver)
        {
            result += "-6";
        }
        else if (note.m_connector.m_curve == CurveUnder)
        {
            result += "6";
        }
        result += std::to_string(Pos(note.m_connector.m_compass));
        result += ")";
        
        // remember line id for end of the line
        if (note.m_connector.m_function == FunSlur)
        {
            m_slurId[note.m_connector.m_number] = m_lineId;
        }
        else
        {
            m_holdId[note.m_connector.m_number] = m_lineId;
        }
    }
    else
    {
        int lineId{0};
        if (note.m_connector.m_function == FunSlur)
        {
            auto it{m_slurId.find(note.m_connector.m_number)};
            if (it != m_slurId.end())
            {
                lineId = it->second;
                m_slurId.erase(it);
            }
            else
            {
                LOGGER << "Can't find start of slur " << note.m_connector.m_number;
            }
        }
        else
        {
            auto it{m_holdId.find(note.m_connector.m_number)};
            if (it != m_holdId.end())
            {
                lineId = it->second;
                m_holdId.erase(it);
            }
            else
            {
                LOGGER << "Can't find start of hold " << note.m_connector.m_number;
            }
        }
        
        result += std::to_string(-lineId);
        result += ":";
        result += std::to_string(Pos(note.m_connector.m_compass));
        result += ")";
    }
    
    return result;
}

int GenTabCode::Pos(Compass compass)
{
    // 123
    // 4 5
    // 678
    switch (compass)
    {
        case North:
            return 2;
        case NorthWest:
            return 1;
        case West:
            return 4;
        case SouthWest:
            return 6;
        case South:
            return 7;
        case SouthEast:
            return 8;
        case East:
            return 5;
        case NorthEast:
            return 3;
    }
    
    return 7; // silly compiler - can't get here but complains if missing
}

std::string GenTabCode::GetRules(const Piece& piece, const Part& part, TabType tabType)
{
    std::ostringstream ss;
    ss << "{<rules>" << '\n';
    
    if (!piece.m_title.empty())
    {
        ss << "    <title>" << piece.m_title << "</title>" << '\n';
    }
    
    std::string notation;
    switch (tabType)
    {
    case TabFrench:
        notation = "french";
        break;
    case TabGerman:
        notation = "german";
        break;
    case TabItalian:
        notation = "italian";
        break;
    case TabSpanish:
        notation = "spanish";
        break;
    default:
        break;
    }

    if (!notation.empty())
    {
        ss << "    <notation>" << notation << "</notation>" << '\n';
    }
    
    if (part.m_staffLines != Course6 && tabType != TabGerman)
    {
        ss << "    <staff_lines>" << part.m_staffLines << "</staff_lines>" << '\n';
    }
    
    // crotchet_flag, 1 flag corresponds to Q
    const std::string flags{"BWHQESTYZ"};
    if (part.m_crotchetFlag >= -2 && part.m_crotchetFlag < static_cast<int>(flags.size()) - 2)
    {
        ss << "    <crotchet_flag>" << flags[part.m_crotchetFlag + 2] << "</crotchet_flag>" << '\n';
    }
    
    ss << "    <pitch>" << part.m_tuning[0].Midi() << "</pitch>" << '\n'
       << "    <tuning>(";
    
    for (size_t i{1}; i < part.m_tuning.size(); ++i)
    {
        if (i > 1)
        {
            ss << " ";
        }
        
        ss << part.m_tuning[i].Midi() -  part.m_tuning[i - 1].Midi();
    }
    
    ss << ")</tuning>" << '\n';
            
    // fretting
    const std::string fretting{GetFretting(part)};
    if (!fretting.empty())
    {
        ss << "    <fretting>" << '\n';
        for (int i{1}; i <= static_cast<int>(Course6); ++i)
        {
            if (i == 1)
            {
                ss << "        (" << fretting << '\n';
            }
            else if (i < static_cast<int>(Course6))
            {
                ss << "        " << fretting << '\n';
            }
            else
            {
                ss << "        " << fretting << ")" << '\n';
            }
        }
        ss << "    </fretting>" << '\n';
    }
    
    ss << "</rules>}" << '\n';
    
    return ss.str();
}

std::string GenTabCode::GetFretting(const Part& part)
{
    if (part.m_missingFrets.empty())
    {
        return "";
    }
    
    // diatonic fret tuning
    // start with 1 semitone per fret
    std::vector<size_t> board(part.MaxFret() + 1, 1);
    board[0] = 0; // fret 0 is always missing
    
    // work forward from lowest missing fret
    for (const auto missing : part.m_missingFrets)
    {
        if (missing + 1 >= static_cast<int>(board.size()))
        {
            continue;
        }
        
        board[missing + 1] += board[missing]; // add the missing fret's width to the next fret
        board[missing] = 0;
    }
    
    std::string result{"("};
    bool sep{false};
    for (const auto width : board)
    {
        if (width == 0)
        {
            continue; // missing fret
        }
        
        if (sep)
        {
            result += ' ';
        }
        
        result += std::to_string(width);
        sep = true;
    }
    
    result += ')';
    
    return result;
}

NoteType GenTabCode::Adjust(const Chord& chord, const Options& options)
{
    // TabCode documentation has Q = 1 flag, therefore B = NoteTypeWhole, not NoteTypeBreve
    NoteType adjusted{static_cast<NoteType>(chord.m_noteType - 1 + options.m_flags)};
    adjusted = std::max(NoteTypeLong, adjusted);
    adjusted = std::min(NoteType256th, adjusted);
    return adjusted;
}

} // namespace luteconv
