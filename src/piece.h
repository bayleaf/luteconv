#ifndef _PIECE_H_
#define _PIECE_H_

#include <limits>
#include <string>
#include <string_view>
#include <vector>

#include "options.h"
#include "pitch.h"
#include "rational.h"

namespace luteconv
{

/**
 * Course
 */
enum Course
{
    CourseNone,
    Course1,
    Course2,
    Course3,
    Course4,
    Course5,
    Course6,
    Course7,
    Course8,
    Course9,
    Course10,
    Course11,
    Course12,
    Course13,
    Course14,
    Course15,
    Course16,
    Course17,
    Course18,
    Course19,
    Course20,
};

/**
 * Rhythm
 */
enum NoteType
{
    //                               Should be   MuseScore           TAB      TabCode
    NoteTypeLong,    //                                              L
    NoteTypeBreve,   //                                              B
    NoteTypeWhole,   // semibreve    0 flags                         W        B
    NoteTypeHalf,    // minim        1 flag      0 flags short stem  w        W
    NoteTypeQuarter, // crotchet     2 flags     0 flags             0 flags  H
    NoteTypeEighth,  // quaver       3 flags     1 flag              1 flags  Q
    NoteType16th,    // semi         4 flags     2 flags             2 flags  E
    NoteType32nd,    // demi         5 flags     3 flags             3 flags  S
    NoteType64th,    // hemi         6 flags     4 flags             4 flags  T
    NoteType128th,   // semihemi     7 flags     5 flags             5 flags  Y
    NoteType256th    // demisemihemi 8 flags     6 flags                      Z
};

enum Fingering
{
    FingerNone,
    FingerFirst,
    FingerSecond,
    FingerThird,
    FingerForth,
    FingerThumb
};

enum Ornament
{
    OrnNone,
    OrnHash,
    OrnPlus,
    OrnCross,
    OrnDot,
    OrnDotDot,
    OrnBrackets, // [] around note
    OrnComma,
    OrnCommaRaised,
    OrnColon,
    OrnColonColon,
    OrnAsterisk,
    OrnSmiley,
    OrnHat,
    OrnDash,
    
    OrnSize,
};

enum Grid
{
    GridNone,
    GridStart,
    GridMid,
    GridEnd
};

enum BarStyle
{
    BarStyleRegular,
    BarStyleDotted,
    BarStyleDashed,
    BarStyleHeavy,
    BarStyleLightLight,
    BarStyleLightHeavy,
    BarStyleHeavyLight,
    BarStyleHeavyHeavy,
    BarStyleTick,
    BarStyleShort
};

enum Repeat
{
    RepNone,
    RepForward,
    RepBackward,
    RepJanus
};

enum TimeSymbol
{
    TimeSyNone,
    TimeSyCommon,
    TimeSyCut,
    TimeSySingleNumber,
    TimeSyNote,
    TimeSyDottedNote,
    TimeSyNormal
};

/**
 * Connector line's end points
 */
enum EndPoint
{
    EndPointNone,
    EndPointStart,
    EndPointStop
};

/**
 * Connector line's curve
 */
enum Curve
{
    CurveStraight,
    CurveOver,
    CurveUnder
};

/**
 * Connector line's function
 */
enum Function
{
    FunUnknown,
    FunSlur,
    FunHold
};

/**
 * Compass points
 */
enum Compass
{
    North,
    NorthWest,
    West,
    SouthWest,
    South,
    SouthEast,
    East,
    NorthEast
};

/**
 * Clef
 */
enum Clef
{
    ClefNone,
    ClefTab,
    ClefF,
    ClefG,
    ClefG8a,
    ClefG8b,
};

/**
 * Key signature, circle of 5ths
 */
enum Key
{
    KeyNone = -10,
    KeyCb   = -7,
    KeyGb   = -6,
    KeyDb   = -5,
    KeyAb   = -4,
    KeyEb   = -3,
    KeyBb   = -2,
    KeyF    = -1,
    KeyC    = 0,
    KeyG    = 1,
    KeyD    = 2,
    KeyA    = 3,
    KeyE    = 4,
    KeyB    = 5,
    KeyFs   = 6,
    KeyCs   = 7
};

/**
 * Written accidental to be displayed, as opposed to the sounding accidental in Pitch::Alter
 */
enum Accidental
{
    AccNone = -10,
    AccFlat = -1,
    AccNatural = 0,
    AccSharp = 1,
};

/**
 * Lyric hyphenation is indicated by the syllabic type. The single, begin, end,
 * and middle values represent single-syllable words, word-beginning syllables,
 * word-ending syllables, and mid-word syllables, respectively.
 */
enum Syllabic
{
    SylSingle,
    SylBegin,
    SylMiddle,
    SylEnd,
};

/*
 * A word or syllable in a lyric line
 */
class Lyric
{
public:
    Lyric() = default;
    explicit Lyric(std::string_view text, Syllabic syllabic);
    ~Lyric() = default;
    
    std::string m_text;
    Syllabic m_syllabic{SylSingle};
};

/**
 * Connector line joining two notes
 */
class Connector
{
public:
    Connector() = default;
    ~Connector() = default;

    EndPoint m_endPoint{EndPointNone};
    Curve m_curve{CurveStraight};
    int m_number{1};
    Function m_function{FunUnknown};
    Compass m_compass{North}; // direction from note to endpoint
};
    
class Note
{
public:
    Note() = default;
    ~Note() = default;

    int m_course{CourseNone}; // 1...
    int m_fret{0}; // 0...
    static const int Anchor{-1}; // means not a note, used to anchor a connector
    static const int Ensemble{-2}; // means an ensemble line. Vertical lines linking members of a chord to be played together
    Pitch m_pitch; // CMN pitch, also used by midi and intabulator, otherwise unused
    Accidental m_accidental{AccNone}; // CMN written accidental

    Fingering m_leftFingering{FingerNone};
    Fingering m_rightFingering{FingerNone};
    Ornament m_leftOrnament{OrnNone};
    Ornament m_rightOrnament{OrnNone};
    
    Connector m_connector;
};

class Chord
{
public:
    Chord() = default;
    ~Chord() = default;

    std::vector<Note> m_notes;
    std::vector<Lyric> m_lyrics; // CMN lyrics
    NoteType m_noteType{NoteTypeQuarter};
    int m_stamp{0}; // bar relative time point of note in ticks, for merging voices
    Grid m_grid{GridNone};
    bool m_dotted{false};
    bool m_fermata{false};
    bool m_noFlag{false};
    bool m_triplet{false}; // first note in a triplet
    bool m_tie{false};
};

class TimeSig
{
public:
    TimeSymbol m_timeSymbol{TimeSyNone};
    int m_beats{0};
    int m_beatType{0};
    
    bool operator ==(const TimeSig& rhs) const
    {
        return m_timeSymbol == rhs.m_timeSymbol &&
        m_beats == rhs.m_beats &&
        m_beatType == rhs.m_beatType;
    }
    
    bool operator !=(const TimeSig& rhs) const
    {
        return !operator ==(rhs);
    }
};

class Bar
{
public:
    Bar() = default;
    ~Bar() = default;
    
    /**
     * Quarter note divisions, default if not otherwise specified
     * Durations are chosen to be divisable by 3 and 2 for triplets and dotted notes
     */
    static const int DivisionsDefault{(1 << (NoteType256th - NoteTypeQuarter)) * 3};
    
    /**
     * Merge voices within a bar.  Each chord must have a timestamp in ticks.
     * 
     * @param divisions, number of ticks in a quarter note
     */
    void MergeVoices(int divisions = DivisionsDefault);
    
    /**
     * Fix note type from the chords' time stamps.
     * 
     * @param[in] divisions, number of ticks in a quarter note
     * @param[in] barEnd stamp for end of bar, or 0 if final chord in bar is already correct
     */
    void FixNoteTypes(int divisions, int barEnd);
    
    /**
     * Get the duration of a note in ticks.
     * 
     * @param noteType
     * @param dotted
     * @param triplet
     * @param divisions, number of ticks in a quarter note
     * 
     * @return duration of note in ticks
     */
    static int Duration(NoteType noteType, bool dotted, bool triplet, int divisions = DivisionsDefault);
    
    /**
     * Get a NoteType as a fraction
     * 
     * @param[in] noteType
     * @return fraction
     */
    static Rational NoteFrac(NoteType noteType);
    
    /**
      * Get the number of notes in this triplet, if it is a triplet
      * 
      * @param[in] chord in this bar
      */
     int TripletNotes(const Chord& chord) const;
     
    /**
     * Get the number of notes in this triplet, if it is a triplet
     * 
     * @param[in] chord in this bar
     * @param[out] index of first note in the triplet
     */
    int TripletNotes(const Chord& chord, size_t& index) const;
    
    /**
     * Clear the bar of all contents
     */
    void Clear();
    
    std::vector<Chord> m_chords;
    Clef m_clef{ClefNone};
    Key m_key{KeyNone};
    TimeSig m_timeSig;
    BarStyle m_barStyle{BarStyleRegular};
    Repeat m_repeat{RepNone};
    int m_volta{0}; // volta brackets.  0 => none, 1 => prima volta, 2 => seconda volta
    bool m_fermata{false};
    bool m_eol{false};
};

/**
 * Part within a piece.  Used for either tablature or CMN.
 */
class Part
{
public:
    Part() = default;
    ~Part() = default;
    
    /**
     * Maximum course used
     * 
     * @return maximum course used
     */
    int MaxCourse() const;
    
    /**
     * Maximum number of notes used in any chord
     * 
     * @return maximum number of notes used in any chord
     */
    int MaxChord() const;
    
    /**
     * Maximum fret used
     * 
     * @return maximum fret used
     */
    int MaxFret() const;
    
    /**
     * Set the tuning
     * 
     * @param[in] options
     */
    void SetTuning(const Options& options);
    
    /**
     * Get the pitch of a course/fret, calculated.
     * 
     * @param[in] note
     * @return pitch
     */
    Pitch CalcPitch(const Note& note) const;
    
    std::string m_partName;
    std::vector<Bar> m_bars;
    std::vector<Pitch> m_tuning;
    std::vector<int> m_missingFrets; // for diatonic instruments sorted increasing fret
    int m_staffLines{6};
    int m_crotchetFlag{std::numeric_limits<int>::min()}; // flags on a quarter note.  std::numeric_limits<int>::min() => use format's default
    TabType m_tabType{TabUnknown};
    TabGermanSubtype m_tabGermanSubtype{GstUnknown};
    bool m_tabbed{false}; // notes has course/fret (e.g. all tablature formats, not midi)
    bool m_pitched{false}; // note has pitches (midi, but not most tablature formats)
};

// Internal representation of tablature.  Source formats are first
// converted to class Piece, then class Piece is converted to the
// destination format.  In this manner for n formats we only need
// n parsers and n generators, rather than n^2 direct converters.
class Piece
{
public:
    Piece();
    ~Piece() = default;
    
    /**
     * get sharps or flats for given key
     * 
     * @param[in] key
     * @return key signature
     */
    static std::vector<Accidental> KeySignature(Key key);
    
    std::string m_title;
    std::string m_composer;
    std::string m_copyright;
    std::vector<std::string> m_credits;
    std::vector<Part> m_parts;
    bool m_copyrightEnabled{false};
};

} // namespace luteconv

#endif // _PIECE_H_
