#include "pitch.h"

#include <algorithm>
#include <array>
#include <cctype>
#include <stdexcept>
#include <utility>

namespace luteconv
{

Pitch::Pitch(char step, int alter, int octave)
: m_step{step}, m_alter(static_cast<int8_t>(alter)), m_octave(static_cast<int8_t>(octave))
{
    
}

Pitch::Pitch(int midi)
{
    const int semitones = midi - 12; // C0 = 0
    
    // 12 notes in an octave.  Ignore enharmonics, prefer B flat over A sharp.
    const std::array<const std::pair<const char, const int8_t>, 12> octaveNotes
        {{{'C', 0}, {'C', 1}, {'D', 0}, {'D', 1},
         {'E', 0}, {'F', 0}, {'F', 1},
         {'G', 0}, {'G', 1}, {'A', 0},
         {'B', -1}, {'B', 0}
        }};
    
    // C++ operator / truncates towards 0, we need floor
    int q{0};
    int r{0};
    if (semitones >= 0)
    {
        q = semitones / 12;
        r = semitones % 12;
    }
    else
    {
        q = -((-semitones) / 12);
        r = (-semitones) % 12;
        if (r != 0)
        {
            --q;
            r = 12 - r;
        }
    }
    m_octave = static_cast<int8_t>(q);
    m_step = octaveNotes.at(r).first;
    m_alter = octaveNotes.at(r).second;
}

char Pitch::Step() const
{
    return m_step;
}

int Pitch::Alter() const
{
    return m_alter;
}

int Pitch::Octave() const
{
    return m_octave;
}

bool Pitch::operator ==(const Pitch & rhs) const
{
    return m_step == rhs.m_step &&
            m_alter == rhs.m_alter &&
            m_octave == rhs.m_octave;
}

bool Pitch::operator <(const Pitch & rhs) const
{
    if (m_octave < rhs.m_octave)
    {
        return true;
    }
    if (m_octave > rhs.m_octave)
    {
        return false;
    }
    
    // order in octave C D E F G A B
    const int lhsRank{m_step < 'C' ? m_step + 7 : m_step};
    const int rhsRank{rhs.m_step < 'C' ? rhs.m_step + 7 : rhs.m_step};
    
    if (lhsRank < rhsRank)
    {
        return true;
    }
    if (lhsRank > rhsRank)
    {
        return false;
    }
 
    return m_alter < rhs.m_alter;
}

int Pitch::Midi() const
{
    // Distance in semitones from the octave's starting C to the given step
    //                                         A   B  C  D  E  F  G
    const std::array<const int, 7> octaveStart{9, 11, 0, 2, 4, 5, 7};
    const int semitones = m_octave * 12 + octaveStart.at(m_step - 'A') + m_alter; // semitones from C0
    return semitones + 12; // MIDI note C4 = 60
}

std::string Pitch::ToString() const
{
    std::string result;
    
    // step
    result += m_step;
    
    // alter
    if (m_alter > 0)
    {
        result += std::string(static_cast<int>(m_alter), '#');
    }
    if (m_alter < 0)
    {
        result += std::string(-(static_cast<int>(m_alter)), 'b');
    }
    
    // octave
    result += std::to_string(static_cast<int>(m_octave));
    return result;
}

bool Pitch::operator !=(const Pitch & rhs) const
{
    return !(operator ==(rhs));
}

Pitch& Pitch::operator +=(int rhs)
{
    *this = *this + rhs;
    return *this;
}

Pitch Pitch::operator +(int rhs) const
{
    return Pitch(Midi() + rhs);
}

Pitch& Pitch::operator -=(int rhs)
{
    *this = *this - rhs;
    return *this;
}

Pitch Pitch::operator -(int rhs) const
{
    return Pitch(Midi() - rhs);
}

int Pitch::operator -(const Pitch& rhs) const
{
    return Midi() - rhs.Midi();
}

void Pitch::SetTuning(int courses, std::vector<Pitch>& tuning)
{
    if (courses == 8)
    {
        SetTuning("G4 D4 A3 F3 C3 G2 F2 D2", tuning);
    }
    else if (courses < 11)
    {
        SetTuningVieilTon(tuning);
    }
    else
    {
        SetTuningBaroque(tuning);
    }
    
    // if there are yet more courses then poulate with descending semitones
    const int lastMidi{tuning.back().Midi()};
    int j{1};
    for (size_t i{tuning.size() + 1}; i <= static_cast<size_t>(courses); ++i)
    {
        tuning.emplace_back(lastMidi - j);
        ++j;
    }
}

void Pitch::SetTuningBaroque(std::vector<Pitch>& tuning)
{
    SetTuning("F4 D4 A3 F3 D3 A2 G2 F2 E2 D2 C2 B1 A1 G1", tuning);
}

void Pitch::SetTuningVieilTon(std::vector<Pitch>& tuning)
{
    SetTuning("G4 D4 A3 F3 C3 G2 F2 Eb2 D2 C2 B1 A1 G1 F1", tuning);
}


Pitch Pitch::Parse(std::string_view s, size_t& pos)
{
    Pitch p;
    
    while (pos < s.size() && s[pos] == ' ')
    {
        ++pos;
    }
    
    if (pos == s.size() || s[pos] < 'A' || s[pos] > 'G')
    {
        throw std::runtime_error(std::string("Error: SPN note syntax"));
    }
    
    p.m_step = s[pos];
    ++pos;
    
    p.m_alter = 0;
    while (pos < s.size() && s[pos] == 'b')
    {
        --p.m_alter;
        ++pos;
    }
    while (pos < s.size() && s[pos] == '#')
    {
        ++p.m_alter;
        ++pos;
    }
    
    try
    {
        size_t len{0};
        p.m_octave = static_cast<int8_t>(std::stoi(std::string(s.substr(pos)), &len));
        pos += len;
    }
    catch (...)
    {
        throw std::runtime_error(std::string("Error: SPN note syntax"));
    }
    
    while (pos < s.size() && s[pos] == ' ')
    {
        ++pos;
    }
    
    return p;
}

std::vector<Pitch> Pitch::SetTuning(std::string_view s)
{
    std::vector<Pitch> tuning;
    SetTuning(s, tuning);
    return tuning;
}

void Pitch::SetTuning(std::string_view s, std::vector<Pitch>& tuning)
{
    tuning.clear();
    
    size_t pos{0};
    while (pos < s.size())
    {
        tuning.push_back(Parse(s, pos));
    }
}

std::string Pitch::GetTuning(const std::vector<Pitch>& tuning)
{
    std::string result;

    for (const auto& pitch : tuning)
    {
        result += pitch.ToString();
    }
    
    return result;
}

} // namespace luteconv

