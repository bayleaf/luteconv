#include "options.h"

#include <popl/include/popl.hpp>

#include <iostream>

#include "logger.h"

namespace luteconv
{

#define XSTRINGIFY(s) STRINGIFY(s)
#define STRINGIFY(s) #s

Options::Options()
: m_version{XSTRINGIFY(VERSION)}
{
}

#undef STRINGIFY
#undef XSTRINGIFY

void Options::PrintHelp(std::string_view allowed)
{
    std::cout << "luteconv " << m_version << '\n'
            << "Convert between lute tablature file formats." << '\n'
            << "Supported source formats: abc, frt, ft2, ft3, jtxml, jtz, lsml, mei, mid, mnx," << '\n'
            << "                          musicxml, mxl, tab, tc" << '\n'
            << "Supported desination formats: abc, frt, lsml, mei, mid, mnx, musicxml, mxl, tab, tc" << '\n'
            << "Usage: luteconv [options ...] [source-file] [destination-file]" << '\n'
            << '\n'
            << allowed << '\n'
            << "The source-file can be specified as the 1st positional parameter." << '\n'
            << "If no source-file is specified then standard input is used," << '\n'
            << "in which case the source file format must be specified, --srcformat." << '\n'
            << '\n'
            << "The destination-file can be specified either using the --output option" << '\n'
            << "or as the 2nd positional parameter, this conforms with GNU options guidelines." << '\n'
            << "If no destination-file is specified then standard output is used," << '\n'
            << "in which case the destination file format must be specified, --dstformat." << '\n'
            << '\n'
            << R"(tabtype = "french" | "german" | "italian" | "neapolitan" | "spanish")" << '\n'
            << "   The source tablature type is usually deduced from the source-file.  However," << '\n'
            << "   for tab files it is sometimes necessary to distinguish between french," << '\n'
            << "   italian and spanish tablatures." << '\n'
            << "   The default destination tablature type is the source's tablature type." << '\n'
            << '\n'
            << R"(gltsubtype = "ABC" | "1AB" | "123")" << '\n'
            << "   Specifies the representation of 6th and highter courses for German tablature." << '\n'
            << '\n'
            << R"(format = "abc" | "frt" | "ft2" | "ft3" | "jtxml" | "jtz" | "lsml" |)" << '\n'
            << R"(         "mei" | "mid" | "mnx" | "musicxml" | "mxl" | "tab" | "tc")" << '\n'
            << "   if a file format is not specified then the filetype is used." << '\n'
            << '\n'
            << "tuning = Courses in scientific pitch notation, in increasing course number." << '\n'
            << "   Luteconv uses the tuning specifed by option --tuning, if given; otherwise" << '\n'
            << "   the tuning specified in the source file, if any; otherwise the" << '\n'
            << "   tuning is based on the number of courses used in the piece as follows:" << '\n'
            << "   = 8   \"G4 D4 A3 F3 C3 G2 F2 D2\"" << '\n'
            << "   <= 10 \"G4 D4 A3 F3 C3 G2 F2 Eb2 D2 C2\"" << '\n'
            << "   >= 11 \"F4 D4 A3 F3 D3 A2 G2 F2 E2 D2 C2 B1 A1 G1\"" << '\n'
            << "   Option --7tuning, if given, will then modify the tuning of the" << '\n'
            << "   7th, 8th, ... courses." << '\n'
            << '\n'
            << "Luteconv supports transcription, by transposition of pitch and by having" << '\n'
            << "a different tuning or number of courses for the destination instrument." << '\n'
            << '\n'
            << "The destination tuning may be specified with --dstTuning, defaults to the" << '\n'
            << "source tuning. If the destination tuning is different from the source" << '\n'
            << "tuning then the music is intabulated." << '\n'
            << '\n'
            << "If the --transpose option is given then all pitches are transposed by the" << '\n'
            << "specified number of semitones, positive or negative, and the music is"  << '\n'
            << "intabulated."  << '\n'
            << '\n'
            << "MIDI .mid may be used a source format, but a lot of the information required" << '\n'
            << "for tablature is missing.  The music has to be intabulated and barring" << '\n'
            << "re-calculated. The default tuning is \"G4 D4 A3 F3 C3 G2 F2 Eb2 D2 C2\"," << '\n'
            << "otherwise the tuning must be specified." << '\n'
            << '\n'
            << "If any of --intabulate, --dstTuning, --transpose or if MIDI is the source format"  << '\n'
            << "then the music is intabulated, which may result in difficult stetches or" << '\n'
            << "unplayable chords. This feature is experimental." << '\n'
            << '\n'
            << "MIDI .mid may be used a destination format in order to play the music." << '\n'
            << "Repeats are ignored. The patch number may be specified with --midipatch." << '\n'
            << "The tempo may be specified in beats per minute with --miditempo." << '\n'
            << "One beat is a rhythm sign with no flags." << '\n'
            << '\n'
            << "Where the source format allows more than one piece/section per file" << '\n'
            << "the --index option selects the desired piece/section, counting from 0." << '\n'
            << "Default 0." << '\n'
            << '\n'
            << "The .tab format does not support multiple piece/sections, nevertheless it can be" << '\n'
            << "used with piece/sections separated by blank line and {section title}." << '\n'
            << "Heuristics are used to determine these piece/section breaks." << '\n'
            << "These herustics are also used to determine the tuning, if not otherwise" << '\n'
            << "specified, and to offer limited support for the diatonic cittern." << '\n'
            << "Option --noHeuristics will disable this behaviour." << '\n'
            << '\n'
            << "If the --multi option is given then all piece/sections, counting from the" << '\n'
            << "index, are output to separate files.  The destination filename is used as a" << '\n'
            << R"(template with the index number inserted before the last ".".  E.g. a)" << '\n'
            << "destination file mytune.tab will output piece/sections to mytune-0.tab," << '\n'
            << "mytune-1.tab etc." << '\n'
            << '\n'
            << "If the --list option is given then all the piece/sections, counting from the" << '\n'
            << "index, are listed to stdout together with their index numbers." << '\n'
            << "No conversion is done." << '\n'
            << '\n'
            << "Some lute software encodes rhythm as the number of lute tablature flags, others" << '\n'
            << "encode the note value (whole, half, quarter etc) unfortunately there is" << '\n'
            << "no fixed mapping between the two.  The --flags option adds (or subtracts) flags" << '\n'
            << "from the destination rhythm to adjust this mapping.  Default 0." << '\n'
            << '\n'
            << "Option --wrap, default 25.  Some formats require explicit stave line endings." << '\n'
            << "Luteconv uses a herustic: count chords, when the wrap threshold is reached end" << '\n'
            << "the stave at the end of the current bar." << '\n'
            << '\n'
            << "If the  --barsupp is given then initial bar line on each system is suppressed," << '\n'
            << "unless it's a repeat.  This only affects formats that require explicit staff" << '\n'
            << "line endings." << '\n'
            << '\n'
            << "If the --rhythmscheme is given then the destination rhythm flags are set" << '\n'
            << "according to the scheme number: 0 (default) preserve the source rhythm scheme;" << '\n'
            << "1 only show arhythm sign if it differs from the previous sign, or if it is" << '\n'
            << "dotted, or the previous sign is dotted; 2 as per 1 but always show the first" << '\n'
            << "rhythm sign in the bar. If either 1 or 2 is used then any beaming is removed." << '\n'
            << '\n'
            << "Report bugs to: paul@bayleaf.org.uk" << '\n'
            << "pkg home page: <https://bitbucket.org/bayleaf/luteconv/src/master/>" << '\n'
            << "General help using GNU software: <https://www.gnu.org/gethelp/>" << '\n'
            ;
}

bool Options::ProcessArgs(int argc, char** argv)
{
    // Using popl library rather than POSIX getopt so can compile for non-POSIX platforms, e.g. Windows
    using namespace popl;
    
    OptionParser op("Allowed options");
    auto helpOption = op.add<Switch>("h", "help", "Show help");
    auto versionOption = op.add<Switch>("v", "version", "Show version");
    auto outputOption = op.add<Value<std::string>>("o", "output", "Set destination-file", "", &m_dstFilename);
    auto srcTabTypeOption = op.add<Value<std::string>>("S", "Srctabtype", "Set source tablature type");
    auto dstTabTypeOption = op.add<Value<std::string>>("D", "Dsttabtype", "Set destination tablature type");
    auto dstGermanSubtypeOption = op.add<Value<std::string>>("G", "dstGermanSubtype", "Set destination German tablature subtype");
    auto srcFormatOption = op.add<Value<std::string>>("s", "srcformat", "Set source format");
    auto dstFormatOption = op.add<Value<std::string>>("d", "dstformat", "Set destination format");
    auto tuningOption = op.add<Value<std::string>>("t", "tuning", "Set tuning for all courses");
    auto sevenTuningOption = op.add<Value<std::string>>("7", "7tuning", "Set tuning from 7th course");
    auto dstTuningOption = op.add<Value<std::string>>("T", "dstTuning", "Set destination tuning for all courses");
    auto indexOption = op.add<Value<int>>("i", "index", "Set section index", 0, &m_index);
    auto multiOption = op.add<Switch>("m", "multi", "Multiple sections, output one per file", &m_multi);
    auto noHeuristicsOption = op.add<Switch>("n", "noheuristics", "Disable heuristics for splitting .tab files", &m_noHeuristics);
    auto flagsOption = op.add<Value<int>>("f", "flags", "Add flags to destination rhythm", 0, &m_flags);
    auto verboseOption = op.add<Switch>("V", "Verbose", "Set verbose output");
    auto wrapOption = op.add<Value<int>>("w", "wrap", "Stave wrap threshold", m_wrapDefault, &m_wrapThreshold);
    auto transposeOption = op.add<Value<int>>("p", "transpose", "Transposition in semitones", 0, &m_transpose);
    auto barSuppOption = op.add<Switch>("b", "barsupp", "Suppress initial bar line on each system", &m_barSupp);
    auto listOption = op.add<Switch>("l", "list", "List sections in the source file", &m_list);
    auto intabulateOption = op.add<Switch>("I", "intabulate", "Intablulate", &m_intabulate);
    auto rhythmSchemeOption = op.add<Value<int>>("r", "rhythmscheme", "Set destination rhythm scheme", 0, &m_rhythmScheme);
    auto midiPatchOption = op.add<Value<int>>("M", "midipatch", "Set midi patch number", 0, &m_midiPatch);
    auto midiTempoOption = op.add<Value<double>>("P", "miditempo", "Set midi tempo", 60, &m_midiTempo);

    op.parse(argc, argv);
    
    if (!op.unknown_options().empty())
    {
        std::ostringstream ss;
        ss << "Error: unknown option " << op.unknown_options()[0];
        throw std::runtime_error(ss.str());
    }
    
    if (op.non_option_args().size() > 2)
    {
        throw std::runtime_error("Error: too many arguments");
    }
    
    if (helpOption->is_set())
    {
        std::ostringstream ss;
        ss << op;
        PrintHelp(ss.str());
        return false;
    }
    
    if (versionOption->is_set())
    {
        std::cout << "luteconv " << m_version << '\n'
        << "Copyright (C) 2020-2025 Paul Overell" << '\n'
        << "License GPLv3+: GNU GPL version 3 or later <https://gnu.org/licenses/gpl.html>" << '\n'
        << "This is free software: you are free to change and redistribute it." << '\n'
        << "There is NO WARRANTY, to the extent permitted by law." << '\n'
        ;
        return false;
    }
    
    if (srcTabTypeOption->is_set())
    {
        m_srcTabType = GetTabType(srcTabTypeOption->value());
        if (m_srcTabType == TabUnknown)
        {
            throw std::runtime_error("Error: unknown source tablature type");
        }
    }    

    if (dstTabTypeOption->is_set())
    {
        m_dstTabType = GetTabType(dstTabTypeOption->value());
        if (m_dstTabType == TabUnknown)
        {
            throw std::runtime_error("Error: unknown destination tablature type");
        }
    }    
    
    if (dstGermanSubtypeOption->is_set())
    {
        m_dstTabGermanSubtype = GetTabGermanSubtype(dstGermanSubtypeOption->value());
        if (m_dstTabGermanSubtype == GstUnknown)
        {
            throw std::runtime_error("Error: unknown destination German tablature subtype");
        }
    }

    if (srcFormatOption->is_set())
    {
        m_srcFormat = GetFormat(srcFormatOption->value());
    }
    
    if (dstFormatOption->is_set())
    {
        m_dstFormat = GetFormat(dstFormatOption->value());
    }

    if (tuningOption->is_set())
    {
        Pitch::SetTuning(tuningOption->value(), m_tuning);
    }

    if (sevenTuningOption->is_set())
    {
        Pitch::SetTuning(sevenTuningOption->value(), m_7tuning);
    }
    
    if (dstTuningOption->is_set())
    {
        Pitch::SetTuning(dstTuningOption->value(), m_dstTuning);
    }

    if (verboseOption->is_set())
    {
        Logger::SetVerbose(true);
    }
    
    if (rhythmSchemeOption->is_set())
    {
        if (m_rhythmScheme < 0 || m_rhythmScheme > 2)
        {
         throw std::runtime_error("Error: unknown rhythm scheme");
        }
    }
    
    if (midiPatchOption->is_set())
    {
        if (m_midiPatch < 0 || m_midiPatch > 127)
        {
            throw std::runtime_error("Error: unknown midi patch number");
        }
    }


    // source filename
    if (!op.non_option_args().empty())
    {
        m_srcFilename = op.non_option_args()[0];
    }
    
    if (op.non_option_args().size() >= 2)
    {
        m_dstFilename = op.non_option_args()[1];
    }
    
    // if file format is not specified use filetype
    SetFormatFilename();
    
    return true;
}

void Options::SetFormatFilename()
{
    if (m_srcFormat == FormatUnknown)
    {
        m_srcFormat = GetFormatFilename(m_srcFilename);
    }

    if (m_dstFormat == FormatUnknown)
    {
        m_dstFormat = GetFormatFilename(m_dstFilename);
    }
}

TabType Options::GetTabType(std::string_view tabType)
{
    if (tabType == "french")
    {
        return TabFrench;
    }
    if (tabType == "german")
    {
        return TabGerman;
    }
    if (tabType == "italian")
    {
        return TabItalian;
    }
    if (tabType == "neapolitan")
    {
        return TabNeapolitan;
    }
    if (tabType == "spanish")
    {
        return TabSpanish;
    }
    
    return TabUnknown;
}

TabGermanSubtype Options::GetTabGermanSubtype(std::string_view tabSubtype)
{
    if (tabSubtype == "ABC")
    {
        return GstABC;
    }
    if (tabSubtype == "1AB")
    {
        return Gst1AB;
    }
    if (tabSubtype == "123")
    {
        return Gst123;
    }
    
    return GstUnknown;
}

const char * Options::GetTabType(TabType tabType)
{
    switch (tabType)
    {
        case TabFrench:
            return "french";
        case TabGerman:
            return "german";
        case TabItalian:
            return "italian";
        case TabNeapolitan:
            return "neapolitan";
        case TabSpanish:
            return "spanish";
        case TabUnknown:
            [[fallthrough]];
        default:
            break;
    }
    return "unknown";
}

const char * Options::GetTabGermanSubtype(TabGermanSubtype tabSubtype)
{
    switch (tabSubtype)
    {
        case GstABC:
            return "ABC";
        case Gst1AB:
            return "1AB";
        case Gst123:
            return "123";
        case GstUnknown:
            // [[fallthough]];
        default:
            break;
    }
    return "unknown";
}

Format Options::GetFormatFilename(std::string_view filename)
{
    const size_t found = filename.find_last_of('.');
    if (found != std::string_view::npos)
    {
        return GetFormat(filename.substr(found + 1));
    }

    return FormatUnknown;
}

Format Options::GetFormat(std::string_view format)
{
    if (format == "abc")
    {
        return FormatAbc;
    }
    if (format == "frt")
    {
        return FormatFrt;
    }
    if (format == "ft2")
    {
        return FormatFt2;
    }
    if (format == "ft3")
    {
        return FormatFt3;
    }
    if (format == "jtxml")
    {
        return FormatJtxml;
    }
    if (format == "jtz")
    {
        return FormatJtz;
    }
    if (format == "lsml")
    {
        return FormatLsml;
    }
    if (format == "ltl")
    {
        return FormatLtl;
    }
    if (format == "mei")
    {
        return FormatMei;
    }
    if (format == "mid")
    {
        return FormatMid;
    }
    if (format == "mnx")
    {
        return FormatMnx;
    }
    if (format == "musicxml")
    {
        return FormatMusicxml;
    }
    if (format == "mxl")
    {
        return FormatMxl;
    }
    if (format == "tab")
    {
        return FormatTab;
    }
    if (format == "tc")
    {
        return FormatTabCode;
    }
    return FormatUnknown;
}

} // namespace luteconv
