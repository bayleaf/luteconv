#include "parserabc.h"

#include <algorithm>
#include <cctype>
#include <chrono>
#include <fstream>
#include <iomanip>
#include <iostream>
#include <regex>
#include <string>

#include "charset.h"
#include "logger.h"
#include "platform.h"

namespace luteconv
{

void ParserAbc::Parse(std::istream &src, const Options &options, Piece &piece)
{
    LOGGER << "Parse abc";
    
    Part& part{piece.m_parts.at(0)}; // only 1 part

    int lineNo{0};
    part.m_bars.emplace_back(); // first bar
    bool barIsClear{true};
    bool inHeader{true};
    int maxLedger{Course6}; // see parsertab.cpp for discussion

    // find our tune
    bool found{false};
    const std::string tuneRef{"X:" + std::to_string(options.m_index + 1)};
    while (!found)
    {
        std::string lineOwner;
        getline(src, lineOwner);
        
        if (src.eof() && lineOwner.empty())
        {
            break;
        }
        
        // remove any trailing \r
        if (!lineOwner.empty() && lineOwner.back() == '\r')
        {
            lineOwner.pop_back();
        }
        
        ++lineNo;
        const std::string_view line{lineOwner}; // string_view is much faster at substringing
        
        if (line.size() >= 2 && line[0] == '%' && line[1] == '%')
        {
            ParsePseudoComment(line, lineNo, part);
        }
        else
        {
            found = (line.substr(0, tuneRef.size()) == tuneRef);
        }
    }
    
    if (found)
    {
        LOGGER << lineNo << ": found tune " << tuneRef;
    }
    else
    {
        throw std::invalid_argument("Error: can't find tune " + tuneRef);
    }

    for (;;)
    {
        std::string lineOwner;
        getline(src, lineOwner);
        
        if (src.eof() && lineOwner.empty())
        {
            break;
        }
        
        // remove any trailing \r
        if (!lineOwner.empty() && lineOwner.back() == '\r')
        {
            lineOwner.pop_back();
        }
        
        ++lineNo;
        std::string_view line{lineOwner}; // string_view is much faster at substringing

        // Each tune starts with an "X:" (reference number) info field and is terminated by a blank line or the end of the file.
        if (line.empty())
        {
            break;
        }

        if (line.size() >= 2 && line[0] == '%' && line[1] == '%')
        {
            ParsePseudoComment(line, lineNo, part);
            continue;
        }

        // strip comments, both start of line and end of line comments
        const auto percent = line.find_first_of('%');
        if (percent != std::string::npos)
        {
            line.remove_suffix(line.size() - percent);
            if (line.empty())
            {
                continue;
            }
        }

        if (inHeader)
        {
            if (line.size() >= 2 && static_cast<bool>(isalpha(line[0])) && line[1] == ':')
            {
                ParseHeader(line, lineNo, piece, part);
                continue;
            }
            inHeader = false;
        }

        ParseMusic(line, lineNo, part, barIsClear, maxLedger);

        // end of system if line ends without a backslash
        if (line.back() != '\\' && barIsClear && part.m_bars.size() > 1)
        {
            Bar &prev = part.m_bars.at(part.m_bars.size() - 2);
            prev.m_eol = true;
        }
    }

    // deal with missing final bar line
    size_t idx{0};
    ParseBarLine("|]", lineNo, idx, part, barIsClear);

    // deal with not needed "next" bar
    if (barIsClear)
    {
        part.m_bars.pop_back();
    }

    // if there is a time signature in the header, but not in the first bar
    // then put it in the first bar
    if (m_timeSig.m_timeSymbol != TimeSyNone && !part.m_bars.empty() && part.m_bars.front().m_timeSig.m_timeSymbol == TimeSyNone)
    {
        part.m_bars.front().m_timeSig = m_timeSig;
    }
    
    part.SetTuning(options);
}

void ParserAbc::ParseHeader(std::string_view line, int lineNo, Piece &piece, Part& part)
{
    switch (line[0])
    {
    case 'T':   // title
        if (piece.m_title.empty())
        {
            piece.m_title = CharSet::TabToUtf8(line.substr(2));
            LOGGER << lineNo << ": title=" << piece.m_title;
        }
        else
        {
            piece.m_credits.emplace_back(CharSet::TabToUtf8(line.substr(2)));
        }
        break;
    case 'C':   // composer
        if (piece.m_composer.empty())
        {
            piece.m_composer = CharSet::TabToUtf8(line.substr(2));
            LOGGER << lineNo << ": composer=" << piece.m_composer;
        }
        else
        {
            piece.m_credits.emplace_back(CharSet::TabToUtf8(line.substr(2)));
        }
        break;
    case 'L':   // default note length
    {
        size_t idx{2};
        bool dotted{false};
        ParseRhythm(line, idx, NoteTypeWhole, 0, m_defaultNoteType, dotted);
        LOGGER << lineNo << ": default note type=" << m_defaultNoteType << " " << line;
        break;
    }
    case 'K':   // key
    {
        // "frenchtab" "french4tab" "french5tab" "spanishtab" "guitartab" "spanish5tab"
        // "banjo5tab" "spanish4tab", "banjo4tab" "ukuleletab" "italiantab" "italian7tab"
        // "italian8tab" "italian4tab" "italian5tab" "germantab" "neapoltab"
        static const std::regex re{R"(french[45]?tab|germantab|italian[4578]?tab|neapoltab|spanish[45]?tab)"};
        std::match_results<std::string_view::const_iterator> results;
        if (std::regex_search(line.cbegin(), line.cend(), results, re))
        {
            const std::string tabType{results.str(0)};
            if (tabType.substr(0, 6) == "french")
            {
                part.m_tabType = TabFrench;
            }
            else if (tabType == "germantab")
            {
                part.m_tabType = TabGerman;
            }
            else if (tabType.substr(0, 7) == "italian")
            {
                part.m_tabType = TabItalian;
            }
            else if (tabType == "neapoltab")
            {
                part.m_tabType = TabNeapolitan;
            }
            else if (tabType.substr(0, 7) == "spanish")
            {
                part.m_tabType = TabSpanish;
            }
            
            if (part.m_tabType == TabGerman && part.m_tabGermanSubtype == GstUnknown)
            {
                part.m_tabGermanSubtype = GstABC; // abctab's default
            }
        }
        else
        {
            throw std::runtime_error("Error: no tablature type specified " + std::string(line));
        }
        
        if (line.find('4') != std::string_view::npos)
        {
            part.m_staffLines = 4;
        }
        else if (line.find('5') != std::string_view::npos)
        {
            part.m_staffLines = 5;
        }
        break;
    }
    case 'A':
    case 'B':
    case 'D':
    case 'F':
    case 'G':
    case 'H':
    case 'N':
    case 'O':
    case 'S':
        // dump these into credits
        piece.m_credits.emplace_back(CharSet::TabToUtf8(line.substr(2)));
        break;
    case 'M':
        ParseTimeSignature(line, lineNo, 0, m_timeSig);
        break;
    default:
        LOGGER << lineNo << ": ignore header " << line;
        break;
    }
}

void ParserAbc::ParseMusic(std::string_view line, int lineNo, Part& part, bool &barIsClear, int& maxLedger)
{
    size_t idx{0}; // character pointer
    Fingering fingering{FingerNone}; // fingering for last note in chord
    bool fermata{false};
    bool nextHalved{false};
    bool nextDotted{false};
    bool nextTriplet{false};
    Connector nextSlur;
    nextSlur.m_function = FunSlur;
    nextSlur.m_curve = CurveUnder;
    nextSlur.m_compass = South;

    while (idx < line.size())
    {
        // skip whitespace
        if (line[idx] == ' ' || line[idx] == '\t')
        {
            ++idx;
            continue;
        }

        switch (line[idx])
        {
        case '|':
            ParseBarLine(line, lineNo, idx, part, barIsClear);
            break;
        case ':':
            // either a bar line or righthand fingering
            if (idx + 1 < line.size() && (line[idx + 1] == '|' || line[idx + 1] == ':'))
            {
                ParseBarLine(line, lineNo, idx, part, barIsClear);
            }
            else
            {
                fingering = FingerSecond;
                ++idx;
            }
            break;
        case '[':
            // either chord, or barline, or information field, or volta bracket
            if (idx + 1 < line.size() && line[idx + 1] == '|')
            {
                ParseBarLine(line, lineNo, idx, part, barIsClear);
            }
            else if (idx + 1 < line.size() && line[idx + 1] >= '1' && line[idx + 1] <= '9')
            {
                part.m_bars.back().m_volta = line[idx + 1] - '0';
                idx += 2;
            }
            else if (idx + 2 < line.size() && line[idx + 2] == ':')
            {
                ParseInformation(line, lineNo, idx, part);
            }
            else
            {
                ParseChord(line, lineNo, idx, part, fermata, fingering, nextHalved, nextDotted, nextTriplet, nextSlur, barIsClear, maxLedger);
            }
            break;
        case 'H':
            fermata = true;
            ++idx;
            break;
        case '.':
            fingering = FingerFirst;
            ++idx;
            break;
        case ';':
            fingering = FingerThird;
            ++idx;
            break;
        case '+':
            fingering = FingerThumb;
            ++idx;
            break;
        case 'T': // trill
        case 'S': // segno sign
        case 'O': // coda sign
            ++idx;
            break;
        case '!':
            ++idx;
            while (idx < line.size() && line[idx] != '!')
            {
                ++idx;
            }
            ++idx;
            break;
        case '\"':
            ++idx;
            while (idx < line.size() && line[idx] != '\"')
            {
                ++idx;
            }
            ++idx;
            break;
        case '\\': // stave continuation
            ++idx;
            break;
        case '>': // ">" means the previous note is dotted, the next note halved
            if (!part.m_bars.back().m_chords.empty())
            {
                part.m_bars.back().m_chords.back().m_dotted = true;
                part.m_bars.back().m_chords.back().m_noFlag = false;
                nextHalved = true;
            }
            ++idx;
            if (idx < line.size() && line[idx] == '>')
            {
                LOGGER << lineNo << ": \"" << line << "\" double dotting is not supported";
                ++idx;
            }
            break;
        case '<': // "<" means 'the previous note is halved, the next dotted'
            if (!part.m_bars.back().m_chords.empty())
            {
                part.m_bars.back().m_chords.back().m_noteType = static_cast<NoteType>(part.m_bars.back().m_chords.back().m_noteType + 1);
                part.m_bars.back().m_chords.back().m_noFlag = false;
                nextDotted = true;
            }
            ++idx;
            if (idx < line.size() && line[idx] == '<')
            {
                LOGGER << lineNo << ": \"" << line << "\" double dotting is not supported";
                ++idx;
            }
            break;
        case '(':
            // tuplet or slur
            ++idx;
            if (idx < line.size() && std::isdigit(line[idx]) != 0)
            {
                nextTriplet = (line[idx] == '3');
                ++idx; // ignore other tuplets
            }
            else
            {
                nextSlur.m_endPoint = EndPointStart;
            }
            break;
        default:
        {
            // Single note chord, no [ ... ]
            ParseChord(line, lineNo, idx, part, fermata, fingering, nextHalved, nextDotted, nextTriplet, nextSlur, barIsClear, maxLedger);
            break;
        }
        }
    }
}

void ParserAbc::ParseBarLine(std::string_view line, int lineNo, size_t &idx, Part &part, bool &barIsClear)
{
    Bar& bar{part.m_bars.back()};
    
    if (line.substr(idx, 3) == "[|]")
    {
        bar.m_barStyle = BarStyleRegular;
        idx += 3;
    }
    else if (line.substr(idx, 2) == "|]")
    {
        bar.m_barStyle = BarStyleLightHeavy;
        idx += 2;
    }
    else if (line.substr(idx, 2) == "||")
    {
        bar.m_barStyle = BarStyleLightLight;
        idx += 2;
    }
    else if (line.substr(idx, 2) == "[|")
    {
        bar.m_barStyle = BarStyleHeavyLight;
        idx += 2;
    }
    else if (line.substr(idx, 2) == ":|")
    {
        bar.m_barStyle = BarStyleRegular;
        bar.m_repeat = RepBackward;
        idx += 2;
    }
    else if (line.substr(idx, 2) == "|:")
    {
        bar.m_barStyle = BarStyleRegular;
        bar.m_repeat = RepForward;
        idx += 2;
    }
    else if (line.substr(idx, 2) == "::")
    {
        bar.m_barStyle = BarStyleRegular;
        bar.m_repeat = RepJanus;
        idx += 2;
    }
    else if (line[idx] == '|')
    {
        bar.m_barStyle = BarStyleRegular;
        ++idx;
    }
    else
    {
        bar.m_barStyle = BarStyleRegular;
        LOGGER << lineNo << ": \"" << line << "\" unknown barline";
        ++idx;
    }
    
    if (barIsClear)
    {
        if (part.m_bars.size() > 1)
        {
            // two adjacent bar lines, combine
            Bar &prev = part.m_bars.at(part.m_bars.size() - 2);
            if (prev.m_repeat == RepBackward && bar.m_repeat == RepForward)
            {
                prev.m_repeat = RepJanus;
            }
    
            if (!prev.m_eol)
            {
                if (prev.m_barStyle == BarStyleRegular && bar.m_barStyle == BarStyleRegular)
                {
                    prev.m_barStyle = BarStyleLightLight;
                }
                else if (prev.m_barStyle == BarStyleRegular && bar.m_barStyle == BarStyleHeavy)
                {
                    prev.m_barStyle = BarStyleLightHeavy;
                }
                else if (prev.m_barStyle == BarStyleHeavy && bar.m_barStyle == BarStyleRegular)
                {
                    prev.m_barStyle = BarStyleHeavyLight;
                }
                else if (prev.m_barStyle == BarStyleHeavy && bar.m_barStyle == BarStyleHeavy)
                {
                    prev.m_barStyle = BarStyleHeavyHeavy;
                }
            }
        }
        bar.m_barStyle = BarStyleRegular;
    }
    else
    {
        // next bar
        part.m_bars.emplace_back();
        barIsClear = true;
    }
    
    // Volta brackets.  When adjacent to bar lines, these can be shortened to "|1" and ":|2"
    if (idx < line.size() && line[idx] >= '1' && line[idx] <= '9')
    {
        part.m_bars.back().m_volta = line[idx] - '0';
        ++idx;
    }
}

void ParserAbc::ParseChord(std::string_view line, int lineNo, size_t &idx, Part& part, bool& fermata,
        Fingering& fingering, bool& nextHalved, bool& nextDotted, bool& nextTriplet, Connector& nextSlur,
        bool& barIsClear, int& maxLedger)
{
    const size_t startIdx{idx};
    
    Bar& bar{part.m_bars.back()};
    bar.m_chords.emplace_back(); // new chord
    Chord &chord = bar.m_chords.back();
    
    chord.m_triplet = nextTriplet;
    nextTriplet = false; // one shot
    
    bool gotRhythm{false};
    int string{1};
    static Chord previousChord;
    bool inChord{false};
    if (line[idx] == '[')
    {
        inChord = true;
        ++idx;
    }
    
    while (idx < line.size() && line[idx] != ']')
    {
        const size_t inChordIdx{idx};
        Ornament rightOrnament = OrnNone;
        Ornament leftOrnament = OrnNone;
        
        for (;;)
        {
            const auto initialIdx{idx};
            
            // unplayed strings
            while (idx < line.size() && line[idx] == ',')
            {
                ++idx;
                ++string;
            }
    
            if (idx >= line.size())
            {
                break;
            }
            
            // ornaments and slurs
            switch (line[idx])
            {
            case '(':
                nextSlur.m_endPoint = EndPointStart;
                ++idx;
                break;
            case '\'':
                rightOrnament = OrnCommaRaised;
                ++idx;
                break;
            case 'X':
                rightOrnament = OrnCross;
                ++idx;
                break;
            case '#':
                rightOrnament = OrnHash;
                ++idx;
                break;
            case '*':   // an '*' after the note.
                rightOrnament = OrnAsterisk;
                ++idx;
                break;
            case 'U':   // an U-shaped arc after the note
                rightOrnament = OrnSmiley;
                ++idx;
                break;
            case 'V':   // an U-shaped arc below the note
                leftOrnament = OrnSmiley;
                ++idx;
                break;
            case 'L':   // An oblique line under the note
                ++idx;
                break;
            default:
                break;
            }
            
            if (idx > initialIdx)
            {
                continue; // process , ornaments and slur
            }
            
            // ornaments and slurs should go against their note, but sometimes they go before the [
            if (!inChord && line[idx] == '[')
            {
                inChord = true;
                ++idx;
            }
            else
            {
                break;
            }
        }
        
        // fret
        if (idx < line.size() && (line[idx] == 'y' || (line[idx] >= 'a' && line[idx] <= 'w')))
        {
            chord.m_notes.emplace_back(); // allocate new note
            Note& note = chord.m_notes.back();
            
            if (nextSlur.m_endPoint == EndPointStart)
            {
                note.m_connector = nextSlur;
                ++nextSlur.m_number;
                nextSlur.m_endPoint = EndPointNone;
            }

            note.m_rightOrnament = rightOrnament;
            note.m_leftOrnament = leftOrnament;

            const int fret{(line[idx] == 'y')
                            ? Note::Anchor
                            : line[idx] - 'a' - (line[idx] <= 'i' ? 0 : 1)}; // fret i = j
            
            if (fret == 0 && string == Course7 && maxLedger == Course6)
            {
                // if this is the first a7 line we've seen then any previous 7
                // will have been misinterpreted as count-courses when should have been count-ledgers
                FixLedgers(part, Course7, Course7);
                maxLedger = Course7;
            }
            
            note.m_course = string;
            note.m_fret = fret;

            ++idx;
            ++string;
        }
        else if (idx < line.size() && line[idx] == '{')
        {
            // diapason
            chord.m_notes.emplace_back(); // allocate new note
            Note& note = chord.m_notes.back();
            
            if (nextSlur.m_endPoint == EndPointStart)
            {
                note.m_connector = nextSlur;
                ++nextSlur.m_number;
                nextSlur.m_endPoint = EndPointNone;
            }

            note.m_rightOrnament = rightOrnament;
            note.m_leftOrnament = leftOrnament;

            ++idx; // {
            if (static_cast<bool>(isdigit(line[idx])))
            {
                try
                {
                    note.m_fret = 0;
                    
                    size_t len{0};
                    note.m_course = std::stoi(std::string(line.substr(idx)), &len); // count-courses
                    if (note.m_course <= maxLedger)
                    {
                        note.m_course += Course7; // count-ledgers
                    }
                    idx += len;
                }
                catch (...)
                {
                    LOGGER << lineNo << ": error bad diapason";
                    ++idx;
                }
            }
            else
            {
                int course{Course7};
                while (idx < line.size() && line[idx] == ',')
                {
                    ++course;
                    ++idx;
                }
                const int fret{line[idx] - 'a' - (line[idx] <= 'i' ? 0 : 1)}; // fret i = j
                ++idx;
                
                if (fret == 0)
                {
                    if (course == Course7 && maxLedger == Course6)
                    {
                        // if this is the first a7 line we've seen then any previous 7
                        // will have been misinterpreted as count-courses when should have been count-ledgers
                        FixLedgers(part, Course7, Course7);
                        maxLedger = Course7;
                    }
                    else if (course > Course7 && maxLedger <= Course7)
                    {
                        // If this is the first ledger line we've seen then any
                        // previous 7 8 9 10 ... will have been misinterpreted
                        // as count-courses when should have been count-ledgers.
                        // Promote to 14 15 ...  Except if 7 has already been promoted
                        // (maxLedger == Course7) only promote from 8. 
                        FixLedgers(part, maxLedger + 1, Course10);
                        maxLedger = Course10; // ledger lines are used so 7 8 9 10, if used, must be count-courses
                    }
                }
                
                note.m_course = course;
                note.m_fret = fret;
            }
            
            if (idx < line.size() && line[idx] == '}')
            {
                ++idx; // }
            }
            else
            {
                LOGGER << lineNo << ": error missing }";
                break;
            }
        }
        else if (idx < line.size() && line[idx] == 'z')
        {
            // rest
            ++idx; // z
        }
        
        if (idx < line.size() && line[idx] == ')')
        {
            nextSlur.m_endPoint = EndPointStop;
            ++idx;
        }

        // rhythm?
        if (idx < line.size() && ((static_cast<bool>(isdigit(line[idx])) || line[idx] == '/')))
        {
            ParseRhythm(line, idx, m_defaultNoteType, m_haveAddFlags ? m_addFlags : m_defaultAddFlags, chord.m_noteType, chord.m_dotted);
            gotRhythm = true;
        }
        
        if (idx < line.size() && line[idx] == ')')
        {
            nextSlur.m_endPoint = EndPointStop;
            ++idx;
        }

        if (nextSlur.m_endPoint == EndPointStop && !chord.m_notes.empty())
        {
            Note& note = chord.m_notes.back();
            --nextSlur.m_number;
            note.m_connector = nextSlur;
            nextSlur.m_endPoint = EndPointNone;
        }
        
        if (!inChord || gotRhythm || inChordIdx == idx)
        {
            break;
        }
    }
    
    if (inChord)
    {
        if (line[idx] == ']')
        {
            ++idx;
            
            // slur end ater closing ] of chord
            if (idx < line.size() && line[idx] == ')' && !chord.m_notes.empty())
            {
                ++idx;
                nextSlur.m_endPoint = EndPointStop;
                Note& note = chord.m_notes.back();
                --nextSlur.m_number;
                note.m_connector = nextSlur;
                nextSlur.m_endPoint = EndPointNone;
            }
        }
        else
        {
            LOGGER << lineNo << ": error ] missing";
        }
    }
    
    if (idx > startIdx)
    {
        // fermata applies to chord
        chord.m_fermata = fermata;
        fermata = false;
    
        // fingering applies to last note in the chord
        if (!chord.m_notes.empty())
        {
            chord.m_notes.back().m_rightFingering = fingering;
        }
        fingering = FingerNone;
        
        if (!gotRhythm)
        {
            chord.m_noteType = previousChord.m_noteType;
            chord.m_dotted = previousChord.m_dotted;
            chord.m_noFlag = true;
        }
        
        // >
        if (nextHalved)
        {
            chord.m_noteType = static_cast<NoteType>(chord.m_noteType + 1);
            bar.m_chords.back().m_noFlag = false;
            nextHalved = false;
        }
        
        // <
        if (nextDotted)
        {
            chord.m_dotted = true;
            bar.m_chords.back().m_noFlag = false;
            nextDotted = false;
        }
        previousChord = chord;
        barIsClear = false;
    }
    else
    {
        // nothing here
        bar.m_chords.pop_back();
        
        LOGGER << lineNo << ": error unknown character \"" << line[idx] << "\" ignored. " << line;
        ++idx;
    }
}

void ParserAbc::FixLedgers(Part& part, int lwb, int upb)
{
    for (Bar& b : part.m_bars)
    {
        for (Chord& c : b.m_chords)
        {
            for (Note& n : c.m_notes)
            {
                if (n.m_fret == 0 && n.m_course >= lwb && n.m_course <= upb)
                {
                    n.m_course += 7;
                }
            }
        }
    }
}

void ParserAbc::ParseRhythm(std::string_view line, size_t &idx, NoteType baseNoteType, int addFlags, NoteType& noteType, bool& dotted)
{
    int num{1};
    int den{1};

    if (static_cast<bool>(isdigit(line[idx])))
    {
        num = line[idx] - '0';
        ++idx;
        if (idx < line.size() && static_cast<bool>(isdigit(line[idx])))
        {
            num = num * 10 + line[idx] - '0';
            ++idx;
        }
    }
    
    if (idx < line.size() && line[idx] == '/')
    {
        ++idx;
        if (idx < line.size() && static_cast<bool>(isdigit(line[idx])))
        {
            den = line[idx] - '0';
            ++idx;
            if (idx < line.size() && static_cast<bool>(isdigit(line[idx])))
            {
                den = den * 10 + line[idx] - '0';
                ++idx;
            }
        }
        else
        {
            den = 2;
            while (idx < line.size() && line[idx] == '/')
            {
                den <<= 1;
                ++idx;
            }
        }
    }

    dotted = (num % 3) == 0;
    if (dotted)
    {
        num *= 2;
        den *= 3;
    }

    int nt{baseNoteType};
    while (num > 1)
    {
        --nt;
        num >>= 1;
    }
    while (den > 1)
    {
        ++nt;
        den >>= 1;
    }

    noteType = static_cast<NoteType>(nt + addFlags);
}


void ParserAbc::ParseInformation(std::string_view line, int lineNo, size_t &idx, Part& part)
{
    Bar& bar{part.m_bars.back()};
    
    // [M:
    ++idx;
    if (line[idx] == 'M')
    {
        ParseTimeSignature(line, lineNo, idx, bar.m_timeSig);
    }
    else
    {
        LOGGER << lineNo << ": ignore body information " << line[1];
    }

    while (idx < line.size() && line[idx] != ']')
    {
        ++idx;
    }
    
    if (idx < line.size() && line[idx] == ']')
    {
        ++idx;
    }
    else
    {
        LOGGER << lineNo << ": error missing ]";
    }
}

void ParserAbc::ParseTimeSignature(std::string_view line, int lineNo, size_t idx, TimeSig& timeSig)
{
    if (line.substr(idx, 6) == "M:none")
    {
        // M:none
        timeSig.m_timeSymbol = TimeSyNone;
        timeSig.m_beats = 0;
        timeSig.m_beatType = 0;
    }
    else if (line.substr(idx, 4) == "M:C|")
    {
        // M:C|
        timeSig.m_timeSymbol = TimeSyCut;
        timeSig.m_beats = 2;
        timeSig.m_beatType = 2;
    }
    else if (line.substr(idx, 3) == "M:C")
    {
        // M:C
        timeSig.m_timeSymbol = TimeSyCommon;
        timeSig.m_beats = 4;
        timeSig.m_beatType = 4;
    }
    else if (idx + 4 < line.size() && line[idx + 2] >= '1' && line[idx + 2] <= '9' && line[idx + 3] == '/' && line[idx + 4] >= '1' && line[idx + 4] <= '9')
    {
        // M:3/4
        timeSig.m_timeSymbol = TimeSyNormal;
        timeSig.m_beats = line[idx + 2] - '0';
        timeSig.m_beatType = line[idx + 4] - '0';
    }
    else if (idx + 2 < line.size() && line[idx + 2] >= '1' && line[idx + 2] <= '9')
    {
        // M:3
        timeSig.m_timeSymbol = TimeSySingleNumber;
        timeSig.m_beats = line[idx + 2] - '0';
        timeSig.m_beatType = 4;
    }
    else
    {
        LOGGER << lineNo << ": unknown time signature. " << line;
    }
}

void ParserAbc::ParsePseudoComment(std::string_view line, int lineNo, Part& part)
{
    {
        static const std::regex re{R"(^%%tabbrummer\s+(\w+))"};
        std::match_results<std::string_view::const_iterator> results;
        if (std::regex_search(line.cbegin(), line.cend(), results, re))
        {
            const std::string param{results.str(1)};
            if (param == "ABC")
            {
                part.m_tabGermanSubtype = GstABC;
            }
            else if (param == "1AB")
            {
                part.m_tabGermanSubtype = Gst1AB;
            }
            else if (param == "123")
            {
                part.m_tabGermanSubtype = Gst123;
            }
            LOGGER << lineNo << ": tabGermanSubtype=" << part.m_tabGermanSubtype << " " << line;
            return;
        }
    }
    
    {
        static const std::regex re{R"(^%%tabrhstyle\s+(\w*))"};
        std::match_results<std::string_view::const_iterator> results;
        if (std::regex_search(line.cbegin(), line.cend(), results, re))
        {
            const std::string param{results.str(1)};
            if (param == "simple" || param == "grid")
            {
                m_defaultAddFlags = 2;
            }
            else
            {
                m_defaultAddFlags = 0;
            }
            LOGGER << lineNo << ": defaultAddFlags=" << m_defaultAddFlags << " " << line;
            return;
        }
    }
    
    {
        static const std::regex re{R"(^%%tabaddflags\s+([+-]?\d+))"};
        std::match_results<std::string_view::const_iterator> results;
        if (std::regex_search(line.cbegin(), line.cend(), results, re))
        {
            const std::string param{results.str(1)};
            try
            {
                m_addFlags = std::stoi(param);
                m_haveAddFlags = true;
            }
            catch (...)
            {
                LOGGER << lineNo << ": addFlags failed: " << line;
            }
            LOGGER << lineNo << ": addFlags=" << m_addFlags << " " << line;
            return;
        }
    }

    LOGGER << lineNo << ": ignore pseudo comment: " << line;
}

} // namespace luteconv
