#ifndef _MEI_H_
#define _MEI_H_

namespace luteconv
{

class Mei
{
public:

    /**
     * Strings for enum BarStyle
     */
    static const char* const barStyle[];
    
    /**
     * Strings for fingering, enum Fingering
     */
    static const char* const fingering[];
    
    /**
     * Strings for notationType, enum TabType
     */
    static const char* const notationtype[];
    
    /**
     * strings for ornaments, enum Ornament
     */
    static const char* const ornament[];
};

} // namespace luteconv

#endif // _MEI_H_
