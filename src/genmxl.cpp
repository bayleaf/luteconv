#include "genmxl.h"

#include <sstream>
#include <cstdio>

#include <miniz.h>

#include "genmusicxml.h"
#include "logger.h"
#include "platform.h"

namespace luteconv
{

void GenMxl::Generate(const Options& options, const Piece& piece, std::ostream& dst)
{
    LOGGER << "Generate mxl";
    
    // The archive's contents must exist until after the archive is closed
    std::string mimetype;
    std::string musicxmlFilename;
    std::string container;
    std::string musicxml;

    // generate MusicXML image
    {
        std::ostringstream ss;
        GenMusicXml genMusicXml;
        genMusicXml.Generate(options, piece, ss);
        musicxml = ss.str();
    }
    
    // create zip archive
    mz_zip_archive zipArchive{};
    
    // miniz can't cope with stdout if it isn't a file, so generate zip on the heap
    if (mz_zip_writer_init_heap(&zipArchive, 0, 0) == 0)
    {
        throw std::runtime_error("Error: failed mz_zip_writer_init_heap: " + std::to_string(zipArchive.m_last_error));
    }
    
    // file: mimetype
    mimetype = "application/vnd.recordare.musicxml";
    if (mz_zip_writer_add_mem(&zipArchive, "mimetype", mimetype.c_str(), mimetype.size(), MZ_DEFAULT_COMPRESSION) == 0)
    {
        throw std::runtime_error("Error: failed mz_zip_writer_add_mem adding file mimetype: " + std::to_string(zipArchive.m_last_error));
    }

    // construct inner filename from archive name bar/foo.mxl -> foo.xml
    musicxmlFilename = options.m_dstFilename;
    const size_t slash = musicxmlFilename.find_last_of(pathSeparator);
    if (slash != std::string::npos)
    {
        musicxmlFilename = musicxmlFilename.substr(slash + 1);
    }

    const size_t dot = musicxmlFilename.find_last_of('.');
    if (dot != std::string::npos)
    {
        musicxmlFilename = musicxmlFilename.substr(0, dot);
    }

    musicxmlFilename = musicxmlFilename + ".musicxml";
        
    container =
            R"(<?xml version="1.0" encoding="UTF-8"?>)" "\n"
            R"(<container>)" "\n"
            R"(  <rootfiles>)" "\n"
            R"(    <rootfile full-path=")" + musicxmlFilename + R"(")" "\n"
            R"(              media-type="application/vnd.recordare.musicxml+xml"/>)" "\n"
            R"(  </rootfiles>)" "\n"
            R"(</container>)" "\n";
    
    if (mz_zip_writer_add_mem(&zipArchive, "META-INF/container.xml", container.c_str(), container.size(), MZ_DEFAULT_COMPRESSION) == 0)
    {
        throw std::runtime_error("Error: failed mz_zip_writer_add_mem adding file container.xml: " + std::to_string(zipArchive.m_last_error));
    }

    // MusicXML
    if (mz_zip_writer_add_mem(&zipArchive, musicxmlFilename.c_str(), musicxml.c_str(), musicxml.size(), MZ_DEFAULT_COMPRESSION) == 0)
    {
        throw std::runtime_error("Error: failed mz_zip_writer_add_mem adding file " + musicxmlFilename + ": "  + std::to_string(zipArchive.m_last_error) );
    }

    void* buf{nullptr};
    size_t bufSize{0};
    if (mz_zip_writer_finalize_heap_archive(&zipArchive, &buf, &bufSize) == 0)
    {
        throw std::runtime_error("Error: failed mz_zip_writer_finalize_heap_archive: " + std::to_string(zipArchive.m_last_error));
    }
    
    // copy to destination
    dst.write(reinterpret_cast<const char*>(buf), static_cast<std::streamsize>(bufSize));
    
    if (mz_zip_writer_end(&zipArchive) == 0)
    {
        throw std::runtime_error("Error: failed mz_zip_writer_end: " + std::to_string(zipArchive.m_last_error));
    }
}

} // namespace luteconv
