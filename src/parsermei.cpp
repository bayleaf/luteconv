#include "parsermei.h"

#include <algorithm>
#include <iostream>
#include <sstream>
#include <stdexcept>
#include <string>

#include "logger.h"
#include "mei.h"
#include "pitch.h"

namespace luteconv
{

using namespace pugi;

void ParserMei::Parse(std::istream& srcFile, const Options& options, Piece& piece)
{
    LOGGER << "Parse mei";
    
    xml_document doc;
    const xml_parse_result result{doc.load(srcFile)};

    if (!result)
    {
        std::ostringstream ss;
        ss << "Error: XML parsed with errors."
                    << " Description: " << result.description() 
                    << " Offset: " << result.offset;
        throw std::runtime_error(ss.str());
    }
    
    if (options.m_index != 0)
    {
        throw std::invalid_argument("Error: Can't find section, index=" + std::to_string(options.m_index));
    }

    // TODO should be per part
    m_slurNumber = 1;
    m_holdNumber = 1;
    
    const xml_node xmlmei{doc.child("mei")};
    if (!xmlmei)
    {
        throw std::runtime_error("Error: Can't find <mei>");
    }
    
    const std::string meiversion{xmlmei.attribute("meiversion").value()};
    if (!meiversion.empty())
    {
        LOGGER << "meiversion=" << meiversion;
    }
    
    xml_node xmlcomposer;
    xml_node xmlnotesStmt;
    if (meiversion.substr(0, 1) == "3")
    {
        const xml_node xmlwork{xmlmei.child("meiHead").child("workDesc").child("work")};
        piece.m_title = xmlwork.child("titleStmt").child("title").text().as_string();
        xmlcomposer = xmlwork.child("titleStmt").child("composer");
        xmlnotesStmt = xmlwork.child("notesStmt");
    }
    else
    {
        const xml_node xmlwork{xmlmei.child("meiHead").child("workList").child("work")};
        piece.m_title = xmlwork.child("title").text().as_string();
        xmlcomposer = xmlwork.child("composer");
        xmlnotesStmt = xmlwork.child("notesStmt");
    }
    
    piece.m_composer = xmlcomposer.text().as_string();
    if (piece.m_composer.empty())
    {
        piece.m_composer = xmlcomposer.child("name").text().as_string();
    }
    if (piece.m_composer.empty())
    {
        piece.m_composer = xmlcomposer.child("name").child("persName").text().as_string();
    }
    
    LOGGER << "title=" << piece.m_title;
    LOGGER << "composer=" << piece.m_composer;
    
    // <notesStmt><annot> map to credits
    for (const xml_node xmlannot : xmlnotesStmt.children("annot"))
    {
        piece.m_credits.emplace_back(xmlannot.text().as_string());
    }
    
    const xml_node xmlscore{xmlmei.child("music").child("body").child("mdiv").child("score")};
    if (!xmlscore)
    {
        throw std::runtime_error("Error: Can't find <score>");
    }
    const xml_node xmlscoreDef{xmlscore.child("scoreDef")};
    
    // each staff corresponds to a part
    std::vector<Clef> clefs;
    std::vector<Key> keys;
    std::vector<TimeSig> timeSigs;
    size_t partIdx{0};
    for (const xml_node xmlstaffDef : xmlscoreDef.child("staffGrp").children("staffDef"))
    {
        clefs.emplace_back(ClefNone);
        keys.emplace_back(KeyNone);
        timeSigs.emplace_back();
        piece.m_parts.resize(partIdx + 1);
        
        Clef& clef{clefs.back()};
        Key& key{keys.back()};
        TimeSig& timeSig{timeSigs.back()};
        Part& part{piece.m_parts.back()};
        
        part.m_partName = xmlstaffDef.child("label").text().as_string();

        clef = ParseClef(xmlstaffDef);
        if (clef == ClefNone)
        {
            clef = ParseClef(xmlscoreDef);
        }
        if (clef == ClefTab || clef == ClefNone)
        {
            const std::string notationtype{xmlstaffDef.attribute("notationtype").value()};
            if (notationtype == Mei::notationtype[TabFrench])
            {
                part.m_tabType = TabFrench;
            }
            else if (notationtype == Mei::notationtype[TabGerman])
            {
                part.m_tabType = TabGerman;
            }
            else if (notationtype == Mei::notationtype[TabItalian])
            {
                part.m_tabType = TabItalian;
            }
            else if (notationtype == Mei::notationtype[TabSpanish])
            {
                part.m_tabType = TabSpanish;
            }
            
            const xml_node xmltuning{xmlstaffDef.child("tuning")};
            if (xmltuning != nullptr)
            {
                ParseTuning(xmltuning, part);
                if (part.m_tabType != TabGerman)
                {
                    // Not German tablature, as this would reflect the number of notes allowed for a chord
                    part.m_staffLines = xmlstaffDef.attribute("lines").as_int(Course6);
                }
            }
        }
        else
        {
            part.m_tabType = TabCmn;
            part.m_staffLines = xmlstaffDef.attribute("lines").as_int(5);
            key = ParseKey(xmlstaffDef);
            if (key == KeyNone)
            {
                key = ParseKey(xmlscoreDef);
            }
        }
        
        timeSig = ParseMeterSig(xmlstaffDef);
        if (timeSig.m_timeSymbol == TimeSyNone)
        {
            timeSig = ParseMeterSig(xmlscoreDef);
        }
        
        ++partIdx;
    }
    
    const xml_node xmlsection{xmlscore.child("section")};
    if (xmlsection == nullptr)
    {
        throw std::runtime_error("Error: Can't find <section>");
    }

    for (const xml_node xmlchild : xmlsection.children())
    {
        const std::string childName{xmlchild.name()};
        if (childName == "measure")
        {
            const xml_node xmlmeasure{xmlchild};
            ParseMeasure(xmlmeasure, piece, clefs, keys, timeSigs, 0);
        }
        else if (childName == "ending")
        {
            const xml_node xmlending{xmlchild};
            const int volta{xmlending.attribute("n").as_int()};
            for (const xml_node xmlchild : xmlending.children())
            {
                const std::string childName{xmlchild.name()};
                if (childName == "measure")
                {
                    const xml_node xmlmeasure{xmlchild};
                    ParseMeasure(xmlmeasure, piece, clefs, keys, timeSigs, volta);
                }
            }
        }
    }
    
    for (auto& part : piece.m_parts)
    {
        if (part.m_tabType != TabCmn)
        {
            part.SetTuning(options);
        }
    }
}

void ParserMei::ParseMeasure(const xml_node xmlmeasure, Piece& piece, std::vector<Clef>& clefs, std::vector<Key>& keys, std::vector<TimeSig>& timeSigs, int volta)
{
    const int measureNo{xmlmeasure.attribute("n").as_int()};
    
    size_t partIdx{0};
    for (const xml_node xmlstaff : xmlmeasure.children("staff"))
    {
        if (partIdx == piece.m_parts.size())
        {
            LOGGER << "Too many <staff>s in measure " << measureNo;
            break;
        }
        
        Part& part{piece.m_parts.at(partIdx)};
        
        part.m_bars.emplace_back();
        Bar& bar{part.m_bars.back()};
        
        bar.m_volta = volta;
        
        // initial clef, key, timesig
        if (measureNo == 1)
        {
            if (part.m_tabType == TabCmn)
            {
                bar.m_clef = clefs.at(partIdx);
                bar.m_key = keys.at(partIdx);
            }
            bar.m_timeSig = timeSigs.at(partIdx);
        }
    
        // repeats
        const std::string xmlright{xmlmeasure.attribute("right").value()};
        if (xmlright == "rptstart")
        {
            bar.m_repeat = RepForward;
        }
        else if (xmlright == "rptend")
        {
            bar.m_repeat = RepBackward;
        }
        else if (xmlright == "rptboth")
        {
            bar.m_repeat = RepJanus;
        }
        
        //  bar style
        if (!xmlright.empty())
        {
            for (int i{0}; Mei::barStyle[i] != nullptr; ++i)
            {
                if (xmlright == Mei::barStyle[i])
                {
                    bar.m_barStyle = static_cast<BarStyle>(i);
                    break;
                }
            }
        }
        
        // fermata over bar line
        for (const xml_node xmlfermata : xmlmeasure.children("fermata"))
        {
            // assume any fermata using @tstamp is for the bar line, and not a tabGrp
            if (!std::string(xmlfermata.attribute("tstamp").value()).empty())
            {
                bar.m_fermata = true;
                break;
            }
        }
    
        int numVoices{0};
        for (const xml_node xmllayer : xmlstaff.children("layer"))
        {
            if (part.m_tabType == TabCmn)
            {
                const Clef clef{ParseClef(xmllayer)};
                if (clef != ClefNone)
                {
                    bar.m_clef = clef;
                }
                const Key key{ParseKey(xmllayer)};
                if (key != KeyNone)
                {
                    bar.m_key = key;
                }
            }
            
            const TimeSig timeSig{ParseMeterSig(xmllayer)};
            if (timeSig.m_timeSymbol != TimeSyNone)
            {
                bar.m_timeSig = timeSig;
            }
            
            ++numVoices;
            int stamp{0}; // chord timestamp from start of the bar in divs
            ParseChordList(xmlmeasure, xmllayer, GridNone, 0, part, bar, stamp);
        }
        
        if (numVoices > 1)
        {
            bar.MergeVoices();
        }
        
        if (numVoices == 0)
        {
            LOGGER << "measure " << measureNo << ": can't find <layer>";
        }
        
        ++partIdx;
    }
}

void ParserMei::ParseChordList(xml_node xmlmeasure, xml_node xmlparent, Grid grid, int tripletCount, Part& part, Bar& bar, int& stamp)
{
    // list of tabGrps/chords/notes
    for (const xml_node xmlchild : xmlparent.children())
    {
        const std::string childName{xmlchild.name()};
        if (childName == "tabGrp" || childName == "chord" || childName == "note" || childName == "rest")
        {
            ParseChord(xmlmeasure, xmlchild, grid, tripletCount, part, bar, stamp);
            if (grid == GridStart)
            {
                grid = GridMid;
            }
            if (tripletCount > 0)
            {
                --tripletCount;
            }
        }
        else if (childName == "space")
        {
            ParseSpace(xmlchild, stamp, part);
        }
        else if (childName == "beam")
        {
            ParseChordList(xmlmeasure, xmlchild, GridStart, tripletCount, part, bar, stamp);
            if (!bar.m_chords.empty())
            {
                bar.m_chords.back().m_grid = GridEnd;
            }
        }
        else if (childName == "tuplet")
        {
            const bool triplet{xmlchild.attribute("num").as_int() == 3 && xmlchild.attribute("numbase").as_int() == 2};
            ParseChordList(xmlmeasure, xmlchild, grid, triplet ? 3 : 0, part, bar, stamp);
        }
        else if (childName == "choice" || childName == "corr")
        {
            ParseChordList(xmlmeasure, xmlchild, grid, tripletCount, part, bar, stamp);
        }
        else if (childName == "sic")
        {
            // ignore original uncorrected version
        }
        else
        {
            LOGGER << "ParseChordList element " << childName << " ignored";
        }
    }
}

void ParserMei::ParseChord(xml_node xmlmeasure, xml_node xmlelement, Grid grid, int tripletCount, Part& part, Bar& bar, int& stamp)
{
    // tabGrp/chord/(single)note/(single)rest
    bar.m_chords.emplace_back();
    Chord& chord{bar.m_chords.back()};
    
    chord.m_noteType = ParseDur(xmlelement, part);
    chord.m_dotted = xmlelement.attribute("dots").as_int() == 1;
    chord.m_stamp = stamp;
    stamp += Bar::Duration(chord.m_noteType, chord.m_dotted, tripletCount > 0);
    chord.m_grid = grid;
    chord.m_triplet = (tripletCount == 3);

    const std::string xmlid{xmlelement.attribute("xml:id").value()};
    
    if (!xmlid.empty())
    {
        // fermata over tabGrp/chord/note
        ParseFermata(xmlmeasure, xmlid, chord);
    }
    
    ParseVerse(xmlelement, chord);
    
    const std::string elementName{xmlelement.name()};
    if (elementName == "note")
    {
        ParseNote(xmlmeasure, xmlelement, part, chord);
    }
    else if (elementName == "rest")
    {
        
    }
    else
    {
        ParseNoteList(xmlmeasure, xmlelement, part, chord);
    }
}

void ParserMei::ParseSpace(xml_node xmlspace, int& stamp, Part& part)
{
    stamp += Bar::Duration(ParseDur(xmlspace, part), xmlspace.attribute("dots").as_int() == 1, false);
}

NoteType ParserMei::ParseDur(xml_node xmlparent, Part& part)
{
    int noteType{0};
    std::string durString{xmlparent.attribute("dur").value()};
    if (durString.empty())
    {
        durString = xmlparent.attribute("dur.ges").value();
    }
    if (durString.empty())
    {
        LOGGER << "ParseDur missing @dur or @dur.ges, substitute 4";
        durString = "4";
    }
    
    if (durString == "long")
    {
        noteType = NoteTypeLong;
    }
    else if (durString == "breve")
    {
        noteType = NoteTypeBreve;
    }
    else
    {
        int dur{std::stoi(durString)};
        noteType = NoteTypeWhole;
        while (dur > 1)
        {
            ++noteType;
            dur >>= 1;
        }
    }
    
    // dur is 1 whole, 2 half, 4 quarter, 8 eighth ... 
    // But quarter note is 1 flag, reduce to 0
    if (part.m_tabType != TabCmn)
    {
        ++noteType;
    }
    
    noteType = std::min(static_cast<int>(NoteType256th), noteType);

    return static_cast<NoteType>(noteType);
}

void ParserMei::ParseNoteList(xml_node xmlmeasure, xml_node xmlparent, Part& part, Chord& chord)
{
    chord.m_noFlag = true;
    for (const xml_node xmlchild : xmlparent.children())
    {
        const std::string childName{xmlchild.name()};
        if (childName == "note")
        {
            ParseNote(xmlmeasure, xmlchild, part, chord);
        }
        else if (childName == "rest")
        {

        }
        else if (childName == "choice" || childName == "corr")
        {
            ParseNoteList(xmlmeasure, xmlchild, part, chord);
        }
        else if (childName == "sic")
        {
            // ignore original uncorrected version
        }
        else if (childName == "tabDurSym")
        {
            // TODO tabDurSym
            chord.m_noFlag = false;
        }
        else
        {
            LOGGER << "ParseNoteList element " << childName << " ignored";
        }
    }
}

void ParserMei::ParseNote(xml_node xmlmeasure, xml_node xmlnote, Part& part, Chord& chord)
{
    chord.m_notes.emplace_back();
    Note& note{chord.m_notes.back()};
    
    if (part.m_tabType == TabCmn)
    {
        const std::string pname{xmlnote.attribute("pname").value()};
        if (!pname.empty())
        {                   
            const std::string accid{xmlnote.attribute("accid").value()};
            if (accid == "f")
            {
                note.m_accidental = AccFlat;
            }
            else if (accid == "n")
            {
                note.m_accidental = AccNatural;
            }
            else if (accid == "s")
            {
                note.m_accidental = AccSharp;
            }
            
            int alter{0};
            if (note.m_accidental != AccNone)
            {
                static_assert(AccSharp == 1);
                static_assert(AccNatural == 0);
                static_assert(AccFlat == -1);
                alter = static_cast<int>(note.m_accidental);
            }
            else
            {
                const std::string accidGes{xmlnote.attribute("accid.ges").value()};
                alter = accidGes == "s" ? 1 : accidGes == "f" ? -1 : 0;
            }
            const int octave{xmlnote.attribute("oct").as_int()};
            note.m_pitch = Pitch(static_cast<char>(std::toupper(pname[0])), alter, octave);

        }
        
        const std::string tie{xmlnote.attribute("tie").value()};
        chord.m_tie = (tie == "i");
    }
    else
    {
        // string & fret
        note.m_course = xmlnote.attribute("tab.course").as_int();
        note.m_fret = xmlnote.attribute("tab.fret").as_int();
        const std::string xmlid{xmlnote.attribute("xml:id").value()};
        
        if (!xmlid.empty())
        {
            ParseFingOrnamConnector(xmlmeasure, xmlid, note);
        }
    }
}

void ParserMei::ParseFermata(xml_node xmlmeasure, std::string_view xmlid, Chord& chord)
{
    const std::string xmlidRef{"#" + std::string(xmlid)};

    for (const xml_node xmlchild : xmlmeasure.children())
    {
        const std::string childName{xmlchild.name()};
        if (childName == "fermata")
        {
            if (xmlchild.attribute("startid").value() == xmlidRef)
            {
                chord.m_fermata = true;
                break;
            }
        }
        else if (childName == "choice" || childName == "corr")
        {
            ParseFermata(xmlchild, xmlid, chord);
        }
        else if (childName == "sic")
        {
            // ignore original uncorrected version
        }
    }
}

void ParserMei::ParseFingOrnamConnector(xml_node xmlmeasure, std::string_view xmlid, Note& note)
{
    const std::string xmlidRef{"#" + std::string(xmlid)};

    // TODO fingering may apply to a range of ids
    // fingering <fing type='right 1' startid='#m3.n8'/>
    // Why is <fing> not a child of <note>?
    for (const xml_node xmlchild : xmlmeasure.children())
    {
        const std::string childName{xmlchild.name()};
        if (childName == "fing")
        {
            if (xmlchild.attribute("startid").value() == xmlidRef)
            {
                const std::string type{xmlchild.attribute("type").value()};
                
                const size_t sep{type.find(' ')};
                if (sep != std::string::npos)
                {
                    const std::string playingHand{type.substr(0, sep)};
                    const std::string playingFinger{type.substr(sep + 1)};
                    
                    Fingering finger{FingerNone};
                    for (int i{1}; Mei::fingering[i] != nullptr; ++i)
                    {
                        if (playingFinger == Mei::fingering[i])
                        {
                            finger = static_cast<Fingering>(i);
                            break;
                        }
                    }
                    
                    if (finger != FingerNone)
                    {
                        if (playingHand == "left")
                        {
                            note.m_leftFingering = finger;
                        }
                        else if (playingHand == "right")
                        {
                            note.m_rightFingering = finger;
                        }
                        else
                        {
                            LOGGER << "Unknown playingHand: " << playingHand;
                        }
                    }
                    else
                    {
                        LOGGER << "Unknown playingFinger: " << playingFinger;
                    }
                }
            }
        }
        else if (childName == "ornam")
        {
            if (xmlchild.attribute("startid").value() == xmlidRef)
            {
                const std::string ornamentText{xmlchild.text().as_string()};
    
                if (!ornamentText.empty())
                {
                    for (int i{1}; Mei::ornament[i] != nullptr; ++i)
                    {
                        if (ornamentText == Mei::ornament[i])
                        {
                            if (xmlchild.attribute("ho").as_int() > 0)
                            {
                                note.m_rightOrnament = static_cast<Ornament>(i);
                            }
                            else
                            {
                                note.m_leftOrnament = static_cast<Ornament>(i);
                            }
                        }
                    }
                }
            }
        }
        else if (childName == "slur" && xmlchild.attribute("startid").value() == xmlidRef)
        {
            note.m_connector.m_endPoint = EndPointStart;
            note.m_connector.m_number = m_slurNumber++;
            note.m_connector.m_function = FunSlur;
            if (xmlchild.attribute("startid").value() == std::string("above"))
            {
                note.m_connector.m_curve = CurveOver;
                note.m_connector.m_compass = North;
            }
            else
            {
                note.m_connector.m_curve = CurveUnder;
                note.m_connector.m_compass = South;
            }
        }
        else if (childName == "slur" && xmlchild.attribute("endid").value() == xmlidRef)
        {
            note.m_connector.m_endPoint = EndPointStop;
            note.m_connector.m_number = --m_slurNumber;
            note.m_connector.m_function = FunSlur;
            if (xmlchild.attribute("startid").value() == std::string("above"))
            {
                note.m_connector.m_curve = CurveOver;
                note.m_connector.m_compass = North;
            }
            else
            {
                note.m_connector.m_curve = CurveUnder;
                note.m_connector.m_compass = South;
            }
        }
        else if (childName == "line" && xmlchild.attribute("startid").value() == xmlidRef)
        {
            note.m_connector.m_endPoint = EndPointStart;
            note.m_connector.m_number = m_holdNumber++;
            note.m_connector.m_curve = CurveStraight;
            note.m_connector.m_function = FunHold;
            note.m_connector.m_compass = South;
        }
        else if (childName == "line" && xmlchild.attribute("endid").value() == xmlidRef)
        {
            note.m_connector.m_endPoint = EndPointStop;
            note.m_connector.m_number = --m_holdNumber;
            note.m_connector.m_curve = CurveStraight;
            note.m_connector.m_function = FunHold;
            note.m_connector.m_compass = South;
        }
        else if (childName == "choice" || childName == "corr")
        {
            ParseFingOrnamConnector(xmlchild, xmlid, note);
        }
        else if (childName == "sic")
        {
            // ignore original uncorrected version
        }
    }
}

TimeSig ParserMei::ParseMeterSig(xml_node xmlparent)
{
    // meter may be an element or attributes, give element priority
    std::string sym;
    int count{0};
    int unit{0};
    std::string form;
    TimeSig timeSig;
    
    const xml_node xmlmeterSig{xmlparent.child("meterSig")};
    
    if (xmlmeterSig != nullptr)
    {
        sym = xmlmeterSig.attribute("sym").value();
        count = xmlmeterSig.attribute("count").as_int();
        unit = xmlmeterSig.attribute("unit").as_int();
        form = xmlmeterSig.attribute("form").value();
    }
    else
    {
        sym = xmlparent.attribute("meter.sym").value();
        count = xmlparent.attribute("meter.count").as_int();
        unit = xmlparent.attribute("meter.unit").as_int();
        form = xmlparent.attribute("meter.form").value();
    }
    
    if (sym == std::string("common"))
    {
        timeSig.m_timeSymbol = TimeSyCommon;
        timeSig.m_beats = 4;
        timeSig.m_beatType = 4;
    }
    else if (sym == std::string("cut"))
    {
        timeSig.m_timeSymbol = TimeSyCut;
        timeSig.m_beats = 2;
        timeSig.m_beatType = 2;
    }
    else if (count > 0 && unit > 0)
    {
        timeSig.m_beats = count;
        timeSig.m_beatType = unit;
        timeSig.m_timeSymbol = (form == std::string("num"))
                ? TimeSySingleNumber
                : TimeSyNormal;
    }

    return timeSig;
}

void ParserMei::ParseTuning(xml_node xmltuning, Part& part)
{
    // @tuning.standard
    const std::string tuningStandard{xmltuning.attribute("tuning.standard").value()};
    if (tuningStandard == "guitar.standard")
    {
        Pitch::SetTuning("E4 B3 G3 D3 A2 E2", part.m_tuning);
    }
    else if (tuningStandard == "guitar.drop.D")
    {
        Pitch::SetTuning("E4 B3 G3 D3 A2 D2", part.m_tuning);
    }
    else if (tuningStandard == "guitar.open.D")
    {
        Pitch::SetTuning("D4 A3 F#3 D3 A2 D2", part.m_tuning);
    }
    else if (tuningStandard == "guitar.open.G")
    {
        Pitch::SetTuning("D4 B3 G3 D3 G2 D2", part.m_tuning);
    }
    else if (tuningStandard == "guitar.open.A")
    {
        Pitch::SetTuning("E4 C#4 A3 E3 A2 E2", part.m_tuning);
    }
    else if (tuningStandard == "lute.renaissance.6")
    {
        Pitch::SetTuning("G4 D4 A3 F3 C3 G2", part.m_tuning);
    }
    else if (tuningStandard == "lute.baroque.d.major")
    {
        Pitch::SetTuning("F#4 D4 A3 F#3 D3 A2 G2 F#2 E2 D2 C#2 B1 A1", part.m_tuning);
    }
    else if (tuningStandard == "lute.baroque.d.minor")
    {
        Pitch::SetTuning("F4 D4 A3 F3 D3 A2 G2 F2 E2 D2 C2 B1 A1", part.m_tuning);
    }

    // tuning table
    part.m_tuning.reserve(20);
    for (const xml_node xmlcourse : xmltuning.children("course"))
    {
        const int course{xmlcourse.attribute("n").as_int()};
        const std::string step{xmlcourse.attribute("pname").value()};
        const int octave{xmlcourse.attribute("oct").as_int()};
        const std::string accid{xmlcourse.attribute("accid").value()};
        
        int alter{0};
        if (accid.size() == 1)
        {
            alter = (accid[0] == 's')
                    ? 1
                    : (accid[0] == 'f')
                    ? - 1
                    : 0;
        }
        
        if (step.size() == 1 && course >= 1 && octave >= 0)
        {
            if (part.m_tuning.size() < static_cast<size_t>(course))
            {
                part.m_tuning.resize(course);
            }
            
            part.m_tuning[course - 1] = Pitch(static_cast<char>(toupper(step[0])), alter, octave);
        }
    }
    LOGGER << "Source tuning " << Pitch::GetTuning(part.m_tuning);
}
        
Clef ParserMei::ParseClef(xml_node xmlparent)
{
    // clef may be an element or attributes, give element priority
    std::string clefShape;
    int dis{0};
    std::string disPlace;
    
    const xml_node xmlclef{xmlparent.child("clef")};
    if (xmlclef != nullptr)
    {
        clefShape = xmlclef.attribute("shape").value();
        dis = xmlclef.attribute("dis").as_int();
        disPlace = xmlclef.attribute("dis.place").value();
    }
    else
    {
         clefShape = xmlparent.attribute("clef.shape").value();
         dis = xmlparent.attribute("clef.dis").as_int();
         disPlace = xmlparent.attribute("clef.dis.place").value();
    }
    
    if (clefShape == "TAB")
    {
        return ClefTab;
    }
    if (clefShape == "F")
    {
        return ClefF;
    }
    if (clefShape == "G")
    {
        if (dis == 8 && disPlace == "below")
        {
             return ClefG8b;
        }
        if (dis == 8 && disPlace == "above")
        {
            return ClefG8a;
        }
        return ClefG;
    }
    
    return ClefNone;
}

Key ParserMei::ParseKey(xml_node xmlparent)
{
    // key may be an element or attributes, give element priority
    std::string sig;
    
    const xml_node xmlkeysig{xmlparent.child("keySig")};
    if (xmlkeysig != nullptr)
    {
        sig = xmlkeysig.attribute("sig").value();
    }
    else
    {
        sig = xmlparent.attribute("keysig").value();
    }

    if (!sig.empty())
    {
        const bool neg{sig[sig.size() - 1] == 'f'};
        const int fifths{stoi(sig)};
        return static_cast<Key>(neg ? -fifths : fifths);
    }
    
    return KeyNone;
}

void ParserMei::ParseVerse(xml_node xmlparent, Chord& chord)
{
    for (const xml_node xmlverse : xmlparent.children("verse"))
    {
        const std::string wordpos{xmlverse.child("syl").attribute("wordpos").value()};
        const Syllabic syllabic{wordpos == "i" ? SylBegin : wordpos == "m" ? SylMiddle : wordpos == "t" ? SylEnd : SylSingle}; 

        chord.m_lyrics.emplace_back(xmlverse.child("syl").text().as_string(), syllabic);
    }
}

} // namespace luteconv
