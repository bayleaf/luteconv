#include "mei.h"

#include "piece.h"

namespace luteconv
{

const char* const Mei::barStyle[] = {"single", "dotted", "dashed", "", "dbl", "end",
        "", "", "", "", nullptr};
    
const char* const Mei::fingering[] = {"", "1", "2", "3", "4", "t", nullptr};

const char* const Mei::notationtype[] = {"", "tab.lute.french", "tab.lute.german", "tab.lute.italian", "", "tab.guitar", nullptr};

const char* const Mei::ornament[] = {"", "#", "+", "x", ".", "..", "", ",", ",", ":", "::", "*", "_", "^", "-", nullptr};
static_assert(OrnSize == sizeof(Mei::ornament)/sizeof(Mei::ornament[0]) - 1);

} // namespace luteconv