#ifndef _GENFRT_H_
#define _GENFRT_H_

#include <iostream>
#include <string>

#include "generator.h"

namespace luteconv
{

/**
 * Generate Humdrum .frt
 */
class GenFrt: public Generator
{
public:
    /**
     * Constructor
     */
    GenFrt() = default;

    /**
     * Destructor
     */
    ~GenFrt() override = default;
    
    /**
     * Generate Humdrum .frt
     * 
     * @param[in] options
     * @param[in] piece
     * @param[out] dst destination
     */
    void Generate(const Options& options, const Piece& piece, std::ostream& dst) override;
    
private:
    static std::string GetTimeSignature(const Bar& bar);
    static std::string GetLeftFingering(const Note& note);
    static std::string GetRightFingering(const Note& note);
    static std::string GetBarStyle(const Bar& bar);
    static std::string GetFretTuning(const Part& part);
};

} // namespace luteconv

#endif // _GENFRT_H_

