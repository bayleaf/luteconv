#include "parser.h"

#include <fstream>
#include <iostream>

namespace luteconv
{

void Parser::Parse(const Options &options, Piece &piece)
{
    if (options.m_srcFilename.empty())
    {
        Parse(std::cin, options, piece);
    }
    else
    {
        std::fstream src;
        src.open(options.m_srcFilename, std::fstream::in | std::fstream::binary);
        if (!src.is_open())
        {
            throw std::runtime_error("Error: Can't open " + options.m_srcFilename);
        }
        Parse(src, options, piece);
    }
}

} // namespace luteconv
