#ifndef _PARSERTAB_H_
#define _PARSERTAB_H_

#include <iostream>
#include <map>
#include <vector>
#include <string_view>

#include "parser.h"

namespace luteconv
{

/**
 * Parse Tab .tab file
 */
class ParserTab: public Parser
{
public:

    /**
     * Constructor
    */
    ParserTab();

    /**
     * Destructor
     */
    ~ParserTab() override = default;
    
    /**
     * Parse .tab file
     *
     * @param[in] src .tab stream
     * @param[in] options
     * @param[out] piece destination
     */
    void Parse(std::istream& srcFile, const Options& options, Piece& piece) override;
    
    /**
     * Set the lute tuning from Wayne Cripps' Tab program
     * Octaves are numbered in decreasing pitch, octaves run from A..G
     * e.g for 10 course lute "C4D4E4F4G4c3f3a2d2g2"
     * 
     * @param[in] Wayne Cripps Tab, last string ... first string
     * @param[out] tuning
     */
    static void SetTuningTab(std::string_view s, std::vector<Pitch>& tuning);
    
private:
    static void ParseBarLine(std::string_view line, bool& barIsClear, Piece& piece);
    void ParseChord(std::string_view line, int lineNo, Piece& piece, Bar& bar, bool& barIsClear, int topString, bool triplet, int& maxLedger);
    static bool ParseNoteType(std::string_view line, size_t& idx, Chord& chord);
    static void ParseTimeSignature(std::string_view line, Bar& bar);
    static void ParseConnector(std::string_view line, size_t& idx, int string, Compass compass, Note& note);
    void ParseKey(std::string_view line, Piece& piece);
    void ParseCmn(std::string_view line, int lineNo, size_t idx, Piece& piece);
    void ParseCmnPart(std::string_view line, int lineNo, size_t& idx, const std::vector<Accidental>& keySignature,
            std::map<Pitch, Accidental>& activeAccidentals, Part& part);
    static void ParseLyricsPart(std::string_view line, size_t& idx, Bar& bar, std::vector<bool>& lyricHyphen);
    static std::string CleanTabString(std::string_view src);
    static void ParseJHRTuning(Piece& piece);
    static void ParseJHRKeywords(std::string_view text, bool& cittern, bool& french, bool& diatonic, bool& transcribed);
    static void FixLedgers(Part& part, int lwb, int upb);

    size_t m_numParts{1};
    const std::vector<Pitch> m_clefGTuning; // for translating TAB's CMN pitches
    const std::vector<Pitch> m_clefFTuning;
    
    std::vector<Accidental> m_keySignature1; // accidentals implied by key signature
    std::vector<Accidental> m_keySignature2;
    std::map<Pitch, Accidental> m_activeAccidentals1;  // active accidentals in bar
    std::map<Pitch, Accidental> m_activeAccidentals2;
    std::vector<bool> m_lyricHyphen1;   // set when the last lyric was hypenated
    std::vector<bool> m_lyricHyphen2;
    
    Clef m_clef{ClefNone}; // clef implied by last specified clef in a previous bar, same for both voices - is that right?
    Key m_key{KeyNone}; // key implied by last specified key, same for both voices - is that right?
};

} // namespace luteconv

#endif // _PARSERTAB_H_
