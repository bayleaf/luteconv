#ifndef _PARSERMUSICXML_H_
#define _PARSERMUSICXML_H_

#include <string_view>

#include <pugixml.hpp>

#include "parser.h"

namespace luteconv
{

/**
 * Parse .musicxml file
 */
class ParserMusicXml: public Parser
{
public:

    /**
     * Constructor
    */
    ParserMusicXml() = default;

    /**
     * Destructor
     */
    ~ParserMusicXml() override = default;
    
    /**
     * Parse .musicxml file image in buffer
     *
     * @param[in] filename .musicxml
     * @param[in] contents .musicxml image
     * @param[in] size
     * @param[in] options
     * @param[out] piece destination
     */
    static void Parse(std::string_view filename, void* contents, size_t size, const Options& options, Piece& piece);
    
    /**
     * Parse .musicxml file
     *
     * @param[in] src stream
     * @param[in] options
     * @param[out] piece destination
     */
    void Parse(std::istream& srcFile, const Options& options, Piece& piece) override;
    
private:
    static void Parse(std::string_view filename, const pugi::xml_document& doc, const pugi::xml_parse_result& result, const Options& options, Piece& piece);
    static void ParsePart(pugi::xml_node xmlpart, const Options &options, Part& part);
    static void ParseStaffTuning(pugi::xml_node xmlpart, Part& part);
    static void ParseBarline(pugi::xml_node xmlmeasure, Part& part);
    static void ParseClef(pugi::xml_node xmlmeasure, Part &part);
    static void ParseKey(pugi::xml_node xmlmeasure, Part &part);
    static void ParseTimeSignature(pugi::xml_node xmlmeasure, Part& part);
    static void ParseCredit(pugi::xml_node xmlscorePartwise, Piece& piece);
    static void ParseOrnaments(pugi::xml_node xmlornaments, Note &note);
    static void MergeVoices(int divisions, Bar& bar);
    
    using Parser::Parse; // avoid -Werror=overloaded-virtual=
};

} // namespace luteconv

#endif // _PARSERMUSICXML_H_
