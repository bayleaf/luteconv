#ifndef _GENABC_H_
#define _GENABC_H_

#include <iostream>
#include <string>

#include "generator.h"

namespace luteconv
{

/**
 * Generate .abc
 */
class GenAbc: public Generator
{
public:
    /**
     * Constructor
     */
    GenAbc() = default;

    /**
     * Destructor
     */
    ~GenAbc() override = default;
    
    /**
     * Generate abc .abc
     * 
     * @param[in] options
     * @param[in] piece
     * @param[out] dst destination
     */
    void Generate(const Options& options, const Piece& piece, std::ostream& dst) override;
    
private:
    static std::string GetTimeSignature(const Bar & bar);
    static std::string GetFlagInfo(const Options& options, const Chord & our);
    static std::string GetRightFingering(const Note & note);
    static std::string GetOrnament(Ornament ornament, bool right);
    static std::string GetFret(const Note & note);
    static int Gcd(int a, int b);
    static std::string GetLeftConnector(const Note & note);
    static std::string GetRightConnector(const Note & note);
};

} // namespace luteconv

#endif // _GENABC_H_

