#ifndef _PARSERMEI_H_
#define _PARSERMEI_H_

#include <string_view>

#include <pugixml.hpp>

#include "parser.h"

namespace luteconv
{

/**
 * Parse .mei file
 */
class ParserMei: public Parser
{
public:

    /**
     * Constructor
    */
    ParserMei() = default;

    /**
     * Destructor
     */
    ~ParserMei() override = default;
    
    /**
     * Parse .mei file
     *
     * @param[in] src stream
     * @param[in] options
     * @param[out] piece destination
     */
    void Parse(std::istream& srcFile, const Options& options, Piece& piece) override;
    
private:
    void ParseChordList(pugi::xml_node xmlmeasure, pugi::xml_node xmlparent, Grid grid, int tripletCount, Part& part, Bar& bar, int& stamp);
    void ParseChord(pugi::xml_node xmlmeasure, pugi::xml_node xmlelement, Grid grid, int tripletCount, Part& part, Bar& bar, int& stamp);
    void ParseNote(pugi::xml_node xmlmeasure, pugi::xml_node xmlnote, Part& part, Chord& chord);
    void ParseNoteList(pugi::xml_node xmlmeasure, pugi::xml_node xmlparent, Part& part, Chord& chord);
    static void ParseFermata(pugi::xml_node xmlmeasure, std::string_view xmlid, Chord& chord);
    void ParseFingOrnamConnector(pugi::xml_node xmlmeasure, std::string_view xmlid, Note& note);
    static void ParseTuning(pugi::xml_node xmltuning, Part& part);
    static TimeSig ParseMeterSig(pugi::xml_node xmlparent);
    static void ParseSpace(pugi::xml_node xmlspace, int& stamp, Part& part);
    void ParseMeasure(pugi::xml_node xmlmeasure, Piece& piece, std::vector<Clef>& clefs, std::vector<Key>& keys, std::vector<TimeSig>& timeSigs, int volta);
    static Clef ParseClef(pugi::xml_node xmlparent);
    static Key ParseKey(pugi::xml_node xmlparent);
    static void ParseVerse(pugi::xml_node xmlparent, Chord& chord);
    static NoteType ParseDur(pugi::xml_node xmlparent, Part& part);
    
    int m_slurNumber{1};
    int m_holdNumber{1};
};

} // namespace luteconv

#endif // _PARSERMEI_H_
