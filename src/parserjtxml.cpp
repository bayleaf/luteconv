#include "parserjtxml.h"

#include <iostream>
#include <sstream>
#include <stdexcept>

#include "pitch.h"
#include "logger.h"

namespace luteconv
{

using namespace pugi;

void ParserJtxml::Parse(std::string_view filename, void* contents, size_t size, const Options& options, Piece& piece)
{
    xml_document doc;
    const xml_parse_result result = doc.load_buffer_inplace(contents, size);
    Parse(filename, doc, result, options, piece);
}

void ParserJtxml::Parse(std::istream& srcFile, const Options& options, Piece& piece)
{
    xml_document doc;
    const xml_parse_result result = doc.load(srcFile);
    Parse(options.m_srcFilename, doc, result, options, piece);
}
  
void ParserJtxml::Parse(std::string_view filename, const xml_document& doc, const xml_parse_result& result, const Options& options, Piece& piece)
{
    LOGGER << "Parse jtxml";
    
    Part& part{piece.m_parts.at(0)}; // only 1 part
    
    if (!result)
    {
        std::ostringstream ss;
        ss << "Error: XML parse error: " << filename
                    << ". Description: " << result.description() 
                    << " Offset: " << result.offset;
        throw std::runtime_error(ss.str());
    }
    
    const xml_node xmlsection = doc.child("DjangoTabXML").child("sections").find_child_by_attribute("section", "index", std::to_string(options.m_index).c_str());
    if (!xmlsection)
    {
        throw std::invalid_argument("Error: Can't find <DjangoTabXML><sections><section> index=" + std::to_string(options.m_index));
    }
    
    piece.m_title = xmlsection.child("section-texts").find_child_by_attribute("track-text", "descriptor", "section-name").child_value();
    piece.m_composer = xmlsection.child("section-texts").find_child_by_attribute("track-text", "descriptor", "section-author").child_value();

    const std::string sectionComments = xmlsection.child("section-texts").find_child_by_attribute("track-text", "descriptor", "section-comments").child_value();
    if (!sectionComments.empty())
    {
        piece.m_credits.push_back(sectionComments);
    }
        
    LOGGER << "title=" << piece.m_title;
    LOGGER << "composer=" << piece.m_composer;
    
    ParseTuning(xmlsection, part);
    
    const xml_node xmlsystems = xmlsection.child("systems");
    for (const xml_node xmlsystem : xmlsystems.children("system"))
    {
        const xml_node xmlstaff = xmlsystem.child("instruments").child("instrument").child("staff");
        if (xmlstaff == nullptr)
        {
            LOGGER << "No xmlstaff";
        }
        
        part.m_bars.emplace_back();
        
        for (const xml_node xmlevent : xmlstaff.children("event"))
        {
            ParseEvent(xmlevent, part);
        }
        
        if (part.m_bars.back().m_chords.empty())
        {
            part.m_bars.pop_back();
        }
    }
    
    part.SetTuning(options);
}

void ParserJtxml::ParseTuning(xml_node xmlsection, Part& part)
{
    const xml_node xmlstrings = xmlsection.child("instruments").child("instrument").child("tablature-definition").child("tuning").child("strings");
    const int numCourses = xmlstrings.attribute("count").as_int();
    part.m_tuning.resize(numCourses);
    
    for (const xml_node xmlstring : xmlstrings.children("string"))
    {
        const int index = xmlstring.attribute("index").as_int();
        const int midiPitch = xmlstring.attribute("midi-pitch").as_int();
        
        // Don't know why midi-pitch is 23 less than midi note
        part.m_tuning[index] = Pitch(midiPitch + 23);
    }
    LOGGER << "Source tuning " << Pitch::GetTuning(part.m_tuning);
}

void ParserJtxml::ParseEvent(xml_node xmlevent, Part& part)
{
    Bar& bar = part.m_bars.back();
    
    const std::string type{xmlevent.attribute("type").value()};
    
    if (type == "chord")
    {
        bar.m_chords.emplace_back();
        Chord& chord = bar.m_chords.back();
        
        ParseFlag(xmlevent.attribute("flag").as_int(), chord);
        
        const xml_node xmlnotes = xmlevent.child("notes");
        if (!xmlnotes)
        {
            LOGGER << "No notes";
        }
        for (const xml_node xmlnote : xmlnotes.children("note"))
        {
            chord.m_notes.emplace_back();
            Note& note = chord.m_notes.back();
            
            note.m_course = xmlnote.attribute("string").as_int() + 1;
            // jtxml doesn't store the fret but rather the pitch as a MIDI note.  Calculate the fret from the tuning.
            note.m_fret = Pitch(xmlnote.attribute("pitch").as_int()) - part.m_tuning[note.m_course - 1];
            // TODO fingering
            // TODO ornaments
        }
        
        // volta brackets and fermata over chord
        for (const xml_node xmlmetaEvent : xmlevent.child("meta-events").children("meta-event"))
        {
            const std::string metaEventName{xmlmetaEvent.attribute("name").value()};
            if (metaEventName == "Ending")
            {
                const std::string voltaText{xmlmetaEvent.child("text").text().as_string()};
                if (!voltaText.empty())
                {
                    if (voltaText.front() == '1')
                    {
                        bar.m_volta = 1;
                    }
                    else if (voltaText.front() == '2')
                    {
                        bar.m_volta = 2;
                    }
                }
            }
            else if (metaEventName == "Fermata" && xmlmetaEvent.child("location").child("tablature").attribute("l").as_int() > 0)
            {
                chord.m_fermata = true;
            }
        }
    }
    else if (type == "bar")
    {
        const std::string barType = xmlevent.attribute("bar-type").value();
        if (barType == "repeat")
        {
            const std::string repeatType = xmlevent.attribute("repeat-type").value();
            if (repeatType== "left")
            {
                bar.m_repeat = RepBackward;
            }
            else if (repeatType== "right")
            {
                bar.m_repeat = RepForward;
            }
            if (repeatType== "both")
            {
                bar.m_repeat = RepJanus;
            }
        }
        
        // fermata over bar line
        for (const xml_node xmlmetaEvent : xmlevent.child("meta-events").children("meta-event"))
        {
            const std::string metaEventName{xmlmetaEvent.attribute("name").value()};
            if (metaEventName == "Fermata" && xmlmetaEvent.child("location").child("tablature").attribute("l").as_int() > 0)
            {
                bar.m_fermata = true;
            }
        }

        part.m_bars.emplace_back();
        // TODO bar-style
        // TODO time signature
    }
    else
    {
        LOGGER << "Unprocessed event=" << type;
    }
}

void ParserJtxml::ParseFlag(int flagNum, Chord& chord)
{
    switch(flagNum)
    {
        case 4096:
        case 0:
            chord.m_noteType = NoteTypeWhole;
            break;
        case 4097:
        case 1:
            chord.m_noteType = NoteTypeHalf;
            break;
        case 4098:
        case 2: 
            chord.m_noteType = NoteTypeQuarter;
            break;
        case 4099 :
        case 3: 
            chord.m_noteType = NoteTypeEighth;
            break;
        case 4100:
        case 4:
            chord.m_noteType = NoteType16th;
            break;
        case 4101:
        case 5:
            chord.m_noteType = NoteType32nd;
            break;
        case 4102:
        case 6:
            chord.m_noteType = NoteType64th;
            break;
        case 7:
        case 4103: 
            chord.m_noteType = NoteType128th;
            break;

        case 4104:
        case 8:
            chord.m_noteType = NoteTypeWhole;
            chord.m_dotted = true;
            break;
        case 4105:
        case 9:
            chord.m_noteType = NoteTypeHalf;
            chord.m_dotted = true;
            break;
        case 4106:
        case 10:
            chord.m_noteType = NoteTypeQuarter;
            chord.m_dotted = true;
            break;
        case 4107:
        case 11:
            chord.m_noteType = NoteTypeEighth;
            chord.m_dotted = true;
            break;
        case 4108:
        case 12:
            chord.m_noteType = NoteType16th;
            chord.m_dotted = true;
            break;
        case 4109:
        case 13:
            chord.m_noteType = NoteType32nd;
            chord.m_dotted = true;
            break;
        case 4110:
        case 14:
            chord.m_noteType = NoteType64th;
            chord.m_dotted = true;
            break;
        case 4111:
        case 15:
            chord.m_noteType = NoteType128th;
            chord.m_dotted = true;
            break;

        default:
            LOGGER << "Unprocessed flag=" << flagNum;
            break;
    }
}


} // namespace luteconv
