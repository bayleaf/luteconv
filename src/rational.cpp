#include "rational.h"

#include <cmath>
#include <cstdint>
#include <numeric>

namespace luteconv
{

Rational::Rational(int n, int d)
: m_num{n}, m_den{d}
{
    // normalize, demoninator > 0, gcd(numerator, denominator) == 1
    if (m_den < 0)
    {
        m_num = -m_num;
        m_den = -m_den;
    }
    
    const int d1{std::gcd(m_num, m_den)};
    m_num /= d1;
    m_den /= d1;
}

Rational Rational::operator +() const
{
    return *this;
}  

Rational Rational::operator -() const
{
    return Rational(-m_num, m_den);
}

bool Rational::operator ==(Rational rhs) const
{
    return m_num == rhs.m_num && m_den == rhs.m_den;
}

bool Rational::operator <(Rational rhs) const
{
    return static_cast<int64_t>(m_num) * rhs.m_den < static_cast<int64_t>(m_den) * rhs.m_num;
}

Rational& Rational::operator +=(Rational rhs)
{
    // Knuth II 4.5.1
    const int d1{std::gcd(m_den, rhs.m_den)};
    if (d1 == 1)
    {
        m_num = m_num * rhs.m_den + m_den * rhs.m_num;
        m_den *= rhs.m_den;
    }
    else
    {
        static_assert(sizeof(int64_t) >= sizeof(int) * 2);
        const int64_t t{static_cast<int64_t>(m_num) * (rhs.m_den / d1) + static_cast<int64_t>(rhs.m_num) * (m_den / d1)};
        const int d2{static_cast<int>(std::gcd(t, static_cast<int64_t>(d1)))};
        
        m_num = static_cast<int>(t / d2);
        m_den = (m_den / d1) * (rhs.m_den / d2);
    }
    return *this;
}

Rational& Rational::operator -=(Rational rhs)
{
    (*this) += -rhs;
    return *this;
}

Rational& Rational::operator *=(Rational rhs)
{
    // Knuth II 4.5.1
    const int d1{std::gcd(m_num, rhs.m_den)};
    const int d2{std::gcd(m_den, rhs.m_num)};
    
    m_num = (m_num / d1) * (rhs.m_num / d2);
    m_den = (m_den / d2) * (rhs.m_den / d1);
    return *this;
}

Rational& Rational::operator /=(Rational rhs)
{
    // Knuth II 4.5.1 ex. 4
    const int d1{std::gcd(m_num, rhs.m_num)};
    const int d2{std::gcd(m_den, rhs.m_den)};

    m_num = (m_num / d1) * (rhs.m_den / d2);
    if (rhs.m_num < 0)
    {
        m_num = -m_num;
    }
    m_den = std::abs((m_den / d2) * (rhs.m_num / d1));

    return *this;
}

std::string Rational::ToString() const
{
    return std::to_string(m_num) + '/' + std::to_string(m_den);
}

int Rational::Trunc() const
{
    return m_num / m_den;
}

// Non-member helpers

Rational operator +(Rational lhs, Rational rhs)
{
    lhs += rhs;
    return lhs;
}

Rational operator -(Rational lhs, Rational rhs)
{
    lhs -= rhs;
    return lhs;
}

Rational operator *(Rational lhs, Rational rhs)
{
    lhs *= rhs;
    return lhs;
}

Rational operator /(Rational lhs, Rational rhs)
{
    lhs /= rhs;
    return lhs;
}

bool operator <=(Rational lhs, Rational rhs)
{
    return lhs == rhs || lhs < rhs;
}

bool operator !=(Rational lhs, Rational rhs)
{
    return !(lhs == rhs);
}

bool operator >(Rational lhs, Rational rhs)
{
    return lhs != rhs && !(lhs < rhs);
}

bool operator >=(Rational lhs, Rational rhs)
{
    return !(lhs < rhs);
}


} // namespace luteconv

