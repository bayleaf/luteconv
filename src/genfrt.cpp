#include "genfrt.h"

#include <algorithm>
#include <fstream>
#include <chrono>
#include <iomanip>

#include "charset.h"
#include "logger.h"
#include "platform.h"

namespace luteconv
{

void GenFrt::Generate(const Options& options, const Piece& piece, std::ostream& dst)
{
    LOGGER << "Generate Humdrum .frt";
    
    // find the first tablature part
    const Part& part{piece.m_parts.at(FindTabPart(piece))};
    
    const std::time_t tt{std::chrono::system_clock::to_time_t(std::chrono::system_clock::now())};
    struct std::tm * ptm{std::gmtime(&tt)};
    
    if (!piece.m_title.empty())
    {
        dst << "!!!OTL: " << piece.m_title << '\n';
    }

    if (!piece.m_composer.empty())
    {
        dst << "!!!COM: " << piece.m_composer << '\n';
    }

    if (!piece.m_copyright.empty())
    {
        dst << "!!!YEM: " << piece.m_copyright << '\n';
    }
    
    if (piece.m_credits.size() == 1)
    {
        dst << "!!!ONB: " << piece.m_credits[0] << '\n';
    }
    else
    {
        for (size_t i{0}; i < piece.m_credits.size(); ++i)
        {
            dst << "!!!ONB" << i + 1 << ": " << piece.m_credits[i] << '\n';
        }
    }
    
    dst << "!!!RNB: Converted to Humdrum .frt by luteconv " << options.m_version << '\n';
    dst << "!!!RDT: " << std::put_time(ptm,"%Y/%m/%d") << '\n';
    
    // spines
    dst << "**recip\t**fret" << '\n';
    
    // Tuning.  Get lowest pitch.  This may not be bottom course.
    Pitch lowest{part.m_tuning.front()};
    for (const Pitch& pitch : part.m_tuning)
    {
        if (pitch.Midi() < lowest.Midi())
        {
            lowest = pitch;
        }
    }
    dst << "*\t*AT:" << lowest.ToString() << '\n';
    dst << "*\t*RT";
    
    // course tuning relative to the lowest pitch. Bottom course first
    for(auto it{part.m_tuning.rbegin()}; it != part.m_tuning.rend(); ++it)
    {
        dst << ":" << it->Midi() - lowest.Midi();
    }
    dst << '\n';
    
    // fret tuning if diatonic
    const std::string fretTuning{GetFretTuning(part)};
    if (!fretTuning.empty())
    {
        dst << fretTuning << '\n';
    }
    
    // body
    size_t barNum{1};

    for (const auto & bar : part.m_bars)
    {
        // time signature
        const std::string timeSignature{GetTimeSignature(bar)};
        if (!timeSignature.empty())
        {
            dst << timeSignature << '\n';
        }
        
        if (barNum == 1)
        {
            // initial invisible bar line
            dst << "=1-\t=1-" << '\n';
        }
        
        int tripletCount{0};
        
        // chords
        for (const auto & chord : bar.m_chords)
        {
            std::vector<std::string> vert(part.m_tuning.size(), "-");
            
            for (const auto & note : chord.m_notes)
            {
                // Ignore ensemble lines and anchors
                if (note.m_fret < 0)
                {
                    continue;
                }

                vert[note.m_course - 1] = "|" +
                    std::to_string(note.m_fret) +
                    GetLeftFingering(note) +
                    GetRightFingering(note);
            }
            
            // Humdrum has 0 = breve, 1 = whole, 2 = half, 4 = quarter etc
            NoteType adjusted{static_cast<NoteType>(chord.m_noteType + options.m_flags)};
            adjusted = std::max(NoteTypeLong, adjusted);
            adjusted = std::min(NoteType128th, adjusted);
            if (adjusted == NoteTypeLong)
            {
                dst << "00";
            }
            else if (adjusted == NoteTypeBreve)
            {
                dst << "0";
            }
            else
            {
                static_assert(NoteTypeBreve == 1);
                int humRhythm{1 << (adjusted - 2)};
                
                if (chord.m_triplet)
                {
                    tripletCount = bar.TripletNotes(chord); // start of triplet
                }
                
                if (tripletCount > 0)
                {
                    humRhythm = (humRhythm * 3) / 2;
                    --tripletCount;
                }
                
                dst << humRhythm;
            }
            
            if (chord.m_dotted)
            {
                dst << ".";
            }
            
            dst << "\t";
            
            // bottom course first
            for (int i{static_cast<int>(vert.size()) - 1}; i >= 0; --i)
            {
                dst << vert[i];
                if (i > 0)
                {
                    dst << " ";
                }
            }
            dst << '\n';
        }
        
        // volta brackets are indicated in this bar's bar line for the following bar.
        // barNum is 1 based so is the index of the following bar
        std::string volta;
        if (barNum < part.m_bars.size() && part.m_bars[barNum].m_volta > 0)
        {
            volta = static_cast<char>(part.m_bars[barNum].m_volta + 'a' - 1);
        }
        
        // end of current bar
        ++barNum;
        std::string barStyle{GetBarStyle(bar)};
        
        // Use a double barline for the last barline, without bar number
        if (barNum > part.m_bars.size())
        {
            barStyle = std::string("==").append(barStyle);
        }
        else
        {
            barStyle = std::string("=").append(std::to_string(barNum)).append(volta).append(barStyle);
        }
        dst << barStyle << "\t" << barStyle << '\n';
    }
    
    dst << "*-\t*-" << '\n';
}
    
std::string GenFrt::GetTimeSignature(const Bar& bar)
{
    std::string timeSig;
    
    switch (bar.m_timeSig.m_timeSymbol)
    {
    case TimeSyNone:
        [[fallthrough]];
    case TimeSyNote:
        [[fallthrough]];
    case TimeSyDottedNote:
        return "";
    case TimeSyCommon:
        [[fallthrough]];
    case TimeSyCut:
        [[fallthrough]];
    case TimeSySingleNumber:
        [[fallthrough]];
    case TimeSyNormal:
        timeSig = std::to_string(bar.m_timeSig.m_beats) + "/" + std::to_string(bar.m_timeSig.m_beatType);
        break;
    }
    return "*M" + timeSig + "\t*M" + timeSig;
}


std::string GenFrt::GetLeftFingering(const Note& note)
{
    switch (note.m_leftFingering)
    {
    case FingerNone:
        return "";
    case FingerFirst:
        return "b";
    case FingerSecond:
        return "c";
    case FingerThird:
        return "d";
    case FingerForth:
        return "e";
    case FingerThumb:
        return "a";
    }
    return "";
}

std::string GenFrt::GetRightFingering(const Note& note)
{
    switch (note.m_rightFingering)
    {
    case FingerNone:
        return "";
    case FingerFirst:
        return "I";
    case FingerSecond:
        return "M";
    case FingerThird:
        return "A";
    case FingerForth:
        return "Q";
    case FingerThumb:
        return "P";
    }
    return "";
}

std::string GenFrt::GetBarStyle(const Bar& bar)
{
    std::string barStyle;
    
    switch (bar.m_barStyle)
    {
    case BarStyleDotted:
        barStyle = "\"";
        break;
    case BarStyleHeavy:
        barStyle = "!";
        break;
    case BarStyleLightLight:
        barStyle = "||";
        break;
    case BarStyleLightHeavy:
        barStyle = "|!";
        break;
    case BarStyleHeavyLight:
        barStyle = "!|";
        break;
    case BarStyleHeavyHeavy:
        barStyle = "!!";
        break;
    case BarStyleRegular:
        [[fallthrough]];
    case BarStyleDashed:
        [[fallthrough]];
    case BarStyleTick:
        [[fallthrough]];
    case BarStyleShort:
        [[fallthrough]];
    default:
        barStyle = "|";
    }
    
    switch (bar.m_repeat)
    {
    case RepNone:
        break;
    case RepForward:
        barStyle += ":";
        break;
    case RepBackward:
        barStyle = ":" + barStyle;
        break;
    case RepJanus:
        barStyle = ":" + barStyle + ":";
    }
    
    if (barStyle == "|")
    {
        barStyle.clear();
    }
    
    if (bar.m_fermata)
    {
        barStyle += ";";
    }
    
    return barStyle;
}
    
std::string GenFrt::GetFretTuning(const Part& part)
{
    if (part.m_missingFrets.empty())
    {
        return "";
    }
    
    // diatonic fret tuning
    // start with 1 semitone per fret
    std::vector<size_t> board(part.MaxFret() + 1, 1);
    board[0] = 0; // fret 0 is always missing
    
    // work forward from lowest missing fret
    for (const auto missing : part.m_missingFrets)
    {
        if (missing + 1 >= static_cast<int>(board.size()))
        {
            continue;
        }
        
        board[missing + 1] += board[missing]; // add the missing fret's width to the next fret
        board[missing] = 0;
    }
    
    std::string result{"*\t*FT:"};
    bool sep{false};
    size_t tuning{0};
    for (const auto width : board)
    {
        if (width == 0)
        {
            continue; // missing fret
        }
        
        if (sep)
        {
            result += ',';
        }
        
        tuning += width;
        result += std::to_string(tuning);
        sep = true;
    }
    
    return result;
}

} // namespace luteconv
