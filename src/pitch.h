#ifndef _PITCH_H_
#define _PITCH_H_

#include <cstdint>
#include <string>
#include <string_view>
#include <vector>

namespace luteconv
{

/**
 * Scientific Pitch Notation
 */
class Pitch
{
public:
    
    /**
     * Constructor
     * 
     * Middle C is C4
     * 
     * @param[in] step
     * @param[in] alter
     * @param[in] octave
     */
    Pitch(char step, int alter, int octave);
    
    /**
     * Constructor
     * 
     * Middle C is 60
     * 
     * @param[in] midi note
     */
    explicit Pitch(int midi);
    
    Pitch() = default;
    ~Pitch() = default;
        
    /**
     * Get MIDI note. Middle C is 60
     * 
     * @return MIDI note
     */
    int Midi() const;
    
    /**
     * Get the step 'A' .. 'G'
     * 
     * @return step
     */
    char Step() const;
    
    /**
     * Get the Alter (accidental)
     * 
     * @return >0 number of sharps, == 0 natural, < 0 number of flats
     */
    int Alter() const;
    
    /**
     * Get the octave
     * 
     * @return octave
     */
    int Octave() const;
    
    /**
     * String representation, e.g. C#3
     * 
     * @return representation
     */
    std::string ToString() const;
    
    /**
     * Equality.  Note that enharmonics are not equal.
     * If enharmonic equality is required then use lhs.Midi() == rhs.Midi()
     * 
     * @param[in] rhs
     * @return true <=> equal
     */
    bool operator ==(const Pitch & rhs) const;
    
    /**
     * Inequality.  Note that enharmonics are not equal.
     * If enharmonic equality is required then use lhs.Midi() != rhs.Midi()
     * 
     * @param[in] rhs
     * @return true <=> not equal
     */
    bool operator !=(const Pitch & rhs) const;
    
    /**
     * Less than.  Note that enharmonics are not equal.
     * If enharmonic less than is required then use lhs.Midi() < rhs.Midi()
     * 
     * @param[in] rhs
     * @return true <=> less than
     */
    bool operator <(const Pitch & rhs) const;

    /**
     * Add semitones to a pitch
     * 
     * @param[in] rhs semitones to add, may be negative
     * @return *this
     */
    Pitch& operator +=(int rhs);
    
    /**
     * Add semitones to a pitch.
     * 
     * @param[in] rhs semitones to add, may be negative
     * @return pitch
     */
    Pitch operator +(int rhs) const;

    /**
     * Subtract semitones from a pitch
     * 
     * @param[in] rhs semitones to subtract, may be negative
     * @return *this
     */
    Pitch& operator -=(int rhs);
    
    /**
     * Subtract semitones from a pitch.
     * 
     * @param[in] rhs semitones to subtract, may be negative
     * @return pitch
     */
    Pitch operator -(int rhs) const;

    /**
     * Difference in semitones between two pitches
     * 
     * @param rhs
     * @return difference in semitones
     */
    int operator -(const Pitch & rhs) const;

    /**
     * Parse a single pitch in Scientific Pitch Notation
     * 
     * @param [in]s string to parse
     * @param[inout] pos position parse from, updated to after pitch
     * @return pitch
     */
    static Pitch Parse(std::string_view s, size_t& pos);
    
    /**
     * Set the default lute tuning according to the number of courses
     * 
     * @param[in] courses number of courses
     * @param[out] tuning
     */
    static void SetTuning(int courses, std::vector<Pitch>& tuning);
    
    /**
     * Set the default lute tuning to baroque D minor
     * 
     * @param[out] tuning
     */
    static void SetTuningBaroque(std::vector<Pitch>& tuning);
    
    /**
     * Set the default lute tuning to vieil ton
     * 
     * @param[out] tuning
     */
    static void SetTuningVieilTon(std::vector<Pitch>& tuning);
    
    /**
     * Set the tuning from Scientific Pitch Notation
     * e.g for 10 course lute "G4 D4 A3 F3 C3 G2 F2 Eb2 D2 C2"
     * Spaces between notes are optional.
     * 
     * @param[in] s scientific pitch notation, first string ... last string
     * @result tuning
     */
    static std::vector<Pitch> SetTuning(std::string_view s);
    
    /**
     * Set the tuning from Scientific Pitch Notation
     * e.g for 10 course lute "G4 D4 A3 F3 C3 G2 F2 Eb2 D2 C2"
     * Spaces between notes are optional.
     * 
     * @param[in] s scientific pitch notation, first string ... last string
     * @param[out] tuning
     */
    static void SetTuning(std::string_view s, std::vector<Pitch>& tuning);
    
    /**
     * Get the lute tuning in Scientific Pitch Notation
     * e.g for 10 course lute "G4D4A3F3C3G2F2Eb2D2C2"
     * 
     * @param[in] tuning
     * @return tuning
     */
    static std::string GetTuning(const std::vector<Pitch>& tuning);

private:
    char m_step{'\0'};
    int8_t m_alter{0};
    int8_t m_octave{0};
};

} // namespace luteconv

#endif // _PITCH_H_
