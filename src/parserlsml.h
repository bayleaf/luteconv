#ifndef _PARSERLSML_H_
#define _PARSERLSML_H_

#include <pugixml.hpp>

#include "parser.h"

namespace luteconv
{

/**
 * Parse LuteScribe .lsml file
 */
class ParserLsml: public Parser
{
public:

    /**
     * Constructor
    */
    ParserLsml() = default;

    /**
     * Destructor
     */
    ~ParserLsml() override = default;
    
    /**
     * Parse .lsml file
     *
     * @param[in] src stream
     * @param[in] options
     * @param[out] piece destination
     */
    void Parse(std::istream& srcFile, const Options& options, Piece& piece) override;
};

} // namespace luteconv

#endif // _PARSERLSML_H_
