#ifndef _PARSERJTZ_H_
#define _PARSERJTZ_H_

#include "parser.h"

#include <string>

namespace luteconv
{

/**
 * Parse Fandango .jtz file
 */
class ParserJtz: public Parser
{
public:

    /**
     * Constructor
    */
    ParserJtz() = default;

    /**
     * Destructor
     */
    ~ParserJtz() override = default;
    
    /**
     * Parse .jtz file
     *
     * @param[in] src stream
     * @param[in] options
     * @param[out] piece destination
     */
    void Parse(std::istream& srcFile, const Options& options, Piece& piece) override;
};

} // namespace luteconv

#endif // _PARSERJTZ_H_
