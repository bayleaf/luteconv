#ifndef _PARSERMID_H_
#define _PARSERMID_H_

#include "parser.h"

namespace luteconv
{

/**
 * Parse MIDI .mid file
 */
class ParserMid: public Parser
{
public:

    /**
     * Constructor
    */
    ParserMid() = default;

    /**
     * Destructor
     */
    ~ParserMid() override = default;
    
    /**
     * Parse .mid MIDI file
     *
     * @param[in] src stream
     * @param[in] options
     * @param[out] piece destination
     */
    void Parse(std::istream& srcFile, const Options& options, Piece& piece) override;
};

} // namespace luteconv

#endif // _PARSERMID_H_
