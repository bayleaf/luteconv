#include "genlsml.h"

#include <algorithm>
#include <chrono>
#include <fstream>
#include <iomanip>
#include <set>
#include <sstream>

#include "logger.h"
#include "gentab.h"

namespace luteconv
{

using namespace pugi;

void GenLsml::Generate(const Options& options, const Piece& piece, std::ostream& dst)
{
    LOGGER << "Generate lsml";
    
    // find the first tablature part
    const Part& part{piece.m_parts.at(FindTabPart(piece))};
    
    // LuteScribe's .lsml is intimately connected to the .tab format, so we use code from the tab generator.
    
    TabType tabType{part.m_tabType};
    const std::set<TabType> tabTypes{ TabFrench, TabItalian, TabSpanish };

    if (tabTypes.find(part.m_tabType) == tabTypes.end())
    {
        LOGGER << "lsml does not support " << Options::GetTabType(part.m_tabType) << " tablature type. Using french.";
        tabType = TabFrench;
    }

    const std::time_t tt{std::chrono::system_clock::to_time_t(std::chrono::system_clock::now())};
    struct std::tm * ptm{std::gmtime(&tt)};
    std::ostringstream encodingDate;
    encodingDate << std::put_time(ptm,"%F");
    
    xml_document doc;
    xml_node decl{doc.prepend_child(node_declaration)};
    decl.append_attribute("version") = "1.0";
    decl.append_attribute("standalone") = "no";
    
    xml_node tabModel{doc.append_child("TabModel")};
    tabModel.append_attribute("xmlns:xsi") = "http://www.w3.org/2001/XMLSchema-instance";
    tabModel.append_attribute("xmlns:xsd") = "http://www.w3.org/2001/XMLSchema";
    tabModel.append_attribute("Version") = "1.0";
    tabModel.append_child(node_comment).set_value(" Converted to .lsml by luteconv " + options.m_version + " ");
    tabModel.append_child(node_comment).set_value(" encoding-date " + encodingDate.str() + " ");

#if 1
    // Release file format, which has multiple Pieces
    xml_node xmlpieces{tabModel.append_child("Pieces")};
    xml_node xmlpiece{xmlpieces.append_child("Piece")};
#else
    // The early beta file format, which did not have multiple Pieces
    xml_node xmlpiece{tabModel};
#endif
    
    xml_node xmlheaders{xmlpiece.append_child("Headers")};
    
    std::vector<std::string> headers;
    GenTab::GetHeaders(piece, part, tabType, 1, headers);
    for (const auto & header : headers)
    {
        xml_node xmlheader{xmlheaders.append_child("Header")};
        xmlheader.append_child("Content").text().set(header);
    }

    xml_node xmlstaves{xmlpiece.append_child("Staves")};
    xml_node xmlstave{xmlstaves.append_child("Stave")};
    
    // body
    int staveNum{1};
    int barNum{1};
    int chordCount{0};
    std::vector<std::string> repForward;
    
    xmlstave.append_child(node_comment).set_value(" Stave " + std::to_string(staveNum) + " ");
    xml_node xmlchords{xmlstave.append_child("Chords")};
    
    for (const auto & bar : part.m_bars)
    {
        if (chordCount == 0)
        {
            // first bar on line
            xmlchords.append_child(node_comment).set_value(" Bar " + std::to_string(barNum) + " ");
            if (!repForward.empty())
            {
                for (const auto& rep : repForward)
                {
                    GenChord(rep, xmlchords);
                }
                repForward.clear();
            }
            else if (!options.m_barSupp)
            {
                GenChord("b", xmlchords);
            }
            else
            {
                GenChord("", xmlchords); // suppress initial bar line
            }
        }
        
        // time signature
        const std::string timeSignature{GenTab::GetTimeSignature(bar.m_timeSig)};
        if (!timeSignature.empty())
        {
            GenChord(timeSignature, xmlchords);
        }
        
        // chords
        NoteType gridNoteType{NoteTypeLong};
        for (const auto & chord : bar.m_chords)
        {
            ++chordCount;
            
            // flags
            std::string flag{GenTab::GetFlagInfo(options, bar.m_chords, chord, gridNoteType)};
            
            // notes
            std::vector<std::string> courses;
            GenTab::GetCourses(part, tabType, chord, flag, courses);
            GenChord(flag, courses, xmlchords);
        }
        
        // end of current bar
        ++barNum;
        xmlchords.append_child(node_comment).set_value(" Bar " + std::to_string(barNum) + " ");
        
        // Tab doesn't automatically add stave endings.  Use herustic:
        // count chords, when the threshold is reached end the stave at the end of
        // the current bar.  Except for last bar.
        const bool lineBreak{barNum < static_cast<int>(part.m_bars.size()) && chordCount > options.m_wrapThreshold};
        std::vector<std::string> barLines;
        GenTab::GetBarLines(bar, lineBreak, repForward, barLines);
        for (const auto& barLine : barLines)
        {
            GenChord(barLine, xmlchords);
        }

        if (lineBreak)
        {
            xmlstave = xmlstaves.append_child("Stave");
            ++staveNum;
            chordCount = 0;
            xmlstave.append_child(node_comment).set_value(" Stave " + std::to_string(staveNum) + " ");
            xmlchords = xmlstave.append_child("Chords");
        }
    }
    
    GenChord("e", xmlchords); // end of piece
    
    doc.save(dst, "    ");
}
    
void GenLsml::GenChord(std::string_view flag, const std::vector<std::string>& courses, xml_node xmlchords)
{
    xml_node xmlchord{xmlchords.append_child("Chord")};

    if (flag.empty() || flag == " " || flag == "x")
    {
        xmlchord.append_child("Flag");
    }
    else
    {
        xmlchord.append_child("Flag").text().set(flag);
    }
    
    size_t i{0};
    for (const auto & course : {"C1", "C2", "C3", "C4", "C5", "C6", "C7"})
    {
        if (i >= courses.size() || courses[i].empty() || courses[i] == " ")
        {
            xmlchord.append_child(course);
        }
        else
        {
            xmlchord.append_child(course).text().set(courses[i]);
        }
        ++i;
    }
}

void GenLsml::GenChord(std::string_view flag, xml_node xmlchords)
{
    const std::vector<std::string> courses;
    GenChord(flag, courses, xmlchords);
}


} // namespace luteconv
