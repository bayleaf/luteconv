#include "parserlsml.h"

#include <iostream>
#include <sstream>
#include <string>
#include <algorithm>
#include <stdexcept>

#include "pitch.h"
#include "logger.h"
#include "parsertab.h"

namespace luteconv
{

using namespace pugi;

void ParserLsml::Parse(std::istream& srcFile, const Options& options, Piece& piece)
{
    LOGGER << "Parse lsml";
    
    xml_document doc;
    const xml_parse_result result = doc.load(srcFile);
    
    if (!result)
    {
        std::ostringstream ss;
        ss << "Error: XML parsed with errors."
                    << " Description: " << result.description() 
                    << " Offset: " << result.offset;
        throw std::runtime_error(ss.str());
    }
    
    // LuteScribe's .lsml is intimately connected to the .tab format, so we convert to .tab then use the .tab parser
    std::stringstream tabImage;
    
    const xml_node xmlTabModel = doc.child("TabModel");
    if (!xmlTabModel)
    {
        throw std::runtime_error("Error: Can't find <TabModel>");
    }
    
    int target{options.m_index};

    // check to see if it is the early beta file format, that did not have multiple Pieces
    const xml_node xmlpieces = xmlTabModel.child("Pieces");
    xml_node xmlpiece{target == 0 ? xmlTabModel : xml_node()};

    if (xmlpieces != nullptr)
    {
        // find our piece
        for (xmlpiece = xmlpieces.child("Piece"); target != 0 && xmlpiece != nullptr; xmlpiece = xmlpiece.next_sibling("Piece"))
        {
            --target;
        }
    }
    else
    {
        LOGGER << "Beta file format";
    }

    if (!xmlpiece)
    {
        throw std::invalid_argument("Error: Can't find <Piece> index=" + std::to_string(options.m_index));
    }
    
    for (const xml_node xmlheader : xmlpiece.child("Headers").children("Header"))
    {
         std::string header{xmlheader.child_value("Content")};
         if (!header.empty())
         {
             const char headerBegin[] = {"{-%$"};
             const char * const headerEnd = headerBegin + sizeof(headerBegin);
             if (std::find(headerBegin, headerEnd, header[0]) == headerEnd)
             {
                 header = std::string("{").append(header).append("}");
             }

             tabImage << header << '\n';
         }
    }
    
    for (const xml_node xmlstave : xmlpiece.child("Staves").children("Stave"))
    {
        for (const xml_node xmlchord : xmlstave.child("Chords").children("Chord"))
        {
            std::string tabChord;
            
            // flags
            const std::string f{xmlchord.child_value("Flag")};
            tabChord += f.empty() ? "x" : f;
            
            // courses 1..7
            for (const auto & course : {"C1", "C2", "C3", "C4", "C5", "C6", "C7"})
            {
                const std::string c{xmlchord.child_value(course)};
                tabChord += c.empty() ? " " : c;
            }
            
            // remove trailing empty courses
            while (!tabChord.empty() && tabChord.back() == ' ')
            {
                tabChord.pop_back();
            }
            
            tabImage << tabChord << '\n';
        }

        tabImage << '\n'; // next stave
    }
    
    // parse the tab file image
    Options tabOptions{options};
    tabOptions.m_srcFormat = FormatTab;
    tabOptions.m_index = 0; // there is only a single piece in the tab image
    ParserTab parserTab;
    parserTab.Parse(tabImage, tabOptions, piece);
}

} // namespace luteconv
