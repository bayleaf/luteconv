#ifndef _CHARSET_H_
#define _CHARSET_H_

#include <string>
#include <string_view>
#include <utility>


namespace luteconv
{

class CharSet
{
public:
    /**
     * Convert text from Tab native to UTF-8
     * 
     * @param[in] text in Tab native
     * @return text in UTF-8
     */
    static std::string TabToUtf8(std::string_view text);
    
    /**
     * Convert text from UTF-8 to Tab native
     * 
     * @param[in] text in UTF-8
     * @return text in Tab native
     */
    static std::string Utf8ToTab(std::string_view text);
    
    /**
     * Convert text from ISO-8859-1 to UTF-8
     * 
     * @param[in] text in ISO-8859-1
     * @return text in UTF-8
     */
    static std::string Iso8859_1ToUtf8(std::string_view text);
    
    /**
     * Convert text from Windows CP1252 to UTF-8
     * 
     * @param[in] text in CP1252
     * @return text in UTF-8
     */
    static std::string Cp1252ToUtf8(std::string_view text);
    
    /**
     * Convert text from UTF-8 to ISO-8859-1
     * 
     * @param[in] text in UTF-8
     * @return text in ISO-8859-1
     */
    static std::string Utf8ToIso8859_1(std::string_view text);

    // UCS named characters
    enum UCS : char32_t
    {
        ReplacementCharacter = U'\uFFFD', // U+FFFD REPLACEMENT CHARACTER
    };
    
private:
    static const std::pair<const char* const, const char32_t> m_tab2CodePoint[];
    static const char32_t m_cp1252ToCodePoint[];

    /**
     * Get next code point from UTF-8
     * Any bad UTF-8 returns ReplacementCharacter
     * 
     * @param[in] text
     * @param[in, out] i index advanced after code point
     * @return code point
     */
    static char32_t NextCodePoint(std::string_view text, size_t& i);
    
    /**
     * Append code point as UTF-8
     * 
     * @param[in] codePoint
     * @param[in, out] text
     */
    static void AppendCodePoint(char32_t codePoint, std::string& text);                                    
};

} // namespace luteconv

#endif // _CHARSET_H_
