#include "parsermusicxml.h"

#include <iostream>
#include <sstream>
#include <string>
#include <algorithm>
#include <stdexcept>

#include "musicxml.h"
#include "pitch.h"
#include "logger.h"

namespace luteconv
{

using namespace pugi;

void ParserMusicXml::Parse(std::string_view filename, void *contents, size_t size, const Options &options,
        Piece &piece)
{
    xml_document doc;
    const xml_parse_result result{doc.load_buffer_inplace(contents, size)};
    Parse(filename, doc, result, options, piece);
}

void ParserMusicXml::Parse(std::istream& srcFile, const Options &options, Piece &piece)
{
    xml_document doc;
    const xml_parse_result result{doc.load(srcFile)};
    Parse(options.m_srcFilename, doc, result, options, piece);
}

void ParserMusicXml::Parse(std::string_view filename, const xml_document &doc, const xml_parse_result &result,
        const Options &options, Piece &piece)
{
    LOGGER << "Parse musicxml";

    if (!result)
    {
        std::ostringstream ss;
        ss << "Error: XML [" << filename << "] parsed with errors." << " Description: " << result.description()
                << " Offset: " << result.offset;
        throw std::runtime_error(ss.str());
    }

    if (options.m_index != 0)
    {
        throw std::invalid_argument("Error: Can't find section, index=" + std::to_string(options.m_index));
    }

    const xml_node xmlscorePartwise{doc.child("score-partwise")};
    if (xmlscorePartwise == nullptr)
    {
        throw std::runtime_error("Error: Can't find <score-partwise>");
    }

    piece.m_title = xmlscorePartwise.child("work").child("work-title").text().as_string();
    piece.m_composer = xmlscorePartwise.child("identification").find_child_by_attribute("creator", "type", "composer")
            .text().as_string();

    piece.m_copyright = xmlscorePartwise.child("identification").child("rights").text().as_string();
    piece.m_copyrightEnabled = !piece.m_copyright.empty();

    ParseCredit(xmlscorePartwise, piece);
    
    // parts-list
    size_t numParts{1};
    for (const xml_node xmlscorePart : xmlscorePartwise.child("part-list").children("score-part"))
    {
        piece.m_parts.resize(numParts++);
        Part& part{piece.m_parts.back()};
        part.m_partName = xmlscorePart.child("part-name").text().as_string();
    }
    
    size_t partIdx{0};
    for (const xml_node xmlpart : xmlscorePartwise.children("part"))
    {
        Part& part{piece.m_parts.at(partIdx)};
        LOGGER << "part: " << partIdx + 1 << " name: " << part.m_partName << " id: " << xmlpart.attribute("id").value();
        const int numStaves{xmlpart.child("measure").child("attributes").child("staves").text().as_int()};
        if (numStaves > 1)
        {
            LOGGER << "Warning: ignoring part with " << numStaves << " staves";
            piece.m_parts.erase(piece.m_parts.begin() + static_cast<std::ptrdiff_t>(partIdx));
            continue;
        }
        ParsePart(xmlpart, options, part);
        ++partIdx;
    }
}

void ParserMusicXml::ParsePart(xml_node xmlpart, const Options &options, Part& part)
{
    // tablature or CMN?
    const std::string clefSign{xmlpart.child("measure").child("attributes").child("clef").child("sign").text().as_string()};
    if (clefSign != "TAB")
    {
        part.m_tabType = TabCmn;
        // staff lines
        part.m_staffLines = xmlpart.child("measure").child("attributes").child("staff-lines").text().as_int(5);
    }
    else
    {
        ParseStaffTuning(xmlpart, part);
    }

    int divisions{0};
    for (const xml_node xmlmeasure : xmlpart.children("measure"))
    {
        const int divs{xmlmeasure.child("attributes").child("divisions").text().as_int()};
        if (divs != 0)
        {
            divisions = divs;
        }
        
        part.m_bars.emplace_back();

        if (part.m_tabType == TabCmn)
        {
            ParseClef(xmlmeasure, part);
            ParseKey(xmlmeasure, part);
        }
        ParseTimeSignature(xmlmeasure, part);
        ParseBarline(xmlmeasure, part);

        Bar& bar{part.m_bars.back()};
        bool firstNote{true};
        int stamp{0}; // chord timestamp from start of the bar in divs
        bool hearVoices{false}; // do we need to merge voices?

        for (const xml_node xmlchild : xmlmeasure.children())
        {
            const std::string childName{xmlchild.name()};
            if (childName == "note")
            {
                const xml_node xmlnote{xmlchild};

                // do we need a new chord?
                if (xmlnote.child("chord") == nullptr)
                {
                    bar.m_chords.emplace_back();
                    firstNote = true;
                }
    
                Chord& chord{bar.m_chords.back()};
    
                const xml_node xmlnotations{xmlnote.child("notations")};
                if (firstNote)
                {
                    const std::string xmltype{xmlnote.child("type").text().as_string()};
                    for (int i{0}; MusicXml::noteType[i] != nullptr; ++i)
                    {
                        if (xmltype == MusicXml::noteType[i])
                        {
                            chord.m_noteType = static_cast<NoteType>(i);
                            break;
                        }
                    }
                    
                    chord.m_stamp = stamp;
                    stamp += xmlnote.child("duration").text().as_int(); // next stamp
    
                    chord.m_dotted = !!xmlnote.child("dot");
                    chord.m_fermata = !!xmlnotations.child("fermata");
                    const std::string tiedType{xmlnotations.child("tied").attribute("type").value()};
                    chord.m_tie = (tiedType == "start");

                    // beam
                    const std::string beamValue{xmlnote.find_child_by_attribute("beam", "number", "1").text().as_string()};
                    chord.m_grid = (beamValue == "begin") ? GridStart : (beamValue == "continue") ? GridMid :
                                   (beamValue == "end") ? GridEnd : GridNone;
                    
                    chord.m_triplet = (xmlnotations.find_child_by_attribute("tuplet", "type", "start") != nullptr &&
                                       xmlnote.child("time-modification").child("actual-notes").text().as_int() == 3 &&
                                       xmlnote.child("time-modification").child("normal-notes").text().as_int() == 2);
    
                    firstNote = false;
                }

                if (part.m_tabType == TabCmn)
                {
                    const xml_node xmlpitch{xmlnote.child("pitch")};
                    if (xmlpitch != nullptr)
                    {
                        chord.m_notes.emplace_back();
                        Note& note{chord.m_notes.back()};
                        note.m_pitch = Pitch((xmlpitch.child("step").text().as_string())[0],
                                xmlpitch.child("alter").text().as_int(),
                                xmlpitch.child("octave").text().as_int());
                        const std::string accidental{xmlnote.child("accidental").text().as_string()};
                        if (accidental == "flat")
                        {
                            note.m_accidental = AccFlat;
                        }
                        else if (accidental == "natural")
                        {
                            note.m_accidental = AccNatural;
                        }
                        else if (accidental == "sharp")
                        {
                            note.m_accidental = AccSharp;
                        }
                        
                        for (const xml_node xmllyric : xmlnote.children("lyric"))
                        {
                            const std::string wordpos{xmllyric.child("syllabic").text().as_string()};
                            const Syllabic syllabic{wordpos == "begin" ? SylBegin : wordpos == "middle" ? SylMiddle : wordpos == "end" ? SylEnd : SylSingle}; 
                            
                            chord.m_lyrics.emplace_back(xmllyric.child("text").text().as_string(), syllabic);
                        }
                    }
                }
                else
                {
                    const xml_node xmlornaments{xmlnotations.child("ornaments")};
                    const xml_node xmltechnical{xmlnotations.child("technical")};
                    if (xmltechnical != nullptr)
                    {
                        chord.m_notes.emplace_back();
                        Note& note{chord.m_notes.back()};
                        // string & fret
                        note.m_course = xmltechnical.child("string").text().as_int();
                        note.m_fret = xmltechnical.child("fret").text().as_int();
                        
                        // The tuning table maybe incomplete and only cover the staff, add missing courses from note's pitch
                        const xml_node xmlpitch{xmlnote.child("pitch")};
                        if (xmlpitch != nullptr)
                        {
                            const Pitch stringPitch{Pitch((xmlpitch.child("step").text().as_string())[0],
                                    xmlpitch.child("alter").text().as_int(),
                                    xmlpitch.child("octave").text().as_int()) - note.m_fret};
                            
                            if (part.m_tuning.size() < static_cast<size_t>(note.m_course))
                            {
                                part.m_tuning.resize(note.m_course);
                            }
                            
                            if (part.m_tuning[note.m_course - 1].Step() != 0)
                            {
                                if (part.m_tuning[note.m_course - 1] != stringPitch)
                                {
                                    const long int measureNo{std::distance(part.m_bars.data(), &bar) + 1};
                                    LOGGER << "measure " << measureNo << ": inconsistent string tuning";
                                }
                            }
                            else
                            {
                                part.m_tuning[note.m_course - 1] = stringPitch;
                            }
                        }
        
                        // fingering
                        const xml_node xmlfingering{xmltechnical.child("fingering")};
                        if (xmlfingering != nullptr)
                        {
                            note.m_leftFingering = static_cast<Fingering>(xmlfingering.text().as_int());
                        }
                        
                        // ornaments
                        if (xmlornaments != nullptr)
                        {
                            ParseOrnaments(xmlornaments, note);
                        }
        
                        // pluck
                        const std::string pluck{xmltechnical.child("pluck").text().as_string()};
                        if (!pluck.empty())
                        {
                            for (int i{1}; MusicXml::pluck[i] != nullptr; ++i)
                            {
                                if (pluck == MusicXml::pluck[i])
                                {
                                    note.m_rightFingering = static_cast<Fingering>(i);
                                    break;
                                }
                            }
                        }
        
                        // slur
                        const xml_node xmlslur{xmlnotations.child("slur")};
                        if (xmlslur != nullptr)
                        {
                            const std::string slurType{xmlslur.attribute("type").value()};
                            if (slurType != "continue")
                            {
                                note.m_connector.m_function = FunSlur;
                                note.m_connector.m_endPoint = (slurType == "start") ? EndPointStart : EndPointStop;
                                note.m_connector.m_number = xmlslur.attribute("number").as_int();
        
                                const std::string slurPlacement{xmlslur.attribute("placement").value()};
                                note.m_connector.m_compass = (slurPlacement == "above") ? North : South;
        
                                const std::string slurOrientation{xmlslur.attribute("orientation").value()};
                                note.m_connector.m_curve = (slurOrientation == "over") ? CurveOver : CurveUnder;
                            }
                        }
                    }
                }
            }
            else if (childName == "backup")
            {
                // move back in time for another voice
                stamp -= xmlchild.child("duration").text().as_int();
                hearVoices = true;
            }
            else if (childName == "forward")
            {
                // move forward in time for another voice
                stamp += xmlchild.child("duration").text().as_int();
                hearVoices = true;
            }
        }
        
        if (divisions > 0 && hearVoices)
        {
            bar.MergeVoices(divisions);
        }
    }

    if (part.m_tabType != TabCmn)
    {
        LOGGER << "Source tuning " << Pitch::GetTuning(part.m_tuning);
        part.SetTuning(options);
    }
}

void ParserMusicXml::ParseStaffTuning(xml_node xmlpart, Part& part)
{
    // tuning
    const xml_node xmlstaffDetails{xmlpart.child("measure").child("attributes").child("staff-details")};
    if (xmlstaffDetails != nullptr)
    {
        // staff lines
        part.m_staffLines = xmlstaffDetails.child("staff-lines").text().as_int(Course6);

        // Specifies tuning of the staff - implicitly gives tuning of the lute.
        // Except may only give 6 courses.  Worst still MuseScore import of MusicXML 
        // only imports 6 courses, and its export is broken: always puts lowest
        // course on line 1, so with more than 6 courses puts the highest courses
        // on line 7 ...
        // The following attempts to compensate.

        // find maximum and minimum staff lines.
        int maxLine{1};
        int minLine{part.m_staffLines};
        for (const xml_node xmlstaffTuning : xmlstaffDetails.children("staff-tuning"))
        {
            const int line{xmlstaffTuning.attribute("line").as_int()};
            maxLine = std::max(maxLine, line);
            minLine = std::min(minLine, line);
        }

        const int numCourses{maxLine - minLine + 1};
        part.m_tuning.resize(numCourses);
        for (const xml_node xmlstaffTuning : xmlstaffDetails.children("staff-tuning"))
        {
            const int line{xmlstaffTuning.attribute("line").as_int()};
            part.m_tuning[maxLine - line] = Pitch((xmlstaffTuning.child("tuning-step").text().as_string())[0],
                    xmlstaffTuning.child("tuning-alter").text().as_int(),
                    xmlstaffTuning.child("tuning-octave").text().as_int());
        }

        // if source is in italian tablature then will need to reverse part.m_tuning.
        // Beware of re-entrant tuning!  Look at all pairs to get most common ordering.
        int direction{0};
        for (size_t i{0}; i < part.m_tuning.size() - 1; ++i)
        {
            if (part.m_tuning[i].Midi() >= part.m_tuning[i + 1].Midi())
            {
                ++direction;
            }
            else
            {
                --direction;
            }
        }
        if (direction < 0)
        {
            // italian tablature, reverse order
            std::reverse(part.m_tuning.begin(), part.m_tuning.end());
            part.m_tabType = TabItalian;
        }
        else
        {
            const std::string showFrets{xmlstaffDetails.attribute("show-frets").value()};
            if (showFrets == "letters")
            {
                part.m_tabType = TabFrench;
            }
            else
            {
                part.m_tabType = TabSpanish;
            }
        }

        LOGGER << "Staff tuning " << Pitch::GetTuning(part.m_tuning);
    }
    else
    {
        throw std::runtime_error("Can't find <part><measure><attributes><staff-details>");
    }
}

void ParserMusicXml::ParseBarline(xml_node xmlmeasure, Part &part)
{
    Bar& bar{part.m_bars.back()};

    const xml_node xmlleftBarline{xmlmeasure.find_child_by_attribute("barline", "location", "left")};
    if (xmlleftBarline != nullptr)
    {
        bar.m_volta = xmlleftBarline.child("ending").attribute("number").as_int();
        
        if (xmlleftBarline.find_child_by_attribute("repeat", "direction", "forward") != nullptr)
        {
            // update previous bar
            const long int ourIndex{std::distance(part.m_bars.data(), &bar)};
            if (ourIndex > 0)
            {
                Bar& prev{part.m_bars[ourIndex - 1]};
                if (prev.m_repeat == RepNone)
                {
                    prev.m_repeat = RepForward;
                }
                else if (prev.m_repeat == RepBackward)
                {
                    prev.m_repeat = RepJanus;
                }
            }
        }
    }

    const xml_node xmlrightBarline{xmlmeasure.find_child_by_attribute("barline", "location", "right")};
    if (xmlrightBarline != nullptr)
    {
        const std::string barStyle{xmlrightBarline.child("bar-style").text().as_string()};
        if (!barStyle.empty())
        {
            for (int i{0}; MusicXml::barStyle[i] != nullptr; ++i)
            {
                if (barStyle == MusicXml::barStyle[i])
                {
                    bar.m_barStyle = static_cast<BarStyle>(i);
                    break;
                }
            }
        }

        bar.m_fermata = !!xmlrightBarline.child("fermata");

        if (xmlrightBarline.find_child_by_attribute("repeat", "direction", "backward") != nullptr)
        {
            bar.m_repeat = RepBackward;
        }
    }
}

void ParserMusicXml::ParseClef(xml_node xmlmeasure, Part &part)
{
    Bar& bar{part.m_bars.back()};
    const xml_node xmlclef{xmlmeasure.child("attributes").child("clef")};
    if (xmlclef != nullptr)
    {
        const std::string sign{xmlclef.child("sign").text().as_string()};
        const int octaveChange{xmlclef.child("clef-octave-change").text().as_int()};
        if (sign == "TAB")
        {
            bar.m_clef = ClefTab;
        }
        else if (sign == "F")
        {
            bar.m_clef = ClefF;
        }
        else if (sign == "G")
        {
            if (octaveChange == 1)
            {
                bar.m_clef = ClefG8a;
            }
            else if (octaveChange == -1)
            {
                bar.m_clef = ClefG8b;
            }
            else
            {
                bar.m_clef = ClefG;
            }
        }
    }
}

void ParserMusicXml::ParseKey(xml_node xmlmeasure, Part &part)
{
    Bar& bar{part.m_bars.back()};
    const xml_node xmlfifths{xmlmeasure.child("attributes").child("key").child("fifths")};
    if (xmlfifths != nullptr)
    {
        bar.m_key = static_cast<Key>(xmlfifths.text().as_int());
    }
}

void ParserMusicXml::ParseTimeSignature(xml_node xmlmeasure, Part &part)
{
    Bar& bar{part.m_bars.back()};
    const xml_node xmltime{xmlmeasure.child("attributes").child("time")};
    if (xmltime != nullptr)
    {
        const std::string timeSymbol{xmltime.attribute("symbol").value()};
        if (!timeSymbol.empty())
        {
            for (int i{1}; MusicXml::timeSymbol[i] != nullptr; ++i)
            {
                if (timeSymbol == MusicXml::timeSymbol[i])
                {
                    bar.m_timeSig.m_timeSymbol = static_cast<TimeSymbol>(i);
                    break;
                }
            }
        }
        else
        {
            bar.m_timeSig.m_timeSymbol = TimeSyNormal;
        }
        bar.m_timeSig.m_beats = xmltime.child("beats").text().as_int();
        bar.m_timeSig.m_beatType = xmltime.child("beat-type").text().as_int();
    }
}

void ParserMusicXml::ParseCredit(xml_node xmlscorePartwise, Piece &piece)
{
    for (const xml_node xmlcredit : xmlscorePartwise.children("credit"))
    {
        for (const xml_node xmlcreditWords : xmlcredit.children("credit-words"))
        {
            const std::string creditWords{xmlcreditWords.text().as_string()};
            if (!creditWords.empty())
            {
                piece.m_credits.emplace_back(creditWords);
            }
        }
    }
}

void ParserMusicXml::ParseOrnaments(xml_node xmlornaments, Note &note)
{
    for (const xml_node xmlotherOrnament : xmlornaments.children("other-ornament"))
    {
        const std::string ornamentText{xmlotherOrnament.text().as_string()};
        if (ornamentText.empty())
        {
            continue;
        }
        
        for (int i{1}; MusicXml::ornament[i] != nullptr; ++i)
        {
            if (ornamentText == MusicXml::ornament[i])
            {
                if (xmlotherOrnament.attribute("default-x").as_int() > 0)
                {
                    note.m_rightOrnament = static_cast<Ornament>(i);
                }
                else
                {
                    note.m_leftOrnament = static_cast<Ornament>(i);
                }
                break;
            }
        }
    }
}

} // namespace luteconv
