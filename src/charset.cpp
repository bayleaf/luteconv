#include "charset.h"

#include <cstring>

namespace luteconv
{

// Tab uses TeX escape sequences (with minor variations).
// Abc uses ISO-8895-1 but will also allow TeX escape sequences.
//
// From TabManual.pdf
// ------------------
//
// Special letters:
// ƒ    \sl
// ß    \ss
// æ    \ae
// Æ    \AE
// œ    \oe
// Œ    \OE
// ø    \oo
// Ø    \OO
//
// Diacritical marks:
// é    accent aigu     \’ + [letter]
// è    accent grave    \` + [letter]
// â    circumflex      \^ + [letter]
// ü    umlaut          \” + [letter]
//      macron          \= + [letter]
//      breve           \u + [letter]
//      double acute    \H + [letter]
// ñ    tilde           \~ + [letter]
// ç    cedilla         \c <space> + [letter] Note the space!
//      dot above       \. + [letter]
//      caron           \v + [letter]
//
// Punctuation (some of these may entail the use of extra spaces to avoid crowding):
// ¿    ?` or >
// ¡    !` or < 
// [    \[
// ]    \]
// {    \{
// }    \}
// 
// From tab.txt
// ------------
//
// \`a is a backslash over the letter a
//   letter takes a's place - \`letter
// \'a puts a slash / over the letter a ( or whatever 
// \^a is a circumflex over the letter a
// \"a is two dots - umlaut over the letter a
// \~a is the wiggle over the letter a
// \=a is a flat horizontal bar over the letter a
// \.a is a dot above the letter a
// \ua is breve accent
// \va is a v above the letter
// \ce is the cedilla with an e - note the space that i
//    used to be neededais no longer needed !
// \sl is a long s
// \{ is {, \} is } \) is ), \( is (
// \[ is [, \] is [
// \! and \? are turned upside down
// \ss = German ss \ae = ae \AE = AE \oe = oe \OE = OE 
// \oo = o with line through it, \OO the same
// \wt is a tie between letters 
//
// \C centers the title
// ?` - upside down ? but \? is preferred
// !` upside down !   but \! is prefrred

// Map Tab native escape sequences to Unicode code points
const std::pair<const char* const, const char32_t> CharSet::m_tab2CodePoint[]
{
    //                     // U+00A0      c2 a0   NO-BREAK SPACE
    {R"(\!)",  U'\u00A1'}, // U+00A1  ¡   c2 a1   INVERTED EXCLAMATION MARK
    {R"(\?)",  U'\u00BF'}, // U+00BF  ¿   c2 bf   INVERTED QUESTION MARK
    {R"(!`)",  U'\u00A1'}, // U+00A1  ¡   c2 a1   INVERTED EXCLAMATION MARK
    {R"(?`)",  U'\u00BF'}, // U+00BF  ¿   c2 bf   INVERTED QUESTION MARK
    {R"(\`A)", U'\u00C0'}, // U+00C0  À   c3 80   LATIN CAPITAL LETTER A WITH GRAVE
    {R"(\'A)", U'\u00C1'}, // U+00C1  Á   c3 81   LATIN CAPITAL LETTER A WITH ACUTE
    {R"(\^A)", U'\u00C2'}, // U+00C2  Â   c3 82   LATIN CAPITAL LETTER A WITH CIRCUMFLEX
    {R"(\~A)", U'\u00C3'}, // U+00C3  Ã   c3 83   LATIN CAPITAL LETTER A WITH TILDE
    {R"(\"A)", U'\u00C4'}, // U+00C4  Ä   c3 84   LATIN CAPITAL LETTER A WITH DIAERESIS
    //                     // U+00C5  Å   c3 85   LATIN CAPITAL LETTER A WITH RING ABOVE
    {R"(\AE)", U'\u00C6'}, // U+00C6  Æ   c3 86   LATIN CAPITAL LETTER AE
    {R"(\cC)", U'\u00C7'}, // U+00C7  Ç   c3 87   LATIN CAPITAL LETTER C WITH CEDILLA
    {R"(\c C)",U'\u00C7'}, // U+00C7  Ç   c3 87   LATIN CAPITAL LETTER C WITH CEDILLA
    {R"(\`E)", U'\u00C8'}, // U+00C8  È   c3 88   LATIN CAPITAL LETTER E WITH GRAVE
    {R"(\'E)", U'\u00C9'}, // U+00C9  É   c3 89   LATIN CAPITAL LETTER E WITH ACUTE
    {R"(\^E)", U'\u00CA'}, // U+00CA  Ê   c3 8a   LATIN CAPITAL LETTER E WITH CIRCUMFLEX
    {R"(\"E)", U'\u00CB'}, // U+00CB  Ë   c3 8b   LATIN CAPITAL LETTER E WITH DIAERESIS
    {R"(\`I)", U'\u00CC'}, // U+00CC  Ì   c3 8c   LATIN CAPITAL LETTER I WITH GRAVE
    {R"(\'I)", U'\u00CD'}, // U+00CD  Í   c3 8d   LATIN CAPITAL LETTER I WITH ACUTE
    {R"(\^I)", U'\u00CE'}, // U+00CE  Î   c3 8e   LATIN CAPITAL LETTER I WITH CIRCUMFLEX
    {R"(\"I)", U'\u00CF'}, // U+00CF  Ï   c3 8f   LATIN CAPITAL LETTER I WITH DIAERESIS
    //                     // U+00D0  Ð   c3 90   LATIN CAPITAL LETTER ETH
    {R"(\~N)", U'\u00D1'}, // U+00D1  Ñ   c3 91   LATIN CAPITAL LETTER N WITH TILDE
    {R"(\`O)", U'\u00D2'}, // U+00D2  Ò   c3 92   LATIN CAPITAL LETTER O WITH GRAVE
    {R"(\'O)", U'\u00D3'}, // U+00D3  Ó   c3 93   LATIN CAPITAL LETTER O WITH ACUTE
    {R"(\^O)", U'\u00D4'}, // U+00D4  Ô   c3 94   LATIN CAPITAL LETTER O WITH CIRCUMFLEX
    {R"(\~O)", U'\u00D5'}, // U+00D5  Õ   c3 95   LATIN CAPITAL LETTER O WITH TILDE
    {R"(\"O)", U'\u00D6'}, // U+00D6  Ö   c3 96   LATIN CAPITAL LETTER O WITH DIAERESIS
    //                     // U+00D7  ×   c3 97   MULTIPLICATION SIGN
    {R"(\OO)", U'\u00D8'}, // U+00D8  Ø   c3 98   LATIN CAPITAL LETTER O WITH STROKE (Tab)
    //{R"(\O)",  U'\u00D8'}, // U+00D8  Ø   c3 98   LATIN CAPITAL LETTER O WITH STROKE (TeX)
    {R"(\`U)", U'\u00D9'}, // U+00D9  Ù   c3 99   LATIN CAPITAL LETTER U WITH GRAVE
    {R"(\'U)", U'\u00DA'}, // U+00DA  Ú   c3 9a   LATIN CAPITAL LETTER U WITH ACUTE
    {R"(\^U)", U'\u00DB'}, // U+00DB  Û   c3 9b   LATIN CAPITAL LETTER U WITH CIRCUMFLEX
    {R"(\"U)", U'\u00DC'}, // U+00DC  Ü   c3 9c   LATIN CAPITAL LETTER U WITH DIAERESIS
    {R"(\'Y)", U'\u00DD'}, // U+00DD  Ý   c3 9d   LATIN CAPITAL LETTER Y WITH ACUTE
    //                     // U+00DE  Þ   c3 9e   LATIN CAPITAL LETTER THORN
    {R"(\ss)", U'\u00DF'}, // U+00DF  ß   c3 9f   LATIN SMALL LETTER SHARP S
    {R"(\`a)", U'\u00E0'}, // U+00E0  à   c3 a0   LATIN SMALL LETTER A WITH GRAVE
    {R"(\'a)", U'\u00E1'}, // U+00E1  á   c3 a1   LATIN SMALL LETTER A WITH ACUTE
    {R"(\^a)", U'\u00E2'}, // U+00E2  â   c3 a2   LATIN SMALL LETTER A WITH CIRCUMFLEX
    {R"(\~a)", U'\u00E3'}, // U+00E3  ã   c3 a3   LATIN SMALL LETTER A WITH TILDE
    {R"(\"a)", U'\u00E4'}, // U+00E4  ä   c3 a4   LATIN SMALL LETTER A WITH DIAERESIS
    //                     // U+00E5  å   c3 a5   LATIN SMALL LETTER A WITH RING ABOVE
    {R"(\ae)", U'\u00E6'}, // U+00E6  æ   c3 a6   LATIN SMALL LETTER AE
    {R"(\cc)", U'\u00E7'}, // U+00E7  ç   c3 a7   LATIN SMALL LETTER C WITH CEDILLA
    {R"(\c c)",U'\u00E7'}, // U+00E7  ç   c3 a7   LATIN SMALL LETTER C WITH CEDILLA
    {R"(\`e)", U'\u00E8'}, // U+00E8  è   c3 a8   LATIN SMALL LETTER E WITH GRAVE
    {R"(\'e)", U'\u00E9'}, // U+00E9  é   c3 a9   LATIN SMALL LETTER E WITH ACUTE
    {R"(\^e)", U'\u00EA'}, // U+00EA  ê   c3 aa   LATIN SMALL LETTER E WITH CIRCUMFLEX
    {R"(\"e)", U'\u00EB'}, // U+00EB  ë   c3 ab   LATIN SMALL LETTER E WITH DIAERESIS
    {R"(\`i)", U'\u00EC'}, // U+00EC  ì   c3 ac   LATIN SMALL LETTER I WITH GRAVE
    {R"(\'i)", U'\u00ED'}, // U+00ED  í   c3 ad   LATIN SMALL LETTER I WITH ACUTE
    {R"(\^i)", U'\u00EE'}, // U+00EE  î   c3 ae   LATIN SMALL LETTER I WITH CIRCUMFLEX
    {R"(\"i)", U'\u00EF'}, // U+00EF  ï   c3 af   LATIN SMALL LETTER I WITH DIAERESIS
    //                     // U+00F0  ð   c3 b0   LATIN SMALL LETTER ETH
    {R"(\~n)", U'\u00F1'}, // U+00F1  ñ   c3 b1   LATIN SMALL LETTER N WITH TILDE
    {R"(\`o)", U'\u00F2'}, // U+00F2  ò   c3 b2   LATIN SMALL LETTER O WITH GRAVE
    {R"(\'o)", U'\u00F3'}, // U+00F3  ó   c3 b3   LATIN SMALL LETTER O WITH ACUTE
    {R"(\^o)", U'\u00F4'}, // U+00F4  ô   c3 b4   LATIN SMALL LETTER O WITH CIRCUMFLEX
    {R"(\~o)", U'\u00F5'}, // U+00F5  õ   c3 b5   LATIN SMALL LETTER O WITH TILDE
    {R"(\"o)", U'\u00F6'}, // U+00F6  ö   c3 b6   LATIN SMALL LETTER O WITH DIAERESIS
    //                     // U+00F7  ÷   c3 b7   DIVISION SIGN
    {R"(\oo)", U'\u00F8'}, // U+00F8  ø   c3 b8   LATIN SMALL LETTER O WITH STROKE (tab)
    //{R"(\o)",  U'\u00F8'}, // U+00F8  ø   c3 b8   LATIN SMALL LETTER O WITH STROKE (TeX)
    {R"(\`u)", U'\u00F9'}, // U+00F9  ù   c3 b9   LATIN SMALL LETTER U WITH GRAVE
    {R"(\'u)", U'\u00FA'}, // U+00FA  ú   c3 ba   LATIN SMALL LETTER U WITH ACUTE
    {R"(\^u)", U'\u00FB'}, // U+00FB  û   c3 bb   LATIN SMALL LETTER U WITH CIRCUMFLEX
    {R"(\"u)", U'\u00FC'}, // U+00FC  ü   c3 bc   LATIN SMALL LETTER U WITH DIAERESIS
    {R"(\'y)", U'\u00FD'}, // U+00FD  ý   c3 bd   LATIN SMALL LETTER Y WITH ACUTE
    //                     // U+00FE  þ   c3 be   LATIN SMALL LETTER THORN
    {R"(\"y)", U'\u00FF'}, // U+00FF  ÿ   c3 bf   LATIN SMALL LETTER Y WITH DIAERESIS
    
    {R"(\=A)", U'\u0100'}, // U+0100  Ā   c4 80   LATIN CAPITAL LETTER A WITH MACRON
    {R"(\=a)", U'\u0101'}, // U+0101  ā   c4 81   LATIN SMALL LETTER A WITH MACRON
    {R"(\uA)", U'\u0102'}, // U+0102  Ă   c4 82   LATIN CAPITAL LETTER A WITH BREVE
    {R"(\ua)", U'\u0103'}, // U+0103  ă   c4 83   LATIN SMALL LETTER A WITH BREVE
    
    {R"(\=E)", U'\u0112'}, // U+0112  Ē   c4 92   LATIN CAPITAL LETTER E WITH MACRON
    {R"(\=e)", U'\u0113'}, // U+0113  ē   c4 93   LATIN SMALL LETTER E WITH MACRON
    {R"(\uE)", U'\u0114'}, // U+0114  Ĕ   c4 94   LATIN CAPITAL LETTER E WITH BREVE
    {R"(\ue)", U'\u0115'}, // U+0115  ĕ   c4 95   LATIN SMALL LETTER E WITH BREVE
    
    {R"(\vE)", U'\u011A'}, // U+011A  Ě   c4 9a   LATIN CAPITAL LETTER E WITH CARON
    {R"(\ve)", U'\u011B'}, // U+011B  ě   c4 9b   LATIN SMALL LETTER E WITH CARON
    
    {R"(\=I)", U'\u012A'}, // U+012A  Ī   c4 aa   LATIN CAPITAL LETTER I WITH MACRON
    {R"(\=i)", U'\u012B'}, // U+012B  ī   c4 ab   LATIN SMALL LETTER I WITH MACRON
    {R"(\uI)", U'\u012C'}, // U+012C  Ĭ   c4 ac   LATIN CAPITAL LETTER I WITH BREVE
    {R"(\ui)", U'\u012D'}, // U+012D  ĭ   c4 ad   LATIN SMALL LETTER I WITH BREVE
    
    {R"(\=O)", U'\u014C'}, // U+014C  Ō   c5 8c   LATIN CAPITAL LETTER O WITH MACRON
    {R"(\=o)", U'\u014D'}, // U+014D  ō   c5 8d   LATIN SMALL LETTER O WITH MACRON
    {R"(\uO)", U'\u014E'}, // U+014E  Ŏ   c5 8e   LATIN CAPITAL LETTER O WITH BREVE
    {R"(\uo)", U'\u014F'}, // U+014F  ŏ   c5 8f   LATIN SMALL LETTER O WITH BREVE
    {R"(\HO)", U'\u0150'}, // U+0150  Ő   c5 90   LATIN CAPITAL LETTER O WITH DOUBLE ACUTE
    {R"(\Ho)", U'\u0151'}, // U+0151  ő   c5 91   LATIN SMALL LETTER O WITH DOUBLE ACUTE
    {R"(\OE)", U'\u0152'}, // U+0152  Œ   c5 92   LATIN CAPITAL LIGATURE OE
    {R"(\oe)", U'\u0153'}, // U+0153  œ   c5 93   LATIN SMALL LIGATURE OE
    
    {R"(\vS)", U'\u0160'}, // U+0160  Š   c5 a0   LATIN CAPITAL LETTER S WITH CARON
    {R"(\vs)", U'\u0161'}, // U+0161  š   c5 a1   LATIN SMALL LETTER S WITH CARON

    {R"(\=U)", U'\u016A'}, // U+016A  Ū   c5 aa   LATIN CAPITAL LETTER U WITH MACRON
    {R"(\=u)", U'\u016B'}, // U+016B  ū   c5 ab   LATIN SMALL LETTER U WITH MACRON
    {R"(\uU)", U'\u016C'}, // U+016C  Ŭ   c5 ac   LATIN CAPITAL LETTER U WITH BREVE
    {R"(\uu)", U'\u016D'}, // U+016D  ŭ   c5 ad   LATIN SMALL LETTER U WITH BREVE
    
    {R"(\HU)", U'\u0170'}, // U+0170  Ű   c5 b0   LATIN CAPITAL LETTER U WITH DOUBLE ACUTE
    {R"(\Hu)", U'\u0171'}, // U+0171  ű   c5 b1   LATIN SMALL LETTER U WITH DOUBLE ACUTE
    
    {R"(\"Y)", U'\u0178'}, // U+0178  Ÿ   c5 b8   LATIN CAPITAL LETTER Y WITH DIAERESIS
    
    {R"(\.Z)", U'\u017B'}, // U+017B  Ż   c5 bb   LATIN CAPITAL LETTER Z WITH DOT ABOVE
    {R"(\.z)", U'\u017C'}, // U+017C  ż   c5 bc   LATIN SMALL LETTER Z WITH DOT ABOVE
    {R"(\vZ)", U'\u017D'}, // U+017D  Ž   c5 bd   LATIN CAPITAL LETTER Z WITH CARON
    {R"(\vz)", U'\u017E'}, // U+017E  ž   c5 be   LATIN SMALL LETTER Z WITH CARON
    {R"(\sl)", U'\u017F'}, // U+017F  ſ   c5 b5   LATIN SMALL LETTER LONG S
    
    {R"(\vA)", U'\u01CD'}, // U+01CD  Ǎ   c7 8d   LATIN CAPITAL LETTER A WITH CARON
    {R"(\va)", U'\u01CE'}, // U+01CE  ǎ   c7 8e   LATIN SMALL LETTER A WITH CARON
    {R"(\vI)", U'\u01CF'}, // U+01CF  Ǐ   c7 8f   LATIN CAPITAL LETTER I WITH CARON
    {R"(\vi)", U'\u01D0'}, // U+01D0  ǐ   c7 90   LATIN SMALL LETTER I WITH CARON
    {R"(\vO)", U'\u01D1'}, // U+01D1  Ǒ   c7 91   LATIN CAPITAL LETTER O WITH CARON
    {R"(\vo)", U'\u01D2'}, // U+01D2  ǒ   c7 92   LATIN SMALL LETTER O WITH CARON
    {R"(\vU)", U'\u01D3'}, // U+01D3  Ǔ   c7 93   LATIN CAPITAL LETTER U WITH CARON
    {R"(\vu)", U'\u01D4'}, // U+01D4  ǔ   c7 94   LATIN SMALL LETTER U WITH CARON
    
    {R"(\()", U'('},
    {R"(\))", U')'},
    {R"(\[)", U'['},
    {R"(\])", U']'},
    {R"(\{)", U'{'},
    {R"(\})", U'}'},
    
    {nullptr, U'\0'},
};

std::string CharSet::TabToUtf8(std::string_view text)
{
    // tab uses TeX escape sequences, not 8-bit characters, but when seen assume ISO-8859-1.
    // abc uses ISO-8859-1, but also allows TeX escape sequences.
    
    const std::string txt{Iso8859_1ToUtf8(text)};
            
    std::string result;
    for (size_t i{0}; i < txt.size(); ++i)
    {
        if (txt[i] == '\\' || txt[i] == '!' || txt[i] == '?')
        {
            // escape sequences are 2, 3 or 4 bytes long
            bool found{false};
            for (size_t seqSize{2}; !found && seqSize <= 4; ++seqSize)
            {
                const std::string seq{txt.substr(i, seqSize)};
                if (seq.size() != seqSize)
                {
                    break;
                }
                
                for (const std::pair<const char* const, const char32_t>* p{m_tab2CodePoint}; p->first != nullptr; ++p)
                {
                    if (seq == p->first)
                    {
                        AppendCodePoint(p->second, result);
                        i += seqSize - 1;
                        found = true;
                        break;
                    }
                }
            }
            
            if (!found)
            {
                result += txt[i]; // unknown escape sequence
            }
        }
        else
        {
            result += txt[i]; // not an escape
        }
    }
    
    return result;
}

std::string CharSet::Utf8ToTab(std::string_view text)
{
    std::string result;
    for (size_t i{0}; i < text.size();)
    {
        const char32_t codePoint{NextCodePoint(text, i)};
        if (codePoint > 0x7f)
        {
            bool found{false};
            for (const std::pair<const char* const, const char32_t>* p{m_tab2CodePoint}; p->first != nullptr; ++p)
            {
                if (codePoint == p->second)
                {
                    result += p->first;
                    found = true;
                    break;
                }
            }
            
            if (!found)
            {
                result += '?'; // unknown non us-ascii char
            }
        }
        else if (strchr("()[]{}", static_cast<char>(codePoint)) != nullptr)
        {
            result += '\\';
            result += static_cast<char>(codePoint);
        }
        else
        {
            result += static_cast<char>(codePoint);
        }
    }
    
    return result;
}

std::string CharSet::Iso8859_1ToUtf8(std::string_view text)
{
    std::string result;

    for (const unsigned char uin : text)
    {
        // ISO-8859-1 chars are already Unicode code points
        AppendCodePoint(uin, result);
    }
    
    return result;
}

// Windows-1252 is the same as ISO-8859-1 except for 0x80 .. 0x9f
const char32_t CharSet::m_cp1252ToCodePoint[]
{
        U'\u20AC',               // 80  €   U+20AC    EURO SIGN
        ReplacementCharacter,    // 81  � unassigned
        U'\u201A',               // 82  ‚   U+201A    SINGLE LOW-9 QUOTATION MARK
        U'\u0192',               // 83  ƒ   U+0192    LATIN SMALL LETTER F WITH HOOK
        U'\u201E',               // 84  „   U+201E    DOUBLE LOW-9 QUOTATION MARK
        U'\u2026',               // 85  …   U+2026    HORIZONTAL ELLIPSIS
        U'\u2020',               // 86  †   U+2020    DAGGER
        U'\u2021',               // 87  ‡   U+2021    DOUBLE DAGGER
        U'\u02C6',               // 88  ˆ   U+02C6    MODIFIER LETTER CIRCUMFLEX ACCENT
        U'\u2030',               // 89  ‰   U+2030    PER MILLE SIGN
        U'\u0160',               // 8A  Š   U+0160    LATIN CAPITAL LETTER S WITH CARON
        U'\u2039',               // 8B  ‹   U+2039    SINGLE LEFT-POINTING ANGLE QUOTATION MARK
        U'\u0152',               // 8C  Œ   U+0152    LATIN CAPITAL LIGATURE OE
        ReplacementCharacter,    // 8D  � unassigned
        U'\u017D',               // 8E  Ž   U+017D    LATIN CAPITAL LETTER Z WITH CARON
        ReplacementCharacter,    // 8F  � unassigned
        ReplacementCharacter,    // 90  � unassigned
        U'\u2018',               // 91  ‘   U+2018    LEFT SINGLE QUOTATION MARK
        U'\u2019',               // 92  ’   U+2019    RIGHT SINGLE QUOTATION MARK
        U'\u201C',               // 93  “   U+201C    LEFT DOUBLE QUOTATION MARK
        U'\u201D',               // 94  ”   U+201D    RIGHT DOUBLE QUOTATION MARK
        U'\u2022',               // 95  •   U+2022    BULLET
        U'\u2013',               // 96  –   U+2013    EN DASH
        U'\u2014',               // 97  —   U+2014    EM DASH
        U'\u02DC',               // 98  ˜   U+02DC    SMALL TILDE
        U'\u2122',               // 99  ™   U+2122    TRADE MARK SIGN
        U'\u0161',               // 9A  š   U+0161    LATIN SMALL LETTER S WITH CARON
        U'\u203A',               // 9B  ›   U+203A    SINGLE RIGHT-POINTING ANGLE QUOTATION MARK
        U'\u0153',               // 9C  œ   U+0153    LATIN SMALL LIGATURE OE
        ReplacementCharacter,    // 9D  � unassigned
        U'\u017E',               // 9E  ž   U+017E    LATIN SMALL LETTER Z WITH CARON
        U'\u0178',               // 9F  Ÿ   U+0178    LATIN CAPITAL LETTER Y WITH DIAERESIS                                               
};

std::string CharSet::Cp1252ToUtf8(std::string_view text)
{
    std::string result;

    for (const unsigned char uin : text)
    {
        static_assert(sizeof(CharSet::m_cp1252ToCodePoint) / sizeof(const char32_t) == 0x20);
        
        if (uin < 0x80 || uin >= 0xa0)
        {
            // windows-1252 where characters are the same as ISO-8859-1
            AppendCodePoint(uin, result);
        }
        else
        {
            // windows-1252 where characters differ from ISO-8859-1
            AppendCodePoint(m_cp1252ToCodePoint[uin - 0x80], result);
        }
    }
    
    return result;
}

std::string CharSet::Utf8ToIso8859_1(std::string_view text)
{
    std::string result;

    for (size_t i{0}; i < text.size();)
    {
        const char32_t codePoint{NextCodePoint(text, i)};
        
        if (codePoint <= 255)
        {
            // 8-bit Unicode code points are already ISO-8859-1 8-bit chars 
            result += static_cast<char>(codePoint);
        }
        else
        {
            result += '?';
        }
    }

    return result;
}

char32_t CharSet::NextCodePoint(std::string_view text, size_t& i)
{
    if (i >= text.size())
    {
        return ReplacementCharacter;
    }
    
    char32_t codePoint{static_cast<unsigned char>(text[i])};
    ++i;
    
    if ((codePoint & 0x80) == 0)
    {
        return codePoint; // us-ascii
    }
    
    // leading code unit, gives number of code units and first few bits of the code point
    size_t numUnits{0};
    
    if ((codePoint & 0xe0) == 0xc0)
    {
        numUnits = 2;
        codePoint &= 0x1f;
    }
    else if ((codePoint & 0xf0) == 0xe0)
    {
        numUnits = 3;
        codePoint &= 0x0f;
    }
    else if ((codePoint & 0xf8) == 0xf0)
    {
        numUnits = 4;
        codePoint &= 0x07;
    }
    
    // bad UTF-8?
    if (numUnits == 0 || numUnits > text.size() - (i - 1))
    {
        return ReplacementCharacter;
    }

    // second and subsequent code units each contribute 6 bits to the code point
    for (size_t j{2}; j <= numUnits; ++j)
    {
        const auto c{static_cast<unsigned char>(text[i])};
        if ((c & 0xc0) != 0x80)
        {
            return ReplacementCharacter; // not a code unit
        }
        ++i;
        
        codePoint = (codePoint << 6) | (c & 0x3f);
    }

    return codePoint;
}

void CharSet::AppendCodePoint(char32_t codePoint, std::string& text)
{
    if (codePoint > U'\U0010FFFF')
    {
        codePoint = ReplacementCharacter;
    }
    
    if (codePoint < (1 << 7))
    {
        // 1 code unit, us-ascii
        text += static_cast<char>(codePoint);
    }
    else if (codePoint < (1 << 11))
    {
        // 2 code units
        text += static_cast<char>((codePoint >> 6) | 0xc0);
        text += static_cast<char>((codePoint & 0x3f) | 0x80);
    }
    else if (codePoint < (1 << 16))
    {
        // 3 code units
        text += static_cast<char>((codePoint >> 12) | 0xe0);
        text += static_cast<char>(((codePoint >> 6) & 0x3f) | 0x80);
        text += static_cast<char>((codePoint & 0x3f) | 0x80);
    }
    else
    {
        // 4 code units
        text += static_cast<char>((codePoint >> 18) | 0xf0);
        text += static_cast<char>(((codePoint >> 12) & 0x3f) | 0x80);
        text += static_cast<char>(((codePoint >> 6) & 0x3f) | 0x80);
        text += static_cast<char>((codePoint & 0x3f) | 0x80);
    }
}

} // namespace luteconv
