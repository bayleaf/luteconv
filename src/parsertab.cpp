#include "parsertab.h"

#include <algorithm>
#include <array>
#include <cassert>
#include <cctype>
#include <chrono>
#include <cstring>
#include <fstream>
#include <iomanip>
#include <iostream>
#include <iterator>
#include <regex>

#include "charset.h"
#include "logger.h"

namespace luteconv
{

ParserTab::ParserTab()
: m_clefGTuning{Pitch::SetTuning("F3 G3 A3 B3 C4 D4 E4 F4 G4 A4 B4 C5 D5 E5 F5 G5 A5 B5 C6 D6")},
  m_clefFTuning{Pitch::SetTuning("A1 B1 C2 D2 E2 F2 G2 A2 B2 C3 D3 E3 F3 G3 A3 B3 C4 D4 E4 F4")}
{
    
}

void ParserTab::Parse(std::istream& src, const Options& options, Piece& piece)
{
    LOGGER << "Parse tab";
    
    piece.m_parts.resize(3);
    Part& partTab{piece.m_parts[0]};
    Part& partVoice1{piece.m_parts[1]};
    Part& partVoice2{piece.m_parts[2]};
    
    partVoice1.m_staffLines = 5;
    partVoice1.m_tabType = TabCmn;
    partVoice1.m_pitched = true;
    
    partVoice2.m_staffLines = 5;
    partVoice2.m_tabType = TabCmn;
    partVoice2.m_pitched = true;
    
    m_numParts = 1;    // assume 1 part (tablature) until proven otherwise
    
    // French tablature has two notations for numeric diapasons
    // count-courses 7 8 9 10 11 12 13 14 15 16 17 18 19 20 ...
    // count-ledgers           4  5  6  7  8  9 10 11 12 13 ...
    // For <= 13 courses there is no problem as the symbols are disjoint but
    // for > 13 courses the notations overlap.  A kludge to resolve the
    // ambiguity: a value <= maxLedger is deemed to be count-ledgers, else
    // count-courses. MaxLedger is initialized to 6, but advances when we see
    // an a7 to 7 or /a //a ///a to 10. This kludge requires that if 7 is
    // count-ledgers then one of a7 /a //a ///a must exist, or if 8 9 10 are
    // count-ledgers then one of /a //a ///a must exist.  So is a falable heurstic.
    // a7 is treated separately because a7 8 9 10 has been used, i.e. 7 is not used.
    int maxLedger{Course6};
    
    int lineNo{0};
    partTab.m_bars.emplace_back(); // first bar
    bool barIsClear{true};
    int section{0};
    bool foundSection{false};
    if (options.m_srcTabType == TabUnknown)
    {
        partTab.m_tabType = TabFrench; // default
    }
    else
    {
        partTab.m_tabType = options.m_srcTabType;
    }
    int topString{partTab.m_tabType == TabItalian ? Course6 : Course1};
    bool thisSectionCandidate{false};
    bool inMusic{false};
    
    for (;;)
    {
        std::string lineOwner;
        getline(src, lineOwner);
        
        if (src.eof() && lineOwner.empty())
        {
            break;
        }
        
        // remove any trailing \r and spaces
        while (!lineOwner.empty() && (lineOwner.back() == '\r' || lineOwner.back() == ' '))
        {
            lineOwner.pop_back();
        }

        ++lineNo;
        const std::string_view line{lineOwner}; // string_view is much faster at substringing

        // heurstic to spot end of piece
        // blank line or command option followed by {text}
        // Beware that first {text} is start of first piece, not the end.
        // assume that the music in a section will have at least one bar line
        const bool prevSectionCandidate{thisSectionCandidate};
        thisSectionCandidate = line.empty() || line[0] == '-' || line[0] == 'p';
        inMusic = inMusic || (!line.empty() && (line[0] == 'b' || line[0] == 'B'));
        
        // once we have found our section, and are in the music, don't process any more -commands
        // mustn't change the number of staff lines or tablature type mid-section
        if (foundSection && inMusic && !line.empty() && line[0] == '-')
        {
            continue;
        }
        
        if (line == "p")
        {
            // JHR if not yet in the music then can discard any title and composer.  It's probably
            // a collection heading or APPENDIX heading
            if (!options.m_noHeuristics && !inMusic)
            {
                piece.m_title.clear();
                piece.m_composer.clear();
                piece.m_credits.clear();
            }
            continue; // ignore end of page
        }
        
        if (line == "e") // end of document - tab will work but will complain without it.
        {
            break;
        }
        
        // must prosess all -commands and $commands before our section as they are sticky
        if (line == "-4")
        {
            partTab.m_staffLines = 4;
            continue;
        }
        if (line == "-5")
        {
            partTab.m_staffLines = 5;
            continue;
        }
        if (line == "-0")
        {
            partTab.m_staffLines = 6;
            continue;
        }
        if (line == "-7")
        {
            partTab.m_staffLines = 7;
            continue;
        }
        
        if (line == "-f")
        {
            if (options.m_srcTabType == TabUnknown)
            {
                partTab.m_tabType = TabFrench;
                topString = Course1;
            }
            continue;
        }
        
        if (line == "-s")
        {
            // italian 7 course tablature
            if (options.m_srcTabType == TabUnknown)
            {
                partTab.m_tabType = TabItalian;
                topString = Course7;
            }
            continue;
        }
        
        if (line == "-i" || line == "-O" || line == "$numstyle=italian")
        {
            // Ignore if spanish or italian has already been esablished
            // italian or spanish tablature: assume italian
            if (options.m_srcTabType == TabUnknown && partTab.m_tabType != TabItalian && partTab.m_tabType != TabSpanish)
            {
                partTab.m_tabType = TabItalian;
                topString = Course6;
            }
            continue;
        }
        
        if (line == "-milan")
        {
            // spanish tablature
            if (options.m_srcTabType == TabUnknown)
            {
                partTab.m_tabType = TabSpanish;
                topString = Course1;
            }
            continue;
        }
        
        // some baroque pieces use -i and -b, assume french tablature
        if (line == "-b")
        {
            if (options.m_srcTabType == TabUnknown)
            {
                partTab.m_tabType = TabFrench;
                topString = Course1;
            }
            continue;
        }
        
        const std::string_view tuning{"-tuning "};
        if (line.substr(0, tuning.size()) == tuning)
        {
            try
            {
                SetTuningTab(line.substr(tuning.size()), partTab.m_tuning);
                LOGGER << lineNo << ": Source tuning " << Pitch::GetTuning(partTab.m_tuning);
            }
            catch (...)
            {
                LOGGER << lineNo << ": \"" << line << "\" -tuning syntax error, ignored";
                partTab.m_tuning.clear();
            }
            continue;
        }
        
        if (line.substr(0, 8) == "$scribe=")
        {
            piece.m_copyright = line.substr(8);
            
            // Prefix "Copyright <year>", unless already there
            if (piece.m_copyright.find("Copyright") == std::string::npos)
            {
                const std::time_t tt{std::chrono::system_clock::to_time_t(std::chrono::system_clock::now())};
                struct std::tm * ptm{std::gmtime(&tt)};
                std::ostringstream ss;
                ss << "Copyright " << std::put_time(ptm,"%Y") << " " << piece.m_copyright;
                piece.m_copyright = ss.str();
            }
            continue;
        }
        
        if (line == "-twostaff")
        {
            m_numParts = 3;
            continue;
        }
        
        if (line == "-G")
        {
            piece.m_copyrightEnabled = true;
            continue;
        }
        
        // Tab files don't support sections, use heuristics, unless disabled.
        if (options.m_noHeuristics)
        {
            if (section != options.m_index)
            {
                break;
            }
        }
        else
        {
            // Need to process all -commands and $commands before our section as they are sticky.
            // Now look for our section start or end.

            if (inMusic && prevSectionCandidate && !line.empty() && line[0] == '{')
            {
                ++section;
                inMusic = false;
                if (section > options.m_index)
                {
                    break;
                }
            }
            
            if (section != options.m_index)
            {
                continue;
            }
        }
        
        foundSection = true;
        
        // Italian tablature with fewer than 6 staff lines.
        if (partTab.m_staffLines < 6 && partTab.m_tabType == TabItalian && topString == Course6)
        {
            topString = partTab.m_staffLines;
        }
        
        if (line.empty())
        {
            if (barIsClear && partTab.m_bars.size() > 1)
            {
                // end of line at end of bar
                Bar& prev = partTab.m_bars.at(partTab.m_bars.size() - 2);
                prev.m_eol = true;
            }
            continue;   // (blank line)  - break line here - you must specify line
                        // breaks yourself!
        }
        
        switch (line[0])
        {
        case '{':   // { words }     - one of more lines of text (unjustified, not wrapped)
        {
            if (partTab.m_bars.size() == 1 && barIsClear)
            {
                std::string left;
                std::string right;
                const size_t slash{line.find('/')};
                if (line.substr(1, 4) == "\\CL/")
                {
                    left = CleanTabString(line.substr(5, line.size() - 6));
                }
                else if (slash != std::string::npos)
                {
                    left = CleanTabString(line.substr(1, slash - 1));
                    right = CleanTabString(line.substr(slash + 1, line.size() - slash - 2));
                }
                else
                {
                    left = CleanTabString(line.substr(1, line.size() - 2));
                }
                
                // always add to the credits, once we have established the title and
                // composer they will be removed from the credits
                if (!left.empty())
                {
                    piece.m_credits.emplace_back(left);
                }
                
                if (!right.empty())
                {
                    piece.m_credits.emplace_back(right);
                }

                // if we have left/right then this is title/composer, unless already established
                // or is a continuation
                if (!left.empty() && !right.empty() && piece.m_composer.empty() && left.substr(0, 2) != "- ")
                {
                    piece.m_title = left;
                    piece.m_composer = right;
                }
            }
            break;
        }
        case '%':   // comment
            break;
        case 'A':   // volta bracket
            if (!barIsClear)
            {
                // A1 in middle of a bar behaves like a bar line
                ParseBarLine("b", barIsClear, piece);
            }
            
            if (line.size() >= 2 && line[1] >= '1' && line[1] <= '9')
            {
                partTab.m_bars.back().m_volta = line[1] - '0';
            }
            else
            {
                partTab.m_bars.back().m_volta = 1;
            }
            break;
        case 'b':   // barline
            [[fallthrough]];
        case '.':   // column of 5 dots (for a repeat) as in .bb.
                    // with the "-count_dots" this will increment the bar count
                    // unless followed by an "X"
            ParseBarLine(line, barIsClear, piece);
            break;
        case 'B':   // a very thick barline (when followed by newline or !)
                    // a breve (when followed by notes)
                    // BX means don't count
            if (line.size() == 1 || line[1] == '!' || line[1] == 'X')
            {
                ParseBarLine(line, barIsClear, piece);
            }
            else
            {
                ParseChord(line, lineNo, piece, partTab.m_bars.back(), barIsClear, topString, false, maxLedger);
            }
            break;
        case 'C':   // a big C
            [[fallthrough]];
        case 'c':   // cut time in both staves if necessary
            [[fallthrough]];
        case 'S':   // a time signature - ie S3-4 or SC
                    // a dash, as 12-8 draws the signature in the music
                    // rather than the tablature.
                    // S2| or S3| draws a line throught the number
                    // a single digit followed by Q is highlighted
                    // SO and So do Narvaez style time sig
                    // S0  (that is a zero) gives  the O for perfect time
            ParseTimeSignature(line, partTab.m_bars.back());
            break;
        case '#':   // first note of a grid followed by number of lines in grid
            [[fallthrough]];
        case '0':
            [[fallthrough]];
        case '1':
            [[fallthrough]];
        case '2':
            [[fallthrough]];
        case '3':
            [[fallthrough]];
        case '4':
            [[fallthrough]];
        case '5':   // a note with that many flags
            [[fallthrough]];
        case 'W':   // a whole note  0
            [[fallthrough]];
        case 'w':   // a half note
            [[fallthrough]];
        case 'L':   // longa
            ParseChord(line, lineNo, piece, partTab.m_bars.back(), barIsClear, topString, false, maxLedger);
            break;
        case 'R':   // rest
            ParseChord(line.substr(1), lineNo, piece, partTab.m_bars.back(), barIsClear, topString, false, maxLedger);
            break;
        case 'x':
            // JHR has used x'\t'<music> - should be M or '\t'
            if (line.size() > 1 && (line[1] == 'M' || line[1] == '\t'))
            {
                ParseCmn(line, lineNo, 2, piece);
            }
            // JHR has used x!'\t'<music> - should be M or '\t'
            else if (line.size() > 2 && line[1] == '!' && (line[2] == 'M' || line[2] == '\t'))
            {
                ParseCmn(line, lineNo, 3, piece);
            }
            else
            {
                // same number of flags as the last one
                ParseChord(line, lineNo, piece, partTab.m_bars.back(), barIsClear, topString, false, maxLedger);
            }
            break;
        case 't':   // first note of a triplet, followed by the
                    // followed by the number of lines there would be
                    // if there were lines - eg. t3 a,x b, xb,
                    // this places a 3 above the second note.
                    // a t before a barline is a tie across the barline
            if (line.size() > 1)
            {
                ParseChord(line.substr(1), lineNo, piece, partTab.m_bars.back(), barIsClear, topString, true, maxLedger);
            }
            break;
        case 'Y':   // fermata
            [[fallthrough]];
        case 'y':   // another fermata
                    // a Yb or yb draws a fermata above a barline
                    // Y. YW Yw YB also work'
            if (line.size() == 1 || line[1] == '.' || line[1] == 'b' || (line.size() == 2 && line[1] == 'B'))
            {
                partTab.m_bars.back().m_fermata = true;
                ParseBarLine(line.substr(1), barIsClear, piece);
            }
            else
            {
                // if the Y replaces the flag count then substitute 0
                bool isFlag{false};
                if (partTab.m_tabType == TabItalian || partTab.m_tabType == TabSpanish)
                {
                    // Italian or Spanish tablature can't tell difference between fret numbers and numeric flags
                    isFlag = std::strchr("wWBL", line[1]) != nullptr;
                }
                else
                {
                    isFlag = std::strchr("012345wWBL", line[1]) != nullptr;
                }
                
                if (isFlag)
                {
                    ParseChord(line.substr(1), lineNo, piece, partTab.m_bars.back(), barIsClear, topString, false, maxLedger);
                }
                else
                {
                    ParseChord("0" + std::string(line.substr(1)), lineNo, piece, partTab.m_bars.back(), barIsClear, topString, false, maxLedger);
                }
                partTab.m_bars.back().m_chords.back().m_fermata = true;
            }
            break;
        case 'k':
            ParseKey(line, piece);
            break;
        case 'M':
            [[fallthrough]];
        case '\t':
            if (line.size() >= 2 && line[1] == 'Z')
            {
                // ignore zigzag
                LOGGER << lineNo << ": \"" << line << "\" ignored";
                break;
            }
            ParseCmn(line, lineNo, 1, piece);
            break;
        
        default:
            LOGGER << lineNo << ": \"" << line << "\" ignored";
        }
    }
    
    if (!foundSection)
    {
        throw std::invalid_argument("Error: Can't find section, index=" + std::to_string(options.m_index));
    }
    
    // deal with not needed "next" bar
    if (partVoice1.m_bars.size() < partTab.m_bars.size() && partVoice2.m_bars.size() < partTab.m_bars.size() && barIsClear)
    {
        partTab.m_bars.pop_back();
    }
    
    // Fixup parts to: equalize number of bars
    const size_t numBars{std::max(partTab.m_bars.size(), std::max(partVoice1.m_bars.size(), partVoice2.m_bars.size()))};
    piece.m_parts.resize(m_numParts);
    for (auto& part : piece.m_parts)
    {
        part.m_bars.resize(numBars);
    }
    
    // Fixup parts to: copy bar attributes from tablature to voices
    for (size_t i{0}; i < numBars; ++i)
    {
        for (size_t j{1}; j < m_numParts; ++j)
        {
            Bar& bar{piece.m_parts[j].m_bars[i]};
            bar.m_timeSig = partTab.m_bars[i].m_timeSig;
            bar.m_barStyle = partTab.m_bars[i].m_barStyle;
            bar.m_repeat = partTab.m_bars[i].m_repeat;
            bar.m_volta = partTab.m_bars[i].m_volta;
            bar.m_fermata = partTab.m_bars[i].m_fermata;
        }
        
        // voice 2 has the same clef and key as voice 1
        if (m_numParts == 3)
        {
            piece.m_parts[2].m_bars[i].m_clef = piece.m_parts[1].m_bars[i].m_clef;
            piece.m_parts[2].m_bars.back().m_key = piece.m_parts[1].m_bars.back().m_key;
        }
    }
    
    // Fixup parts to: fix grids
    for (auto& part : piece.m_parts)
    {
        for (auto& bar : part.m_bars)
        {
            for (size_t i{1}; i < bar.m_chords.size(); ++i)
            {
                if (bar.m_chords[i - 1].m_grid == GridStart && bar.m_chords[i].m_grid == GridStart)
                {
                    LOGGER << "bar " << std::distance(&part.m_bars.front(), &bar) + 1 << " chord " << i << ": adjacent grid starts";
                    
                    // 2 adjacent grid starts, shouldn't happen, a bug in some tab files
                    if (bar.m_chords[i - 1].m_noteType == bar.m_chords[i].m_noteType &&
                        bar.m_chords[i - 1].m_dotted == bar.m_chords[i].m_dotted
                    )
                    {
                        // #2; #2 ought to be #2; x
                        bar.m_chords[i].m_grid = GridMid;
                    }
                    else
                    {
                        // ignore first grid start
                        bar.m_chords[i - 1].m_grid = GridNone;
                    }
                }
                else if (bar.m_chords[i - 1].m_grid == GridMid && bar.m_chords[i].m_grid != GridMid)
                {
                    // end of grid
                    bar.m_chords[i - 1].m_grid = GridEnd;
                }
            }
            
            // #9: TabCode: missing beam end
            if (!bar.m_chords.empty())
            {
                if (bar.m_chords.back().m_grid == GridMid)
                {
                    bar.m_chords.back().m_grid = GridEnd;   // grid end at end of bar
                }
                else if (bar.m_chords.back().m_grid == GridStart)
                {
                    bar.m_chords.back().m_grid = GridNone; // can't start a grid at the end of a bar
                }
            }
        }
    }

    // if no title use the first credit, if any
    if (piece.m_title.empty() && !piece.m_credits.empty())
    {
        piece.m_title = piece.m_credits.front();
    }
    
    // remove the tile and composer from the credits
    if (!piece.m_title.empty())
    {
        LOGGER << "title=" << piece.m_title;
        auto it{std::find(piece.m_credits.begin(), piece.m_credits.end(), piece.m_title)};
        if (it != piece.m_credits.end())
        {
            piece.m_credits.erase(it);
        }
    }
    if (!piece.m_composer.empty())
    {
        LOGGER << "composer=" << piece.m_composer;
        auto it{std::find(piece.m_credits.begin(), piece.m_credits.end(), piece.m_composer)};
        if (it != piece.m_credits.end())
        {
            piece.m_credits.erase(it);
        }
    }
    
    ParseJHRTuning(piece);
    partTab.SetTuning(options);
    
    LOGGER << "Parts: " << m_numParts;
    
    if (m_numParts == 2)
    {
        // TAB gerarates parts in the order tab, voice 1 but we need parts in order down the page

        std::swap(piece.m_parts[0], piece.m_parts[1]);
        piece.m_parts[0].m_partName = "Voice";
        piece.m_parts[1].m_partName = "Lute";
    }
    else if (m_numParts == 3)
    {
        // TAB gerarates parts in the order tab, voice 2, voice 1 but we need parts in order down the page
        std::swap(piece.m_parts[0], piece.m_parts[2]);
        piece.m_parts[0].m_partName = "Voice 1";
        piece.m_parts[1].m_partName = "Voice 2";
        piece.m_parts[2].m_partName = "Lute";
    }
    
    // any open lyric hyphens?  Just for logging.
    if (std::find(m_lyricHyphen1.begin(), m_lyricHyphen1.end(), true) != m_lyricHyphen1.end())
    {
        LOGGER << "Lyric part 1 trailing hyphen";
    }
    if (std::find(m_lyricHyphen2.begin(), m_lyricHyphen2.end(), true) != m_lyricHyphen2.end())
    {
        LOGGER << "Lyric part 2 trailing hyphen";
    }
}

std::string ParserTab::CleanTabString(std::string_view src)
{
    std::string dst;
    
    // remove font
    for (size_t i{0}; i < src.size(); ++i)
    {
        if (src.size() - i >= 3 && src[i] == '^' && static_cast<bool>(isdigit(src[i + 1])) && static_cast<bool>(isdigit(src[i + 2])))
        {
            i += 3; // skip font
        }
        else
        {
            dst += src[i];
        }
    }
    
    // remove leading and trailing spaces
    dst.erase(dst.begin(), find_if_not(dst.begin(), dst.end(),[](int c){return isspace(c);}));
    
    // FIXME this seems to break on Macs, adds and extra \0 to the string.
    // rtf.erase(find_if_not(dst.rbegin(), dst.rend(),[](int c){return isspace(c);}).base(), dst.end());
    // recast
    while (!dst.empty() && dst.back() == ' ')
    {
        dst.pop_back();
    }

    // convert to UTF-8
    return CharSet::TabToUtf8(dst);
}

void ParserTab::ParseBarLine(std::string_view line, bool& barIsClear, Piece& piece)
{
    Part& partTab{piece.m_parts[0]};
    Part& partVoice1{piece.m_parts[1]};
    Part& partVoice2{piece.m_parts[2]};
    Bar& bar{partTab.m_bars.back()};
    
    const bool barNeeded{!barIsClear || partVoice1.m_bars.size() == partTab.m_bars.size() || partVoice2.m_bars.size() == partTab.m_bars.size()};

    // Some tab staff lines start with a redundant bar line, ignore it
    if (partTab.m_bars.size() == 1 && !barNeeded)
    {
        return;
    }

    if (line.substr(0, 4) == ".bb." || line.substr(0, 5) == ".b.b.")
    {
        bar.m_barStyle = BarStyleLightLight;
        bar.m_repeat = RepJanus;
    }
    else if (line.substr(0, 3) == ".b.")
    {
        bar.m_barStyle = BarStyleRegular;
        bar.m_repeat = RepJanus;
    }
    else if (line.substr(0, 3) == ".bb")
    {
        bar.m_barStyle = BarStyleLightLight;
        bar.m_repeat = RepBackward;
    }
    else if (line.substr(0, 3) == "bb.")
    {
        bar.m_barStyle = BarStyleLightLight;
        bar.m_repeat = RepForward;
    }
    else if (line.substr(0, 3) == "b.b")
    {
        bar.m_barStyle = BarStyleLightLight;
        bar.m_repeat = RepJanus;
    }
    else if (line.substr(0, 2) == ".b")
    {
        bar.m_barStyle = BarStyleRegular;
        bar.m_repeat = RepBackward;
    }
    else if (line.substr(0, 2) == "b.")
    {
        bar.m_barStyle = BarStyleRegular;
        bar.m_repeat = RepForward;
    }
    else if (line.substr(0, 2) == "bb")
    {
        bar.m_barStyle = BarStyleLightLight;
    }
    else if (line.substr(0, 1) == "B")
    {
        bar.m_barStyle = BarStyleHeavy;
    }
    else if (line.substr(0, 1) == "b")
    {
        bar.m_barStyle = BarStyleRegular;
    }
    else if (line == ".")
    {
        if (barIsClear)
        {
            // previous bar is a repForward
            if (partTab.m_bars.size() > 1)
            {
                Bar& prev = partTab.m_bars.at(partTab.m_bars.size() - 2);
                prev.m_repeat = RepForward;
            }
        }
        else
        {
            bar.m_repeat = RepBackward;
        }
    }
    
    // if first bar on staff line, and bar is clear consider making the previous bar janus
    
    if (!barNeeded)
    {
        Bar& prev = partTab.m_bars.at(partTab.m_bars.size() - 2);
        
        if (prev.m_eol)
        {           
            if (prev.m_repeat == RepBackward && bar.m_repeat == RepForward)
            {
                prev.m_repeat = RepJanus;
                bar.m_repeat = RepNone;
            }
        }
        else
        {
            if (prev.m_barStyle == BarStyleRegular && bar.m_barStyle == BarStyleRegular)
            {
                prev.m_barStyle = BarStyleLightLight;
            }
            else if (prev.m_barStyle == BarStyleRegular && bar.m_barStyle == BarStyleLightLight)
            {
                prev.m_barStyle = BarStyleLightLight;
            }
            else if (prev.m_barStyle == BarStyleRegular && bar.m_barStyle == BarStyleHeavy)
            {
                prev.m_barStyle = BarStyleLightHeavy;
            }
            else if (prev.m_barStyle == BarStyleHeavy && bar.m_barStyle == BarStyleRegular)
            {
                prev.m_barStyle = BarStyleHeavyLight;
            }
            else if (prev.m_barStyle == BarStyleHeavy && bar.m_barStyle == BarStyleHeavy)
            {
                prev.m_barStyle = BarStyleHeavyHeavy;
            }
        }
        
        bar.m_barStyle = BarStyleRegular;
        return;
    }

    // next bar
    partTab.m_bars.emplace_back();
    barIsClear = true;
}

void ParserTab::ParseChord(std::string_view line, int lineNo, Piece& piece, Bar& bar, bool& barIsClear, int topString, bool triplet, int& maxLedger)
{
    bar.m_chords.emplace_back(); // new chord
    Chord & chord{bar.m_chords.back()};
    static Chord previousChord;
    
    chord.m_triplet = triplet;
    
    // flags
    size_t idx{0};
    if (line[idx] == 'x')
    {
        ++idx;
        
        // same number of flags as the last one
        // get flags from previous chord, but not the dot, if any.
        chord.m_noteType = previousChord.m_noteType;

        if (previousChord.m_grid == GridNone)
        {
            chord.m_noFlag = true;
        }
        else
        {
            chord.m_grid = GridMid;
            
            //  *   a dotted note within a grid
            if (idx < line.size() && line[idx] == '*')
            {
                chord.m_dotted = true;
                ++idx;
            }
            else if (idx < line.size() && line[idx] == '|')
            {
                // |    a flag with an extra bar within a grid.
                chord.m_noteType = static_cast<NoteType>(chord.m_noteType + 1);
                ++idx;
            }
        }
    }
    else
    {
        if (line[idx] == '#')
        {
            // first note of a grid
            chord.m_grid = GridStart;
            ++idx;
            if (idx >= line.size())
            {
                return;
            }
        }
        
        if (!ParseNoteType(line, idx, chord))
        {
            // ignore
            bar.m_chords.pop_back();
            return;
        }
        
        // ! after a number - don't print flag but use its value for spacing
        if (idx < line.size() && line[idx] == '!')
        {
            ++idx;
            if (idx < line.size() && (line[idx] == 'M' || line[idx] == '\t'))
            {
                bar.m_chords.pop_back();
                ParseCmn(line, lineNo, idx + 1, piece);
                return;
            }

            if (idx < line.size() && line[idx] == '-')
            {
                // seen in the wild 2!-
                ++idx;
                LOGGER << lineNo << ": \"" << line << "\" ignoring ensemble line after non-print spacing flag";
            }

            // ignore non-print spacing flag if there is nothing else
            if (idx >= line.size())
            {
                bar.m_chords.pop_back();
                return;
            }
        }
        
        if (idx < line.size() && (line[idx] == 't' || line[idx] == 'W' || line[idx] == 'Q'))
        {
            // tie, tie and dot, highlight - ignore
            ++idx;
        }
        
        if (idx < line.size() && (line[idx] == '.' || line[idx] == '@'))
        {
            // a dotted flag
            chord.m_dotted = true;
            ++idx;
        }
        
        // # can appear before or after the flag - beware #2# = <grid,flags,ornament>
        if (idx < line.size() && idx == 1 && line[idx] == '#' && chord.m_grid != GridStart)
        {
            // first note of a grid
            chord.m_grid = GridStart;
            ++idx;
        }
        else if (idx < line.size() && idx == 1 && line[idx] == '*' && chord.m_grid != GridStart)
        {
            // first note of a grid with an initial dot
            chord.m_grid = GridStart;
            chord.m_dotted = true;
            ++idx;
        }
        
        previousChord = chord; // only need to save the flags data, not the notes.
        
        if (idx < line.size() && idx == 1 && line[idx] == '|' && chord.m_grid != GridStart)
        {
            // using | instead of # at the beginning of a series starts with a note with extra flag
            chord.m_grid = GridStart;
            chord.m_noteType = static_cast<NoteType>(chord.m_noteType + 1);
            ++idx;
        }
     }

    if (idx < line.size() && idx == 1 && line[idx] == '-')
    {
        // a place marker, technically should be the default second character
        // but usually can be left out - it helps when you want to use a
        // *, # or - ornament on the character on the top line.
        ++idx;
    }
    
    // notes
    const int stringInc{topString == 1 ? +1 : -1}; // up for french, down for italian
    int string{topString - stringInc};
    const std::string slurConnectors{"[]UX"}; // slur connectors
    const std::string holdConnectors{"(){}"}; // hold connectors
    const std::string allConnectors{slurConnectors + holdConnectors}; // all connectors
    const std::string_view ornaments{"#+x*%Q,'=$@_^<"}; // supported ornaments

    barIsClear = false;
    
    while (idx < line.size())
    {
        string += stringInc;
        
        if (line[idx] == ' ')
        {
            // empty character position
            ++idx;
            continue;
        }
        
        chord.m_notes.emplace_back(); // allocate new note
        Note& note{chord.m_notes.back()};
        
        // optional left ornament, left fingering followed by letter or diapason, or connector 
        while (idx < line.size())
        {
            // left fingering
            if (line[idx] == '\\')
            {
                ++idx;
                if (idx < line.size())
                {
                    if (line[idx] >= '1' && line[idx] <= '4')
                    {
                        // left hand fingerings placed before the next letter.
                        note.m_leftFingering = static_cast<Fingering>(FingerNone + line[idx] - '0');
                        ++idx;
                        continue;
                    }
                }
            }
            else if (idx < line.size() - 1 && line[idx] == '\"')
            {
                // prefix operator
                ++idx;
                if (allConnectors.find(line[idx]) != std::string::npos)
                {
                    // connector to the left of the note
                    ParseConnector(line, idx, string, West, note);
                    ++idx;
                    continue;
                }
                
                // using prefix operator with left ornament is redundant, but does occur
                const size_t leftOrnPos{ornaments.find(line[idx])};
                if (leftOrnPos != std::string_view::npos)
                {
                    note.m_leftOrnament = static_cast<Ornament>(leftOrnPos + OrnHash);
                    ++idx;
                    continue;
                }
                
                // ignore other prefixes
                LOGGER<< lineNo << ": \"" << line << "\" ignoring prefix operator \"" << line[idx] << "\"";
                ++idx;
                continue;
            }
            else
            {
                // left ornament
                
                // backtick for comma on the line is obselete, use ","
                const char orn{line[idx] == '`' ? ',' : line[idx]};
                const size_t leftOrnPos{ornaments.find(orn)};
                if (leftOrnPos != std::string_view::npos)
                {
                    note.m_leftOrnament = static_cast<Ornament>(leftOrnPos + OrnHash);
                    ++idx;
                    continue;
                }
                // ignore other prefixes
                const std::string_view prefix{"?>"};
                const size_t prefixPos{prefix.find(line[idx])};
                if (prefixPos != std::string::npos)
                {
                    LOGGER<< lineNo << ": \"" << line << "\" ignoring left ornament \"" << line[idx] << "\"";
                    ++idx;
                    continue;
                }
            }
            
            // ledgerLines
            int ledgerLines{0};
            if (string >= Course7 && line[idx] == '/')
            {
                do
                {
                    ++ledgerLines;
                    ++idx;
                }
                while (idx < line.size() && line[idx] == '/');
            }

            // Ensemble line
            if (idx < line.size() && line[idx] == '-')
            {
                note.m_fret = Note::Ensemble;
                note.m_course = string;
                ++idx;
                break;
            }
            
            // fret letters
            // Tab support a..p but JHR uses q (a '/. sign for Kapsberger)
            // allow q as a fret
            if (idx < line.size() && line[idx] >= 'a' && line[idx] <= 'q')
            {
                const int fret{line[idx] - 'a' - (line[idx] <= 'i' ? 0 : 1)}; // fret i = j
                const int course{(ledgerLines >= 1) ? Course7 + ledgerLines : string};
                ++idx;
                
                if (fret == 0)
                {
                    if (course == Course7 && maxLedger == Course6)
                    {
                        // if this is the first a7 we've seen then any previous 7
                        // will have been misinterpreted as count-courses when should have been count-ledgers
                        FixLedgers(piece.m_parts[0], Course7, Course7);
                        maxLedger = Course7;
                    }
                    else if (ledgerLines > 0 && maxLedger <= Course7)
                    {
                        // If this is the first ledger line we've seen then any
                        // previous 7 8 9 10 ... will have been misinterpreted
                        // as count-courses when should have been count-ledgers.
                        // Promote to 14 15 ...  Except if 7 has already been promoted
                        // (maxLedger == Course7) only promote from 8. 
                        FixLedgers(piece.m_parts[0], maxLedger + 1, Course10);
                        maxLedger = Course10; // ledger lines are used so 7 8 9 10, if used, must be count-courses
                    }
                }

                note.m_fret = fret;
                note.m_course = course;
                break;
            }
            
            // upper case fret letters: F G H I give characters like M Board used
            // D and E are not documented, but are used in github/tab-source and
            // affect the glyphs used depending on the -b header.
            if (idx < line.size() && line[idx] >= 'D' && line[idx] <= 'I')
            {
                // fret
                note.m_fret = line[idx] - 'A';
                note.m_course = (ledgerLines >= 1) ? Course7 + ledgerLines : string;
                ++idx;
                break;
            }
             
            // 1 or 2 digit number
            // italian and spanish: fret number
            // french: diapason number, count-ledgers or count-courses
            int numericItem{-1};
            if (idx < line.size() && static_cast<bool>(isdigit(line[idx])))
            {
                numericItem = line[idx] - '0';
                ++idx;
            }
            else if (idx < line.size() - 2 && line[idx] == 'N' && static_cast<bool>(isdigit(line[idx + 1])) && static_cast<bool>(isdigit(line[idx + 2])))
            {
                numericItem = (line[idx + 1] - '0') * 10 + (line[idx + 2] - '0');
                idx += 3;
            }
            if (numericItem >= 0)
            {
                if (topString == Course1 && string >= Course7)
                {
                    // diapason
                    note.m_fret = 0;
                    if (numericItem > maxLedger)
                    {
                        note.m_course = numericItem; // count-courses
                    }
                    else
                    {
                        note.m_course = numericItem + Course7; // count-ledgers
                    }
                }
                else
                {
                    // fret
                    note.m_fret = numericItem;
                    note.m_course = (ledgerLines >= 1) ? Course7 + ledgerLines : string;
                }
                
                break;
            }
            
            // fret number roman X
            if (idx < line.size() - 1 && line[idx] == '!' && line[idx + 1] == 'x')
            {
                // fret
                note.m_fret = 10;
                note.m_course = (ledgerLines >= 1) ? Course7 + ledgerLines : string;
                idx += 2;
                break;
            }
            
            if (idx < line.size() && line[idx] == '&')
            {
                // postfix operator but no note at character position
                break;
            }
            
            if (idx < line.size() - 1 && (line[idx] == '!' || (line[idx] == '\\' && line[idx + 1] == '\\')))
            {
                // escape operator or \\ one backslash in note position
                LOGGER << lineNo << ": \"" << line << "\" ignoring escape";
                idx += 2;
                break;
            }
            
            if (idx < line.size() && (allConnectors.find(line[idx]) != std::string::npos))
            {
                break; // connectors
            }

            // TODO what to do with text without music?
            // ignore text
            if (idx < line.size() && line[idx] == 'T')
            {
                LOGGER << lineNo << ": \"" << line << "\" ignoring text";
                idx = line.size();
                break;
            }
            
            // CMN
            if (idx < line.size() && (line[idx] == 'M' || line[idx] == '\t'))
            {
                ParseCmn(line, lineNo, idx + 1, piece);
                idx = line.size();
                break;
            }
                
            if (idx >= line.size())
            {
                LOGGER << lineNo << ": \"" << line << "\" ignoring missing note";
            }
            else
            {
                // ignore other prefixes
                LOGGER << lineNo << ": \"" << line << "\" ignoring non-note \"" << line[idx] << "\"";
                ++idx;
            }
            break;
        }
    
        // optional right fingering, ornament and connector
        
        while (idx < line.size())
        {
            if (line[idx] == '.')
            {
                note.m_rightFingering = FingerFirst;
                string += stringInc; // right hand fingering replaces note on next line
            }
            else if (line[idx] == ':')
            {
                note.m_rightFingering = FingerSecond;
                string += stringInc; // right hand fingering replaces note on next line
            }
            else if (line[idx] == ';')
            {
                note.m_rightFingering = FingerThird;
                string += stringInc; // right hand fingering replaces note on next line
            }
            else if (line[idx] == '|')
            {
                note.m_rightFingering = FingerThumb;
                string += stringInc; // right hand fingering replaces note on next line
            }
            else if (line[idx] == '&')
            {
                ++idx;
                if (idx >= line.size())
                {
                    break;
                }
                
                // backtick for comma on the line is obselete, use ","
                const char orn{line[idx] == '`' ? ',' : line[idx]};
                const size_t rightOrnPos{ornaments.find(orn)};
                if (rightOrnPos != std::string_view::npos)
                {
                    note.m_rightOrnament = static_cast<Ornament>(rightOrnPos + OrnHash);
                }
                else if (allConnectors.find(line[idx]) != std::string::npos)
                {
                    // connector to the right of the note
                    ParseConnector(line, idx, string, East, note);
                }
                else
                {
                    LOGGER<< lineNo << ": \"" << line << "\" ignoring postfix \"&" << line[idx] << "\"";
                }
            }
            else if (allConnectors.find(line[idx]) != std::string::npos)
            {
                // connectors starts or stops below the note
                ParseConnector(line, idx, string, South, note);
                // a place holder has no height
                if (note.m_fret != Note::Anchor)
                {
                    string += stringInc; // south connector replaces note on next line
                }
            }
            else
            {
                break;
            }
            
            ++idx;
        }
        
        // note not needed
        if (note.m_course == 0 && note.m_connector.m_endPoint == EndPointNone)
        {
            chord.m_notes.pop_back();
        }
    }
}

void ParserTab::FixLedgers(Part& part, int lwb, int upb)
{
    for (Bar& b : part.m_bars)
    {
        for (Chord& c : b.m_chords)
        {
            for (Note& n : c.m_notes)
            {
                if (n.m_fret == 0 && n.m_course >= lwb && n.m_course <= upb)
                {
                    n.m_course += 7;
                }
            }
        }
    }
}

void ParserTab::ParseTimeSignature(std::string_view line, Bar& bar)
{
    if (line == "C" || line == "SC")
    {
        bar.m_timeSig.m_timeSymbol = TimeSyCommon;
        bar.m_timeSig.m_beats = 4;
        bar.m_timeSig.m_beatType = 4;
    }
    else if (line == "c" || line == "Sc")
    {
        bar.m_timeSig.m_timeSymbol = TimeSyCut;
        bar.m_timeSig.m_beats = 2;
        bar.m_timeSig.m_beatType = 2;
    }
    else if (line.size() == 2 && line[1] >= '1' && line[1] <= '9')
    {
        bar.m_timeSig.m_timeSymbol = TimeSySingleNumber;
        bar.m_timeSig.m_beats = line[1] - '0';
        bar.m_timeSig.m_beatType = 4;
    }
    else if (line.size() == 3 && line[1] >= '1' && line[1] <= '9' && line[2] >= '1' && line[2] <= '9')
    {
        bar.m_timeSig.m_timeSymbol = TimeSyNormal;
        bar.m_timeSig.m_beats = line[1] - '0';
        bar.m_timeSig.m_beatType = line[2] - '0';
    }
    else if (line.size() == 4 && line[1] >= '1' && line[1] <= '9' && line[3] >= '1' && line[3] <= '9')
    {
        bar.m_timeSig.m_timeSymbol = TimeSyNormal;
        bar.m_timeSig.m_beats = line[1] - '0';
        bar.m_timeSig.m_beatType = line[3] - '0';
    }
}

void ParserTab::ParseConnector(std::string_view line, size_t& idx, int string, Compass compass, Note& note)
{
    if (line[idx] == '[' || line[idx] == 'U')
    {
        // start a slur.
        note.m_connector.m_function = FunSlur;
        note.m_connector.m_endPoint = EndPointStart;
        note.m_connector.m_curve = CurveUnder;
        note.m_connector.m_number = (line[idx] == '[') ? 1 : 2;
    }
    else if (line[idx] == ']' || line[idx] == 'X')
    {
        // stop a slur.
        note.m_connector.m_function = FunSlur;
        note.m_connector.m_endPoint = EndPointStop;
        note.m_connector.m_number = (line[idx] == ']') ? 1 : 2;

        if (note.m_connector.m_number == 1 && idx + 1 < line.size() && line[idx + 1] == 'v')
        {
            ++idx;
            note.m_connector.m_curve = CurveOver;
        }
        else
        {
            note.m_connector.m_curve = CurveUnder;
        }
    }
    else if (line[idx] == '(' || line[idx] == '{')
    {
        // start a hold.
        note.m_connector.m_function = FunHold;
        note.m_connector.m_endPoint = EndPointStart;
        note.m_connector.m_curve = CurveStraight;
        note.m_connector.m_number = (line[idx] == '(') ? 1 : 2;
    }
    else if (line[idx] == ')' || line[idx] == '}')
    {
        // stop a hold.
        note.m_connector.m_function = FunHold;
        note.m_connector.m_endPoint = EndPointStop;
        note.m_connector.m_curve = CurveStraight;
        note.m_connector.m_number = (line[idx] == ')') ? 1 : 2;
    }
    
    note.m_connector.m_compass = compass;
    
    if (note.m_course == 0)
    {
        note.m_course = string;
        note.m_fret = Note::Anchor; // not a note, anchor for connector
    }
}

void ParserTab::ParseKey(std::string_view line, Piece& piece)
{
    // key signature kg is key of G, also d, a, f, c, b, e
    const std::array<const Key, 7> keys{KeyA, KeyBb, KeyC, KeyD, KeyEb, KeyF, KeyG};
    if (line.size() >= 2)
    {
        const ptrdiff_t i{line[1] - 'a'};
        if (i >= 0 && i < static_cast<ptrdiff_t>(keys.size()))
        {
            m_key = keys.at(i);
        }
    }
    
    // Part1 create bar(s).  A single voice part is not pre-announced, announce it here
    if (m_numParts == 1)
    {
        m_numParts = 2;
    }
    
    const size_t numBars{std::max(piece.m_parts[0].m_bars.size(), static_cast<size_t>(1))};
    
    if (numBars > piece.m_parts[1].m_bars.size())
    {
        piece.m_parts[1].m_bars.resize(numBars);
    }
    piece.m_parts[1].m_bars.back().m_key = m_key;
    m_keySignature1 = Piece::KeySignature(m_key);
    
    if (m_numParts == 3)
    {
        if (numBars > piece.m_parts[2].m_bars.size())
        {
            piece.m_parts[2].m_bars.resize(numBars);
        }
        piece.m_parts[2].m_bars.back().m_key = m_key;
        m_keySignature2 = Piece::KeySignature(m_key);
    }
}

void ParserTab::ParseCmn(std::string_view line, int lineNo, size_t idx, Piece& piece)
{
    // Voice1 create bar(s).  A single voice part is not pre-announced, announce it here
    if (m_numParts == 1)
    {
        m_numParts = 2;
    }
    
    const size_t numBars{std::max(piece.m_parts[0].m_bars.size(), static_cast<size_t>(1))};
    
    if (numBars > piece.m_parts[1].m_bars.size())
    {
        piece.m_parts[1].m_bars.resize(numBars);
        m_keySignature1 = Piece::KeySignature(m_key);
        m_activeAccidentals1.clear();
    }

    // Voice 1 music
    ParseCmnPart(line, lineNo, idx, m_keySignature1, m_activeAccidentals1, piece.m_parts[1]);
    
    if (m_numParts == 3)
    {
        // Voice2 create bar(s)
        if (numBars > piece.m_parts[2].m_bars.size())
        {
            piece.m_parts[2].m_bars.resize(numBars);
            m_keySignature2 = Piece::KeySignature(m_key);
            m_activeAccidentals2.clear();
        }

        if (idx < line.size() && (line[idx] == 'M' || line[idx] == '\t' || line[idx] == 'R'))
        {
            ParseCmnPart(line, lineNo, idx, m_keySignature2, m_activeAccidentals2, piece.m_parts[2]);
        }
    }
    
    // lyrics
    
    // Voice1 lyrics
    if (idx < line.size() && (line[idx] == 'T' || line[idx] == '\t'))
    {
        ++idx;
        ParseLyricsPart(line, idx, piece.m_parts[1].m_bars.back(), m_lyricHyphen1);
    }
    
    if (m_numParts == 3)
    {
        // Voice2 lyrics
        if (idx < line.size() && line[idx] == '^')
        {
            ++idx;
            ParseLyricsPart(line, idx, piece.m_parts[2].m_bars.back(), m_lyricHyphen2);
        }
    }
}

void ParserTab::ParseCmnPart(std::string_view line, int lineNo, size_t& idx, const std::vector<Accidental>& keySignature,
        std::map<Pitch, Accidental>& activeAccidentals, Part& part)
{
    Bar& bar{part.m_bars.back()};
    
    // You may place one tab after the M to line up the music.
    if (idx < line.size() && line[idx] == '\t')
    {
        ++idx;
    }
    
    // M - the next three characters are the time (just like the flag), pitch [A-Ga-g],
    // and + (sharp) -(flat)  . (dot)  ^ (sharp and dot) v (flat and dot) n (natural)
    // or anything else for music above the line of tablature.  For pitch you can also
    // use ? @ for the low F and G and 1 2 3 4 for high a b c d. if the note is R you
    // get a rest. MG gives a g clef in the music M000 does not print anything, it is
    // a space holder. a # following the three characters starts a beamed set, an x
    // following the three characters continues it. A - following the three characters
    // is a tie to the next note. = is a tie at the end of a beamed set.
    
    // An additional useful symbol is the zig-zag at the end of a staff which indicates the pitch of the
    // first note on the following staff of music. Use Z for duration; hence Zb- will a print the zig-zag at
    // bflat, middle line.
   
    if (line.substr(idx, 3) == "000" || line.substr(idx, 1) == "Z" )
    {
        idx += 3;
        return; 
    }

    // clef F G G8a G8b 
    if (line.substr(idx, 3) == "G8a")
    {
        bar.m_clef = ClefG8a;
        m_clef = ClefG8a;
        idx += 3;
        return; 
    }

    if (line.substr(idx, 3) == "G8b")
    {
        bar.m_clef = ClefG8b;
        m_clef = ClefG8b;
        idx += 3;
        return; 
    }

    if (line.substr(idx, 1) == "F")
    {
        bar.m_clef = ClefF;
        m_clef = ClefF;
        ++idx;
        return; 
    }

    if (line.substr(idx, 1) == "G")
    {
        bar.m_clef = ClefG;
        m_clef = ClefG;
        ++idx;
        return; 
    }
    
    if (idx + 2 > line.size())
    {
        return;
    }
    
    bar.m_chords.emplace_back();
    Chord& chord{bar.m_chords.back()};
        
    // duration
    if (!ParseNoteType(line, idx, chord))
    {
        return;
    }
    
    chord.m_notes.emplace_back();
    Note& note{chord.m_notes.back()};
    
    // rest.  Treat as a note to get any dot or tie
    bool rest{line[idx] == 'R'};
    const char* p{nullptr};
    const char cmnPitch[]{"?@ABCDEFGabcdefg1234"};

    if (!rest)
    {
        p = std::strchr(cmnPitch, line[idx]);
        if (p == nullptr)
        {
            rest = true;
            LOGGER<< lineNo << ": \"" << line << "\" ignoring bad note \"" << line[idx] << "\" treat as rest";
        }
    }

    if (!rest)
    {
        const ptrdiff_t i{p - cmnPitch};
        ++idx;
        
        switch (m_clef)
        {
            default:
                [[fallthrough]];
            case ClefNone:
                [[fallthrough]];
            case ClefG:
                note.m_pitch = m_clefGTuning.at(i);
                break;
            case ClefG8a:
                note.m_pitch = m_clefGTuning.at(i);
                note.m_pitch = Pitch(note.m_pitch.Step(), note.m_pitch.Alter(), note.m_pitch.Octave() + 1);
                break;
            case ClefG8b:
                note.m_pitch = m_clefGTuning.at(i);
                note.m_pitch = Pitch(note.m_pitch.Step(), note.m_pitch.Alter(), note.m_pitch.Octave() - 1);
                break;
            case ClefF:
                note.m_pitch = m_clefFTuning.at(i);
                break;
        }
    
        // explicit accidental
        if (line[idx] == '+' || line[idx] == '^')
        {
            // sharp
            note.m_accidental = AccSharp;
            activeAccidentals[note.m_pitch] = note.m_accidental;
        }
        else if (line[idx] == '-' || line[idx] == 'v')
        {
            // flat
            note.m_accidental = AccFlat;
            activeAccidentals[note.m_pitch] = note.m_accidental;
        }
        else if (line[idx] == 'n' || line[idx] == 'N')
        {
            // natural
            note.m_accidental = AccNatural;
            activeAccidentals[note.m_pitch] = note.m_accidental;
        }
    }
    
    // dot: limitation all notes in the chord share the same dot
    chord.m_dotted = (line[idx] == '.' || line[idx] == '^' || line[idx] == 'v' || line[idx] == 'N');
    ++idx;

    // TAB only specifies explicit accidentals, but we need to calculate the pitch of all
    // the notes. Using here the modern CMN rules for accidentals, i.e. an accidental applies to all
    // subsequent notes in the bar on the same staff line or space unless countermanded by
    // another accidental.  With the exception that a tied note, with accidental, that crosses
    // a bar line does not repeat the accidental on the second note, but does have the same pitch.
    //
    // This can be a problem for renaissance lute songs since the CMN rules for accidentals didn't
    // become established until the 18th century.
    
    if (!rest)
    {
        // If this note does not have an accidental and is the first in this bar and 
        // is tied to the last note in the previous bar and they have the same
        // step and octave then set the pitch of this note to be that of the previous note
        
        bool tieAccrossBarLine{false};
        if (note.m_accidental == AccNone && bar.m_chords.size() == 1 && part.m_bars.size() >= 2)
        {
            const Bar& prevBar{part.m_bars.at(part.m_bars.size() - 2)};
            if (!prevBar.m_chords.empty() && prevBar.m_chords.back().m_tie && !prevBar.m_chords.back().m_notes.empty())
            {
                const Note& prevNote{prevBar.m_chords.back().m_notes.back()};
                if (prevNote.m_pitch.Octave() == note.m_pitch.Octave() && prevNote.m_pitch.Step() == note.m_pitch.Step())
                {
                    note.m_pitch = prevNote.m_pitch;
                    tieAccrossBarLine = true;
                }
            }
        }
        
        if (!tieAccrossBarLine)
        {
            // calculate the note's pitch from any active accidental or key signature
            Accidental reqAccidental{AccNone};
            
            const auto itr{activeAccidentals.find(note.m_pitch)};
            if (itr != activeAccidentals.end())
            {
                reqAccidental = itr->second;
            }
            else
            {
                reqAccidental = keySignature.at(note.m_pitch.Step() - 'A');
            }
            
            if (reqAccidental == AccNone)
            {
                reqAccidental = AccNatural;
            }
            
            static_assert(AccFlat == -1);
            static_assert(AccNatural == 0);
            static_assert(AccSharp == 1);
            note.m_pitch = Pitch(note.m_pitch.Step(), static_cast<int>(reqAccidental), note.m_pitch.Octave());
        }
    }
    
    // grid and tie
    if (idx < line.size())
    {
        if (line[idx] == '#')
        {
            chord.m_grid = GridStart;
            ++idx;
        }
        else if (line[idx] == 'x')
        {
            chord.m_grid = GridMid;
            ++idx;
        }
        else if (line[idx] == '-')
        {
            chord.m_tie = true;
            ++idx;
        }
        else if (line[idx] == '=')
        {
            // CMN tie at end of grid
            chord.m_grid = GridMid;
            chord.m_tie = true;
            ++idx;
        }
    }
    
    // if rest then we don't want the note
    if (rest)
    {
        chord.m_notes.pop_back();
    }
}

void ParserTab::ParseLyricsPart(std::string_view line, size_t& idx, Bar& bar, std::vector<bool>& lyricHyphen)
{
    size_t startText{idx};
    
    for (size_t lyricNum{1}; ; ++lyricNum)
    {
        // find word or syllable
        while (idx < line.size() && line[idx] != '^' && line[idx] != '\t')
        {
            ++idx;
        }
        
        if (bar.m_chords.empty())
        {
            LOGGER << "ParseLyricsPart: no chord for lyrics";
        }
        else
        {
            std::string text{CleanTabString(line.substr(startText, idx - startText))};
            const bool thisHyphen{!text.empty() && text.back() == '-'};
            if (thisHyphen)
            {
                text.pop_back();
            }
            
            // did the previous syllable end in a hyphen?
            if (lyricHyphen.size() < lyricNum)
            {
                lyricHyphen.resize(lyricNum, false);
            }
            
            const bool prevHyphen{lyricHyphen.at(lyricNum - 1)};

            const Syllabic syllabic{!prevHyphen && thisHyphen ? SylBegin
                    : prevHyphen && !thisHyphen ? SylEnd
                    : prevHyphen && thisHyphen ? SylMiddle
                    : SylSingle};

            bar.m_chords.back().m_lyrics.emplace_back(text, syllabic);
            assert(bar.m_chords.back().m_lyrics.size() == lyricNum);
            lyricHyphen.at(lyricNum - 1) = thisHyphen;
        }
        
        if (idx == line.size() || line[idx] == '^')
        {
            break;
        }
        
        ++idx;
        startText = idx;
    }
}

bool ParserTab::ParseNoteType(std::string_view line, size_t& idx, Chord& chord)
{
    if (idx >= line.size())
    {
        return false;
    }

    if (line[idx] >= '0' && line[idx] <= '5')
    {
        // a note with that many flags
        // TODO TAB thinks 0 flag is a crotchet.
        chord.m_noteType = static_cast<NoteType>(NoteTypeQuarter + line[idx] - '0');
    }
    else if (line[idx] == 'w')
    {
        // a half note |
        //             0
        chord.m_noteType = NoteTypeHalf;
    }
    else if (line[idx] == 'W')
    {
        // a whole note 0
        chord.m_noteType = NoteTypeWhole;
    }
    else if (line[idx] == 'B')
    {
        // a breve (when followed by notes)
        chord.m_noteType = NoteTypeBreve;
    }
    else if (line[idx] == 'L')
    {
        // longa
        chord.m_noteType = NoteTypeLong;
    }
    else
    {
        return false;
    }

    ++idx;
    return true;
}

void ParserTab::SetTuningTab(std::string_view s, std::vector<Pitch>& tuning)
{
    tuning.clear();
    
    for (size_t i{0}; i < s.size(); )
    {
        char step{'\0'};
        int alter{0};
        int octave{0};
        
        //  Tab allows both upper and lower case
        const char c{static_cast<char>(std::toupper(s[i]))};
        if (c < 'A' || c > 'G')
        {
            throw std::runtime_error("Error: Tab note syntax");
        }
        step = c;
        ++i;
        
        alter = 0;
        while (s[i] == '-')
        {
            --alter;
            ++i;
        }
        while (s[i] == '+')
        {
            ++alter;
            ++i;
        }
        
        if (s[i] < '0' || s[i] > '9')
        {
            throw std::runtime_error("Error: Tab octave syntax");
        }
        
        // Tab octaves decrease with increasing pitch, and run A..G
        octave = 6 - (s[i] - '0');
        if (step == 'A' || step == 'B')
        {
            --octave;
        }
        
        ++i;
        
        // Tab lowest to highest
        tuning.insert(tuning.begin(), Pitch(step, alter, octave));
    }
}

void ParserTab::ParseJHRTuning(Piece& piece)
{
    Part& partTab{piece.m_parts[0]};
    
    bool cittern{false};
    bool french{false};
    bool diatonic{false};
    bool transcribed{false};
    
    // look for clues in the text that this is for cittern and french tuning and diatonic or transcribed
    ParseJHRKeywords(piece.m_title, cittern, french, diatonic, transcribed);
    ParseJHRKeywords(piece.m_composer, cittern, french, diatonic, transcribed);
    for (const auto& text : piece.m_credits)
    {
        ParseJHRKeywords(text, cittern, french, diatonic, transcribed);
    }

    if (diatonic && cittern && !transcribed && partTab.m_staffLines == 4)
    {
        partTab.m_missingFrets = {4, 18};
    }
    
    if (!partTab.m_tuning.empty())
    {
        return; // tuning already specified
    }

    // JHR sometimes encodes the tuning in the title.  Absolute, relative or both
    // 10. Courante Ballarde Saman - 7F8E9C A14B18
    //                               ^^^^^^
    // R6i. Rocantins - Bouvier - (dedff) A12A8BB8/Ballard 1631, ff. 46v-47r
    //                             ^^^^^
    // App 5. Ringing, or Bell-Galliard - (dedff) 7F8Ef9D10C11Bf A8B10/Mace 1676,  p. 180
    //                                     ^^^^^  ^^^^^^^^^^^^^^
    // Note that this last examples does not specify the tuning of the 6th course.
    
    // Absolute tuning
    static const std::regex reAbs{R"([ \-]((\d\d?[A-G](b|f|flat|#|s|sharp)? ?)+)([ /]|$))"};
    std::smatch results;
    std::string jhrAbsolute;
    if (std::regex_search(piece.m_title, results, reAbs))
    {
        jhrAbsolute = results.str(1);
    }
    
    if (jhrAbsolute.empty())
    {
        // sometimes seen instread of "6F"
        if (piece.m_title.find("6th course tuned to F") != std::string::npos)
        {
            jhrAbsolute = "6F";
        }
        else if (piece.m_title.find("7th in D") != std::string::npos || piece.m_title.find("7th to D") != std::string::npos)
        {
            jhrAbsolute = "7D";
        }
    }
    
    if (!jhrAbsolute.empty())
    {
        LOGGER << "JHR Absolute Tuning: " << jhrAbsolute;
    }
    
    // Relative tuning, but ignore if "trans"cribed is present
    static const std::regex reRel{R"(\(([a-h]{3,6})\))"};
    std::string jhrRelative;
    if (std::regex_search(piece.m_title, results, reRel) && !transcribed)
    {
        jhrRelative = results.str(1);
    }

    if (!jhrRelative.empty())
    {
        LOGGER << "JHR Relative Tuning: " << jhrRelative;
    }
    
    // bandora?
    const bool bandora{ (piece.m_title.find("bandora") != std::string::npos ||
                        piece.m_title.find("BANDORA") != std::string::npos) &&
                        !transcribed };

    if (jhrAbsolute.empty() && jhrRelative.empty() && bandora)
    {
        Pitch::SetTuning("A3 E3 C3 G2 D2 C2 G1", partTab.m_tuning);
        return;
    }
    
    if (jhrAbsolute.empty() && jhrRelative.empty() && partTab.m_staffLines != 4)
    {
        return;
    }

    // 4 course?
    if (jhrAbsolute.empty() && (jhrRelative.size() == 3 || partTab.m_staffLines == 4))
    {
        if (cittern)
        {
            if (french || diatonic)
            {
                // Assume French cittern tuning.
                Pitch::SetTuning("E4 D4 G3 A3", partTab.m_tuning);
            }
            else
            {
                // Assume Italian cittern tuning.
                Pitch::SetTuning("E4 D4 G3 B3", partTab.m_tuning);
            }
        }
        else
        {
            // Assume guitar/gittern
            Pitch::SetTuning("A4 E4 C4 G3", partTab.m_tuning);
        }
        
        // apply relative tuning, if any from high to low pitch
        if (jhrRelative.size() + 1 > partTab.m_tuning.size())
        {
            partTab.m_tuning.resize(jhrRelative.size() + 1);
        }
        
        for (size_t i{1}; i <= jhrRelative.size(); ++i)
        {
            partTab.m_tuning[i] = partTab.m_tuning[i - 1] - (jhrRelative[i - 1] - 'a');
        }

        return;
    }
    
    // start with default tuning, then apply JHR absolute tuning and allocate octaves.
    // then apply relative tuning
    if (bandora)
    {
        Pitch::SetTuning("A3 E3 C3 G2 D2 C2 G1", partTab.m_tuning);
    }
    else if (piece.m_title.find("baroque") != std::string::npos || piece.m_title.find("BAROQUE") != std::string::npos)
    {
        Pitch::SetTuningBaroque(partTab.m_tuning); // baroque
    }
    else
    {
        Pitch::SetTuningVieilTon(partTab.m_tuning); // vieil ton
    }
    
    std::vector<Pitch> jhrTuning;
    
    for (size_t i = 0; i < jhrAbsolute.size();)
    {
        if (jhrAbsolute[i] == ' ')
        {
            ++i;
            continue;
        }
        
        size_t string{0};
        while (static_cast<bool>(isdigit(jhrAbsolute[i])))
        {
            string = string * 10 + jhrAbsolute[i] - '0';
            ++i;
        }
        
        if (string == 0 || string > Course14)
        {
            return; // ignore if not sensible
        }

        const char step{jhrAbsolute[i]};
        ++i;
        
        int alter{0};
        if (jhrAbsolute[i] == 'f' || jhrAbsolute[i] == 'b')
        {
            ++i;
            alter = -1;
            
            // skip lat
            while (static_cast<bool>(static_cast<bool>(isalpha(jhrAbsolute[i]))))
            {
                ++i;
            }
        }
        else if (jhrAbsolute[i] == '#' || jhrAbsolute[i] == 's')
        {
            ++i;
            alter = 1;
            
            // skip harp
            while (static_cast<bool>(static_cast<bool>(isalpha(jhrAbsolute[i]))))
            {
                ++i;
            }
        }
        
        if (jhrTuning.size() < string)
        {
            jhrTuning.resize(string);
        }
        jhrTuning[string - 1] = Pitch(step, alter, 0); // The octave is not specified, allocated below
    }
    
    const size_t minCourses{std::max(jhrTuning.size(), jhrRelative.size() + 1)};
    if (minCourses > partTab.m_tuning.size())
    {
        // The JHR tuning needs more courses than the default tuning.
        // Fill the remainder of partTab.m_tuning with copies of the last pitch.
        const auto filler{partTab.m_tuning.back()};
        std::fill_n(std::back_inserter(partTab.m_tuning), minCourses - partTab.m_tuning.size(), filler);
    }
    
    for (size_t i{0}; i < jhrTuning.size(); ++i)
    {
        if (jhrTuning[i].Step() == '\0')
        {
            continue; // not JHR specified, use default
        }
        
        // octave not specified - herustics.
        if (i == 0)
        {
            // first course assume same octave as default
            partTab.m_tuning[i] = Pitch(jhrTuning[i].Step(), jhrTuning[i].Alter(), partTab.m_tuning[i].Octave());
        }
        else
        {
            // second and subsequent course, assume course[i] lower pitch than course[i - 1]
            // This will give the wrong result with re-entrant tuning.
            for (int octave{partTab.m_tuning[i - 1].Octave()}; ; --octave)
            {
                partTab.m_tuning[i] = Pitch(jhrTuning[i].Step(), jhrTuning[i].Alter(), octave);
                if (partTab.m_tuning[i].Midi() < partTab.m_tuning[i - 1].Midi())
                {
                    break;
                }
            }
        }
    }
    
    // apply relative tuning, from low to high pitch
    for (size_t i{jhrRelative.size()}; i > 0; --i)
    {
        partTab.m_tuning[i - 1] = partTab.m_tuning[i] + (jhrRelative[i - 1] - 'a');
    }
} 

void ParserTab::ParseJHRKeywords(std::string_view text, bool& cittern, bool& french, bool& diatonic, bool& transcribed)
{
    diatonic = diatonic || text.find("diatonic") != std::string_view::npos;
    cittern = cittern || text.find("cittern") != std::string_view::npos;
    french = french || text.find("french") != std::string_view::npos;
    transcribed = transcribed || text.find("trans") != std::string_view::npos;
};

} // namespace luteconv
