#include "parsermxl.h"

#include <vector>
#include <string>

#include "parsermusicxml.h"
#include "unzipper.h"
#include "logger.h"

namespace luteconv
{

void ParserMxl::Parse(std::istream& srcFile, const Options& options, Piece& piece)
{
    LOGGER << "Parse mxl";
    
    std::vector<char> image;
    std::string zipFilename;
    Unzipper::Unzip(srcFile, image, zipFilename);
    ParserMusicXml::Parse(zipFilename, image.data(), image.size(), options, piece);
}

} // namespace luteconv
