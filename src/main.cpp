#include "converter.h"

#include <cstdlib>
#include <iostream>
#include <stdexcept>
#include <string>

/**
 * Convert between various lute tablature file formats
 * 
 * @param[in] argc number of arguments
 * @param[in] argv arguments.
 * @retval 0 => OK
 * @retval 1 => error
 */
int main(int argc, char *argv[]) 
{
    try
    {
        luteconv::Options options;
        if (options.ProcessArgs(argc, argv))
        {
            luteconv::Converter::Convert(options);
        }
    }
    catch (const std::exception & e)
    {
        std::cerr << e.what() << '\n';
        return 1;
    }
    
    return 0;
}
