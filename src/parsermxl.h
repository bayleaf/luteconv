#ifndef _PARSERMXL_H_
#define _PARSERMXL_H_

#include "parser.h"

#include <string>

namespace luteconv
{

/**
 * Parse MusicXML .mxl file
 */
class ParserMxl: public Parser
{
public:

    /**
     * Constructor
    */
    ParserMxl() = default;

    /**
     * Destructor
     */
    ~ParserMxl() override = default;
    
    /**
     * Parse .mxl file
     *
     * @param[in] src stream
     * @param[in] options
     * @param[out] piece destination
     */
    void Parse(std::istream& srcFile, const Options& options, Piece& piece) override;
};

} // namespace luteconv

#endif // _PARSERMXL_H_
