#include "parserft3.h"

#include <algorithm>
#include <array>
#include <iomanip>
#include <iterator>
#include <map>
#include <regex>
#include <stdexcept>

#include "charset.h"
#include "logger.h"
#include "gunzipper.h"

namespace luteconv
{

enum Bits : unsigned int
{
    Bit0  = 1 << 0,
    Bit1  = 1 << 1,
    Bit2  = 1 << 2,
    Bit3  = 1 << 3,
    Bit4  = 1 << 4,
    Bit5  = 1 << 5,
    Bit6  = 1 << 6,
    Bit7  = 1 << 7,
    Bit8  = 1 << 8,
    Bit9  = 1 << 9,
    Bit10 = 1 << 10,
    Bit11 = 1 << 11,
    Bit12 = 1 << 12,
    Bit13 = 1 << 13,
    Bit14 = 1 << 14,
    Bit15 = 1 << 15,
};

void ParserFt3::Parse(std::istream& srcFile, const Options& options, Piece& piece)
{
    LOGGER << "Parse ft2 and ft3 " << options.m_srcFilename;
    
    Part& part{piece.m_parts.at(0)}; // only 1 part

    // .ft3 files are (usually) gzipped.  A curious choice of compression for a Windows program.
    std::vector<uint8_t> ft3Image;
    Gunzipper::Gunzip(srcFile, ft3Image);
    
    // if CFronimoCntrItem is present then advance class tags by 2
    uint8_t cpieceIndex{1};
    uint8_t cbarIndex{3};
    const std::string_view cFronimoCntrItem{"CFronimoCntrItem"};
    auto ItrFronimoCntrItem = std::search(ft3Image.cbegin(), ft3Image.cend(), cFronimoCntrItem.cbegin(), cFronimoCntrItem.cend());
    if (ItrFronimoCntrItem != ft3Image.cend())
    {
        LOGGER << "CFronimoCntrItem present";
        cpieceIndex = 3;
        cbarIndex = 5;
    }

    const std::string_view cpiece{"CPiece"};
    auto pieceBegin = std::search(ft3Image.cbegin(), ft3Image.cend(), cpiece.cbegin(), cpiece.cend());
    if (pieceBegin == ft3Image.cend())
    {
        throw std::runtime_error(std::string("Error: CPiece not found"));
    }

    pieceBegin += static_cast<long int>(cpiece.size());
    // find our piece
    auto pieceEnd{ft3Image.cend()};
    int pieceNo{0};
    const std::array<const uint8_t, 2> cPieceTag{cpieceIndex, 0x80}; // "CPiece" class tag
    
    for (auto itr{pieceBegin}; ;)
    {
        pieceEnd = std::search(itr, ft3Image.cend(), cPieceTag.cbegin(), cPieceTag.cend());

        // The CPiece class tag might occur as data, not a CPiece class tag.  Herustic
        // to spot a valid CPiece class tag.  The CPiece class tag is followed by 10 unknown bytes
        // then {\rtf1.  Assume this might be variable.
        
        const ptrdiff_t pieceEndFindSize{32};
        auto last{pieceEnd + pieceEndFindSize};
        if (std::distance(pieceEnd, ft3Image.cend()) >= pieceEndFindSize
                && std::find(pieceEnd, last, '{') == last)
        {
            // not a CPiece class tag
            itr = pieceEnd + cPieceTag.size();
            continue;
        }
        
        if (pieceNo == options.m_index)
        {
            break;
        }

        if (pieceEnd == ft3Image.cend())
        {
            throw std::invalid_argument("Error: Can't find piece index=" + std::to_string(options.m_index));
        }

        pieceBegin = pieceEnd + cPieceTag.size();
        itr = pieceBegin;
        ++pieceNo;
    }

    auto headerEnd{pieceEnd};
    auto bodyBegin{pieceEnd};
    ptrdiff_t tabTypeIndex{};

    if (pieceNo == 0)
    {
        const std::string_view cbar{"CBar"};
        headerEnd = std::search(pieceBegin, pieceEnd, cbar.cbegin(), cbar.cend());
        bodyBegin = headerEnd + static_cast<long int>(cbar.size());
        tabTypeIndex = -34;
    }
    else
    {
        const std::array<const uint8_t, 2> cBarTag{cbarIndex, 0x80}; // "CBar" class tag
        headerEnd = std::search(pieceBegin, pieceEnd, cBarTag.cbegin(), cBarTag.cend());
        bodyBegin = headerEnd + cBarTag.size();
        tabTypeIndex = -26;
    }

    if (headerEnd == pieceEnd)
    {
        throw std::runtime_error(std::string("Error: CBar not found"));
    }

    ParseHeader(pieceBegin, piece);
    
    // the tab type byte is calculated backwards from the start of the first bar.
    if (std::distance(pieceBegin, bodyBegin + tabTypeIndex) > 0)
    {
        ParseTabType(options, bodyBegin[tabTypeIndex], part);
    }
    
    ParseBody(options, ft3Image.begin(), bodyBegin, pieceEnd, part);
    ParseTuning(options, pieceBegin, pieceEnd, part);

    // remove any trailing empty bars
    while (!part.m_bars.empty() && part.m_bars.back().m_chords.empty())
    {
        part.m_bars.pop_back();
    }

    if (!part.m_bars.empty())
    {
        // final bar
        part.m_bars.back().m_barStyle = BarStyleLightHeavy;
    }

    part.SetTuning(options);
}

void ParserFt3::ParseHeader(const std::vector<uint8_t>::const_iterator headerBegin, Piece& piece)
{
    auto ptr{headerBegin + 8};
    piece.m_title = ExtractText(ptr);
    LOGGER << "title=" << piece.m_title;

    const std::string author{ExtractText(ptr)};
    LOGGER << "author=" << author;
    if (!author.empty())
    {
        piece.m_credits.emplace_back(author);
    }

    // Seen the composer field being used for (long) notes
    const size_t maxComposer{40};
    const std::string composer{ExtractText(ptr)};
    LOGGER << "composer=" << composer;
    if (composer.size() <= maxComposer)
    {
        piece.m_composer = composer;
    }
    else if (!composer.empty())
    {
        piece.m_credits.emplace_back(composer);
    }
}

std::string ParserFt3::ExtractText(std::vector<uint8_t>::const_iterator& ptr)
{
    int strLen{*ptr++};
    if (strLen == 255)
    {
        // 16 bit string length
        strLen = *ptr++;
        strLen += *ptr++ << 8;
    }

    
    const char * textPtr{reinterpret_cast<const char *>(&*ptr)};
    ptr += strLen;
    if (strLen > 0 && *textPtr != '{' && textPtr[strLen] == '\0')
    {
        // raw text, null not included in strLen
        ++ptr;
    }
    return ExtractRtf(textPtr, strLen);
}

// Extract the text from RTF.
std::string ParserFt3::ExtractRtf(const char * rtfBegin, int strLen)
{
    // "{\rtf1\ansi\ansicpg1252\deff0\deflang1031{\fonttbl{\f0\fnil\fcharset0 MS Shell Dlg;}}\r\n
    // \viewkind4\uc1\pard\f0\fs-22 Johann Sebastian Bach\par\r\n
    // BWV 997\par
    // \r\n}
    // \r\n"

    std::string rtf{rtfBegin, rtfBegin + strLen};

    // c++ regex doesn't have the equivalent of perl's /s to enable matching \n and \r
    rtf.erase(std::remove(rtf.begin(), rtf.end(), '\r'), rtf.end());
    std::replace(rtf.begin(), rtf.end(), '\n', ' ');

    // remove outer {}
    {
        static const std::regex re{R"(^\s*\{(.*)\}\s*$)"};
        std::smatch results;
        if (std::regex_search(rtf, results, re))
        {
            rtf = results.str(1);
        }
    }

    // remove any {text} maybe nested
    {
        static const std::regex re{R"(\{[^\{\}]*\}\s*)"};
        std::smatch results;
        while (std::regex_search(rtf, results, re))
        {
            rtf = results.prefix().str() + results.suffix().str();
        }
    }

    // remove any \stuff\morestuff...
    {
        static const std::regex re{R"((\\[\w\-;]+)+)"};
        std::smatch results;
        while (std::regex_search(rtf, results, re))
        {
            rtf = results.prefix().str() + results.suffix().str();
        }
    }

    // remove leading and trailing spaces
    rtf.erase(rtf.begin(), find_if_not(rtf.begin(), rtf.end(),[](int c){return isspace(c);}));
    rtf.erase(find_if_not(rtf.rbegin(), rtf.rend(),[](int c){return isspace(c);}).base(), rtf.end());
    

    // Mac has been seen to leave a trailing null
    while (!rtf.empty() && rtf.back() == '\0')
    {
        LOGGER << "Error: trailing null: " << rtf;
        rtf.pop_back();
    }

    // deal with \'xx escapes
    std::string unRtf;
    for (size_t i{0}; i < rtf.size();)
    {
        if (rtf.size() - i >= 4 && rtf[i] == '\\' && rtf[i + 1] == '\'')
        {
            const char c{static_cast<char>(strtol(rtf.substr(i + 2, 2).data(), nullptr, 16))};
            if (c != 0)
            {
                unRtf += c;
                i += 4;
            }
            else
            {
                unRtf += rtf[i];
                ++i;
            }

        }
        else
        {
            unRtf += rtf[i];
            ++i;
        }
    }

    // Windows CP1252
    return CharSet::Cp1252ToUtf8(unRtf);
}

void ParserFt3::ParseTabType(const Options& options, uint8_t tabType, Part& part)
{
    if (options.m_srcFormat == FormatFt2)
    {
        // TODO can ft2 specify a tab type?
        part.m_tabType = TabFrench;
        return;
    }
    
    switch (tabType & (Bit2 | Bit1 | Bit0))
    {
        case 0:
            part.m_tabType = TabItalian;
            break;
        case 1:
            part.m_tabType = TabFrench;
            break;
        case 2:
            part.m_tabType = TabSpanish;
            break;
        case 3:
            part.m_tabType = TabNeapolitan;
            break;
        case 4:
            part.m_tabType = TabGerman;
            switch (tabType & (Bit4 | Bit3))
            {
                case 0:
                    part.m_tabGermanSubtype = Gst1AB;
                    break;
                case Bit3:
                    part.m_tabGermanSubtype = GstABC;
                    break;
                case Bit4:
                    part.m_tabGermanSubtype = Gst123;
                    break;
                default:
                    part.m_tabGermanSubtype = Gst1AB;
                    LOGGER << "unknown German tablature subtype 0x" << static_cast<unsigned>(tabType);
                    break;
            }
            break;
        default:
            LOGGER << "unknown tabType 0x" << std::hex << static_cast<unsigned>(tabType);
            break;
    }
}

void ParserFt3::ParseBody(const Options& options, const std::vector<uint8_t>::const_iterator ft3ImageBegin, const std::vector<uint8_t>::const_iterator bodyBegin,
        const std::vector<uint8_t>::const_iterator bodyEnd, Part& part)
{
    const std::array<const uint8_t, 2> cBarTag{0x03, 0x80}; // "CBar" class marker
    auto barBegin = bodyBegin;

    for (;;)
    {
        const int barNum = static_cast<int>(part.m_bars.size()) + 1;
        auto barEnd = std::search(barBegin, bodyEnd, cBarTag.cbegin(), cBarTag.cend());
        ParseBar(options, ft3ImageBegin, barNum, barBegin, barEnd, part);
        barBegin = barEnd;

        if (barBegin == bodyEnd)
        {
            break;
        }

        barBegin += cBarTag.size();
    }
}

void ParserFt3::ParseBar(const Options& options, const std::vector<uint8_t>::const_iterator ft3ImageBegin, int barNum, const std::vector<uint8_t>::const_iterator barBegin,
        const std::vector<uint8_t>::const_iterator barEnd, Part& part)
{
    Bar bar;
    static Chord previousChord;

    ParseBarProperties(barBegin, bar, part);

    // ft3 should be 32 bytes after barBegin, Bach_BWV_997.ft3 has some notes starting 2 bytes earlier
    auto ptr = barBegin + (options.m_srcFormat == FormatFt3 ? 28 : 22);

    // chords
    for (;;)
    {
        //LOGGER << "bar=" << barNum << " pos=#"
        //       << std::hex << std::distance(ft3ImageBegin, ptr) << " hex=#" << HexDump(flagStart, flagSize)

        // find the next note
        auto nextNote{ptr};

        while (std::distance(nextNote, barEnd) >= 9 && !AtNextNote(nextNote))
        {
            ++nextNote;
        }

        if (!AtNextNote(nextNote))
        {
            break;
        }

        Chord chord;

        // Rhythm flag, nominally 4 bytes as per Luke's documentation, xx yy ?? ??
        // But also observed in the wild:
        // variable number bytes then xx yy ?? ??
        // 1 byte flag, same as previous note
        // 6 byte flag xx yy 02 10 00 00
        // 5 bytes: 4 byte flag followed by 1 byte flag: rest then note with same flags
        auto flagSize{std::distance(ptr, nextNote)};
        auto flagStart{ptr};
        auto flagStop{nextNote};
        
        if (flagSize >= 16 && AtRest(flagStop - 16))
        {
            flagStart = flagStop - 16;
            flagSize = 4;
            nextNote = flagStop - 10;
            
            //LOGGER << "bar=" << barNum << " rest pair pos=#"
            //       << std::hex << std::distance(ft3ImageBegin, flagStart) << " hex=#" << HexDump(flagStart, flagSize);
        }
        else if (flagSize >= 10 && AtRest(flagStop - 10))
        {
            flagStart = flagStop - 10;
            flagSize = 4;
            nextNote = flagStop - 4;
            
            //LOGGER << "bar=" << barNum << " rest pos=#"
            //       << std::hex << std::distance(ft3ImageBegin, flagStart) << " hex=#" << HexDump(flagStart, flagSize);
        }
        else if (flagSize >= 6 && AtNextFlag(flagStop - 4))
        {
            // usual case
            flagSize = 4;
            flagStart = flagStop - 4;
        }
        else if (flagSize >= 7 && AtNextFlag(flagStop - 5))
        {
            // 4 byte rest followed by 1 byte flag
            flagSize = 4;
            flagStart = flagStop - 5;
            nextNote = flagStop - 1;
            
            //LOGGER << "bar=" << barNum << " rest followed 1 byte flag pos=#"
            //       << std::hex << std::distance(ft3ImageBegin, flagStart) << " hex=#" << HexDump(flagStart, flagSize);
        }
        else if (flagSize >= 6)
        {
            flagStart = flagStop - 4;
            if (!AtNextFlag(flagStart)) // not 4 byte flag
            {
                --flagStart;    // try 4 byte flag followed by 1 byte
            }
            if (!AtNextFlag(flagStart))
            {
                --flagStart; // try 6 byte flag
            }
            if (!AtNextFlag(flagStart))
            {
                LOGGER << "bar=" << barNum << " unknown use 1 byte flag flagSize=" << flagSize << " pos=#"
                       << std::hex << std::distance(ft3ImageBegin, flagStart) << " hex=#" << HexDump(ptr, flagSize);

                flagStart += 5; // give up, use one byte flag
            }
        }

        if (flagSize >= 4 && !AtNextFlag(flagStart))
        {
            // rubbish flag, assume same flag as previous note
            LOGGER << "bar=" << barNum << " bad flag size=" << flagSize << " pos=#"
                   << std::hex << std::distance(ft3ImageBegin, flagStart) << " hex=#" << HexDump(flagStart, flagSize);
            flagStart = flagStop - 1;
        }

        auto flagAvailSize{std::distance(flagStart, flagStop)};

        if (flagAvailSize >= 4)
        {
            static_assert(NoteTypeQuarter == 4);
            chord.m_noteType = static_cast<NoteType>(flagStart[0] + 2); // crotchet with 0 flags
            
            // Some Fronimo files have grids on 1 flag rhythm signs (quavers), but Fronimo doesn't show them
            if (chord.m_noteType >= NoteType16th)
            {
                if (static_cast<bool>(flagStart[1] & Bit1))
                {
                    chord.m_grid = GridStart;
                }
                else if (static_cast<bool>(flagStart[1] & Bit2))
                {
                    chord.m_grid = GridMid;
                }
                else if (static_cast<bool>(flagStart[1] & Bit3))
                {
                    chord.m_grid = GridEnd;
                }
            }
            chord.m_dotted = static_cast<bool>(flagStart[1] & Bit4);
            chord.m_fermata = static_cast<bool>(flagStart[2] & Bit0);
        }
        else if (flagAvailSize == 1 || flagAvailSize == 3)
        {
            chord.m_noFlag = true;
            chord.m_noteType = previousChord.m_noteType;
            chord.m_dotted = previousChord.m_dotted;
        }
        else
        {
            LOGGER << "bar=" << barNum << " flagAvailSize=" << flagAvailSize << " pos=#" << std::hex << std::distance(ft3ImageBegin, flagStart)
                   << " flag=#" << HexDump(flagStart, flagAvailSize);
        }

        ptr = nextNote;

        // notes
        while (AtNextNote(ptr))
        {
            // non-note?  Appear where tablature letters would otherwise go, e.g. # below a letter
            if (ptr[1] == 0x20)
            {
#if false
                // TODO discover more non-note characters
                static const std::map<uint8_t, char> nonNoteMap{{0x10, ','}, {0x14, ','}, {0x20, '^'},
                    {0x28, '~'}, {0x2C, '~'}, {0x38, '#'}, {0x3C, '/'}, {0x3E, '\\'}};
                auto found{nonNoteMap.find(ptr[3])};
                const char nonNote{found != nonNoteMap.end() ? found->second : '?'};
                LOGGER << "bar=" << barNum << " non-note " << nonNote << " pos=#" << std::hex << std::distance(ft3ImageBegin, ptr)
                       << " flag=#" << HexDump(ptr, 5);
#endif
                ptr += 5;
                continue;
            }

            Note note;

            if (ptr[0] < 8)
            {
                note.m_course = ptr[0] - 1; // string 2..7 => 1..6
                note.m_fret = ptr[1] - '0'; // fret a..p
            }
            else if (ptr[0] == 8)
            {
                // use the note flag slot to determine what kind of string or fret we have here...
                // Bit7 means red.  Bit6 means use slashes, else ledger lines.
                const uint8_t detail{static_cast<uint8_t>(ptr[4] & ~(Bit7 | Bit2 | Bit1 | Bit0))};
                
                if (detail == 0)
                {
                    // fretted 7th course, letter or number will be given
                    note.m_course = Course7;
                    note.m_fret = (ptr[1] < 'a') ? ptr[1] - '0' : ptr[1] - 'a';
                }
                else if (detail == Bit5)
                {
                    // open diapason string below 7th (00 = 7, 01 = 8 etc)
                    note.m_course = ptr[1] - '0' + Course7;
                    note.m_fret = 0;
                }
                else if (detail == (Bit6 | Bit3) || detail == Bit3)
                {
                    // means a fretted 8th - letter will be given
                    note.m_course = Course8;
                    note.m_fret = (ptr[1] < 'a') ? ptr[1] - '0' : ptr[1] - 'a';
                }
                else if (detail == (Bit6 | Bit4) || detail == Bit4)
                {
                    // means a fretted 9th - letter will be given
                    note.m_course = Course9;
                    note.m_fret = (ptr[1] < 'a') ? ptr[1] - '0' : ptr[1] - 'a';
                }
                else if (detail == (Bit6 | Bit4 | Bit3) || detail == (Bit4 | Bit3))
                {
                    // means a fretted 10th - letter will be given
                    note.m_course = Course10;
                    note.m_fret = (ptr[1] < 'a') ? ptr[1] - '0' : ptr[1] - 'a';
                }
                else
                {
                    LOGGER << "bar=" << barNum << " Unknown fretted diapason.  ptr[4]=#" << std::hex << static_cast<int>(ptr[4])
                           << " pos=#" << std::hex << std::distance(ft3ImageBegin, ptr);
                    note.m_course = Course7;
                    note.m_fret = (ptr[1] < 'a') ? ptr[1] - '0' : ptr[1] - 'a';
                }
            }

            const uint16_t extras = ptr[3] << 8 | ptr[2];
            note.m_rightFingering = GetRightFingering(extras);
            note.m_leftFingering = GetLeftFingering(extras);
            note.m_rightOrnament = GetRightOrnament(extras);
            note.m_leftOrnament = GetLeftOrnament(extras);

            ptr += 5;
            chord.m_notes.push_back(note);
        }
        bar.m_chords.push_back(chord);
        previousChord = chord;
    }

    if (!bar.m_chords.empty())
    {
        part.m_bars.push_back(bar);
    }
}

std::string ParserFt3::HexDump(const std::vector<uint8_t>::const_iterator start, ptrdiff_t len)
{
    std::ostringstream ss;
    for (auto it = start; it < start + len; ++it)
    {
        if (it != start)
        {
            ss << ' ';
        }
        ss << std::hex << std::setw(2) << std::setfill('0') << static_cast<int>(*it);
    }
    
    return ss.str();
}

void ParserFt3::ParseBarProperties(const std::vector<uint8_t>::const_iterator barBegin, Bar& bar, Part& part)
{
    const uint8_t timeSignature = barBegin[0] & (Bit2 | Bit1 | Bit0);

    // time signature
    switch (timeSignature)
    {
        case 1:
            // Common time C
            bar.m_timeSig.m_timeSymbol = TimeSyCommon;
            bar.m_timeSig.m_beats = 4;
            bar.m_timeSig.m_beatType = 4;
            break;

        case 2:
            // alla breve, cut C
            bar.m_timeSig.m_timeSymbol = TimeSyCut;
            bar.m_timeSig.m_beats = 2;
            bar.m_timeSig.m_beatType = 2;
            break;

        case 3:
            // 3
            [[fallthrough]];

        case 4:
            // C3
            [[fallthrough]];

        case 5:
            // cut C 3
            bar.m_timeSig.m_timeSymbol = TimeSySingleNumber;
            bar.m_timeSig.m_beats = 3;
            bar.m_timeSig.m_beatType = 4;
            break;

        case 6:
            bar.m_timeSig.m_timeSymbol = TimeSyNormal;
            bar.m_timeSig.m_beats = barBegin[9];
            bar.m_timeSig.m_beatType = barBegin[8];
            break;

        default:
            bar.m_timeSig.m_timeSymbol = TimeSyNone;
            bar.m_timeSig.m_beats = 4;
            bar.m_timeSig.m_beatType = 4;
    }
    
    // Fronimo also support additional time signatures
    // barBegin[1] & 0xF0
    // 0x20 O, 0x40 O cut, 0x60 O dot, 0x80 O cut dot, 0xA0 C dot, 0xC0 C cut dot
    
    // volta brackets
    if (static_cast<bool>(barBegin[0] & Bit5))
    {
        bar.m_volta = 1;
    }
    else if (static_cast<bool>(barBegin[0] & Bit6))
    {
        bar.m_volta = 2;
    }
    
    // Repeats
    if (static_cast<bool>(barBegin[0] & Bit4))
    {
        bar.m_repeat = RepBackward;
    }
    
    if (static_cast<bool>(barBegin[0] & Bit3) && !part.m_bars.empty())
    {
        // forward repeat belongs in previous bar - our bars only own their right hand bar line.
        if (part.m_bars.back().m_repeat == RepBackward)
        {
            part.m_bars.back().m_repeat = RepJanus;
        }
        else
        {
            part.m_bars.back().m_repeat = RepForward;
        }
    }
    
    // barstyle right hand bar
    if (static_cast<bool>(barBegin[0] & Bit7))
    {
        bar.m_barStyle = static_cast<bool>(barBegin[1] & Bit0) ? BarStyleLightHeavy : BarStyleLightLight;
    }
    
    // barstyle left hand bar belongs in previous bar - our bars only own their right hand bar line.
    if (static_cast<bool>(barBegin[1] & Bit4) && !part.m_bars.empty())
    {
        part.m_bars.back().m_barStyle = (part.m_bars.back().m_repeat == RepJanus) ? BarStyleHeavyHeavy : BarStyleHeavyLight;
    }
    
    // dashed left hand bar line, belongs in previous bar - our bars only own their right hand bar line.
    if (static_cast<bool>(barBegin[1] & Bit3) && !part.m_bars.empty() && part.m_bars.back().m_barStyle == BarStyleRegular)
    {
        part.m_bars.back().m_barStyle = BarStyleDashed;
    }
}

bool ParserFt3::AtNextFlag(const std::vector<uint8_t>::const_iterator ptr)
{
    return ptr[0] >= 1 && ptr[0] < 9 && (ptr[1] < '0' || ptr[1] > '>');
}

bool ParserFt3::AtRest(const std::vector<uint8_t>::const_iterator ptr)
{
    // flag and possible dot, but no grid
    return ptr[0] >= 1 && ptr[0] < 9 && (ptr[1] & ~(Bit7 | Bit6 | Bit5 | Bit4)) == 0 && ptr[2] == 0 && ptr[3] == 0;
}

// detects if we are at another note or non-note in the current chord
bool ParserFt3::AtNextNote(const std::vector<uint8_t>::const_iterator ptr)
{
    const bool onNonNote{ptr[1] == 0x20 && ptr[2] == 0 && (ptr[5] < '0' || ptr[5] > '>')};
    const bool onFret{(ptr[1] >= '0') && (ptr[1] <= '>')}; // up to fret p
    const bool onDiapason{(ptr[0] == 8) && (ptr[1] >= 'a') && (ptr[1] <= 'i')}; // up to fret i on diapasons
    const bool ornOk{ptr[2] < ' ' || ptr[2] >= 0x80 || ptr[3] < ' ' || ptr[3] >= 0x80}; // herustic to avoid ascii text for fonts
    const bool onString{(ptr[0] >= 2) && (ptr[0] <= 8)};

    return onString && (onNonNote || onFret || onDiapason) && ornOk;
}

Fingering ParserFt3::GetRightFingering(uint16_t extras)
{
    // right hand fingering
    if (static_cast<bool>(extras & Bit1)) // thumbstroke
    {
        return FingerThumb;
    }
    if (static_cast<bool>(extras & Bit2)) // . underneath
    {
        return FingerFirst;
    }
    if (static_cast<bool>(extras & Bit3)) // .. underneath
    {
        return FingerSecond;
    }

    // TODO tab supports ... with ";", does Fronimo?

    return FingerNone;
}

Fingering ParserFt3::GetLeftFingering(uint16_t extras)
{
    // left hand fingering
    if (static_cast<bool>(extras & Bit5)) // left finger1
    {
        return FingerFirst;
    }
    if (static_cast<bool>(extras & Bit6)) // left finger2
    {
        return FingerSecond;
    }
    if (static_cast<bool>(extras & Bit7)) // left finger3
    {
        return FingerThird;
    }
    if (static_cast<bool>(extras & Bit8)) // left finger 4
    {
        return FingerForth;
    }

    return FingerNone;
}

Ornament ParserFt3::GetLeftOrnament(uint16_t extras)
{
    // left ornaments
    const uint16_t ornament = extras & 0xfe00;
    if (ornament == Bit10) // # ornament
    {
        return OrnHash;;
    }
    if (ornament == Bit11) // + ornament
    {
        return OrnPlus;
    }
    if (ornament == (Bit14 | Bit11 | Bit9)) // single . on left
    {
        return OrnDot;
    }
    if (ornament == (Bit11 | Bit10)) // x ornament
    {
        return OrnCross;
    }
    if (extras == (Bit13 | Bit12 | Bit10)) // sq brackets on both sides e.g. [a]
    {
        return OrnBrackets;
    }

    return OrnNone;
}

Ornament ParserFt3::GetRightOrnament(uint16_t extras)
{
    // right ornaments
    const uint16_t ornament = extras & 0xfe00;
    if (ornament == (Bit10 | Bit9)) // # ornament
    {
        return OrnHash;
    }
    if (ornament == Bit12) // , ornament
    {
        return OrnComma;
    }

    // TODO other rhs ornaments when I know the values

    return OrnNone;
}

void ParserFt3::ParseTuning(const Options& options, const std::vector<uint8_t>::const_iterator pieceBegin,
        const std::vector<uint8_t>::const_iterator pieceEnd, Part& part)
{
    // tuning table is 58(ish!) bytes before the end of the piece
    const int tuningTableOffset{(options.m_srcFormat == FormatFt3 ? 58 : 35)};
    if (std::distance(pieceBegin, pieceEnd) < tuningTableOffset)
    {
        return;
    }

    auto ptr = pieceEnd - tuningTableOffset;
    // find start of table
    for (; *ptr != 0 && *ptr < 0xF0 && ptr != pieceBegin; --ptr)
    {

    }

    ++ptr;

    for (; (*ptr & ~Bit7) >= 12 && ptr != pieceEnd && part.m_tuning.size() < 20; ++ptr)
    {
        part.m_tuning.emplace_back(*ptr & ~Bit7);
    }

    // Have seen files where the tuning table is 78 before pieceEnd and files where the
    // CPiece tag occurs but without being a CPiece tag so we get the wrong pieceEnd.
    // Reject short tuning tables.
    if (part.m_tuning.size() <= 3)
    {
        part.m_tuning.clear();
        return;
    }

    LOGGER << "Source tuning " << Pitch::GetTuning(part.m_tuning);
}

} // namespace luteconv
