#ifndef _GENMUSICXML_H_
#define _GENMUSICXML_H_

#include <iostream>

#include <pugixml.hpp>

#include "generator.h"

namespace luteconv
{

/**
 * Generate .musicxml
 */
class GenMusicXml: public Generator
{
public:
    /**
     * Constructor
     */
    GenMusicXml() = default;

    /**
     * Destructor
     */
    ~GenMusicXml() override = default;
    
    /**
     * Generate .musicxml
     * 
     * @param[in] options
     * @param[in] piece
     * @param[out] dst destination
     */
    void Generate(const Options& options, const Piece& piece, std::ostream& dst) override;
    
private:
    static void Measure(pugi::xml_node xmlPart, const Part& part, TabType tabType, const Bar& bar, int n,
            bool& repForward, Key& curKey, TimeSig& curTimeSig, Clef& curClef, bool& tiedStopPending, const Options& options);
    static void FirstMeasureAttributes(pugi::xml_node measure, const Part& part, TabType tabType, const Bar& bar);
    static void AddKey(pugi::xml_node attributes, const Bar& bar);
    static void AddTimeSignature(pugi::xml_node attributes, const Bar& bar);
    static void AddClef(pugi::xml_node attributes, const Bar& bar);
    static void AddOrnaments(pugi::xml_node notations, const Note& luteNote);
    static void AddSlur(pugi::xml_node notations, const Note& note);
    static NoteType Adjust(const Chord& chord, const Options& options, const Part& part);
    static void Triplet(pugi::xml_node note, pugi::xml_node& notations, const Chord& chord, int& tripletCount, bool firstNote);
};

} // namespace luteconv

#endif // _GENMUSICXML_H_
