#include "parserltl.h"

#include <algorithm>
#include <cstring>
#include <regex>

#include "logger.h"

namespace luteconv
{

void ParserLtl::Parse(std::istream &src, const Options &options, Piece &piece)
{
    LOGGER << "Parse Lute Tablature with Ligatures font .ltl";
    
    Part& part{piece.m_parts.at(0)}; // only 1 part
    part.m_tabType = TabFrench;

    int lineNo{0};

    if (options.m_index != 0)
    {
        throw std::invalid_argument("Error: Can't find section, index=" + std::to_string(options.m_index));
    }

    part.m_bars.emplace_back(); // first bar
    
    for (;;)
    {
        std::string lineOwner;
        getline(src, lineOwner);
        
        if (src.eof() && lineOwner.empty())
        {
            break;
        }
        
        // remove any trailing \r
        if (!lineOwner.empty() && lineOwner.back() == '\r')
        {
            lineOwner.pop_back();
        }
        
        ++lineNo;
        const std::string_view line{lineOwner}; // string_view is much faster at substringing
        
        size_t idx{0};
        while (idx < line.size())
        {
            // empty tablature column, negative space for chord, or whitespace
            if (line.at(idx) == ' ' || line.at(idx) == '_' || line.at(idx) == '\\')
            {
                ++idx;
                continue;
            }
            
            // repeats and bar lines
            int barSize{0};
            if (line.substr(idx, 4) == ":||:")
            {
                part.m_bars.back().m_repeat = RepJanus;
                barSize = 4;
            }
            else if (line.substr(idx, 3) == "||:")
            {
                part.m_bars.back().m_repeat = RepForward;
                barSize = 3;
            }
            else if (line.substr(idx, 3) == ":||")
            {
                part.m_bars.back().m_repeat = RepBackward;
                barSize = 3;
            }
            else if (line.substr(idx, 2) == "||")
            {
                 part.m_bars.back().m_barStyle = BarStyleLightLight;
                 barSize = 2;
            }
            else if (line.at(idx) == '|'  || line.at(idx) == '(' || line.at(idx) == ')')
            {
                barSize = 1;
            }
            
            if (barSize > 0)
            {
                // If current bar is unused then keep it as the next bar, otherwise start a new bar
                if (!part.m_bars.back().m_chords.empty())
                {
                    part.m_bars.emplace_back(); // allocate a new bar
                }
                idx += barSize;
                continue;
            }
            
            //  time signatures [TC],  [TC-] and  [T3]
            if (line.substr(idx, 4) == "[TC]")
            {
                part.m_bars.back().m_timeSig.m_timeSymbol = TimeSyCommon;
                part.m_bars.back().m_timeSig.m_beats = 4;
                part.m_bars.back().m_timeSig.m_beatType = 4;
                idx += 4;
                continue;
            }

            if (line.substr(idx, 5) == "[TC-]")
            {
                part.m_bars.back().m_timeSig.m_timeSymbol = TimeSyCut;
                part.m_bars.back().m_timeSig.m_beats = 2;
                part.m_bars.back().m_timeSig.m_beatType = 2;
                idx += 5;
                continue;
            }

            if (line.substr(idx, 4) == "[T3]")
            {
                part.m_bars.back().m_timeSig.m_timeSymbol = TimeSySingleNumber;
                part.m_bars.back().m_timeSig.m_beats = 3;
                part.m_bars.back().m_timeSig.m_beatType = 4;     
                idx += 4;
                continue;
            }
            
            if (ChordFunc(line, idx))
            {
                // chord.  Rhythm, notes and fingering dots can occur in any order
                part.m_bars.back().m_chords.emplace_back();
                Chord& chord{part.m_bars.back().m_chords.back()};
                
                chord.m_notes.resize(10); // notes can arrive in any order
                chord.m_noteType = m_previousChord.m_noteType;
                chord.m_dotted = m_previousChord.m_dotted;
                chord.m_noFlag = true;
                
                while (ChordFunc(line, idx))
                {
                    int diapason{0};
                    int diapasonFret{0};
                    char param{'\0'};
                    char func{'\0'};
                    
                    // diapasons
                    // [C8], [C9], [C10]
                    // [CA8] ..[CE10] fretted
                    static const std::regex re{R"(^\[C([A-E])?([89]|10)\])"};
                    std::match_results<std::string_view::const_iterator> results;
                    if (std::regex_search(line.cbegin() + idx, line.cend(), results, re))
                    {
                        idx += results.length(0);
                        const std::string diaFret{results.str(1)};
                        const std::string diaCourse{results.str(2)};
                        
                        if (!diaFret.empty())
                        {
                            diapasonFret = diaFret.back() - 'A';
                        }
                        
                        diapason = std::stoi(diaCourse);
                    }
                    else
                    {
                        func = line.at(idx);
                        ++idx;
    
                        if (idx >= line.size())
                        {
                            LOGGER << lineNo << ":" << idx << ": Error unexpected eol";
                            break;
                        }
                    
                        param = line.at(idx);
                        ++idx;
                    }
                    
                    if (diapason != 0)
                    {
                        chord.m_notes.at(diapason - 1).m_course = diapason;
                        chord.m_notes.at(diapason - 1).m_fret = diapasonFret;
                    }
                    else if (func == '#')
                    {
                        if (param == '^')
                        {
                            chord.m_fermata = true;
                            chord.m_noteType = NoteTypeQuarter; // longest note we have
                            chord.m_dotted = false;
                            chord.m_noFlag = false;
                        }
                        else if (param >= '0' && param <= '5')
                        {
                            // Ltl has 0 = no flag, 1 = 1 flag, 2 = 2 flags, ...,  5 = 5 flags.
                            chord.m_noteType = static_cast<NoteType>(param - '0' + NoteTypeQuarter);
                            chord.m_noFlag = false;
                            
                            if (idx < line.size() && line.at(idx) == '.')
                            {
                                chord.m_dotted = true;
                                ++idx;
                            }
                            else
                            {
                                chord.m_dotted = false;
                            }
                        }
                        else
                        {
                            LOGGER << lineNo << ":" << idx << ": Error rhythm sign: " << func << param << " ignored";
                        }
                    }
                    else if (param >= '1' && param <= '7')
                    {
                        // note or fingering dot
                        const int course{param - '0'};
                        
                        if (func == '.')
                        {
                            // fingering dot
                            if (course > 1)
                            {
                                // dot is placed on the course below
                                chord.m_notes.at(course - 1 - 1).m_rightFingering = FingerFirst;
                            }
                            else
                            {
                                LOGGER << lineNo << ":" << idx << ": Error fingering dot out of range: " << func << param << " ignored";
                            }
                        }
                        else if (func == '+')
                        {
                            // ornament +
                            if (course > 1)
                            {
                                // + is placed on the course below
                                chord.m_notes.at(course - 1 - 1).m_leftOrnament = OrnPlus; // move ornament below the not to the left
                            }
                            else
                            {
                                LOGGER << lineNo << ":" << idx << ": Error ornament + out of range: " << func << param << " ignored";
                            }
                        }
                        else if (func == '=')
                        {
                            // ornament #
                            if (course > 1)
                            {
                                // = is placed on the course below
                                chord.m_notes.at(course - 1 - 1).m_leftOrnament = OrnHash; // move ornament below the not to the left
                            }
                            else
                            {
                                LOGGER << lineNo << ":" << idx << ": Error ornament # out of range: " << func << param << " ignored";
                            }
                        }
                        else
                        {
                            // note
                            chord.m_notes.at(course - 1).m_course = course;
                            chord.m_notes.at(course - 1).m_fret = func - 'a' - (func >= 'j' ? 1 : 0); // fret j == i
                        }
                    }
                    else
                    {
                        LOGGER << lineNo << ":" << idx << ": Error invalid course: " << func << param << " ignored";
                    }
                }
                
                // remove unused notes from the chord
                chord.m_notes.erase(std::remove_if(chord.m_notes.begin(), chord.m_notes.end(),
                        [](const Note& note)
                        {
                            return note.m_course == CourseNone;
                        }), chord.m_notes.end());

                m_previousChord = chord; // only need to save the rhythm, not the notes.
            }
            else
            {
                LOGGER << lineNo << ":" << idx << ": Error unexpected character: " << line.at(idx) << " ignored";
                ++idx;
            }
        }
    }
    
    // ignore the last bar if it is empty
    if (part.m_bars.back().m_chords.empty())
    {
        part.m_bars.pop_back();
    }
    
    part.SetTuning(options);
}

// Is this character used for constructing a chord?
bool ParserLtl::ChordFunc(std::string_view line, size_t idx)
{
    if (idx >= line.size())
    {
        return false;
    }
    
    const char c{line[idx]};
    return c == '#' || c == '.' || c == '+' || c == '=' || (c >= 'a' && c <= 'n') || line.substr(idx, 2) == "[C";
}

} // namespace luteconv
