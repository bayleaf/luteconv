#ifndef _GENMNX_H_
#define _GENMNX_H_

#include <iostream>
#include <map>
#include <string>
#include <utility>

#include <pugixml.hpp>

#include "generator.h"

namespace luteconv
{

/**
 * Generate .mnx
 */
class GenMnx: public Generator
{
public:
    /**
     * Constructor
     */
    GenMnx() = default;

    /**
     * Destructor
     */
    ~GenMnx() override = default;
    
    /**
     * Generate .mnx
     * 
     * @param[in] options
     * @param[in] piece
     * @param[out] dst destination
     */
    void Generate(const Options& options, const Piece& piece, std::ostream& dst) override;
    
private:
    static void MeasureGlobal(pugi::xml_node global, const Bar& bar, int measureNo, bool& repForward);
    void MeasurePart(pugi::xml_node xmlpart, const Part& part, const Bar& bar, int measureNo, const Options& options);
    std::string MakeId();
    static NoteType Adjust(const Chord& chord, const Options& options);
    
    int m_nextId{0};
    std::map<int, std::pair<std::string, std::string>> m_slurTarget;
};



} // namespace luteconv

#endif // _GENMNX_H_
