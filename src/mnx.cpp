#include "mnx.h"

namespace luteconv
{

const char* const Mnx::noteType[] = {"*4", "*2", "/1", "/2",
                "/4", "/8", "/16", "/32", "/64", "/128", "/256", nullptr};

const char* const Mnx::barStyle[] = {"regular", "dotted", "dashed", "heavy", "light-light", "light-heavy",
        "heavy-light", "heavy-heavy", "tick", "short", nullptr};
    
} // namespace luteconv