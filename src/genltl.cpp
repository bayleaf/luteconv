#include "genltl.h"

#include <algorithm>

#include "logger.h"

namespace luteconv
{

void GenLtl::Generate(const Options& options, const Piece& piece, std::ostream& dst)
{
    LOGGER << "Generate Lute Tablature with Ligatures font .ltl";
    
    const Part& part{piece.m_parts.at(0)};
    
    if (part.m_tabType != TabFrench)
    {
        LOGGER << "Ltl does not support " << Options::GetTabType(part.m_tabType) << " tablature type. Using french.";
    }
    
    int col{0};
    
    for (const auto & bar : part.m_bars)
    {
        // time signature
        switch (bar.m_timeSig.m_timeSymbol)
        {
            case TimeSyCommon:
                dst << " [TC]";
                ++col;
                break;
            case TimeSyCut:
                dst << " [TC-]";
                ++col;
                break;
            case TimeSySingleNumber:
                if (bar.m_timeSig.m_beats == 3)
                {
                    dst << " [T3]";
                    ++col;
                }
                break;
            default:
                break;
        }

        for (const auto & chord : bar.m_chords)
        {
            dst << ' ';
            ++col;

            // Ltl has 0 = no flag, 1 = 1 flag, 2 = 2 flags, ...,  5 = 5 flags. Clamp
            NoteType adjusted{static_cast<NoteType>(chord.m_noteType + options.m_flags)};
            adjusted = std::max(NoteTypeQuarter, adjusted);
            adjusted = std::min(NoteType128th, adjusted);

            if (chord.m_fermata)
            {
                dst << "#^";
            }
            else if (!chord.m_noFlag)
            {
                dst << '#' << adjusted - NoteTypeQuarter;
                if (chord.m_dotted)
                {
                    dst << '.';
                }
            }
            
            int numNotes{0};
            for (const auto & note : chord.m_notes)
            {
                // ltl only supports frets a .. n, courses 1 .. 10 and if course >= 8 frets a .. e
                if (note.m_course == CourseNone || note.m_course > Course10 || note.m_fret < 0
                        || note.m_fret > 12 || (note.m_course >= 8 && note.m_fret > 4))
                {
                    if (note.m_fret != Note::Anchor && note.m_fret != Note::Ensemble)
                    {
                        LOGGER << "Bar " << std::distance(&part.m_bars.front(), &bar) + 1
                                << " note out of range: fret " << note.m_fret << " course " << note.m_course << " ignored";
                    }
                    continue;
                }
                
                if (note.m_course >= 8)
                {
                    dst << "[C";
                    if (note.m_fret > 0)
                    {
                        dst << static_cast<char>(note.m_fret + 'A');
                    }
                    dst << note.m_course << ']';
                }
                else
                {
                    dst << static_cast<char>('a' + note.m_fret + (note.m_fret > 8 ? 1 : 0)) << note.m_course; // a..i, k..n
                }
                
                ++numNotes;
                
                // Ltl only support fingering dots or ornaments on courses 1 .. 6.
                // Ltl only supports ornaments below the note, move a left or right ornament below.
                // Ensure that there is room, i.e. no note on line below.
                const size_t noteIdx{static_cast<size_t>(std::distance(&chord.m_notes.front(), &note))}; 
                if (note.m_course < 7 && (noteIdx + 1 == chord.m_notes.size() || chord.m_notes.at(noteIdx + 1).m_course != note.m_course + 1))
                {
                    if (note.m_rightFingering == FingerFirst)
                    {
                        dst << '.' << note.m_course + 1;
                    }
                    else if (note.m_leftOrnament == OrnPlus || note.m_rightOrnament == OrnPlus)
                    {
                        dst << '+' << note.m_course + 1;
                    }
                    else if (note.m_leftOrnament == OrnHash || note.m_rightOrnament == OrnHash)
                    {
                        dst << '=' << note.m_course + 1;
                    }
                }
            }
            
            // if no notes were output then at least ensure that the rhythm sign is shown
            // can happen if when only courses 11... exist, unsupported by ltl
            if (numNotes == 0 && chord.m_noFlag)
            {
                dst << '#' << adjusted - NoteTypeQuarter;
                if (chord.m_dotted)
                {
                    dst << '.';
                }
            }
            
            // newline if we reach the threshold
            if (col >= options.m_wrapThreshold)
            {
                dst << '\n';
                col = 0;
            }
        }
        
        if (bar.m_repeat == RepForward)
        {
            dst << " ||:";
        }
        else if (bar.m_repeat == RepBackward)
        {
            dst << " :||";
        }
        else if (bar.m_repeat == RepJanus)
        {
            dst << " :||:";
        }
        else if (&bar == &part.m_bars.back() && bar.m_barStyle == BarStyleRegular) // last bar?
        {
            dst << ")";
        }
        else
        {
            switch (bar.m_barStyle)
            {
                case BarStyleLightLight:
                    [[fallthrough]];
                case BarStyleLightHeavy:
                    [[fallthrough]];
                case BarStyleHeavyLight:
                    [[fallthrough]];
                case BarStyleHeavyHeavy:
                    dst << " ||";
                    break;
                default:
                    dst << " |";
                    break;
            }
        }
        ++col;
        
        // newline if we reach the threshold
        if (col >= options.m_wrapThreshold)
        {
            dst << '\n';
            col = 0;
        }
    }
    
    if (col != 0)
    {
        dst << '\n';
    }
}

} // namespace luteconv
