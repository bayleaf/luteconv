#include "genabc.h"

#include <algorithm>
#include <fstream>
#include <chrono>
#include <iomanip>
#include <set>

#include "charset.h"
#include "logger.h"
#include "platform.h"
#include "rational.h"

namespace luteconv
{

// all flags are relative to this
const NoteType baseNoteType{NoteTypeWhole};

void GenAbc::Generate(const Options& options, const Piece& piece, std::ostream& dst)
{
    LOGGER << "Generate abc";
    
    // find the first tablature part
    const Part& part{piece.m_parts.at(FindTabPart(piece))};
    
    TabType tabType{part.m_tabType};
    const std::set<TabType> tabTypes{ TabFrench, TabGerman, TabItalian, TabNeapolitan, TabSpanish };

    if (tabTypes.find(part.m_tabType) == tabTypes.end())
    {
        LOGGER << "abc does not support " << Options::GetTabType(part.m_tabType) << " tablature type. Using french.";
        tabType = TabFrench;
    }
    
    // Grid iron style?
    bool gridiron{false};
    for (const auto & bar : part.m_bars)
    {
        for (const auto & chord : bar.m_chords)
        {
            if (chord.m_grid != GridNone)
            {
                gridiron = true;
                break;
            }
        }
        if (gridiron)
        {
            break;
        }
    }
     
    const std::time_t tt = std::chrono::system_clock::to_time_t(std::chrono::system_clock::now());
    struct std::tm * ptm = std::gmtime(&tt);
    
    dst << "X:1" << '\n';

    // pseudo comments
    if (gridiron)
    {
        dst << "%%tabrhstyle grid" << '\n';
    }
    
    dst << "%%tabaddflags 0" << '\n';   // don't mess with the flags!
    
    if (tabType == TabGerman)
    {
        std::string subtype;
        switch (part.m_tabGermanSubtype)
        {
            case GstABC:
                subtype = "ABC";
                break;
            case Gst1AB:
                subtype = "1AB";
                break;
            case Gst123:
                subtype = "123";
                break;
            case GstUnknown:
                [[fallthrough]];
            default:
                break;
        }
      
        if (!subtype.empty())
        {
            dst << "%%tabbrummer " << subtype << '\n';
        }
        
        dst << "%%tabfontgerman deFraktur" << '\n';
    }

    if (!piece.m_title.empty())
    {
        dst << "T:" << CharSet::Utf8ToIso8859_1(piece.m_title) << '\n';
    }

    if (!piece.m_composer.empty())
    {
        dst << "C:" << CharSet::Utf8ToIso8859_1(piece.m_composer) << '\n';
    }

    if (!piece.m_copyright.empty())
    {
        dst << "N:" << CharSet::Utf8ToIso8859_1(piece.m_copyright) << '\n';
    }
    
    for (const auto & credit : piece.m_credits)
    {
        dst << "N:" << CharSet::Utf8ToIso8859_1(credit) << '\n';
    }
    
    const size_t slash = options.m_dstFilename.find_last_of(pathSeparator);
    if (slash == std::string::npos)
    {
        dst << "F:" << options.m_dstFilename << '\n';
    }
    else
    {
        dst << "F:" << options.m_dstFilename.substr(slash + 1) << '\n';
    }

    static_assert(baseNoteType == NoteTypeWhole);
    dst << "L:1" << '\n';
    
    dst << "Z:Converted to abc .abc by luteconv " << options.m_version << " encoding-date " << std::put_time(ptm,"%F") << '\n';

    switch (tabType)
    {
    case TabNeapolitan:
        dst << "K:neapoltab" << '\n';
        break;
    case TabCmn:
        // TODO support CMN
        [[fallthrough]];
    case TabUnknown:
        [[fallthrough]];
    case TabFrench:
        switch (part.m_staffLines)
        {
            case 4:
                dst << "K:french4tab" << '\n';
                break;
            case 5:
                dst << "K:french5tab" << '\n';
                break;
            default:
                dst << "K:frenchtab" << '\n';
                break;
        }
        break;
    case TabGerman:
        dst << "K:germantab" << '\n';
        break;
    case TabItalian:
        if (part.m_tuning.size() <= Course6)
        {
            switch (part.m_staffLines)
            {
                case 4:
                    dst << "K:italian4tab" << '\n';
                    break;
                case 5:
                    dst << "K:italian5tab" << '\n';
                    break;
                default:
                    dst << "K:italiantab" << '\n';
                    break;
            }
        }
        else if (part.m_tuning.size() == Course7)
        {
            dst << "K:italian7tab" << '\n';
        }
        else
        {
            dst << "K:italian8tab" << '\n';
        }
        break;
    case TabSpanish:
        switch (part.m_staffLines)
        {
            case 4:
                dst << "K:spanish4tab" << '\n';
                break;
            case 5:
                dst << "K:spanish5tab" << '\n';
                break;
            default:
                dst << "K:spanishtab" << '\n';
                break;
        }
        break;
    }
    
    // body
    int staveNum{1};
    int barNum{1};
    int chordCount{0};
    std::string repForward;
    
    dst << "% Stave " << staveNum <<  '\n';
    
    for (const auto & bar : part.m_bars)
    {
        if (chordCount == 0)
        {
            // first bar on line
            dst <<  "% Bar " << barNum << '\n';
            if (!repForward.empty())
            {
                dst << repForward << "\\" << '\n';
                repForward.clear();
            }
            else if (!options.m_barSupp)
            {
                dst << "|" << "\\" << '\n';
            }
        }
        
        // prima, seconda ... volta
        if (bar.m_volta > 0)
        {
            dst << "[" << bar.m_volta << "\\" << '\n';
        }
        
        // time signature
        const std::string timeSignature = GetTimeSignature(bar);
        if (!timeSignature.empty())
        {
            dst << timeSignature << "\\" << '\n';
        }
        
        // chords
        for (const auto & chord : bar.m_chords)
        {
            ++chordCount;
            
            std::vector<std::string> vert;
            vert.reserve(Course6);
            std::string diapason;
            
            vert.emplace_back(","); // in case no notes 
            
            for (const auto & note : chord.m_notes)
            {
                // Ignore ensemble lines
                if (note.m_fret == Note::Ensemble)
                {
                    continue;
                }

                if (note.m_course <= Course6)
                {
                    while (static_cast<int>(vert.size()) < note.m_course)
                    {
                        vert.emplace_back(",");
                    }

                    // abc only supports right ornaments, if none use left ornament instead
                    const std::string ornament = (note.m_rightOrnament != OrnNone)
                                                    ? GetOrnament(note.m_rightOrnament, true)
                                                    : GetOrnament(note.m_leftOrnament, false);

                    vert[note.m_course - 1] = ornament +
                            GetLeftConnector(note) +
                            GetFret(note) +
                            GetRightConnector(note)
                            ;
                }
                else
                {
                    diapason += GetFret(note);
                }
            }
            
            std::string tabWord;
            
            if (chord.m_fermata)
            {
                tabWord += "H";
            }
            
            // abc only supports right hand fingering on lowest plucked course
            if (!chord.m_notes.empty())
            {
                tabWord += GetRightFingering(chord.m_notes.back());
            }
            
            if (chord.m_triplet)
            {
                tabWord += "(3";
            }

            tabWord += "[";  // always use chord notation
            
            for (const auto & s : vert)
            {
                tabWord += s;
            }
            
            tabWord += diapason
                    + GetFlagInfo(options, chord) 
                    + "]";
            
            // end of grid marked by a space
            if (gridiron && chord.m_grid != GridStart && chord.m_grid != GridMid)
            {
                tabWord += " ";
            }
            
            dst << tabWord << "\\" << '\n';
        }
        
        // end of current bar
        ++barNum;
        dst <<  "% Bar " << barNum << '\n';
        
        // Stave ending Use herustic:
        // count chords, when the threshold is reached end the stave at the end of
        // the current bar.  Except for last bar.
        const bool lineBreak = barNum < static_cast<int>(part.m_bars.size()) && chordCount > options.m_wrapThreshold;
        
        std::string barStyle;
        switch (bar.m_barStyle)
        {
        case BarStyleLightLight:
            barStyle = "||";
            break;
        case BarStyleLightHeavy:
            barStyle = "|]";
            break;
        case BarStyleHeavyLight:
            barStyle = "[|";
            break;
        default:
            barStyle = "|";
        }
        
        switch (bar.m_repeat)
        {
        case RepNone:
            break;
        case RepForward:
            barStyle = "|:";
            break;
        case RepBackward:
            barStyle = ":|";
            break;
        case RepJanus:
            if (lineBreak)
            {
                // backward repeat here, forward repeat in next bar, next line
                repForward = "|:";
                barStyle = ":|";
            }
            else
            {
                barStyle = "::";
            }
        }
        
        if (lineBreak)
        {
            dst << barStyle << '\n';
            ++staveNum;
            chordCount = 0;
            dst << "% Stave " << staveNum << '\n';
        }
        else
        {
            dst << barStyle << "\\" << '\n';
        }
    }
}
    
std::string GenAbc::GetTimeSignature(const Bar & bar)
{
    std::string timeSig;
    
    switch (bar.m_timeSig.m_timeSymbol)
    {
    case TimeSyNone:
        return "";
    case TimeSyCommon:
        timeSig = "M:C";
        break;
    case TimeSyCut:
        timeSig = "M:C|";
        break;
    case TimeSySingleNumber:
        timeSig = "M:" + std::to_string(bar.m_timeSig.m_beats);
        break;
    case TimeSyNote:
        return "";
    case TimeSyDottedNote:
        return "";
    case TimeSyNormal:
        timeSig = "M:" + std::to_string(bar.m_timeSig.m_beats) + "/" + std::to_string(bar.m_timeSig.m_beatType);
        break;
    }
    return "[" + timeSig + "]";
}

std::string GenAbc::GetFlagInfo(const Options& options, const Chord & our)
{
    if (our.m_noFlag)
    {
        return "";
    }
    
    // abc thinks 0 flags = crotchet
    NoteType adjusted{static_cast<NoteType>(our.m_noteType + options.m_flags)};
    adjusted = std::max(NoteTypeLong, adjusted);
    adjusted = std::min(NoteType128th, adjusted);

    Rational noteFrac{Bar::NoteFrac(adjusted)};

    if (our.m_dotted)
    {
        noteFrac *= Rational(3, 2);
    }

    std::string ourFlags;
    if (noteFrac.Den() == 1)
    {
        // multiplier, no divisor
        ourFlags += std::to_string(noteFrac.Num());
    }
    else if (noteFrac == Rational(1, 2))
    {
        // 1/2 => /
        ourFlags += "/";
    }
    else if (noteFrac.Num() == 1)
    {
        // 1/den => /den
        ourFlags += "/" + std::to_string(noteFrac.Den());
    }
    else
    {
        // num/den
        ourFlags += noteFrac.ToString();
    }
    
    return ourFlags;
}

std::string GenAbc::GetRightFingering(const Note & note)
{
    switch (note.m_rightFingering)
    {
    case FingerNone:
        return "";
    case FingerFirst:
        return ".";
    case FingerSecond:
        return ":";
    case FingerThird:
        return ";";
    case FingerForth:
        return "";
    case FingerThumb:
        return "+";
    }
    return "";
}
    
std::string GenAbc::GetOrnament(Ornament ornament, bool right)
{
    switch (ornament)
    {
    case OrnNone:
        return "";
    case OrnHash:
        return "#";
    case OrnPlus:
        return "";
    case OrnCross:
        return "X";
    case OrnDot:
        return "";
    case OrnDotDot:
        return "";
    case OrnBrackets:
        return "";
    case OrnComma:
        return "";
    case OrnCommaRaised:
        return "'";
    case OrnColon:
        return "";
    case OrnColonColon:
        return "";
    case OrnAsterisk:
        return "*";
    case OrnSmiley:
        return right ? "U" : "V";
    case OrnHat:
        return "";
    case OrnDash:
        return "";
        
    case OrnSize:
        return "";
    }
    return "";
}

std::string GenAbc::GetFret(const Note & note)
{
    // a..p, excluding j
    std::string letter{static_cast<char>(
                    (note.m_fret == Note::Anchor)
                    ? 'y' // dummy anchor for connectors
                    : 'a' + note.m_fret + (note.m_fret > 8 ? 1 : 0)
              )};
    
    if (note.m_course >= Course11)
    {
        return "{" + std::to_string(note.m_course - Course7) + "}";
    }
    if (note.m_course >= Course7)
    {
        return "{" + std::string(note.m_course - Course7, ',') + letter + "}";
    }
    return letter;
}

std::string GenAbc::GetLeftConnector(const Note & note)
{
    if (note.m_connector.m_endPoint == EndPointStart)
    {
        if (note.m_connector.m_function == FunSlur)
        {
            return "(";
        }
    }
    
    // TODO hold lines as "Tenuto strokes" !ten(! don't work satifactorily in abctab.
    // The start of the line is to the right of the opening note, that's ok, but
    // the end of the line is to the north west of the closing note, not ok.
    return "";
}

std::string GenAbc::GetRightConnector(const Note & note)
{
    if (note.m_connector.m_endPoint == EndPointStop)
    {
        if (note.m_connector.m_function == FunSlur)
        {
            return ")";
        }
    }
    return "";
}

} // namespace luteconv
