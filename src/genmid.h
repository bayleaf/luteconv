#ifndef _GENMID_H_
#define _GENMID_H_

#include <iostream>

#include "generator.h"

namespace luteconv
{

/**
 * Generate .mid MIDI
 */
class GenMid: public Generator
{
public:
    /**
     * Constructor
     */
    GenMid() = default;

    /**
     * Destructor
     */
    ~GenMid() override = default;
    
    /**
     * Generate .mid
     * 
     * @param[in] options
     * @param[in] piece
     * @param[out] dst destination
     */
    void Generate(const Options& options, const Piece& piece, std::ostream& dst) override;
    
};

} // namespace luteconv

#endif // _GENMID_H_
