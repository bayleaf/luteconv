#include "unzipper.h"

#include <iostream>
#include <stdexcept>

#include <miniz.h>

#include "logger.h"

namespace luteconv
{

void Unzipper::Unzip(std::istream& srcFile, std::vector<char>& image, std::string& zipFilename)
{
    // read the source file into memory
    
    const size_t srcImageInc{10000};
    std::vector<uint8_t> srcImage(srcImageInc);
    size_t srcSize{0};
    
    for (;;)
    {
        const bool result{srcFile.read(reinterpret_cast<char*>(srcImage.data() + srcSize),
                static_cast<std::streamsize>(srcImage.size() - srcSize))};
        srcSize += static_cast<size_t>(srcFile.gcount());
        if (!result)
        {
            break;
        }
        srcImage.resize(srcImage.size() + srcImageInc);
    }
    srcImage.resize(srcSize);
    
    mz_zip_archive zipArchive{};
    
    if (mz_zip_reader_init_mem(&zipArchive, srcImage.data(), srcSize, 0) == 0)
    {
        throw std::runtime_error("Error: Can't open zip archive");
    }
    
    std::vector<std::pair<std::string, mz_uint64>> index;
    for (mz_uint i = 0; i < mz_zip_reader_get_num_files(&zipArchive); i++)
    {
        mz_zip_archive_file_stat fileStat{};

        if (mz_zip_reader_file_stat(&zipArchive, i, &fileStat) != 0)
        {
            LOGGER << "name=" << fileStat.m_filename << " size=" << fileStat.m_uncomp_size;
            index.emplace_back(fileStat.m_filename, fileStat.m_uncomp_size);
        }
    }

    if (index.empty())
    {
        throw std::runtime_error("Error: zip index is empty " + zipFilename);
    }

    // zip files are archives, find our particular file
    mz_uint entry{0};
    for (mz_uint i = 0; i < static_cast<mz_uint>(index.size()); ++i)
    {
        if (index[i].first != "mimetype" && index[i].first != "META-INF/container.xml")
        {
            entry = i;
            break;
        }
    }

    zipFilename = index[entry].first;
    image.resize(static_cast<size_t>(index[entry].second));
    const bool status{mz_zip_reader_extract_to_mem(&zipArchive, entry, image.data(), image.size(), 0) != 0};
    mz_zip_reader_end(&zipArchive);
    
    if (!status)
    {
        throw std::runtime_error("Error: Can't extract " + zipFilename);
    }
}

} // namespace luteconv
