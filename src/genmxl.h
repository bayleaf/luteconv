#ifndef _GENMXL_H_
#define _GENMXL_H_

#include <iostream>

#include "generator.h"

namespace luteconv
{

/**
 * Generate .mxl (compressed MusicXML)
 */
class GenMxl: public Generator
{
public:
    /**
     * Constructor
     */
    GenMxl() = default;

    /**
     * Destructor
     */
    ~GenMxl() override = default;
    
    /**
     * Generate .mxl
     * 
     * @param[in] options
     * @param[in] piece
     * @param[out] dst destination
     */
    virtual void Generate(const Options& options, const Piece& piece, std::ostream& dst) override;

};

} // namespace luteconv

#endif // _GENMXL_H_
