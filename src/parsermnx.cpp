#include "parsermnx.h"

#include <iostream>
#include <sstream>
#include <string>
#include <algorithm>
#include <stdexcept>

#include "logger.h"
#include "mnx.h"
#include "pitch.h"


namespace luteconv
{

using namespace pugi;

void ParserMnx::Parse(std::istream& srcFile, const Options& options, Piece& piece)
{
    LOGGER << "Parse mnx";
    
    xml_document doc;
    xml_parse_result result{doc.load(srcFile)};
    
    Part& part{piece.m_parts.at(0)}; // only 1 part
    
    if (!result)
    {
        std::ostringstream ss;
        ss << "Error: XML parsed with errors."
                    << " Description: " << result.description() 
                    << " Offset: " << result.offset;
        throw std::runtime_error(ss.str());
    }
    
    if (options.m_index != 0)
    {
        throw std::invalid_argument("Error: Can't find section, index=" + std::to_string(options.m_index));
    }
    
    xml_node xmlmnx{doc.child("mnx")};
    if (!xmlmnx)
    {
        throw std::runtime_error("Error: Can't find <mnx>");
    }

    piece.m_title = xmlmnx.child("title").child_value();
    
    xml_node xmlglobal{xmlmnx.child("global")};
    if (!xmlglobal)
    {
        throw std::runtime_error("Error: Can't find <mnx><global>");
    }

    xml_node xmlpart{xmlmnx.child("part")};
    if (!xmlpart)
    {
        throw std::runtime_error("Error: Can't find <mnx><part>");
    }
    
    ParseGlobal(xmlglobal, part);
    ParsePart(xmlpart, part);
    part.SetTuning(options);
}

void ParserMnx::ParseGlobal(xml_node xmlglobal, Part& part)
{
    int measureNo{1};
    for (xml_node xmlmeasureglobal : xmlglobal.children("measure-global"))
    {
        part.m_bars.emplace_back();
        Bar& bar{part.m_bars.back()};
        
        xml_node xmldirectionsglobal{xmlmeasureglobal.child("directions-global")};
        if (xmldirectionsglobal != nullptr)
        {
            // time signature
            xml_node xmltime{xmldirectionsglobal.child("time")};
            if (xmltime != nullptr)
            {
                const std::string signature{xmltime.attribute("signature").value()};
                if (!signature.empty())
                {
                    const size_t slash{signature.find('/')};
                    if (slash != std::string::npos)
                    {
                        try
                        {
                            const int beats{std::stoi(signature.substr(0, slash))};
                            const int beatType{std::stoi(signature.substr(slash + 1))};
                            bar.m_timeSig.m_beats = beats;
                            bar.m_timeSig.m_beatType = beatType;
                            bar.m_timeSig.m_timeSymbol = TimeSyNormal;
                        }
                        catch (...)
                        {
                            LOGGER << "measure-global " << measureNo << ": time signature error, ignored";
                        }
                    }
                    else
                    {
                        LOGGER << "measure-global " << measureNo << ": time signature missing /";
                    }
                }
                else
                {
                    LOGGER << "measure-global " << measureNo << ": missing <time> @signature";
                }
            }
            
            // volta brackets
            bar.m_volta = xmldirectionsglobal.child("ending").attribute("number").as_int();
            
            // repeats
            for (xml_node xmlrepeat : xmldirectionsglobal.children("repeat"))
            {
                const std::string type{xmlrepeat.attribute("type").value()};
                if (type == "start")
                {
                    // MNX "start" refers to the measure's left barline, but our bars only own their right barline
                    // therefore we need to adjust the right barline of the previous bar
                    if (measureNo > 1)
                    {
                        Bar& prevBar{part.m_bars[measureNo - 2]};
                        prevBar.m_repeat = (prevBar.m_repeat == RepBackward)
                                        ? RepJanus
                                        : RepForward;
                            
                    }
                }
                else if (type == "end")
                {
                    bar.m_repeat = RepBackward;
                }
                else
                {
                    LOGGER << "measure-global " << measureNo << ": unrecognized <repeat> @type " << type;
                }
            }
        }
        ++measureNo;
    }
}

void ParserMnx::ParsePart(xml_node xmlpart, Part& part)
{
    int measureNo{1};
    for (xml_node xmlmeasurepart : xmlpart.children("measure"))
    {
        if (part.m_bars.size() < static_cast<size_t>(measureNo))
        {
            part.m_bars.resize(measureNo);
            LOGGER << "measure " << measureNo << ": more measures in <part> than in <global>";
        }
        
        Bar& bar{part.m_bars[measureNo - 1]};
        
        // barline
        const std::string barStyle{xmlmeasurepart.attribute("barline").value()};
        if (!barStyle.empty())
        {
            int i{0};
            for (; Mnx::barStyle[i] != nullptr; ++i)
            {
                if (barStyle == Mnx::barStyle[i])
                {
                    bar.m_barStyle = static_cast<BarStyle>(i);
                    break;
                }
            }
            if (Mnx::barStyle[i] == nullptr)
            {
                LOGGER << "measure " << measureNo << ": unrecognized <measure> @barline " << barStyle;
            }
        }
        else if (static_cast<size_t>(measureNo) == part.m_bars.size())
        {
            // If barline is not given, then if the measure is the last in the document, use light-heavy.
            bar.m_barStyle = BarStyleLightHeavy;
        }
    
        int numVoices{0};
        bool heardVoices{false};
        for (xml_node xmlsequence : xmlmeasurepart.children("sequence"))
        {
            ++numVoices;
            int stamp{0}; // chord timestamp from start of the bar in divs
            
            // beams
            std::map<std::string, Grid> eventIdToGrid;
            for (xml_node xmlbeam : xmlsequence.child("beams").children("beam"))
            {
                const std::string xmlevents{xmlbeam.attribute("events").value()};
                
                const size_t first{xmlevents.find_first_not_of(' ')};
                if (first == std::string::npos)
                {
                    continue;
                }
    
                size_t last{xmlevents.find_last_not_of(' ')};
                if (last == std::string::npos)
                {
                    continue;
                }
                
                ++last; // after last non-space character
                
                size_t posEnd{first};
                
                while (posEnd < last)
                {
                    // start of id
                    const size_t posStart{xmlevents.find_first_not_of(' ', posEnd)};
                    if (posStart == std::string::npos)
                    {
                        break;
                    }
                    
                    posEnd = xmlevents.find_first_of(' ', posStart);
                    if (posEnd == std::string::npos)
                    {
                        posEnd = last;
                    }
                    
                    if (posEnd > posStart)
                    {
                        eventIdToGrid[xmlevents.substr(posStart, posEnd - posStart)] =
                                (posStart == first)
                                ? GridStart
                                : (posEnd == last)
                                ? GridEnd
                                : GridMid;
                    }
                }
            }
            
            // <event>, <forward> and <tuple>
            for (xml_node xmlchild : xmlsequence.children())
            {
                const std::string childName{xmlchild.name()};

                if (childName == "event")
                {
                     ParseEvent(xmlchild, part, bar, measureNo, stamp, eventIdToGrid, 0);
                }
                else if (childName == "forward")
                {
                    ParseForward(xmlchild, measureNo, stamp, heardVoices);
                }
                else if (childName == "tuplet")
                {
                    // is this tuplet a triplet?
                    const std::string inner{xmlchild.attribute("inner").value()};
                    const std::string outer{xmlchild.attribute("outer").value()};
                    
                    int multiplier{};
                    NoteType noteType{};
                    bool dotted{};
                    bool isTriplet{false};
                    
                    if (ParseNoteValueQuantity(inner, multiplier, noteType, dotted))
                    {
                        const Rational innerNoteFrac{Bar::NoteFrac(noteType) * Rational(multiplier)};
                        if (ParseNoteValueQuantity(outer, multiplier, noteType, dotted))
                        {
                            const Rational outerNoteFrac{Bar::NoteFrac(noteType) * Rational(multiplier)};
                            isTriplet = (innerNoteFrac / outerNoteFrac == Rational(3, 2));
                        }
                    }
                    
                    if (!isTriplet)
                    {
                        LOGGER << "measure " << measureNo << ": non-triplet tuplet, inner=" << inner << " outer=" << outer;
                    }

                    ParseTuplet(xmlchild, part, bar, measureNo, stamp, eventIdToGrid, heardVoices, isTriplet ? 3 : 0);
                }
            }
        }
        
        if (numVoices > 1 || heardVoices)
        {
            bar.MergeVoices();
        }

        ++measureNo;
    }
}

void ParserMnx::ParseEvent(xml_node xmlevent, Part& part, Bar& bar, int measureNo, int& stamp,
        const std::map<std::string, Grid>& eventIdToGrid, int tripletCount)
{
    bar.m_chords.emplace_back();
    Chord& chord{bar.m_chords.back()};
    chord.m_triplet = (tripletCount == 3);
    
    // note type
    const std::string xmlvalue{xmlevent.attribute("value").value()};
    
    if (!xmlvalue.empty())
    {
        if (!ParseNoteValue(xmlvalue, chord.m_noteType, chord.m_dotted))
        {
            LOGGER << "measure " << measureNo << ": unrecognized <event> @value " << xmlvalue;
        }
    }
    else
    {
        LOGGER << "measure " << measureNo << ": no <event> @value";
    }
    
    chord.m_stamp = stamp;
    stamp += Bar::Duration(chord.m_noteType, chord.m_dotted, tripletCount > 0);
    
    for (xml_node xmlnote : xmlevent.children("note"))
    {
        // string & fret
        chord.m_notes.emplace_back();
        Note& note{chord.m_notes.back()};
        note.m_course = xmlnote.child("string").text().as_int();
        note.m_fret = xmlnote.child("fret").text().as_int();
        
        // Pitch to deduce tuning
        // TODO this won't we needed when MNX spcifies the tuning.
        const std::string spn{xmlnote.attribute("pitch").value()};
        
        try
        {
            size_t pos{0};
            const Pitch p{Pitch::Parse(spn, pos) - note.m_fret}; // pitch of open string
            
            if (part.m_tuning.size() < static_cast<size_t>(note.m_course))
            {
                part.m_tuning.resize(note.m_course);
            }
            
            if (part.m_tuning[note.m_course - 1].Step() != 0)
            {
                if (part.m_tuning[note.m_course - 1] != p)
                {
                    LOGGER << "measure " << measureNo << ": inconsistent string tuning";
                }
            }
            else
            {
                part.m_tuning[note.m_course - 1] = p;
            }

        }
        catch (...)
        {
            LOGGER << "measure " << measureNo << ": can't parse pitch " << spn;
        }
    }
    
    // slur start
    xml_node xmlslur{xmlevent.child("slur")};
    if (xmlslur != nullptr)
    {
        const std::string target{xmlslur.attribute("target").value()};
        const std::string start_note{xmlslur.attribute("start-note").value()};
        const std::string end_note{xmlslur.attribute("end-note").value()};
        const std::string side{xmlslur.attribute("side").value()};
        
        if (target.empty())
        {
            LOGGER << "measure " << measureNo << ": missing <slur> @target";
        }
        else
        {
            size_t index{0};
            
            // need a note to carry the slur's start
            if (chord.m_notes.empty())
            {
                // use a dummy note
                chord.m_notes.emplace_back();
                chord.m_notes.back().m_fret = Note::Anchor; // dummy
            }
            else
            {
                // find note with id = start-note
                for (xml_node xmlnote : xmlevent.children("note"))
                {
                    if (xmlnote.attribute("id").value() == start_note)
                    {
                        break;
                    }
                    index++;
                }
                if (index >= chord.m_notes.size())
                {
                    LOGGER << "measure " << measureNo << ": can't find note with slur's @start-note " << start_note;
                    index = 0;
                }
            }
            
            chord.m_notes[index].m_connector.m_curve = (side == "up") ? CurveOver : CurveUnder;
            chord.m_notes[index].m_connector.m_compass = (side == "up") ? North : South;
            chord.m_notes[index].m_connector.m_endPoint = EndPointStart;
            chord.m_notes[index].m_connector.m_function = FunSlur;
            
            // allocate lowest available slur number
            int slurNumber{1};
            while (m_slurNumber.find(slurNumber) != m_slurNumber.end())
            {
                ++slurNumber;
            }
            m_slurNumber.insert(slurNumber);
            chord.m_notes[index].m_connector.m_number = slurNumber;
            
            m_startConnector[target] = std::make_pair(end_note, chord.m_notes[index].m_connector);
        }
    }
    
    // slur stop
    const std::string eventId{xmlevent.attribute("id").value()};
    if (!eventId.empty())
    {
        auto connIt{m_startConnector.find(eventId)};
        if (connIt != m_startConnector.end())
        {
            size_t index{0};
            
            // find end-note
            const std::string end_note{(connIt->second).first};
            // need a note to carry the slur's stop
            if (chord.m_notes.empty())
            {
                // use a dummy note
                chord.m_notes.emplace_back();
                chord.m_notes.back().m_fret = Note::Anchor; // dummy
            }
            else
            {
                // find note with id = end-note
                for (xml_node xmlnote : xmlevent.children("note"))
                {
                    if (xmlnote.attribute("id").value() == end_note)
                    {
                        break;
                    }
                    index++;
                }
                if (index >= chord.m_notes.size())
                {
                    LOGGER << "measure " << measureNo << ": can't find note with slur's @end-note " << end_note;
                    index = 0;
                }
            }

            chord.m_notes[index].m_connector = (connIt->second).second;
            chord.m_notes[index].m_connector.m_endPoint = EndPointStop;
            m_slurNumber.erase(chord.m_notes[index].m_connector.m_number); // finished with slur number
        }
        
        // grid
        auto gridIt{eventIdToGrid.find(eventId)};
        if (gridIt != eventIdToGrid.end())
        {
            chord.m_grid = gridIt->second;
        }
    }
}

void ParserMnx::ParseForward(xml_node xmlforward, int measureNo, int& stamp, bool& heardVoices)
{
    const std::string xmlduration{xmlforward.attribute("duration").value()};
    int multiplier{1};
    NoteType noteType{NoteTypeQuarter};
    bool dotted{false};
    if (ParseNoteValueQuantity(xmlduration, multiplier, noteType, dotted))
    {
        stamp += multiplier * Bar::Duration(noteType, dotted, false);
        heardVoices = true;
    }
    else
    {
        LOGGER << "measure " << measureNo << ": unrecognized <forward> @duration " << xmlduration;
    }
}

void ParserMnx::ParseTuplet(xml_node xmltuplet, Part& part, Bar& bar, int measureNo, int& stamp,
        const std::map<std::string, Grid>& eventIdToGrid, bool& heardVoices, int tripletCount)
{
    for (xml_node xmlchild : xmltuplet.children())
    {
        const std::string childName{xmlchild.name()};

        if (childName == "event")
        {
            // mark the first event in a triplet
            ParseEvent(xmlchild, part, bar, measureNo, stamp, eventIdToGrid, tripletCount);
            if (tripletCount > 0)
            {
                --tripletCount;
            }
        }
        else if (childName == "forward")
        {
            ParseForward(xmlchild, measureNo, stamp, heardVoices);
        }
        else if (childName == "tuplet")
        {
            // FIXME not supporting nested triplets
            ParseTuplet(xmlchild, part, bar, measureNo, stamp, eventIdToGrid, heardVoices, 0);
        }
    }
}

bool ParserMnx::ParseNoteValueQuantity(const std::string& noteValueQuantity, int& multiplier, NoteType& noteType, bool& dotted)
{
    size_t i{0};
    try
    {
        multiplier = std::stoi(noteValueQuantity, &i);
    }
    catch (...)
    {
        multiplier = 1;
    }
    
    return ParseNoteValue(noteValueQuantity.substr(i), noteType, dotted);
}

bool ParserMnx::ParseNoteValue(const std::string& noteValue, NoteType& noteType, bool& dotted)
{
    std::string value{noteValue};
    dotted = false;
    if (!value.empty() && value.back() == 'd')
    {
        dotted = true;
        value.pop_back();
    }
        
    int t{0};
    for (; Mnx::noteType[t] != nullptr; ++t)
    {
        if (value == Mnx::noteType[t])
        {
            noteType = static_cast<NoteType>(t);
            break;
        }
    }
    
    return Mnx::noteType[t] != nullptr;
}

} // namespace luteconv
