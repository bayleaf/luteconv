#ifndef _GENTABCODE_H_
#define _GENTABCODE_H_

#include <iostream>
#include <string>
#include <map>

#include "generator.h"

namespace luteconv
{

/**
 * Generate .tab
 */
class GenTabCode: public Generator
{
public:
    /**
     * Constructor
     */
    GenTabCode() = default;

    /**
     * Destructor
     */
    ~GenTabCode() override = default;
    
    /**
     * Generate TabCode .tc
     * 
     * @param[in] options
     * @param[in] piece
     * @param[out] dst destination
     */
    void Generate(const Options& options, const Piece& piece, std::ostream& dst) override;
    
private:
    static std::string GetTimeSignature(const Bar& bar);
    static std::string GetFlagInfo(const Options& options, const std::vector<Chord>& chords, const Chord& our, int& lhsNumBeams);
    static std::string GetRightFingering(const Note& note);
    static std::string GetLeftFingering(const Note& note);
    static std::string GetRightOrnament(const Note& note);
    static std::string GetLeftOrnament(const Note& note);
    static std::string GetFret(const Note& note);
    std::string GetConnector(const Note& note);
    static int Pos(Compass compass);
    static std::string GetRules(const Piece& piece, const Part& part, TabType tabType);
    static std::string GetFretting(const Part& part);
    static NoteType Adjust(const Chord& chord, const Options& options);
    
    int m_lineId{0};
    std::map<int, int> m_slurId;
    std::map<int, int> m_holdId;
};

} // namespace luteconv

#endif // _GENTABCODE_H_

