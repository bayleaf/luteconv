#ifndef _GENMEI_H_
#define _GENMEI_H_

#include <cstdint>
#include <iostream>
#include <map>

#include <pugixml.hpp>

#include "generator.h"

namespace luteconv
{

/**
 * Generate .mei
 */
class GenMei: public Generator
{
public:
    /**
     * Constructor
     */
    GenMei() = default;

    /**
     * Destructor
     */
    ~GenMei() override = default;
    
    /**
     * Generate .mei
     * 
     * @param[in] options
     * @param[in] piece
     * @param[out] dst destination
     */
    void Generate(const Options& options, const Piece& piece, std::ostream& dst) override;
    
private:
    static void FileDesc(pugi::xml_node xmlfileDesc, const Piece& piece);
    static void EncodingDesc(pugi::xml_node xmlEncodingDesc, const Options& options);
    static void Work(pugi::xml_node xmlwork, const Piece& piece);
    void Body(pugi::xml_node xmlbody, const Options& options, const Piece& piece);
    void Section(pugi::xml_node xmlsection, const Options& options, const Piece& piece);
    void AddNotesTab(pugi::xml_node xmltabGrp, pugi::xml_node xmlmeasure, const Part& part, const Chord& chord,
            const Options& options, std::map<int, std::string>& slurStart, std::map<int, std::string>& holdStart);
    static void AddNotesCmn(pugi::xml_node xmlparent, const Chord& chord, uint8_t& tieTerminalPending);
    std::string MakeId();
    static void AddClef(pugi::xml_node xmlparent, Clef clef);
    static void AddKeySignature(pugi::xml_node xmlparent, Key key);
    void AddTimeSignature(pugi::xml_node xmlparent, const TimeSig& timeSig);
    static std::string GetDur(NoteType noteType);
    
    int m_nextId{0};
    int m_beats{4}; // used to position a fermata over a barline
};

} // namespace luteconv

#endif // _GENMEI_H_
