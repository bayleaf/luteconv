#ifndef _GUNZIPPER_H_
#define _GUNZIPPER_H_

#include <cstdint>
#include <vector>
#include <string>

namespace luteconv
{

/**
 * Ungzip a file
 */
class Gunzipper
{
public:
    
    /**
     * Constructor
     */
    Gunzipper()= default;
    
    /**
     * Destructor
     */
    ~Gunzipper() = default;
    
    /**
     * Gunzip a file into a memory image
     * 
     * @param[in] filename
     * @param[out] image
     */
    static void Gunzip(std::istream& srcFile, std::vector<uint8_t>& outImage);
};

} // namespace luteconv

#endif // _GUNZIPPER_H_
