#include "parserfrt.h"

#include <algorithm>
#include <cctype>
#include <chrono>
#include <cstring>
#include <fstream>
#include <iomanip>
#include <iostream>
#include <regex>

#include "logger.h"

namespace luteconv
{

void ParserFrt::Parse(std::istream &src, const Options &options, Piece &piece)
{
    LOGGER << "Parse Humdrum .frt";
    
    Part& part{piece.m_parts.at(0)}; // only 1 part

    int lineNo{0};
    Bar bar;
    bool barIsClear{true};
    bool inHeader{true};
    int recipSpineIdx{-1};
    int fretSpineIdx{-1};
    int lowestMidi{Pitch('E', 0, 2).Midi()}; // default pitch of lowest course E2
    size_t numSpines{0};
    TimeSig timeSig;
    int tripletCount{0};
    NoteType tripletNoteType{};

    if (options.m_index != 0)
    {
        throw std::invalid_argument("Error: Can't find section, index=" + std::to_string(options.m_index));
    }

    for (;;)
    {
        std::string lineOwner;
        getline(src, lineOwner);
        
        if (src.eof() && lineOwner.empty())
        {
            break;
        }
        
        // remove any trailing \r
        if (!lineOwner.empty() && lineOwner.back() == '\r')
        {
            lineOwner.pop_back();
        }
        
        ++lineNo;
        const std::string_view line{lineOwner}; // string_view is much faster at substringing
        
        if (line.empty())
        {
            LOGGER << lineNo << ": Error: empty line";
            continue;
        }
        
        if (line[0] == '!')
        {
            std::string referenceRecord;
            if (line.substr(0, 3) == "!!!")
            {
                referenceRecord = ParseReferenceRecord(line);
            }
            
            // comment record
            if (line.substr(0, 6) == "!!!OTL" && piece.m_title.empty() && !referenceRecord.empty())
            {
                piece.m_title = referenceRecord;
                LOGGER << "title=" << piece.m_title;
            }
            else if (line.substr(0, 6) == "!!!COM" && piece.m_composer.empty() && !referenceRecord.empty())
            {
                piece.m_composer = referenceRecord;
                LOGGER << "composer=" << piece.m_composer;
            }
            else if (line.substr(0, 6) == "!!!YEM" && piece.m_copyright.empty() && !referenceRecord.empty())
            {
                piece.m_copyright = referenceRecord;
            }
            else if (!referenceRecord.empty())
            {
                piece.m_credits.push_back(referenceRecord);
            }
            
            // ignore other comments, global and local
            continue;
        }
        
        // split into tokens
        std::vector<std::string_view> tokens;
        Split(line, '\t', tokens);
        
        if (line.substr(0, 2) == "**")
        {
            // exclusive interpretation record
            
            if (recipSpineIdx != -1 && fretSpineIdx != -1)
            {
                LOGGER << lineNo << ": already have our spines' representations, ignored : " << line;
                continue;
            }
            
            // need both to be specified
            recipSpineIdx = -1;
            fretSpineIdx = -1;
            
            // use first occurance of **recip and **fret
            for (size_t i{0}; i < tokens.size(); ++i)
            {
                if (tokens[i] == "**recip")
                {
                    if (recipSpineIdx == -1)
                    {
                        recipSpineIdx = static_cast<int>(i);
                    }
                }
                else if (tokens[i] == "**fret")
                {
                    if (fretSpineIdx == -1)
                    {
                        fretSpineIdx = static_cast<int>(i);
                    }
                }
            }
            
            numSpines = tokens.size();
            continue;
        }
        
        if (numSpines != tokens.size())
        {
            LOGGER << lineNo << ": Error: wrong number of tokens : " << line;
            continue;
        }
        
        if (recipSpineIdx == -1 || fretSpineIdx == -1)
        {
            LOGGER << lineNo << ": Error: missing spines' representations : " << line;
            continue;
        }

        const std::string_view recipTok{tokens[recipSpineIdx]};
        const std::string_view fretTok{tokens[fretSpineIdx]};
        if (recipTok.empty() || fretTok.empty())
        {
            LOGGER << lineNo << ": Error: empty record : " << line;
            continue;
        }
        
        if (recipTok[0] == '.' || fretTok[0] == '.')
        {
            LOGGER << lineNo << ": Error: null tokens : " << line;
            continue;
        }

        if (line[0] == '*')
        {
            // tandem interpretation record
            if (recipTok.substr(0, 2) == "*-")
            {
                recipSpineIdx = -1;
            }
            
            if (fretTok.substr(0, 2) == "*-")
            {
                fretSpineIdx = -1;
            }
            else if (fretTok.substr(0, 4) == "*AT:")
            {
                // lowest pitched course
                size_t pos{4};
                lowestMidi = Pitch::Parse(fretTok, pos).Midi();
            }
            else if (fretTok.substr(0, 4) == "*RT:")
            {
                ParseRelativeTuning(line, lineNo, fretTok, lowestMidi, part);
            }
            else if (fretTok.substr(0, 4) == "*FT:")
            {
                ParseFretTuning(line, lineNo, fretTok, part);
            }
            else if (fretTok.substr(0, 2) == "*M" && fretTok.size() >= 5 && isdigit(fretTok[2]) != 0)
            {
                // M*3/4
                if (inHeader)
                {
                    ParseTimeSignature(fretTok, timeSig);
                }
                else
                {
                    ParseTimeSignature(fretTok, bar.m_timeSig);
                }
            }
            else
            {
                LOGGER << lineNo << ": tandem interpretation record, ignored : " << line;
            }
            continue;
        }

        // data record
        
        inHeader = false;

        if (fretTok[0] == '=')
        {
            tripletCount = 0; // should be necessary unless triplet count goes wrong
            ParseBarLine(fretTok, bar, barIsClear, part);
        }
        else
        {
            barIsClear = false;
            bar.m_chords.emplace_back(); // new chord
            Chord &chord{bar.m_chords.back()};
            ParseRhythm(line, lineNo, recipTok, chord, tripletCount, tripletNoteType);
            ParseChord(line, lineNo, fretTok, chord, part);
        }
    }

    // deal with possible missing final bar line
    ParseBarLine("==", bar, barIsClear, part);
    
    // if there is a time signature in the header, but not in the first bar
    // then put it in the first bar
    if (timeSig.m_timeSymbol != TimeSyNone && !part.m_bars.empty() && part.m_bars.front().m_timeSig.m_timeSymbol == TimeSyNone)
    {
        part.m_bars.front().m_timeSig = timeSig;
    }

    part.SetTuning(options);
}

std::string ParserFrt::ParseReferenceRecord(std::string_view line)
{
    // !!!XXX: text
    const size_t colon{line.find_first_of(':')};
    if (colon == std::string_view::npos)
    {
        return "";
    }
    
    // ignore leading spaces
    const size_t notSpace{line.find_first_not_of(' ', colon + 1)};
    if (notSpace == std::string_view::npos)
    {
        return "";
    }

    return std::string(line.substr(notSpace));
}

void ParserFrt::Split(std::string_view s, const char c, std::vector<std::string_view>& splits)
{
    for (size_t startField{0}; startField < s.size();)
    {
        const size_t endField{s.find_first_of(c, startField)};
        if (endField == std::string_view::npos)
        {
            splits.emplace_back(s.substr(startField));
            return;
        }

        splits.emplace_back(s.substr(startField, endField - startField));
        startField = endField + 1;
    }
}

void ParserFrt::ParseBarLine(std::string_view fretTok, Bar& bar, bool& barIsClear, Part& part)
{
    if (fretTok.find("||") != std::string_view::npos)
    {
        bar.m_barStyle = BarStyleLightLight;
    }
    else if (fretTok.find("|!") != std::string_view::npos)
    {
        bar.m_barStyle = BarStyleLightHeavy;
    }
    else if (fretTok.find("!|") != std::string_view::npos)
    {
        bar.m_barStyle = BarStyleHeavyLight;
    }
    else if (fretTok.find("!!") != std::string_view::npos)
    {
        bar.m_barStyle = BarStyleHeavyHeavy;
    }
    else if (fretTok.find('!') != std::string_view::npos)
    {
        bar.m_barStyle = BarStyleHeavy;
    }
    else if (fretTok.find('\"') != std::string_view::npos)
    {
        bar.m_barStyle = BarStyleDotted;
    }
    else if (fretTok.find("==") != std::string_view::npos)
    {
        bar.m_barStyle = BarStyleLightHeavy;
    }
    else
    {
        bar.m_barStyle = BarStyleRegular;
    }

    bar.m_fermata = (fretTok.find(';') != std::string_view::npos);
    
    // repeats
    static const std::regex reJanus{R"(:.*:)"};
    static const std::regex reForward{R"([|!]:)"};
    static const std::regex reBackward{R"(:[|!])"};
    std::match_results<std::string_view::const_iterator> results;
    if (std::regex_search(fretTok.cbegin(), fretTok.cend(), results, reJanus))
    {
        bar.m_repeat = RepJanus;
    }
    else if (std::regex_search(fretTok.cbegin(), fretTok.cend(), results, reForward))
    {
        bar.m_repeat = RepForward;
    }
    else if (std::regex_search(fretTok.cbegin(), fretTok.cend(), results, reBackward))
    {
        bar.m_repeat = RepBackward;
    }
    else
    {
        bar.m_repeat = RepNone;
    }
    
    if (!barIsClear)
    {
        part.m_bars.push_back(bar);
    }

    bar.Clear();
    barIsClear = true;
    
    // volta brackets for the following bar
    static const std::regex reVolta{R"(\d+([a-z]))"};
    if (std::regex_search(fretTok.cbegin(), fretTok.cend(), results, reVolta))
    {
        bar.m_volta = results[1].str()[0] - 'a' + 1;
    }
}

void ParserFrt::ParseRhythm(std::string_view line, int lineNo, std::string_view recipTok, Chord& chord, int& tripletCount, NoteType& tripletNoteType)
{
    // rhythm
    if (recipTok.substr(0, 2) == "00")
    {
        chord.m_noteType = NoteTypeLong;
    }
    else
    {
        static_assert(NoteTypeBreve == 1);
        
        int humRhythm{0};
        try
        {
            humRhythm = std::stoi(std::string(recipTok));
        }
        catch (...)
        {
            LOGGER << lineNo << ": Error: bad pitch duration : " << line;
        }
        
        if (humRhythm % 3 == 0)
        {
            // triplet
            humRhythm = (humRhythm * 2) / 3;

            if (tripletCount == 0)
            {
                chord.m_triplet = true;
                tripletCount = 3;
            }
        }
        
        int noteType{static_cast<int>(NoteTypeBreve)};
        while (humRhythm > 0)
        {
            ++noteType;
            humRhythm >>= 1;
        }
        chord.m_noteType = static_cast<NoteType>(noteType);
        
        if (tripletCount == 3)
        {
            tripletNoteType = chord.m_noteType;
        }
        else if (tripletCount == 2 && chord.m_noteType != tripletNoteType)
        {
            // 2 note triplet, i.e. second note has different rhythm from first
            --tripletCount;
        }
        
        if (tripletCount != 0)
        {
            --tripletCount;
        }
    }
    
    chord.m_dotted = (recipTok.find('.') != std::string_view::npos);
}

void ParserFrt::ParseChord(std::string_view line, int lineNo, std::string_view fretTok, Chord& chord, Part& part)
{
    // rest?
    if (fretTok[0] == 'r')
    {
        return;
    }
    
    // split into sub-tokens, 1 per course
    std::vector<std::string_view> subTokens;
    Split(fretTok, ' ', subTokens);
    
    if (subTokens.size() != part.m_tuning.size())
    {
        LOGGER << lineNo << ": Error: wrong number of course sub-tokens:" << subTokens.size() << " != tuning:" << part.m_tuning.size() << " : " << line;
        return;
    }
    
    for (size_t course{1}; course <= part.m_tuning.size(); ++course) 
    {
        const std::string_view st{subTokens.at(part.m_tuning.size() - course)};
        size_t i{0};

        while (i < st.size() && (st[i] == '<' || st[i] == '>' || st[i] == '%'))
        {
             ++i;
        }
            
        if (i == st.size() || strchr("|/\\#z", st[i]) == nullptr)
        {
            continue; // course is not plucked
        }
        ++i;

        chord.m_notes.emplace_back(); // new note
        Note &note{chord.m_notes.back()};
        
        note.m_course = static_cast<int>(course);
        note.m_fret = 0;
        
        while (i < st.size())
        {
            if (isdigit(st[i]) != 0)
            {
                note.m_fret = note.m_fret * 10 + (st[i] - '0');
                ++i;
                continue;
            }
            
            // left fingering
            const char left[]{"bcdea"};
            const char* lp{strchr(left, st[i])};
            if (lp != nullptr)
            {
                note.m_leftFingering = static_cast<Fingering>(lp - left + 1);
                ++i;
                continue;
            }
            
            // right fingering
            const char right[]{"IAMQP"};
            const char* rp{strchr(right, st[i])};

            if (rp != nullptr)
            {
                note.m_rightFingering = static_cast<Fingering>(rp - right + 1);
                ++i;
                continue;
            }
            
            LOGGER << lineNo << ": signifier " << st.substr(i, 1) << " ignored : " << line;
            ++i;
        }
    }
}

void ParserFrt::ParseTimeSignature(std::string_view fretTok, TimeSig& timeSig)
{
    // M*3/2
    timeSig.m_beats = 0;
    timeSig.m_beatType = 0;
    size_t i{2};
    
    while (i < fretTok.size() && isdigit(fretTok[i]) != 0)
    {
        timeSig.m_beats = timeSig.m_beats * 10 + fretTok[i] - '0';
        ++i;
    }
    
    if (i < fretTok.size() && fretTok[i] == '/')
    {
        ++i;
    }
    
    while (i < fretTok.size() && isdigit(fretTok[i]) != 0)
    {
        timeSig.m_beatType = timeSig.m_beatType * 10 + fretTok[i] - '0';
        ++i;
    }
    
    if (timeSig.m_beats > 0 && timeSig.m_beatType > 0)
    {
        timeSig.m_timeSymbol = TimeSyNormal;
    }
}

void ParserFrt::ParseRelativeTuning(std::string_view line, int lineNo, std::string_view fretTok, int lowestMidi, Part& part)
{
    try
    {
        // *RT:0,12:5,17:10,22:14,14:19,19:24,24
        size_t pos{4};
        
        while (pos < fretTok.size())
        {
            // strings within a course, we want the lowest relative pitch
            int lowString{std::numeric_limits<int>::max()};
            while (pos < fretTok.size())
            {
                size_t len{0};
                lowString = std::min(lowString, static_cast<int>(std::stoi(std::string(fretTok.substr(pos)), &len)));
                pos += len;
                if (pos < fretTok.size() && fretTok[pos] == ',')
                {
                    ++pos;
                }
                else
                {
                    break;
                }
            }

            
            // Humdrum has bottom course first
            part.m_tuning.insert(part.m_tuning.begin(), Pitch(lowString + lowestMidi));
            if (pos < fretTok.size() && fretTok[pos] == ':')
            {
                ++pos;
            }
            else
            {
                break;
            }
        }
        LOGGER << lineNo << ": Source tuning " << Pitch::GetTuning(part.m_tuning);
    }
    catch (...)
    {
        LOGGER << lineNo << ": Error: source tuning : " << line;
    }
}

void ParserFrt::ParseFretTuning(std::string_view line, int lineNo, std::string_view fretTok, Part& part)
{
    // missing fret 4
    // *FT:1,2,3,5,6,7,8,9,10,11,12
    
    // An instrument constructed with nine 1/4-tone fret positions
    // *FT:.5,1,1.5,2,2.5,3,3.5,4,4.5 
    
    try
    {
        size_t pos{4};
        int fret{1};
    
        while (pos < fretTok.size())
        {
            size_t len{0};
            auto tuning{static_cast<int>(std::stod(std::string(fretTok.substr(pos)), &len))};
            pos += len;
            
            while (fret < tuning)
            {
                part.m_missingFrets.push_back(fret);
                LOGGER << lineNo << ": missing fret : " << fret;
                ++fret;
            }
            ++fret;
            
            if (pos < fretTok.size() && fretTok[pos] == ',')
            {
                ++pos;
            }
        }
    }
    catch (...)
    {
        LOGGER << lineNo << ": Error: fret tuning : " << line;
        part.m_missingFrets.clear();
    }
}

} // namespace luteconv
