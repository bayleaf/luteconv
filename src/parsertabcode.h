#ifndef _PARSERTABCODE_H_
#define _PARSERTABCODE_H_

#include <iostream>
#include <map>
#include <string_view>

#include "parser.h"

namespace luteconv
{

/**
 * Parse TabCode .tc file
 * 
 * http://doc.gold.ac.uk/isms/ecolm/?page=TabCode
 */
class ParserTabCode: public Parser
{
public:

    /**
     * Constructor
    */
    ParserTabCode() = default;

    /**
     * Destructor
     */
    ~ParserTabCode() override = default;
    
    /**
     * Parse .tab file
     *
     * @param[in] src .tab stream
     * @param[in] options
     * @param[out] piece destination
     */
    void Parse(std::istream& srcFile, const Options& options, Piece& piece) override;
    
private:
    void ParseCodeWord(std::string_view tabword, int lineNo, Bar& bar, bool& barIsClear, Part& part);
    void ParseExtra(std::string_view tabword, int lineNo, std::string_view extra, Note& note);
    static void ParseBarLine(std::string_view tabword, int lineNo, Bar& bar, bool& barIsClear, Part& part);
    void ParseChord(std::string_view tabword, int lineNo, Bar& bar, bool& barIsClear);
    static void ParseTimeSignature(std::string_view tabword, int lineNo, Bar& bar);
    void ParseConnector(std::string_view tabword, int lineNo, std::string_view extra, Note& note);
    static Compass Pos(int pos);
    static void ParseRules(std::string_view rules, Piece& piece, Part& part);
    static void ParseFretting(std::string_view fretting, Part& part);
    
    std::map<int, Connector> m_startConnector;
    int m_slurNumber{1};
    int m_holdNumber{1};
    
    Chord m_previousChord;
    int m_lhsNumBeams{0};
};

} // namespace luteconv

#endif // _PARSERTABCODE_H_
