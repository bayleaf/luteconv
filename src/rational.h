#ifndef _RATIONAL_H_
#define _RATIONAL_H_

#include <string>

namespace luteconv
{

/**
 * Minimal rational number class
 */
class Rational
{
public:
    
    /**
     * Constructor
     * 
     */
    Rational() = default;
    
    /**
     * Constructor
     * 
     * param[in] n numerator
     */
    explicit Rational(int n)
    : m_num{n}, m_den{1}
    {
        
    }
    
    /**
     * Constructor
     * 
     * param[in] n numerator
     * param[in] d denominator
     */
    explicit Rational(int n, int d);
    
    /**
     * Get numerator
     * 
     * @return numerator
     */
    int Num() const
    {
        return m_num;
    }
    
    /**
     * Get demonimator
     * 
     * @return demonimator
     */
    int Den() const
    {
        return m_den;
    }
    
    /**
     * Unary oprators
     */
    Rational operator +() const;
    Rational operator -() const;
    
    /**
     * Relations
     */
    bool operator ==(Rational rhs) const;
    bool operator <(Rational rhs) const;
    
    /**
     * Assignment operators
     */
    Rational& operator +=(Rational rhs);
    Rational& operator -=(Rational rhs);
    Rational& operator *=(Rational rhs);
    Rational& operator /=(Rational rhs);
    
    /**
     * Rational number as a string
     * 
     * @return a/b
     */
    std::string ToString() const;
    
    /**
     * Get value as int, truncated towards 0
     * 
     * @return truncated
     */
    int Trunc() const;
    
private:
    int m_num{0};
    int m_den{1};
};

/**
 * Binary operators
 */
Rational operator +(Rational lhs, Rational rhs);
Rational operator -(Rational lhs, Rational rhs);
Rational operator *(Rational lhs, Rational rhs);
Rational operator /(Rational lhs, Rational rhs);

/**
 * The rest of the relations
 */
bool operator <=(Rational lhs, Rational rhs);
bool operator !=(Rational lhs, Rational rhs);
bool operator >(Rational lhs, Rational rhs);
bool operator >=(Rational lhs, Rational rhs);

} // namespace luteconv

#endif // _RATIONAL_H_
