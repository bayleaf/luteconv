#ifndef _GENTAB_H_
#define _GENTAB_H_

#include <iostream>
#include <string>

#include "generator.h"
#include "genlsml.h"

namespace luteconv
{

/**
 * Generate .tab
 */
class GenTab: public Generator
{
    friend class GenLsml;
    
public:
    /**
     * Constructor
     */
    GenTab();

    /**
     * Destructor
     */
    ~GenTab() override = default;
    
    /**
     * Generate .tab
     * 
     * @param[in] options
     * @param[in] piece
     * @param[out] dst destination
     */
    void Generate(const Options& options, const Piece& piece, std::ostream& dst) override;
    
    /**
     * Get the lute tuning from Wayne Cripps' Tab program
     * Octaves are numbered in decreasing pitch, octaves run from A..G
     * e.g for 10 course lute "C4D4E4F4G4c3f3a2d2g2"
     * 
     * @param[in] tuning
     * @return Wayne Cripps Tab, last string ... first string
     */
    static std::string GetTuningTab(const std::vector<Pitch>& tuning);
    
private:
    static void GetHeaders(const Piece& piece, const Part& partTab, TabType tabType, size_t numParts, std::vector<std::string>& headers);
    static void GetCourses(const Part& partTab, TabType tabType, const Chord& chord, std::string& flag, std::vector<std::string>& courses);
    static void GetBarLines(const Bar& bar, bool lineBreak,
            std::vector<std::string>& repForward, std::vector<std::string>& barLines);
    static std::string GetClef(Clef clef);
    static std::string GetKeySignature(Key key);
    static std::string GetTimeSignature(const TimeSig & timeSig);
    static std::string GetRhythm(const Options& options, const Chord & chord);
    static std::string GetFlagInfo(const Options& options, const std::vector<Chord> & chords, const Chord & our, NoteType& gridNoteType);
    static std::string GetRightFingering(const Note & note);
    static std::string GetLeftFingering(const Note & note);
    static std::string GetRightOrnament(const Note & note);
    static std::string GetLeftOrnament(const Note & note);
    static std::string GetFret(const Note & note, TabType tabType);
    static std::string GetLeftConnector(const Connector & connector);
    static std::string GetRightConnector(const Connector & connector);
    static std::string GetConnector(const Connector & connector);
    static Compass Bearing(const Compass compass);
    static std::string GetLyrics(const Chord& chord);
    std::string GetCmnNote(const Options& options, const Chord& chord, Clef clef);
    
    const std::vector<Pitch> m_clefGTuning; // for translating TAB's CMN pitches
    const std::vector<Pitch> m_clefFTuning;
};

} // namespace luteconv

#endif // _GENTAB_H_

