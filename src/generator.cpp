#include "generator.h"

#include <fstream>
#include <iostream>

namespace luteconv
{

void Generator::Generate(const Options& options, const Piece& piece)
{
    if (options.m_dstFilename.empty())
    {
        Generate(options, piece, std::cout);
    }
    else
    {
        std::fstream dst;
        dst.open(options.m_dstFilename, std::fstream::out | std::fstream::binary | std::fstream::trunc);
        if (!dst.is_open())
        {
            throw std::runtime_error(std::string("Error: Can't open ") + options.m_dstFilename);
        }
        
        Generate(options, piece, dst);
    }
}

size_t Generator::FindTabPart(const Piece& piece)
{
    // find the first tablature part
    for (size_t partIdx = 0; partIdx < piece.m_parts.size(); ++partIdx)
    {
        if (piece.m_parts.at(partIdx).m_tabType != TabCmn)
        {
            return partIdx;
        }
    }
    
    throw std::runtime_error(std::string("Error: No tablature found"));
}

} // namespace luteconv
