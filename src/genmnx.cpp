#include "genmnx.h"

#include <algorithm>
#include <chrono>
#include <ctime>
#include <fstream>
#include <iomanip>
#include <iostream>
#include <sstream>
#include <string>

#include "logger.h"
#include "mnx.h"

namespace luteconv
{

using namespace pugi;

void GenMnx::Generate(const Options& options, const Piece& piece, std::ostream& dst)
{
    LOGGER << "Generate mnx";
    
    // find the first tablature part
    const Part& part{piece.m_parts.at(FindTabPart(piece))};
    
    xml_document doc;
    
    xml_node decl{doc.prepend_child(node_declaration)};
    decl.append_attribute("version") = "1.0";
    decl.append_attribute("standalone") = "no";
    
    xml_node mnx{doc.append_child("mnx")};
    
    // title
    mnx.append_child("title").text().set(piece.m_title.c_str());
    
    // TODO metadata not yet specified
    if (!piece.m_composer.empty())
    {
        mnx.append_child(node_comment).set_value((" composer: " + piece.m_composer + " ").c_str());
    }

    // credits
    for (const auto & credit : piece.m_credits)
    {
        mnx.append_child(node_comment).set_value((" credit-words: " + credit + " ").c_str());
    }
    
    // copyright
    if (!piece.m_copyright.empty() && piece.m_copyrightEnabled)
    {
        mnx.append_child(node_comment).set_value((" rights: " + piece.m_copyright + " ").c_str());
    }
    
    mnx.append_child(node_comment).set_value((" software: luteconv " + options.m_version + " ").c_str());
    
    // encoding-date
    {
        std::time_t tt{std::chrono::system_clock::to_time_t(std::chrono::system_clock::now())};
        struct std::tm * ptm{std::gmtime(&tt)};

        std::ostringstream ss;
        ss << std::put_time(ptm,"%F");
        mnx.append_child(node_comment).set_value((" encoding-date: " + ss.str() + " ").c_str());
     }
    
    mnx.append_child(node_comment).set_value(" instrument: lute ");
    mnx.append_child(node_comment).set_value((" tuning: " + Pitch::GetTuning(part.m_tuning) + " ").c_str());
    
    // global
    xml_node global{mnx.append_child("global")};
    
    // part
    xml_node xmlpart{mnx.append_child("part")};
    xmlpart.append_child("part-name").text().set("Lute");
    
    const int count{static_cast<int>(part.m_bars.size())};
    bool repForward{false};
    for (int i{0}; i < count; i++)
    {
        MeasureGlobal(global, part.m_bars[i], i + 1, repForward);
        MeasurePart(xmlpart, part, part.m_bars[i], i + 1, options);
    }
    
    doc.save(dst, "    ");
}

void GenMnx::MeasureGlobal(xml_node global, const Bar& bar, int measureNo, bool& repForward)
{
    xml_node measureGlobal{global.append_child("measure-global")};
    measureGlobal.append_attribute("index") = measureNo;
    
    if (bar.m_timeSig.m_timeSymbol != TimeSyNone || bar.m_repeat == RepBackward || bar.m_repeat == RepJanus || repForward || bar.m_volta > 0)
    {
        xml_node directionsGlobal{measureGlobal.append_child("directions-global")};
        
        // time signature
        if (bar.m_timeSig.m_timeSymbol != TimeSyNone)
        {
            xml_node time{directionsGlobal.append_child("time")};
            
            // TODO: common time, cut time etc.
            time.append_attribute("signature") = (std::to_string(bar.m_timeSig.m_beats) + '/' + std::to_string(bar.m_timeSig.m_beatType)).c_str();
        }
        
        if (bar.m_volta > 0)
        {
            xml_node endingStart{directionsGlobal.append_child("ending")};
            endingStart.append_attribute("number") = bar.m_volta;
            endingStart.append_attribute("type") = "start";

            xml_node endingStop{directionsGlobal.append_child("ending")};
            endingStop.append_attribute("type") = (bar.m_volta == 1 ? "stop" : "discontinue");
        }
        
        // repeat
        if (repForward)
        {
            xml_node repeat{directionsGlobal.append_child("repeat")};
            repeat.append_attribute("type") = "start";
        }
        
        if (bar.m_repeat == RepBackward || bar.m_repeat == RepJanus)
        {
            xml_node repeat{directionsGlobal.append_child("repeat")};
            repeat.append_attribute("type") = "end";
        }
    }
    
    repForward = (bar.m_repeat == RepJanus || bar.m_repeat == RepForward);
}

void GenMnx::MeasurePart(xml_node xmlpart, const Part& part, const Bar& bar, int measureNo, const Options& options)
{
    xml_node measurePart{xmlpart.append_child("measure")};
    measurePart.append_attribute("index") = measureNo;
    
    if (measureNo == 1)
    {
        xml_node directionsPart{measurePart.append_child("directions-part")};
        xml_node clef{directionsPart.append_child("clef")};
        clef.append_attribute("sign") = "TAB";
        clef.append_attribute("line") = "5";
    }
    
    xml_node sequence{measurePart.append_child("sequence")};
    
    xml_node beams;
    std::vector<xml_node> beamElement;
    std::vector<std::string> beamEvents;
    int tripletCount{0};
    xml_node tuplet{};
    
    for (const auto & luteChord : bar.m_chords)
    {
        const NoteType adjusted{Adjust(luteChord, options)};
        
        std::string value{Mnx::noteType[adjusted]};
        if (luteChord.m_dotted)
        {
            value += "d";
        }
        
        if (luteChord.m_triplet)
        {
            size_t index{};
            tripletCount = bar.TripletNotes(luteChord, index);
            
            // if this is a two note triplet then the triplet's note type
            // is the quickest of the two notes
            if (tripletCount == 2 && bar.m_chords.at(index).m_noteType < bar.m_chords.at(index + 1).m_noteType)
            {
                ++index;
            }
            const NoteType tripletAdjusted{Adjust(bar.m_chords.at(index), options)};
            
            std::string tripletValue{Mnx::noteType[tripletAdjusted]};
            if (bar.m_chords.at(index).m_dotted)
            {
                tripletValue += "d";
            }

            tuplet = sequence.append_child("tuplet");
            tuplet.append_attribute("inner") = ('3' + tripletValue).c_str();
            tuplet.append_attribute("outer") = ('2' + tripletValue).c_str();
        }
        
        xml_node event{};
        if (tripletCount > 0)
        {
            event = tuplet.append_child("event");
            --tripletCount;
        }
        else
        {
            event = sequence.append_child("event");
        }
        
        event.append_attribute("value") = value.c_str();
        
        std::string eventId;

        if (luteChord.m_notes.empty())
        {
            event.append_child("rest");
        }
        else
        {
            for (const auto & luteNote : luteChord.m_notes)
            {
                // Ignore ensemble lines
                if (luteNote.m_fret == Note::Ensemble)
                {
                    continue;
                }
                
                xml_node note{};
                
                if (luteNote.m_fret != Note::Anchor) // ignore connector place holder
                {
                    note = event.append_child("note");
                    
                    const Pitch pitch{part.m_tuning[luteNote.m_course - 1] + luteNote.m_fret};
                    note.append_attribute("pitch") = pitch.ToString().c_str();
                    
                    // TODO elements "string" and "fret" are not yet specified for MNX.
                    note.append_child("string").text().set(luteNote.m_course);
                    note.append_child("fret").text().set(luteNote.m_fret);
                }
                    
                // slur
                if (luteNote.m_connector.m_function == FunSlur)
                {
                    if (luteNote.m_connector.m_endPoint == EndPointStart)
                    {
                        if (m_slurTarget.find(luteNote.m_connector.m_number) != m_slurTarget.end())
                        {
                            LOGGER << "measure " << measureNo << ": slur number in use " << luteNote.m_connector.m_number;
                        }
                        else
                        {
                            const std::string start_note{MakeId()};
                            const std::string target{MakeId()};
                            const std::string end_note{MakeId()};
                            m_slurTarget[luteNote.m_connector.m_number] = std::make_pair(target, end_note);
                            
                            if (note != nullptr)
                            {
                                note.append_attribute("id") = start_note.c_str();
                            }
                            
                            xml_node slur{event.append_child("slur")};
                            slur.append_attribute("target") = target.c_str();
                            slur.append_attribute("start-note") = start_note.c_str();
                            slur.append_attribute("end-note") = end_note.c_str();
                            slur.append_attribute("side") = (luteNote.m_connector.m_curve == CurveOver) ? "up" : "down";
                        }
                    }
                    else if (luteNote.m_connector.m_endPoint == EndPointStop)
                    {
                        auto it{m_slurTarget.find(luteNote.m_connector.m_number)};
                        if (it == m_slurTarget.end())
                        {
                            LOGGER << "measure " << measureNo << ": can't find slur endpoint number " << luteNote.m_connector.m_number;
                        }
                        else
                        {
                            if (!eventId.empty())
                            {
                                LOGGER << "measure " << measureNo << ": event is already a slur endpoint " << luteNote.m_connector.m_number;
                            }
                            else
                            {
                                eventId = (it->second).first;
                                event.append_attribute("id") = eventId.c_str();
                                
                                if (note != nullptr)
                                {
                                    note.append_attribute("id") = (it->second).second.c_str();
                                }
                            }
                            m_slurTarget.erase(it);
                        }
                    }
                }
            }
        }
        
        // beams
        const int numBeams{std::max(static_cast<int>(Adjust(luteChord, options)) - static_cast<int>(NoteTypeEighth) + 1, 0)};

        if (luteChord.m_grid != GridNone && numBeams >= 1)
        {
            if (eventId.empty()) // may already have an eventId if this event is a target of a slur
            {
                 eventId = MakeId();
                 event.append_attribute("id") = eventId.c_str();
            }
            
            const long int ourIndex{std::distance(bar.m_chords.data(), &luteChord)};
            const int lhsNumBeams{(ourIndex > 0 && luteChord.m_grid != GridStart)
                            ? std::max(static_cast<int>(Adjust(bar.m_chords[ourIndex - 1], options)) - static_cast<int>(NoteTypeEighth) + 1, 0)
                            : 0
                            };
            const int rhsNumBeams{ourIndex + 1 < static_cast<long int>(bar.m_chords.size()) && luteChord.m_grid != GridEnd
                            ? std::max(static_cast<int>(Adjust(bar.m_chords[ourIndex + 1], options)) - static_cast<int>(NoteTypeEighth) + 1, 0)
                            : 0
                            };

            if (static_cast<int>(beamElement.size()) < numBeams + 1)
            {
                beamElement.resize(numBeams + 1);
            }
            
            if (static_cast<int>(beamEvents.size()) < numBeams + 1)
            {
                beamEvents.resize(numBeams + 1);
            }
            
            if (luteChord.m_grid == GridStart)
            {
                if (!beams)
                {
                    beams = sequence.prepend_child("beams"); // before all <event>
                }
                beamElement[0] = beams; // parent of all beam elements in this <sequence>
            }
            
            for (int i{1}; i <= numBeams; ++i)
            {
                if (lhsNumBeams < i)
                {
                    if (i <= rhsNumBeams)
                    {
                        // begin new (sub)beam
                        xml_node beam{beamElement[i - 1].append_child("beam")};
                        beamElement[i] = beam;
                        beamEvents[i] = eventId;
                    }
                    else
                    {
                        // forward hook
                        // TODO what if this event has two or more hooks?
                        //      <beam-hook> has no contents so where do we parent the second <beam-hook>?
                        if (beamElement[i - 1] != nullptr)
                        {
                            xml_node beamHook{beamElement[i - 1].append_child("beam-hook")};
                            beamHook.append_attribute("direction") = "right";
                            beamHook.append_attribute("event") = eventId.c_str();
                        }
                    }
                }
                else
                {
                    if (i <= rhsNumBeams)
                    {
                        // continue (sub)beam
                        beamEvents[i] += " " + eventId;
                    }
                    else
                    {
                        // end of (sub)beam
                        beamEvents[i] += " " + eventId;
                        beamElement[i].append_attribute("events") = beamEvents[i].c_str();
                        beamElement[i] = xml_node{};
                        beamEvents[i].clear();
                    }
                }
            }
            
            if (luteChord.m_grid == GridEnd)
            {
                beamElement.clear();
                beamEvents.clear();
            }
        }
    }
    
    // If barline is not given, then the barline should be interpreted as follows:
    // If the measure is the last in the document, use light-heavy.
    // Otherwise, use regular.

    if ((static_cast<size_t>(measureNo) == part.m_bars.size() && bar.m_barStyle != BarStyleLightHeavy) ||
        (static_cast<size_t>(measureNo) != part.m_bars.size() && bar.m_barStyle != BarStyleRegular)
    )
    {
        measurePart.append_attribute("barline") = Mnx::barStyle[bar.m_barStyle];
    }
}

std::string GenMnx::MakeId()
{
    return "id" + std::to_string(m_nextId++); 
}

NoteType GenMnx::Adjust(const Chord& chord, const Options& options)
{
    // MuseScore has crotchet == 0 flags
    NoteType adjusted{static_cast<NoteType>(chord.m_noteType + options.m_flags)};
    adjusted = std::max(NoteTypeLong, adjusted);
    adjusted = std::min(NoteType256th, adjusted);
    return adjusted;
}

} // namespace luteconv
