#include "genmusicxml.h"

#include <algorithm>
#include <chrono>
#include <ctime>
#include <fstream>
#include <iomanip>
#include <iostream>
#include <set>
#include <sstream>
#include <string>

#include "logger.h"
#include "musicxml.h"


namespace luteconv
{

using namespace pugi;

const char doctype[]{R"(score-partwise PUBLIC "-//Recordare//DTD MusicXML 4.0 Partwise//EN")" "\n"
                     R"(            "http://www.musicxml.org/dtds/partwise.dtd")"};

void GenMusicXml::Generate(const Options& options, const Piece& piece, std::ostream& dst)
{
    LOGGER << "Generate musicxml";
    
    
    xml_document doc;
    xml_node decl{doc.prepend_child(node_declaration)};
    decl.append_attribute("version") = "1.0";
    decl.append_attribute("standalone") = "no";
    
    doc.append_child(node_doctype).set_value(doctype);
    xml_node scorePartwise{doc.append_child("score-partwise")};
    scorePartwise.append_attribute("version") = "4.0";
    
    // work
    xml_node work{scorePartwise.append_child("work")};
    work.append_child("work-title").text().set(piece.m_title);
    
    // identification
    xml_node identification{scorePartwise.append_child("identification")};
    
    if (!piece.m_composer.empty())
    {
        xml_node creator{identification.append_child("creator")};
        creator.text().set(piece.m_composer);
        creator.append_attribute("type") = "composer";
    }
    
    // copyright
    if (!piece.m_copyright.empty() && piece.m_copyrightEnabled)
    {
        identification.append_child("rights").text().set(piece.m_copyright);
    }
    
    xml_node encoding{identification.append_child("encoding")};
    encoding.append_child("software").text().set(("luteconv " + options.m_version));
    
    // encoding-date
    {
        const std::time_t tt{std::chrono::system_clock::to_time_t(std::chrono::system_clock::now())};
        struct std::tm * ptm{std::gmtime(&tt)};
        std::ostringstream ss;
        ss << std::put_time(ptm,"%F");
        encoding.append_child("encoding-date").text().set(ss.str());
    }
    
    // credit
    for (const auto & credit : piece.m_credits)
    {
        xml_node xmlcredit{scorePartwise.append_child("credit")};
        xmlcredit.append_attribute("page") = 1;
        xmlcredit.append_child("credit-words").text().set(credit);
    }
    
    // part-list
    xml_node xmlPartList{scorePartwise.append_child("part-list")};
    int partNum{1};
    for (const auto& part : piece.m_parts)
    {
        TabType tabType{part.m_tabType};
        const std::set<TabType> tabTypes{ TabFrench, TabItalian, TabSpanish, TabCmn };

        if (tabTypes.find(part.m_tabType) == tabTypes.end())
        {
            LOGGER << "MusicXML does not support " << Options::GetTabType(part.m_tabType) << " tablature type. Using french.";
            tabType = TabFrench;
        }

        const std::string partId{"P" + std::to_string(partNum)};
        xml_node xmlScorePart{xmlPartList.append_child("score-part")};
        xmlScorePart.append_attribute("id") = partId;
        if (part.m_partName.empty())
        {
            xmlScorePart.append_child("part-name").text().set(partId);
        }
        else
        {
            xmlScorePart.append_child("part-name").text().set(part.m_partName);
        }
        
        // part
        xml_node xmlPart{scorePartwise.append_child("part")};
        xmlPart.append_attribute("id") = partId;
            
        const size_t count{part.m_bars.size()};
        bool repForward{false};
 
        Key curKey{KeyNone}; // key, time sig, clef debouncing
        TimeSig curTimeSig{};
        Clef curClef{ClefNone};
        bool tiedStopPending{false};

        for (size_t i{0}; i < count; i++)
        {
            Measure(xmlPart, part, tabType, part.m_bars[i], static_cast<int>(i + 1),
                    repForward, curKey, curTimeSig, curClef, tiedStopPending, options);
        }
        ++partNum;
    }
    
    doc.save(dst, "    ");
}

void GenMusicXml::Measure(xml_node xmlPart, const Part& part, TabType tabType, const Bar& bar,
        int n, bool& repForward, Key& curKey, TimeSig& curTimeSig, Clef& curClef, bool& tiedStopPending, const Options& options)
{
    xml_node measure{xmlPart.append_child("measure")};
    measure.append_attribute("number") = n;
    
    //  forward repeat carried over from previous bar
    if (repForward || bar.m_volta > 0)
    {
        xml_node barline{measure.append_child("barline")};
        barline.append_attribute("location") = "left";
        
        if (bar.m_volta > 0)
        {
            xml_node ending{barline.append_child("ending")};
            ending.append_attribute("number") = bar.m_volta;
            ending.append_attribute("type") = "start";
        }
        if (repForward)
        {
            barline.append_child("bar-style").text().set(MusicXml::barStyle[BarStyleHeavyLight]);
            xml_node repeat{barline.append_child("repeat")};
            repeat.append_attribute("direction") = "forward";
            repForward = false;
        }
    }
    
    if (n == 1)
    {
        FirstMeasureAttributes(measure, part, tabType, bar);
        curKey = bar.m_key;
        curTimeSig = bar.m_timeSig;
        curClef = bar.m_clef;
    }
    else
    {
        const bool needKey{bar.m_key != KeyNone && bar.m_key != curKey};
        const bool needTimeSig{bar.m_timeSig.m_timeSymbol != TimeSyNone && bar.m_timeSig != curTimeSig};
        const bool needClef{bar.m_clef != ClefNone && bar.m_clef != curClef};

        if (needKey || needTimeSig || needClef)
        {
            const xml_node attributes{measure.append_child("attributes")};
    
            // key
            if (needKey)
            {
                AddKey(attributes, bar);
                curKey = bar.m_key;
            }
            
            // time signature
            if (needTimeSig)
            {
                AddTimeSignature(attributes, bar);
                curTimeSig = bar.m_timeSig;
            }
    
            // clef
            if (needClef)
            {
                AddClef(attributes, bar);
                curClef = bar.m_clef;
            }
        }
    }
    
    int tripletCount{0};
    
    for (const auto & luteChord : bar.m_chords)
    {
        if (luteChord.m_triplet)
        {
            tripletCount = bar.TripletNotes(luteChord);
        }

        const NoteType adjusted{Adjust(luteChord, options, part)};
        const int duration{Bar::Duration(adjusted, luteChord.m_dotted, tripletCount > 0)};
        
        if (luteChord.m_notes.empty())
        {
            xml_node note{measure.append_child("note")};
            note.append_child("rest");
            note.append_child("duration").text().set(duration);
            note.append_child("type").text().set(MusicXml::noteType[adjusted]);
            
            if (luteChord.m_dotted)
            {
                note.append_child("dot");
            }
            
            xml_node notations{};
            Triplet(note, notations, luteChord, tripletCount, true);
        }
        else
        {
            bool fermata{luteChord.m_fermata};
            Note orphan{};
            xml_node foster{};
            bool firstNote{true};
            for (const auto & luteNote : luteChord.m_notes)
            {
                if (luteNote.m_connector.m_function == FunSlur)
                {                
                    orphan = luteNote;
                }
                
                // ignore ensenble lines
                if (luteNote.m_fret == Note::Ensemble)
                {
                    continue;
                }
                
                // lutenote might just be an anchor for the slur, not a note at all - orphan!
                if (luteNote.m_fret == Note::Anchor)
                {
                    // is there a foster parent available?
                    if (foster != nullptr && orphan.m_connector.m_function == FunSlur)
                    {
                        AddSlur(foster, orphan);
                        foster = xml_node();
                        orphan = Note();
                    }
                    continue;
                }
                
                Pitch pitch;
                if (part.m_tabType == TabCmn)
                {
                    pitch = luteNote.m_pitch;
                }
                else
                {
                    pitch = part.CalcPitch(luteNote);
                }
                const char step[]{pitch.Step(), '\0'};
                
                xml_node note{measure.append_child("note")};
                
                if (!firstNote)
                {
                    note.append_child("chord");
                }
                
                xml_node xmlpitch{note.append_child("pitch")};
                xmlpitch.append_child("step").text().set(step);
                if (pitch.Alter() != 0)
                {
                    xmlpitch.append_child("alter").text().set(pitch.Alter());
                }
                xmlpitch.append_child("octave").text().set(pitch.Octave());
                note.append_child("duration").text().set(duration);
                note.append_child("type").text().set(MusicXml::noteType[adjusted]);

                if (luteChord.m_dotted)
                {
                    note.append_child("dot");
                }
                
                switch (luteNote.m_accidental)
                {
                    case AccNone:
                        break;
                    case AccFlat:
                        note.append_child("accidental").text().set("flat");
                        break;
                    case AccNatural:
                        note.append_child("accidental").text().set("natural");
                        break;
                    case AccSharp:
                        note.append_child("accidental").text().set("sharp");
                        break;
                }
                
                xml_node notations{};
                
                if (firstNote && (luteChord.m_tie || tiedStopPending))
                {
                    if (notations == nullptr)
                    {
                        notations = note.append_child("notations");
                    }
                    xml_node xmltied{notations.append_child("tied")};
                    if (luteChord.m_tie)
                    {
                        xmltied.append_attribute("type") = "start";
                        tiedStopPending = true;
                    }
                    else
                    {
                        xmltied.append_attribute("type") = "stop";
                        tiedStopPending = false;
                    }
                }
                
                Triplet(note, notations, luteChord, tripletCount, firstNote);
                
                if (notations == nullptr)
                {
                    notations = note.append_child("notations");
                }

                if (fermata)
                {
                    notations.append_child("fermata");
                    fermata = false; // only on first note of chord
                }

                // ornaments
                AddOrnaments(notations, luteNote);
                
                // slur
                if (orphan.m_connector.m_function == FunSlur)
                {
                    AddSlur(notations, orphan);
                    foster = xml_node();
                    orphan = Note();
                }
                else
                {
                    foster = notations;
                }
                
                if (part.m_tabType != TabCmn)
                {
                    xml_node technical{notations.append_child("technical")};
                    technical.append_child("string").text().set(luteNote.m_course);
                    technical.append_child("fret").text().set(luteNote.m_fret);
              
                    if (luteNote.m_leftFingering != FingerNone)
                    {
                        technical.append_child("fingering").text().set(luteNote.m_leftFingering);
                    }
                    
                    if (luteNote.m_rightFingering != FingerNone)
                    {
                        technical.append_child("pluck").text().set(MusicXml::pluck[luteNote.m_rightFingering]);
                    }
                }
                
                // lyrics
                xml_node firstLyric{};
                if (firstNote)
                {
                    // TODO put lyrics on first note of chord or last?
                    int lyricNum{1};
                    for (const auto& lyric : luteChord.m_lyrics)
                    {
                        xml_node xmlLyric{note.append_child("lyric")};
                        xmlLyric.append_attribute("number") = lyricNum;
                        xml_node syllabic{xmlLyric.append_child("syllabic")};
                        xmlLyric.append_child("text").text().set(lyric.m_text);

                        if (lyric.m_syllabic == SylBegin)
                        {
                            syllabic.text().set("begin");
                        }
                        else if (lyric.m_syllabic == SylMiddle)
                        {
                            syllabic.text().set("middle");
                        }
                        else if (lyric.m_syllabic == SylEnd)
                        {
                            syllabic.text().set("end");
                        }
                        else
                        {
                            syllabic.text().set("single");
                        }
                        
                        if (firstLyric == nullptr)
                        {
                            firstLyric = xmlLyric;
                        }
                        ++lyricNum;
                    }
                }
               
                // beam (before <lyric> before <notations>)
                if (firstNote && luteChord.m_grid != GridNone)
                {
                    const long int ourIndex{std::distance(bar.m_chords.data(), &luteChord)};
                    const int lhsNumBeams{(ourIndex > 0 && luteChord.m_grid != GridStart)
                                        ? std::max(static_cast<int>(Adjust(bar.m_chords[ourIndex - 1], options, part)) - static_cast<int>(NoteTypeEighth) + 1, 0)
                                        : 0
                                        };
                    const int rhsNumBeams{ourIndex + 1 < static_cast<long int>(bar.m_chords.size()) && luteChord.m_grid != GridEnd
                                        ? std::max(static_cast<int>(Adjust(bar.m_chords[ourIndex + 1], options, part)) - static_cast<int>(NoteTypeEighth) + 1, 0)
                                        : 0
                                        };
                    
                    const int numBeams{std::max(static_cast<int>(Adjust(luteChord, options, part)) - static_cast<int>(NoteTypeEighth) + 1, 0)};
                    
                    for (int i{1}; i <= numBeams; ++i)
                    {
                        // content
                        const char * content{nullptr};
                        
                        if (lhsNumBeams < i)
                        {
                            if (i <= rhsNumBeams)
                            {
                                content = "begin";
                            }
                            else
                            {
                                content = "forward hook";
                            }
                        }
                        else
                        {
                            if (i <= rhsNumBeams)
                            {
                                content = "continue";
                            }
                            else
                            {
                                content = "end";
                            }
                        }

                        xml_node beam{note.append_child("beam")};
                        beam.text().set(content);
                        beam.append_attribute("number") = i;
                        
                        // beam before notations before lyric
                        if (firstLyric != nullptr)
                        {
                            if (notations != nullptr)
                            {
                                note.insert_move_before(notations, firstLyric);
                                note.insert_move_before(beam, notations);
                            }
                            else
                            {
                                note.insert_move_before(beam, firstLyric);
                            }
                        }
                        else if (notations != nullptr)
                        {
                            note.insert_move_before(beam, notations);
                        }
                    }
                }

                firstNote = false;
            }
        }
    }
    
    if (bar.m_barStyle != BarStyleRegular || bar.m_repeat != RepNone || bar.m_fermata || bar.m_volta > 0)
    {
        // forward repeat carried to next bar
        repForward = (bar.m_repeat == RepForward || bar.m_repeat == RepJanus);
        const bool repBackward{(bar.m_repeat == RepBackward || bar.m_repeat == RepJanus)};

        const char * const direction{repBackward ? "backward" : nullptr};
        
        const BarStyle style{repBackward ? BarStyleLightHeavy : bar.m_barStyle};
        
        xml_node barline{measure.append_child("barline")};
        barline.append_attribute("location") = "right";
        
        if (bar.m_barStyle != BarStyleRegular)
        {
            barline.append_child("bar-style").text().set(MusicXml::barStyle[style]);
        }

        if (bar.m_fermata)
        {
            barline.append_child("fermata");
        }
        
        if (bar.m_volta > 0)
        {
            xml_node ending{barline.append_child("ending")};
            ending.append_attribute("number") = bar.m_volta;
            ending.append_attribute("type") = (bar.m_volta == 1 ? "stop" : "discontinue");
        }

        if (direction != nullptr)
        {
            xml_node repeat{barline.append_child("repeat")};
            repeat.append_attribute("direction") = direction;
        }
    }
}

void GenMusicXml::Triplet(xml_node note, xml_node& notations, const Chord& chord, int& tripletCount, bool firstNote)
{
    if (tripletCount == 0)
    {
        return;
    }
    
    xml_node xmltimeModification{note.append_child("time-modification")};
    xmltimeModification.append_child("actual-notes").text().set(3);
    xmltimeModification.append_child("normal-notes").text().set(2);

    if (!firstNote)
    {
        return;
    }

    if (chord.m_triplet || tripletCount == 1)
    {
        if (notations == nullptr)
        {
            notations = note.append_child("notations");
        }
    
        xml_node xmltuplet{notations.append_child("tuplet")};
        if (chord.m_triplet)
        {
            xmltuplet.append_attribute("type") = "start";
        }
        else
        {
            xmltuplet.append_attribute("type") = "stop";
        }
    }
    --tripletCount;
}

void GenMusicXml::FirstMeasureAttributes(xml_node measure, const Part& part, TabType tabType, const Bar& bar)
{
    xml_node attributes{measure.append_child("attributes")};
    
    // divisions
    attributes.append_child("divisions").text().set(Bar::Duration(NoteTypeQuarter, false, false));
    
    // key
    AddKey(attributes, bar);
    
    // time signature
    AddTimeSignature(attributes, bar);

    // clef
    AddClef(attributes, bar);
    
    // staff-details
    xml_node staffDetails{attributes.append_child("staff-details")};
    staffDetails.append_child("staff-lines").text().set(part.m_staffLines);
    if (tabType == TabFrench)
    {
        staffDetails.append_attribute("show-frets") = "letters";
    }
    
    // lines are numbered 1 at bottom of stave to 6 at the top.
    // French tablature: courses are numbered 1 at the top of the stave to 6 the bottom,
    // then contine 0, -1, -2 ... below the stave.
    // Italian tablature: courses are numbered 1 at the bottom of the stave to 6 the top,
    // then contine 7, 8, 9 ... above the stave.
    const size_t numCourses{part.m_tuning.size()};
    for (size_t course{1}; course <= numCourses; ++course)
    {
        // TODO MusicXML insists that line > 0 so can't give the tuning
        //      of diapasons in French tablature.
        const int line{(tabType == TabItalian) ? static_cast<int>(course) : part.m_staffLines - static_cast<int>(course - 1)};
        
        if (line > 0)
        {
            xml_node staffTuning{staffDetails.append_child("staff-tuning")};
            staffTuning.append_attribute("line") = line;
            
            const char step[]{part.m_tuning[course - 1].Step(), '\0'};
            staffTuning.append_child("tuning-step").text().set(step);
            if (part.m_tuning[course - 1].Alter() != 0)
            {
                staffTuning.append_child("tuning-alter").text().set(part.m_tuning[course - 1].Alter());
            }
            staffTuning.append_child("tuning-octave").text().set(part.m_tuning[course - 1].Octave());
        }
    }
}

void GenMusicXml::AddKey(xml_node attributes, const Bar& bar)
{
    if (bar.m_key == KeyNone)
    {
        return;
    }

    xml_node key{attributes.append_child("key")};
    key.append_attribute("print-object") = "yes";
    key.append_child("fifths").text().set(bar.m_key);
}

void GenMusicXml::AddTimeSignature(xml_node attributes, const Bar& bar)
{
    if (bar.m_timeSig.m_timeSymbol == TimeSyNone)
    {
        return;
    }
    
    xml_node time{attributes.append_child("time")};
    
    if (bar.m_timeSig.m_timeSymbol != TimeSyNormal)
    {
        time.append_attribute("symbol") = MusicXml::timeSymbol[bar.m_timeSig.m_timeSymbol];
    }
    time.append_child("beats").text().set(bar.m_timeSig.m_beats);
    time.append_child("beat-type").text().set(bar.m_timeSig.m_beatType);
}

void GenMusicXml::AddClef(xml_node attributes, const Bar& bar)
{
    xml_node clef{attributes.append_child("clef")};
    
    switch (bar.m_clef)
    {
        case ClefNone:
            [[fallthrough]];
        case ClefTab:
            clef.append_attribute("print-object") = "no";
            clef.append_child("sign").text().set("TAB");
            clef.append_child("line").text().set("5");
            break;
        case ClefF:
            clef.append_attribute("print-object") = "yes";
            clef.append_child("sign").text().set("F");
            clef.append_child("line").text().set("4");
            break;
        case ClefG:
            clef.append_attribute("print-object") = "yes";
            clef.append_child("sign").text().set("G");
            clef.append_child("line").text().set("2");
            break;
        case ClefG8a:
            clef.append_attribute("print-object") = "yes";
            clef.append_child("sign").text().set("G");
            clef.append_child("line").text().set("2");
            clef.append_child("clef-octave-change").text().set(1);
            break;
        case ClefG8b:
            clef.append_attribute("print-object") = "yes";
            clef.append_child("sign").text().set("G");
            clef.append_child("line").text().set("2");
            clef.append_child("clef-octave-change").text().set(-1);
            break;
    }
}

void GenMusicXml::AddOrnaments(xml_node notations, const Note& luteNote)
{
    const std::string_view leftText{MusicXml::ornament[luteNote.m_leftOrnament]};
    const std::string_view rightText{MusicXml::ornament[luteNote.m_rightOrnament]};
    
    if (leftText.empty() && rightText.empty())
    {
        return;
    }
    
    xml_node ornaments{notations.append_child("ornaments")};
    
    if (!leftText.empty())
    {
        xml_node otherOrnament{ornaments.append_child("other-ornament")};
        otherOrnament.text().set(leftText);
        // position left
        otherOrnament.append_attribute("default-x") = -10;
    }
    
    if (!rightText.empty())
    {
        xml_node otherOrnament{ornaments.append_child("other-ornament")};
        otherOrnament.text().set(rightText);
        // position right
        otherOrnament.append_attribute("default-x") = 10;
    }
}

void GenMusicXml::AddSlur(xml_node notations, const Note& note)
{
    xml_node slur{notations.append_child("slur")};
    slur.append_attribute("number") = note.m_connector.m_number;
    if (note.m_connector.m_endPoint == EndPointStart)
    {
        slur.append_attribute("type") = "start";
    }
    else
    {
        slur.append_attribute("type") = "stop";
    }
    
    if (note.m_connector.m_curve == CurveUnder)
    {
        slur.append_attribute("placement") = "below";
        slur.append_attribute("orientation") = "under";
    }
    else
    {
        slur.append_attribute("placement") = "above";
        slur.append_attribute("orientation") = "over";
    }
}

NoteType GenMusicXml::Adjust(const Chord& chord, const Options& options, const Part& part)
{
    // only tablature gets adjusted
    if (part.m_tabType == TabCmn)
    {
        return chord.m_noteType;
    }
    
    // TODO MuseScore has crotchet == 0 flags
    NoteType adjusted{static_cast<NoteType>(chord.m_noteType + options.m_flags)};
    adjusted = std::max(NoteTypeLong, adjusted);
    adjusted = std::min(NoteType256th, adjusted);
    return adjusted;
}

} // namespace luteconv
