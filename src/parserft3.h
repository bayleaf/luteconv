#ifndef _PARSERFT3_H_
#define _PARSERFT3_H_

#include <iostream>
#include <vector>
#include <cstdint>

#include "parser.h"

namespace luteconv
{

/**
 * Parse Fronimo .ft3 file
 * 
 * Fronimo is a closed source Windows lute tablature editor that uses
 * MFC CArchive as its file format.  The contents are undocumented and proprietary.
 * https://sites.google.com/view/fronimo/home
 *
 * Thanks to Luke Emmet for reverse engineering .ft3
 * https://bitbucket.org/loemmet/lutescribe
 *
 * Limitations
 * -----------
 * Single lute part
 * One piece per file
 * Renaissance lute, up to 10 courses
 * French tablature
 * No voice text
 * No mensural notation
 * No ties
 */
class ParserFt3: public Parser
{
public:

    /**
     * Constructor
    */
    ParserFt3() = default;

    /**
     * Destructor
     */
    ~ParserFt3() override = default;
    
    /**
     * Parse .ft2 or .ft3 file
     *
     * @param[in] src stream
     * @param[in] options
     * @param[out] piece destination
     */
    void Parse(std::istream& srcFile, const Options& options, Piece& piece) override;
    
private:
    static void Gunzip(const std::string& filename, std::vector<uint8_t>& ft3Image);
    
    static void ParseHeader(const std::vector<uint8_t>::const_iterator headerBegin,
            Piece& piece);
    
    static void ParseTabType(const Options& options, uint8_t tabType, Part& part);
    
    static std::string ExtractRtf(const char* rtfBegin, int strLen);
    
    static void ParseBody(const Options& options, const std::vector<uint8_t>::const_iterator ft3ImageBegin, const std::vector<uint8_t>::const_iterator bodyBegin,
            const std::vector<uint8_t>::const_iterator bodyEnd, Part& part);
    
    static void ParseBar(const Options& options, const std::vector<uint8_t>::const_iterator ft3ImageBegin, int barNum, const std::vector<uint8_t>::const_iterator barBegin,
            const std::vector<uint8_t>::const_iterator barEnd, Part& part);
    
    static void ParseBarProperties(const std::vector<uint8_t>::const_iterator barBegin, Bar& bar, Part& part);
    
    static bool AtNextFlag(const std::vector<uint8_t>::const_iterator ptr);
    
    static bool AtRest(const std::vector<uint8_t>::const_iterator ptr);
    
    static std::string HexDump(const std::vector<uint8_t>::const_iterator start, ptrdiff_t len);
    
    static bool AtNextNote(const std::vector<uint8_t>::const_iterator ptr);
    
    static Fingering GetRightFingering(uint16_t extras);
    
    static Fingering GetLeftFingering(uint16_t extras);
    
    static Ornament GetLeftOrnament(uint16_t extras);
    
    static Ornament GetRightOrnament(uint16_t extras);
    
    static void ParseTuning(const Options& options, const std::vector<uint8_t>::const_iterator pieceBegin,
            const std::vector<uint8_t>::const_iterator pieceEnd, Part& part);
    
    static std::string ExtractText(std::vector<uint8_t>::const_iterator& ptr);

};

} // namespace luteconv

#endif // _PARSERFT3_H_
