#ifndef _GENLTL_H
#define _GENLTL_H

#include "generator.h"

namespace luteconv
{

/**
 * Generate Lute Tablature with Ligatures font .ltl
 */
class GenLtl: public Generator
{
public:
    /**
     * Constructor
     */
    GenLtl() = default;

    /**
     * Destructor
     */
    ~GenLtl() override = default;
    
    /**
     * Generate Lute Tablature with Ligatures font .ltl
     * 
     * @param[in] options
     * @param[in] piece
     * @param[out] dst destination
     */
    void Generate(const Options& options, const Piece& piece, std::ostream& dst) override;
};

} // namespace luteconv

#endif // _GENLTL_H