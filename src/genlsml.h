#ifndef _GENLSML_H_
#define _GENLSML_H_

#include <iostream>
#include <string>
#include <string_view>

#include <pugixml.hpp>

#include "generator.h"

namespace luteconv
{

/**
 * Generate LuteScribe .lsml
 */
class GenLsml: public Generator
{
public:
    /**
     * Constructor
     */
    GenLsml() = default;

    /**
     * Destructor
     */
    ~GenLsml() override = default;
    
    /**
     * Generate .lsml
     * 
     * @param[in] options
     * @param[in] piece
     * @param[out] dst destination
     */
    void Generate(const Options& options, const Piece& piece, std::ostream& dst) override;
    
private:
    static void GenChord(std::string_view flag, const std::vector<std::string>& courses, pugi::xml_node xmlchords);
    static void GenChord(std::string_view flag, pugi::xml_node xmlchords);
};

} // namespace luteconv

#endif // _GENLSML_H_

