#ifndef _PARSERJTXML_H_
#define _PARSERJTXML_H_

#include <pugixml.hpp>

#include <string>
#include <string_view>

#include "parser.h"

namespace luteconv
{

/**
 * Parse Fandango .jtxml file
 */
class ParserJtxml: public Parser
{
public:

    /**
     * Constructor
    */
    ParserJtxml() = default;

    /**
     * Destructor
     */
    ~ParserJtxml() override = default;
    
    /**
     * Parse .jtxml file image in buffer
     *
     * @param[in] filename .jtxml
     * @param[in] contents .jtxml image
     * @param[in] size
     * @param[in] options
     * @param[out] piece destination
     */
    static void Parse(std::string_view filename, void* contents, size_t size, const Options& options, Piece& piece);
    
    /**
     * Parse .jtxml file
     *
     *@param[in] src stream
     * @param[in] options
     * @param[out] piece destination
     */
    void Parse(std::istream& srcFile, const Options& options, Piece& piece) override;
    
private:
    static void Parse(std::string_view filename, const pugi::xml_document& doc, const pugi::xml_parse_result& result, const Options& options, Piece& piece);
    static void ParseTuning(pugi::xml_node xmlsection, Part& part);
    static void ParseEvent(pugi::xml_node xmlevent, Part& part);
    static void ParseFlag(int flagNum, Chord& chord);
    
    using Parser::Parse; // avoid -Werror=overloaded-virtual=
};

} // namespace luteconv

#endif // _PARSERJTXML_H_
