#ifndef _CONVERTER_H_
#define _CONVERTER_H_

#include <memory>
#include <vector>

#include "generator.h"
#include "options.h"
#include "parser.h"

namespace luteconv
{

/**
 * Convert lute tablature formats
 */
class Converter
{
public:
    /**
     * Constructor
     */
    Converter() = default;
    
    /**
     * Destructor
     */
    ~Converter() = default;
    
    /**
     * Covert lute tablature from src to destination format
     * 
     * @param[in] options
     */
    static void Convert(const Options& options);
    
private:
    static std::unique_ptr<Generator> GetGenerator(const Options& options);
    static std::unique_ptr<Parser> GetParser(const Options& options);
    static void Transform(const Options& options, Piece& piece);
    static void Transmute(const Options& options, Piece& piece, Part& part);
    static void Intabulate(Piece& piece, Part& part);
    static void IntabulateNote(Part& part, size_t n, int lastLhPos, int& bestLHPos, int& bestScore, std::vector<Note>& notes, std::vector<Note>& candidate);
    static int Score(const std::vector<Note>& candidate, int lastLhPos, int& lhPos);
    static int MaxFret(int course);
};


} // namespace luteconv

#endif // _CONVERTER_H_