#include "converter.h"

#include "genabc.h"
#include "genfrt.h"
#include "genlsml.h"
#include "genltl.h"
#include "genmei.h"
#include "genmid.h"
#include "genmnx.h"
#include "genmusicxml.h"
#include "genmxl.h"
#include "gentab.h"
#include "gentabcode.h"
#include "logger.h"
#include "parserabc.h"
#include "parserfrt.h"
#include "parserft3.h"
#include "parserjtxml.h"
#include "parserjtz.h"
#include "parserlsml.h"
#include "parserltl.h"
#include "parsermei.h"
#include "parsermid.h"
#include "parsermnx.h"
#include "parsermusicxml.h"
#include "parsermxl.h"
#include "parsertab.h"
#include "parsertabcode.h"
#include "piece.h"

#include <algorithm>
#include <array>
#include <cassert>
#include <limits>
#include <stdexcept>

namespace luteconv
{

std::unique_ptr<Generator> Converter::GetGenerator(const Options& options)
{
    switch (options.m_dstFormat)
    {
    case FormatUnknown:
    {
        throw std::runtime_error(std::string("Error: Unknown destination file format: ") + options.m_dstFilename);
    }
    case FormatAbc:
    {
        return std::make_unique<GenAbc>();
    }
    case FormatFrt:
    {
        return std::make_unique<GenFrt>();
    }
    case FormatLsml:
    {
        return std::make_unique<GenLsml>();
    }
    case FormatLtl:
    {
        return std::make_unique<GenLtl>();
    }
    case FormatMei:
    {
        return std::make_unique<GenMei>();
    }
    case FormatMid:
    {
        return std::make_unique<GenMid>();
    }
    case FormatMnx:
    {
        return std::make_unique<GenMnx>();
    }
    case FormatMusicxml:
    {
        return std::make_unique<GenMusicXml>();
    }
    case FormatMxl:
    {
        return std::make_unique<GenMxl>();
    }
    case FormatTab:
    {
        return std::make_unique<GenTab>();
    }
    case FormatTabCode:
    {
        return std::make_unique<GenTabCode>();
    }
    default:
    {
        throw std::runtime_error(std::string("Error: destination file format not supported: ") + options.m_dstFilename);
    }
    }
    
    assert(false); // can't happen
    return {};
}

std::unique_ptr<Parser> Converter::GetParser(const Options& options)
{
    switch (options.m_srcFormat)
    {
    case FormatUnknown:
    {
        throw std::runtime_error(std::string("Error: Unknown source file format: ") + options.m_srcFilename);
    }
    case FormatAbc:
    {
        return std::make_unique<ParserAbc>();
    }
    case FormatFrt:
    {
        return  std::make_unique<ParserFrt>();
    }
    case FormatFt2:
    case FormatFt3:
    {
        return  std::make_unique<ParserFt3>();
    }
    case FormatJtxml:
    {
        return std::make_unique<ParserJtxml>();
    }
    case FormatJtz:
    {
        return std::make_unique<ParserJtz>();
    }
    case FormatLsml:
    {
        return std::make_unique<ParserLsml>();
    }
    case FormatLtl:
    {
        return std::make_unique<ParserLtl>();
    }
    case FormatMei:
    {
        return std::make_unique<ParserMei>();
    }
    case FormatMid:
    {
        return std::make_unique<ParserMid>();
    }
    case FormatMnx:
    {
        return std::make_unique<ParserMnx>();
    }
    case FormatMusicxml:
    {
        return std::make_unique<ParserMusicXml>();
    }
    case FormatMxl:
    {
        return std::make_unique<ParserMxl>();
    }
    case FormatTab:
    {
        return std::make_unique<ParserTab>();
    }
    case FormatTabCode:
    {
        return std::make_unique<ParserTabCode>();
    }
    default:
    {
        throw std::runtime_error(std::string("Error: source file format not supported: ") + options.m_srcFilename);
    }
    }

    assert(false); // can't happen
    return {};
}

void Converter::Convert(const Options& options)
{
    if (options.m_list)
    {
        // list multiple sections
        for (int i = options.m_index; ; ++i)
        {
            try
            {
                Options sectionOptions{options};
                sectionOptions.m_index = i;
                
                std::unique_ptr<Parser> parser{GetParser(sectionOptions)};

                Piece piece;
                parser->Parse(sectionOptions, piece);
                std::cout << i << ":";
                
                if (!piece.m_title.empty())
                {
                    std::cout << " " << piece.m_title;
                }

                if (!piece.m_composer.empty())
                {
                    std::cout << " " << piece.m_composer;
                }
                
                std::cout << '\n';
            }
            catch (const std::invalid_argument&)
            {
                break;
            }
        }
    }
    else if (options.m_multi)
    {
        // convert multiple sections
        for (int i = options.m_index; ; ++i)
        {
            try
            {
                Options sectionOptions{options};
                sectionOptions.m_index = i;
                
                // add index to destination filename
                if (!sectionOptions.m_dstFilename.empty())
                {
                    const std::string indexStr{"-" + std::to_string(i)};
                    const size_t found = sectionOptions.m_dstFilename.find_last_of('.');
                    if (found != std::string::npos)
                    {
                        sectionOptions.m_dstFilename.insert(found, indexStr);
                    }
                    else
                    {
                        sectionOptions.m_dstFilename += indexStr;
                    }
                }

                std::unique_ptr<Generator> generator{GetGenerator(sectionOptions)};
                std::unique_ptr<Parser> parser{GetParser(sectionOptions)};

                Piece piece;
                parser->Parse(sectionOptions, piece);
                Transform(options, piece);
                generator->Generate(sectionOptions, piece);
            }
            catch (const std::invalid_argument&)
            {
                break;
            }
        }
    }
    else
    {
        std::unique_ptr<Generator> generator{GetGenerator(options)};
        std::unique_ptr<Parser> parser{GetParser(options)};

        Piece piece;
        parser->Parse(options, piece);
        Transform(options, piece);
        generator->Generate(options, piece);
    }
}

void Converter::Transform(const Options& options, Piece& piece)
{
    for (auto& part : piece.m_parts)
    {
        // tablature type and subtype
        if (options.m_dstTabType != TabUnknown && part.m_tabType != TabCmn)
        {
            part.m_tabType = options.m_dstTabType;
        }
    
        if (options.m_dstTabGermanSubtype != GstUnknown && part.m_tabType != TabCmn)
        {
            part.m_tabGermanSubtype = options.m_dstTabGermanSubtype;
        }
        
        // santitize number of staff lines
        if (part.m_tabType == TabGerman)
        {
            part.m_staffLines = 0;
        }
        else if (part.m_staffLines == 0)
        {
            part.m_staffLines = Course6;
        }
        
        // Parsers can produce notes in any order, but generators assume course order
        // sort notes by course
        if (part.m_tabbed && part.m_tabType != TabCmn)
        {
            for (auto& bar : part.m_bars)
            {
                for (auto& chord : bar.m_chords)
                {
                    std::sort(chord.m_notes.begin(), chord.m_notes.end(),
                        [](const Note& a, const Note& b){return a.m_course < b.m_course;});
                }
            }
        }
        
        // do we need to transcribe? I.e. transpose, re-tune or intabulate?
        // FIXME can't transcribe CMN
        if (part.m_tabType != TabCmn &&
            (!part.m_tabbed ||
            options.m_transpose != 0 ||
            options.m_intabulate ||
            (!options.m_dstTuning.empty() && options.m_dstTuning != part.m_tuning)))
        {
            Transmute(options, piece, part);
        }
        
        // rhythm scheme
        if (part.m_tabType != TabCmn && options.m_rhythmScheme > 0)
        {
            Chord previousChord;

            for (auto& bar : part.m_bars)
            {
                for (auto& chord : bar.m_chords)
                {
                    // kill grids
                    chord.m_grid = GridNone;
                    chord.m_noFlag = (&chord != &(part.m_bars.front().m_chords.front()) &&
                            previousChord.m_noteType == chord.m_noteType &&
                            !previousChord.m_dotted &&
                            !chord.m_dotted &&    
                            (options.m_rhythmScheme == 1 || &chord != &(bar.m_chords.front())));
                    previousChord = chord;
                }
            }
        }
    }
}

void Converter::Transmute(const Options& options, Piece& piece, Part& part)
{
    // Set the notes' pitches according to the source tuning
    if (!part.m_pitched)
    {
        for (auto& bar : part.m_bars)
        {
            for (auto& chord : bar.m_chords)
            {
                for (auto& note : chord.m_notes)
                {
                    // This will plant correct pitches for diatonic cittern
                    note.m_pitch = part.CalcPitch(note);
                }
            }
        }
        part.m_pitched = true;
    }
    
    // Transpose
    if (options.m_transpose != 0)
    {
        for (auto& bar : part.m_bars)
        {
            for (auto& chord : bar.m_chords)
            {
                for (auto& note : chord.m_notes)
                {
                    if (note.m_pitch.Step() != '\0')
                    {
                        note.m_pitch = note.m_pitch + options.m_transpose;
                    }
                }
            }
        }
    }
    
    // Set destination tuning
    if (!options.m_dstTuning.empty())
    {
        part.m_tuning = options.m_dstTuning;
    }
    
    // if no destination tuning, and no source tuning in part, then use source tuning from options
    if (part.m_tuning.empty())
    {
        part.m_tuning = options.m_tuning;
    }
    
    // if no tuning has been specified default to renaissance 10 course
    if (part.m_tuning.empty())
    {
        Pitch::SetTuning(10, part.m_tuning);
    }
    
    // intabulate
    LOGGER << "Intabulate";
    Intabulate(piece, part);
    
    // trim tuning to number of courses used
    const int numCourses{part.MaxCourse()};
    LOGGER << "number of destination courses used=" << numCourses;
    part.m_tuning.resize(numCourses);
    LOGGER << "Final destination tuning " << Pitch::GetTuning(part.m_tuning);
    
    // increase number of staff lines if needed
    part.m_staffLines = std::max(part.m_staffLines, std::min(numCourses, static_cast<int>(Course6)));
}

void Converter::Intabulate(Piece& piece, Part& part)
{
    // We cannot transcribe to a diatonic cittern, but we can transcribe from a diatonic cittern.
    // Mark the piece "transcribed" to stop the JHR herustics in tab from assuming that
    // the transcription is also diatonic.
    piece.m_credits.emplace_back("transcribed by luteconv");
    
    int lastLhPos{0}; // last left hand position, used to estimate hand movement for next chord

    for (auto& bar : part.m_bars)
    {
        for (auto& chord : bar.m_chords)
        {
            auto& notes{chord.m_notes};
            
            // intabulating means that any existing connectors, ensembles or left fingering
            // may no longer be in the right place.  For now remove them.
            // Ornaments can remain.  As can right hand fingering for a single note chord.
            // TODO can we re-establish connectors, ensembles or left fingering after intabulation?

            // remove anchors and ensembles, just leaving notes with pitches to intabulate
            notes.erase(std::remove_if(notes.begin(), notes.end(),
                    [](const Note& note)
                    {
                        return note.m_fret < 0;
                    }), notes.end());
            
            // remove connectors, left fingering and right fingering if more than one note
            for (auto& note : notes)
            {
                if (notes.size() > 1)
                {
                    note.m_rightFingering = FingerNone;
                }
                note.m_leftFingering = FingerNone;
                note.m_connector = Connector();
            }
            
            // TODO cannot intabulate to a diatonic instrument
            part.m_missingFrets.clear();
            
            int bestScore(std::numeric_limits<int>::max());
            int bestLHPos{0};
            std::vector<Note> candidates(notes.size());
            IntabulateNote(part, 0, lastLhPos, bestLHPos, bestScore, notes, candidates);
            lastLhPos = bestLHPos;
            
            // remove unused notes
            notes.erase(std::remove_if(notes.begin(), notes.end(),
                    [](const Note& note)
                    {
                        return note.m_course == CourseNone;
                    }), notes.end());
            
            // sort notes by course
            std::sort(notes.begin(), notes.end(),
                    [](const Note& a, const Note& b){return a.m_course < b.m_course;});
            
        }
    }
    part.m_tabbed = true;
}

void Converter::IntabulateNote(Part& part, size_t n, int lastLhPos, int& bestLHPos, int& bestScore, std::vector<Note>& notes, std::vector<Note>& candidates)
{
    if (n == notes.size())
    {
        // we have intabulated all the notes, get the score
        int lhPos{0};
        const int score{Score(candidates, lastLhPos, lhPos)};
        if (score < bestScore)
        {
            // best candidate so far
            for (size_t i{0}; i < notes.size(); ++i)
            {
                notes[i].m_course = candidates[i].m_course;
                notes[i].m_fret = candidates[i].m_fret;
            }
            bestScore = score;
            bestLHPos = lhPos;
        }
    }
    else
    {
        for (int course{1}; course <= static_cast<int>(part.m_tuning.size()); ++course)
        {
            // Can this course sound this pitch? Is this course available?
            const int courseMidi{part.m_tuning[course - 1].Midi()};
            const int noteMidi{notes[n].m_pitch.Midi()};
            
            if (noteMidi >= courseMidi && noteMidi <= courseMidi + MaxFret(course) &&
                    (std::find_if(candidates.begin(), candidates.end(),
                        [course](const Note& note)
                        {
                            return note.m_course == course;
                        }) == candidates.end()) )
            {
                candidates[n].m_fret = noteMidi - courseMidi;
                candidates[n].m_course = course;
            }
            IntabulateNote(part, n + 1, lastLhPos, bestLHPos, bestScore, notes, candidates); // fit rest of the notes
            candidates[n].m_course = CourseNone; // try elsewhere
        }
    }
}
    
namespace
{
// TODO tune the weights
enum Weight
{
    WtNumStopped = 10, // Number of stopped courses
    WtShift = 15, // shift in left hand position from last stopped chord
    WtHStretch = 20, // horizontal stretch
    WtVStretch = 15, // vertical stretch
    WtLhPos = 10, // left hand position
    WtMissed = 400, // missed notes, can't be intabulated
};
}

int Converter::Score(const std::vector<Note>& candidates, int lastLhPos, int& lhPos)
{
    int numNotes{0};
    int numStopped{0};
    int maxFret{0};
    int minFret{std::numeric_limits<int>::max()};
    int maxCourse{0};
    int minCourse{std::numeric_limits<int>::max()};
    for (const auto& note : candidates)
    {
        if (note.m_course != CourseNone)
        {
            if (note.m_fret > 0)
            {
                ++numStopped;
                maxFret = std::max(maxFret, note.m_fret);
                minFret = std::min(minFret, note.m_fret);
                maxCourse = std::max(maxCourse, note.m_course);
                minCourse = std::min(minCourse, note.m_course);
            }
            ++numNotes;
        }
    }
    
    if (minFret > maxFret)
    {
        // only open strings, lhPos is unchanged
        minFret = maxFret;
        minCourse = maxCourse;
    }
    else
    {
        lhPos = (maxFret + minFret) / 2;
    }
    
    return WtNumStopped * numStopped +
            WtShift * std::abs(lastLhPos - lhPos) +
            WtHStretch * (maxFret - minFret) +
            WtVStretch * (maxCourse - minCourse) +
            WtLhPos * lhPos +
            WtMissed * (static_cast<int>(candidates.size()) - numNotes);
}

int Converter::MaxFret(int course)
{
    const std::array<const int, 11> maxFret{0, 14, 12, 11, 10, 9, 8, 8, 8, 8, 8};
    
    if (course >= static_cast<int>(maxFret.size()))
    {
        return 0;
    }
    
    return maxFret[course];
}
        
} // namespace luteconv
