#include "piece.h"

#include <algorithm>
#include <cassert>
#include <stdexcept>

#include "logger.h"

namespace luteconv
{

// class Piece

Piece::Piece()
: m_parts(1)
{
    
}

std::vector<Accidental> Piece::KeySignature(Key key)
{
    switch (key)
    {
        //             A         B         C         D         E         F         G
        case KeyCb:
            return {AccFlat,  AccFlat,  AccFlat,  AccFlat,  AccFlat,  AccFlat,  AccFlat};
        case KeyGb:
            return {AccFlat,  AccFlat,  AccFlat,  AccFlat,  AccFlat,  AccNone,  AccFlat};
        case KeyDb:
            return {AccFlat,  AccFlat,  AccNone,  AccFlat,  AccFlat,  AccNone,  AccFlat};
        case KeyAb:
            return {AccFlat,  AccFlat,  AccNone,  AccFlat,  AccFlat,  AccNone,  AccNone};
        case KeyEb:
            return {AccFlat,  AccFlat,  AccNone,  AccNone,  AccFlat,  AccNone,  AccNone};
        case KeyBb:
            return {AccNone,  AccFlat,  AccNone,  AccNone,  AccFlat,  AccNone,  AccNone};
        case KeyF:
            return {AccNone,  AccFlat,  AccNone,  AccNone,  AccNone,  AccNone,  AccNone};
        case KeyNone:
            [[fallthrough]];
        case KeyC:
            return {AccNone,  AccNone,  AccNone,  AccNone,  AccNone,  AccNone,  AccNone};
        case KeyG:
            return {AccNone,  AccNone,  AccNone,  AccNone,  AccNone,  AccSharp, AccNone};
        case KeyD:
            return {AccNone,  AccNone,  AccSharp, AccNone,  AccNone,  AccSharp, AccNone};
        case KeyA:
            return {AccNone,  AccNone,  AccSharp, AccNone,  AccNone,  AccSharp, AccSharp};
        case KeyE:
            return {AccNone,  AccNone,  AccSharp, AccSharp, AccNone,  AccSharp, AccSharp};
        case KeyB:
            return {AccSharp, AccNone,  AccSharp, AccSharp, AccNone,  AccSharp, AccSharp};
        case KeyFs:
            return {AccSharp, AccNone,  AccSharp, AccSharp, AccSharp, AccSharp, AccSharp};
        case KeyCs:
            return {AccSharp, AccSharp, AccSharp, AccSharp, AccSharp, AccSharp, AccSharp};
    }
    
    assert(false);
    return {};
}

// class Part

int Part::MaxCourse() const
{
    int result{0};
    
    // maximum course used
    for (const auto & bar : m_bars)
    {
        for (const auto & chord : bar.m_chords)
        {
            for (const auto & note : chord.m_notes)
            {
                if (note.m_fret >= 0) // beware of connector anchors and ensembles
                {
                    result = std::max(result, note.m_course);
                }
            }
        }
    }
    return result;
}

int Part::MaxChord() const
{
    int result{0};
    
    // maximum number of notes used in any chord
    for (const auto & bar : m_bars)
    {
        for (const auto & chord : bar.m_chords)
        {
            int numNotes{0};
            for (const auto & note : chord.m_notes)
            {
                if (note.m_fret >= 0) // beware of connector anchors and ensembles
                {
                    ++numNotes;
                }
            }
            result = std::max(result, numNotes);
        }
    }
    return result;
}

int Part::MaxFret() const
{
    int result{0};
    
    // maximum fret used
    for (const auto & bar : m_bars)
    {
        for (const auto & chord : bar.m_chords)
        {
            for (const auto & note : chord.m_notes)
            {
                if (note.m_fret >= 0) // beware of connector anchors and ensembles
                {
                    result = std::max(result, note.m_fret);
                }
            }
        }
    }
    return result;
}

void Part::SetTuning(const Options& options)
{
    m_tabbed = true;
    
    const int numCourses{MaxCourse()};
    LOGGER << "number of source courses used=" << numCourses;
    
    // tuning in the command line takes precedence
    // then tuning from source file
    // then default tuning based on number of numCourses
    
    // where the tuning is specified
    const std::string tuningSpec = (!options.m_tuning.empty() || !options.m_7tuning.empty())
            ? "cmd"
            : (!m_tuning.empty())
            ? "source"
            : "default";
    
    std::vector<Pitch> defaultTuning;
    Pitch::SetTuning(numCourses, defaultTuning);
    
    m_tuning.resize(numCourses);
    
    for (int i = 0; i < numCourses; ++i)
    {
        if (i < static_cast<int>(options.m_tuning.size()))
        {
            m_tuning[i] = options.m_tuning[i];
        }
        else if (m_tuning[i].Step() == '\0')
        {
            m_tuning[i] = defaultTuning[i];
        }
    }
    
    // optionally modify tuning of 7th ... course
    for (int i = Course6; i < numCourses; ++i)    
    {
        if (i < static_cast<int>(Course6 + options.m_7tuning.size()))
        {
            m_tuning[i] = options.m_7tuning[i - Course6];
        }
    }
    
    LOGGER << "Final source tuning (" << tuningSpec << ") " << Pitch::GetTuning(m_tuning);
    
    // source tablature type
    
    if (m_tabType == TabGerman)
    {
        LOGGER << "Source TabType=" << Options::GetTabType(m_tabType) << " Subtype=" << Options::GetTabGermanSubtype(m_tabGermanSubtype);
    }
    else
    {
        LOGGER << "Source TabType=" << Options::GetTabType(m_tabType);
    }
}

Pitch Part::CalcPitch(const Note& note) const
{
    if (note.m_fret < 0 || note.m_course == CourseNone)
    {
        return {};
    }

    int semi{note.m_fret};
    for (const auto missing : m_missingFrets)
    {
        // diatonic cittern has some frets missing
        if (note.m_fret >= missing)
        {
            ++semi;
        }
    }
    return m_tuning[note.m_course - 1] + semi;
}

// class Bar

void Bar::Clear()
{
    m_chords.clear();
    m_clef = ClefNone;
    m_key = KeyNone;
    m_timeSig = TimeSig();
    m_barStyle = BarStyleRegular;
    m_repeat = RepNone;
    m_volta = 0;
    m_fermata = false;
    m_eol = false;
}

void Bar::MergeVoices(int divisions)
{
    // sort the chords by timestamp
    std::sort(m_chords.begin(), m_chords.end(),
            [](const Chord& a, const Chord& b){return a.m_stamp < b.m_stamp;});
    
    // merge adjacent chords with the same timestamp into one chord
    for (size_t i = 0; i < m_chords.size() - 1;)
    {
        if (m_chords[i].m_stamp == m_chords[i + 1].m_stamp)
        {
            m_chords[i].m_notes.insert(m_chords[i].m_notes.end(), m_chords[i + 1].m_notes.begin(), m_chords[i + 1].m_notes.end());

            // sort notes by string
            std::sort(m_chords[i].m_notes.begin(), m_chords[i].m_notes.end(),
                    [](const Note& a, const Note& b){return a.m_course < b.m_course;});
            
            // fermata if either has one
            m_chords[i].m_fermata = m_chords[i].m_fermata || m_chords[i + 1].m_fermata;
            
            // triplet if either is one
            m_chords[i].m_triplet = m_chords[i].m_triplet || m_chords[i + 1].m_triplet;
            
            // tie if either is one
            m_chords[i].m_tie = m_chords[i].m_tie || m_chords[i + 1].m_tie;
            
            // merging grids
            if (m_chords[i].m_grid == m_chords[i + 1].m_grid || m_chords[i + 1].m_grid == GridNone)
            {
                // [i + 1] has no grid or is the same as [i]'s. leave merged chord's grid as is
            }
            else if (m_chords[i].m_grid == GridNone)
            {
                // [i] has no grid, use [i + 1]'s grid
                m_chords[i].m_grid = m_chords[i + 1].m_grid;
            }
            else
            {
                // some combination of start, mid and end - all become mid
                m_chords[i].m_grid = GridMid;
            }

            // merged so erase [i + 1]th chord
            m_chords.erase(m_chords.begin() + static_cast<long int>(i) + 1);
        }
        else
        {
            i++;
        }
    }
    
    // fixup the note types to maintain the timestamps
    // final note must already be correct as its duration is to the end of the bar
    FixNoteTypes(divisions, 0);
}

void Bar::FixNoteTypes(int divisions, int barEnd)
{
    // fixup the note types to maintain the timestamps
    
    int tripletCount{0};
    
    for (size_t i = 0; i < m_chords.size(); ++i)
    {
        if (m_chords[i].m_triplet)
        {
            tripletCount = TripletNotes(m_chords[i]);
        }
        
        int eventDur{0};
        if (i == m_chords.size() - 1)
        {
            // final chord
            if (barEnd == 0)
            {
                // if no bar end stamp is given the final note must already be correct
                // as its duration is to the end of the bar
                eventDur = Duration(m_chords[i].m_noteType, m_chords[i].m_dotted, tripletCount > 0, divisions);
            }
            else
            {
                eventDur = barEnd - m_chords[i].m_stamp;
            }
        }
        else
        {
            eventDur = m_chords[i + 1].m_stamp - m_chords[i].m_stamp;
        }
        
        int duration{Duration(NoteTypeLong, false, false, divisions)};
        
        // if we are in a triplet then need to select the note on the basis of its untripletted duration
        if (tripletCount > 0)
        {
            eventDur = eventDur * 3 / 2;
            --tripletCount;
        }
        
        for (int t{NoteTypeLong}; t <= NoteType256th; ++t)
        {
            if (eventDur >= duration)
            {
                m_chords[i].m_noteType = static_cast<NoteType>(t);
                m_chords[i].m_dotted = (eventDur > duration);
                
                // suppress flag if same as the previous flag
                m_chords[i].m_noFlag = (i > 0 &&
                        !m_chords[i].m_notes.empty() &&
                        !m_chords[i].m_dotted &&
                        !m_chords[i - 1].m_dotted &&
                        m_chords[i].m_noteType == m_chords[i - 1].m_noteType);
                break;
            }
            
            duration /= 2;
        }
    }
}

int Bar::Duration(NoteType noteType, bool dotted, bool triplet, int divisions)
{
    int result{(NoteFrac(noteType) * Rational(divisions * 4)).Trunc()};

    result = dotted ? 3 * result / 2 : result;
    result = triplet ? 2 * result / 3 : result;
    return result;
}

Rational Bar::NoteFrac(NoteType noteType)
{
    int num{1};
    int den{1};
    if (noteType > NoteTypeWhole)
    {
        den <<= noteType - NoteTypeWhole;
    }
    else if (noteType < NoteTypeWhole)
    {
        num <<= NoteTypeWhole - noteType;
    }
    
    return Rational(num, den);
}

int Bar::TripletNotes(const Chord& chord) const
{
    size_t index{};
    return TripletNotes(chord, index);
}

int Bar::TripletNotes(const Chord& chord, size_t& index) const
{
    if (!chord.m_triplet)
    {
        return 0;
    }
    
    // a triplet can contain 2 or 3 notes, if 2 then the first 2 notes will have different note types
    // e.g. 2 crotchet triplet can contain 3 crotchets or 1 minim and 1 crotchet
    index = static_cast<size_t>(std::distance(&m_chords.front(), &chord));
    
    if (index + 1 >= m_chords.size())
    {
        return 0;
    }
    
    if (chord.m_noteType != m_chords.at(index + 1).m_noteType || index + 2 >= m_chords.size())
    {
        return 2;
    }
    
    return 3;
}

// class Lyric

Lyric::Lyric(std::string_view text, Syllabic syllabic)
: m_text{text}, m_syllabic{syllabic}
{
    
}


} // namespace luteconv
