#ifndef _PARSERFRT_H_
#define _PARSERFRT_H_

#include <iostream>
#include <string_view>

#include "parser.h"

namespace luteconv
{

/**
 * Parse Humdrum .frt file
 * 
 * https://www.humdrum.org
 */
class ParserFrt: public Parser
{
public:

    /**
     * Constructor
    */
    ParserFrt() = default;

    /**
     * Destructor
     */
    ~ParserFrt() override = default;
    
    /**
     * Parse Humdrum .frt file
     *
     * @param[in] src .frt stream
     * @param[in] options
     * @param[out] piece destination
     */
    void Parse(std::istream& srcFile, const Options& options, Piece& piece) override;
    
private:
    static std::string ParseReferenceRecord(std::string_view line);
    static void Split(std::string_view s, const char c, std::vector<std::string_view>& splits);
    static void ParseBarLine(std::string_view fretTok, Bar& bar, bool& barIsClear, Part& part);
    static void ParseRhythm(std::string_view line, int lineNo, std::string_view recipTok, Chord& chord, int& tripletCount, NoteType& tripletNoteType);
    static void ParseChord(std::string_view line, int lineNo, std::string_view fretTok, Chord& chord, Part& part);
    static void ParseTimeSignature(std::string_view fretTok, TimeSig& timeSig);
    static void ParseRelativeTuning(std::string_view line, int lineNo, std::string_view fretTok, int lowestMidi, Part& part);
    static void ParseFretTuning(std::string_view line, int lineNo, std::string_view fretTok, Part& part);
};

} // namespace luteconv

#endif // _PARSERFRT_H_
