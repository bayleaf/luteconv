#ifndef _MNX_H_
#define _MNX_H_

namespace luteconv
{

class Mnx
{
public:
    
    /**
     * Strings for enum NoteType
     */
    static const char* const noteType[];

    /**
     * Strings for enum BarStyle
     */
    static const char* const barStyle[];
    
};

} // namespace luteconv

#endif // _MNX_H_
