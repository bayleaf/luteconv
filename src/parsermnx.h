#ifndef _PARSERMNX_H_
#define _PARSERMNX_H_

#include <set>
#include <map>
#include <utility>

#include <pugixml.hpp>

#include "parser.h"

namespace luteconv
{

/**
 * Parse .mnx file
 */
class ParserMnx: public Parser
{
public:

    /**
     * Constructor
    */
    ParserMnx() = default;

    /**
     * Destructor
     */
    ~ParserMnx() override = default;
    
    /**
     * Parse .mnx file
     *
     * @param[in] src stream
     * @param[in] options
     * @param[out] piece destination
     */
    void Parse(std::istream& srcFile, const Options& options, Piece& piece) override;
    
private:
    static void ParseGlobal(pugi::xml_node xmlglobal, Part& part);
    void ParsePart(pugi::xml_node xmlpart, Part& part);
    void ParseEvent(pugi::xml_node xmlevent, Part& part, Bar& bar, int measureNo, int& stamp,
            const std::map<std::string, Grid>& eventIdToGrid, int tripletCount);
    static void ParseForward(pugi::xml_node xmlforward, int measureNo, int& stamp, bool& heardVoices);
    void ParseTuplet(pugi::xml_node xmltuple, Part& part, Bar& bar, int measureNo, int& stamp,
            const std::map<std::string, Grid>& eventIdToGrid, bool& heardVoices, int tripletCount);
    static bool ParseNoteValueQuantity(const std::string& noteValueQuantity, int& multiplier, NoteType& noteType, bool& dotted);
    static bool ParseNoteValue(const std::string& noteValueQuantity, NoteType& noteType, bool& dotted);
    
    std::map<std::string, std::pair<std::string, Connector>> m_startConnector;
    std::set<int> m_slurNumber;
};




} // namespace luteconv

#endif // _PARSERMNX_H_
