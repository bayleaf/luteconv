#include "parsertabcode.h"

#include <iostream>
#include <fstream>
#include <algorithm>
#include <cctype>
#include <chrono>
#include <iomanip>

#include "logger.h"
#include "platform.h"

#include <pugixml.hpp>

namespace luteconv
{

void ParserTabCode::Parse(std::istream& src, const Options& options, Piece& piece)
{
    LOGGER << "Parse tc";
    
    Part& part{piece.m_parts.at(0)}; // only 1 part
    
    if (options.m_index != 0)
    {
        throw std::invalid_argument("Error: Can't find section, index=" + std::to_string(options.m_index));
    }

    // Some metadata can appear in the commented rules in xml.
    // Use the filename for the title by default, this will be overwritten if a title exists in the rules.
    const size_t slash{options.m_srcFilename.find_last_of(pathSeparator)};
    if (slash == std::string::npos)
    {
        piece.m_title = options.m_srcFilename;
    }
    else
    {
        piece.m_title = options.m_srcFilename.substr(slash + 1);
    }

    int lineNo{0};
    Bar bar;
    bool barIsClear{true};
    bool inComment{false};
    bool inRules{false};
    std::string rules;
    
    for (;;)
    {
        std::string lineOwner;
        getline(src, lineOwner);
        
        if (src.eof() && lineOwner.empty())
        {
            break;
        }
        
        // remove any trailing \r
        if (!lineOwner.empty() && lineOwner.back() == '\r')
        {
            lineOwner.pop_back();
        }
        
        ++lineNo;
        const std::string_view line{lineOwner}; // string_view is much faster at substringing
        
        // Tokenize: a token is delimited by whitespace or endl or a comment.
        // Comments are tokenized as they can contain semantic information.
        // (IMHO comments should only contain comments!)
        size_t tokEnd{0};
        for (size_t tokBegin{0}; tokBegin < line.size(); tokBegin = tokEnd)
        {
            // find start of token
            while (tokBegin < line.size() && (line[tokBegin] == ' ' || line[tokBegin] == '\t'))
            {
                ++tokBegin;
            }
            
            if (tokBegin >= line.size())
            {
                break;
            }

            // find end of token
            tokEnd = tokBegin + 1;
            while (tokEnd < line.size() && line[tokEnd] != ' ' && line[tokEnd] != '\t' && line[tokEnd] != '{' && line[tokEnd] != '}')
            {
                ++tokEnd;
            }
            
            if (tokEnd < line.size() && line[tokEnd] == '}')
            {
                ++tokEnd; // keep end of comment in token
            }
            
            const std::string token{line.substr(tokBegin, tokEnd - tokBegin)};
            
            // deal with comments, end of system and rules.
            
            if (token[0] == '{')
            {
                // end of system is encoded in a comment {^}!
                if (token == "{^}" && barIsClear && !part.m_bars.empty())
                {
                    Bar& prev{part.m_bars.back()};
                    prev.m_eol = true;
                    continue;
                }
                
                inComment = true;
            }

            if (inComment)
            {
                size_t rulesBegin{0};
                if (!inRules && rules.empty())
                {
                    rulesBegin = token.find("<rules>");
                    inRules = (rulesBegin != std::string::npos);
                }
                
                if (inRules)
                {
                    rules += " " + token.substr(rulesBegin);
                }

                if (token.back() == '}')
                {
                    inComment = false;
                    
                    if (inRules)
                    {
                        inRules = false;
                        rules.pop_back(); // remove }
                        ParseRules(rules, piece, part);
                    }
                }
                continue;
            }
            
            ParseCodeWord(token, lineNo, bar, barIsClear, part);
        }
    }
    
    // deal with missing final bar line
    ParseBarLine("|", lineNo, bar, barIsClear, part);

    part.SetTuning(options);
}

void ParserTabCode::ParseCodeWord(std::string_view tabword, int lineNo, Bar& bar, bool& barIsClear, Part& part)
{
    switch (tabword[0])
    {
    case '|':   // barline
        [[fallthrough]];
    case ':':   // barline
        ParseBarLine(tabword, lineNo, bar, barIsClear, part);
        m_lhsNumBeams = 0; // shouldn't be neceassary, but stop [] mismatch affecting later bars
        break;
    case 'M':
        ParseTimeSignature(tabword, lineNo, bar);
        break;
    case '(':
        if (tabword.size() >= 7 && tabword.substr(0, 5) == "(T+:\\" && tabword[5] >= '1' && tabword[5] <= '9')
        {
            bar.m_volta = tabword[5] - '0';
        }
        else
        {
            LOGGER << lineNo << ": error \"" << tabword << "\" unknown tabword";
        }
        break;
    default:
        ParseChord(tabword, lineNo, bar, barIsClear);
        break;
    }
}

void ParserTabCode::ParseBarLine(std::string_view tabword, int lineNo, Bar& bar, bool& barIsClear, Part& part)
{
    if (tabword == "|")
    {
        bar.m_barStyle = BarStyleRegular;
    }
    else if (tabword == "||")
    {
        bar.m_barStyle = BarStyleLightLight;
    }
    else if (tabword == "|:")
    {
        bar.m_barStyle = BarStyleRegular;
        bar.m_repeat = RepForward;
    }
    else if (tabword == "||:")
    {
        bar.m_barStyle = BarStyleLightLight;
        bar.m_repeat = RepForward;
    }
    else if (tabword == ":|")
    {
        bar.m_barStyle = BarStyleRegular;
        bar.m_repeat = RepBackward;

    }
    else if (tabword == ":||")
    {
        bar.m_barStyle = BarStyleLightLight;
        bar.m_repeat = RepBackward;
    }
    else if (tabword == ":|:")
    {
        bar.m_barStyle = BarStyleRegular;
        bar.m_repeat = RepJanus;
    }
    else if (tabword == ":||:")
    {
        bar.m_barStyle = BarStyleLightLight;
        bar.m_repeat = RepJanus;
    }
    else if (tabword == "|=")
    {
        bar.m_barStyle = BarStyleRegular;
    }
    else if (tabword == "|0")
    {
        bar.m_barStyle = BarStyleRegular;
    }
    else if (tabword == "|=0")
    {
        bar.m_barStyle = BarStyleRegular;
    }
    else
    {
        bar.m_barStyle = BarStyleRegular;
        LOGGER << lineNo << ": error \"" << tabword << "\" unknown barline";
    }
   
    if (barIsClear && !part.m_bars.empty())
    {
        // two adjacent bar lines, combine
        Bar& prev{part.m_bars.back()};
        if (prev.m_repeat == RepBackward && bar.m_repeat == RepForward)
        {
            prev.m_repeat = RepJanus;
        }
        
        if (!prev.m_eol)
        {
            if (prev.m_barStyle == BarStyleRegular && bar.m_barStyle == BarStyleRegular)
            {
                prev.m_barStyle = BarStyleLightLight;
            }
            else if (prev.m_barStyle == BarStyleRegular && bar.m_barStyle == BarStyleHeavy)
            {
                prev.m_barStyle = BarStyleLightHeavy;
            }
            else if (prev.m_barStyle == BarStyleHeavy && bar.m_barStyle == BarStyleRegular)
            {
                prev.m_barStyle = BarStyleHeavyLight;
            }
            else if (prev.m_barStyle == BarStyleHeavy && bar.m_barStyle == BarStyleHeavy)
            {
                prev.m_barStyle = BarStyleHeavyHeavy;
            }
        }
    }
    else if (!barIsClear)
    {
        part.m_bars.push_back(bar);
    }
    
    bar.Clear();
    barIsClear = true;
}

void ParserTabCode::ParseChord(std::string_view tabword, int lineNo, Bar& bar, bool& barIsClear)
{
    bar.m_chords.emplace_back(); // new chord
    Chord & chord{bar.m_chords.back()};
    
    // flags
    size_t idx{0};
    const std::string_view flags{"BWHQESTYZ"};
    const size_t flagPos{flags.find(tabword[idx])};
    if (flagPos != std::string_view::npos)
    {
        // TabCode documentation has Q = 1 flag, therefore B = NoteTypeWhole, not NoteTypeBreve
        chord.m_noteType = static_cast<NoteType>(NoteTypeBreve + 1 + flagPos);
        ++idx;
        
        if (idx < tabword.size() && tabword[idx] == '.')
        {
            chord.m_dotted = true;
            ++idx;
        }
        m_previousChord = chord; // only need to save the flags data, not the notes.
    }
    else if (idx < tabword.size() && tabword[idx] == 'F')
    {
        chord.m_fermata = true;
        chord.m_noteType = NoteTypeHalf;
        ++idx;
    }
    else if (idx < tabword.size() && (tabword[idx] == '[' || tabword[idx] == ']'))
    {
        int upBeams{0};
        int downBeams{0};
        for (;;)
        {
            if (idx < tabword.size() && tabword[idx] == '[')
            {
                ++upBeams;
                ++idx;
            }
            else if (idx < tabword.size() && tabword[idx] == ']')
            {
                ++downBeams;
                ++idx;
            }
            else
            {
                break;
            }
        }
        
        if (upBeams > 0 && m_lhsNumBeams == 0)
        {
            chord.m_grid = GridStart;
        }

        m_lhsNumBeams += upBeams;
        chord.m_noteType = static_cast<NoteType>(NoteTypeQuarter + m_lhsNumBeams);
        
        // having both up and down beams means a hook []
        // down beams ']' affect the rhythm of the next chord, not this one
        m_lhsNumBeams -= downBeams;

        if (m_lhsNumBeams < 0)
        {
            LOGGER << "error mismatched start and end beam";
            chord.m_grid = GridEnd;
            m_lhsNumBeams = 0;
        }
        else if (m_lhsNumBeams == 0)
        {
            chord.m_grid = GridEnd;
        }
        else if (chord.m_grid != GridStart)
        {
            chord.m_grid = GridMid;
        }
        
        if (idx < tabword.size() && tabword[idx] == '.')
        {
            chord.m_dotted = true;
            ++idx;
        }
        
        // note type for the next chord if it doesn't specify its own flags
        m_previousChord = chord;
        m_previousChord.m_noteType = static_cast<NoteType>(NoteTypeQuarter + m_lhsNumBeams);
    }
    else
    {
        chord.m_noteType = m_previousChord.m_noteType;
        chord.m_dotted = m_previousChord.m_dotted;
        chord.m_noFlag = (m_previousChord.m_grid == GridNone);
        if (m_previousChord.m_grid == GridStart || m_previousChord.m_grid == GridMid)
        {
            chord.m_grid = GridMid;
        }
    }
    
    if (idx < tabword.size() && tabword[idx] == '3')
    {
        chord.m_triplet = true;
        ++idx;
    }
    
    // notes
    
    barIsClear = idx >= tabword.size();

    while (idx < tabword.size())
    {
        if (tabword[idx] == '-' || (tabword[idx] >= 'a' && tabword[idx] <= 'p'))
        {
            chord.m_notes.emplace_back(); // allocate new note
            Note& note{chord.m_notes.back()};

            note.m_fret = (tabword[idx] == '-')
                    ? Note::Anchor // connector anchor
                    : tabword[idx] - 'a' - (tabword[idx] <= 'i' ? 0 : 1); // fret i = j
            ++idx;
            
            if (idx < tabword.size() && tabword[idx] >= '0' && tabword[idx] <= '6')
            {
                note.m_course = tabword[idx] - '0';
                ++idx;
            }
            else
            {
                LOGGER << lineNo << ": error \"" << tabword << "\" missing course number";
            }
        }
        else if (tabword[idx] == 'X')
        {
            chord.m_notes.emplace_back(); // allocate new note
            Note& note{chord.m_notes.back()};

            // diapasons
            ++idx;
            
            // french, course 11 ... X4 ...
            if (idx < tabword.size() && static_cast<bool>(isdigit(tabword[idx])))
            {
                try
                {
                    note.m_fret = 0;
                    
                    size_t len{0};
                    note.m_course = std::stoi(std::string(tabword.substr(idx)), &len) + Course7;
                    idx += len;
                }
                catch (...)
                {
                    LOGGER << lineNo << ": error bad diapason";
                    ++idx;
                }
            }
            else
            {
                // fret letters
                if (idx < tabword.size() && tabword[idx] >= 'a' && tabword[idx] <= 'p')
                {
                    // fret
                    note.m_fret = tabword[idx] - 'a' - (tabword[idx] <= 'i' ? 0 : 1); // fret i = j
                     ++idx;
                }
                else
                {
                    LOGGER << lineNo << ": error \"" << tabword << "\" missing fret letter";
                }

                // /diapason
                int diapason{0};
                if (idx < tabword.size() && tabword[idx] == '/')
                {
                    // diapason
                    do
                    {
                        ++diapason;
                        ++idx;
                    }
                    while (idx < tabword.size() && tabword[idx] == '/');
                }
    
                note.m_course = Course7 + diapason;
            }
        }
        else if (tabword[idx] == '(')
        {
            // fingering, ornament or line
            std::string extra;
            extra = '(';
            ++idx;
            while (idx < tabword.size() && tabword[idx] != ')')
            {
                extra += tabword[idx];
                ++idx;
            }
            extra += ')';
            
            if (idx < tabword.size())
            {
                ++idx; // eat )
            }
            
            if (!chord.m_notes.empty())
            {
                if (extra == "(E)")
                {
                    // Ensemble line
                    const int lastString{chord.m_notes.back().m_course};
                    chord.m_notes.emplace_back(); // allocate new note
                    Note& note{chord.m_notes.back()};
                    note.m_fret = Note::Ensemble;
                    note.m_course = lastString + 1; // TabCode does not record position of ensemble line
                }
                else
                {
                    ParseExtra(tabword, lineNo, extra, chord.m_notes.back());
                }
            }
        }
        else if (tabword[idx] == '#')
        {
            if (!chord.m_notes.empty())
            {
                chord.m_notes.back().m_rightOrnament = OrnHash;
            }
            ++idx;
        }
        else if (tabword[idx] == 'x')
        {
            if (!chord.m_notes.empty())
            {
                chord.m_notes.back().m_rightOrnament = OrnCross;
            }
            ++idx;
        }
        else if (tabword[idx] == '.')
        {
            if (!chord.m_notes.empty())
            {
                chord.m_notes.back().m_rightFingering = FingerFirst;
            }
            ++idx;
        }
        else if (tabword[idx] == ':')
        {
            if (!chord.m_notes.empty())
            {
                chord.m_notes.back().m_rightFingering = FingerSecond;
            }
            ++idx;
        }
        else if (tabword[idx] == '!')
        {
            if (!chord.m_notes.empty())
            {
                chord.m_notes.back().m_rightFingering = FingerThumb;
            }
            ++idx;
        }
        else
        {
            LOGGER << lineNo << ": error \"" << tabword << "\" unknown tabword";
            ++idx;
        }
    }
}

void ParserTabCode::ParseExtra(std::string_view tabword, int lineNo, std::string_view extra, Note& note)
{
    // left hand fingering, at any position
    if (extra.substr(0, 5) == "(Fl1:")
    {
        note.m_leftFingering = FingerFirst;
    }
    else if (extra.substr(0, 5) == "(Fl2:")
    {
        note.m_leftFingering = FingerSecond;
    }
    else if (extra.substr(0, 5) == "(Fl3:")
    {
        note.m_leftFingering = FingerThird;
    }
    else if (extra.substr(0, 5) == "(Fl4:")
    {
        note.m_leftFingering = FingerForth;
    }
    // right hand fingering, at any position
    else if (extra.substr(0, 5) == "(Fr.:")
    {
        note.m_rightFingering = FingerFirst;
    }
    else if (extra.substr(0, 6) == "(Fr..:")
    {
        note.m_rightFingering = FingerSecond;
    }
    else if (extra.substr(0, 7) == "(Fr...:")
    {
        note.m_rightFingering = FingerThird;
    }
    else if (extra.substr(0, 5) == "(Fr!:")
    {
        note.m_rightFingering = FingerThumb;
    }
    // # ornament.  Put positions 358 on the right, 12467 on the left
    // 123
    // 4 5
    // 678
    else if ((extra.substr(0, 4) == "(Oe:"))
    {
        if (extra[4] == '3' || extra[4] == '5' || extra[4] == '8')
        {
            note.m_rightOrnament = OrnHash;
        }
        else
        {
            note.m_leftOrnament = OrnHash;
        }
    }
    // x ornament.  Put positions 358 on the right, 12467 on the left
    // 123
    // 4 5
    // 678
    else if ((extra.substr(0, 4) == "(Of:"))
    {
        if (extra[4] == '3' || extra[4] == '5' || extra[4] == '8')
        {
            note.m_rightOrnament = OrnCross;
        }
        else
        {
            note.m_leftOrnament = OrnCross;
        }
    }
    // , ornament.  Put positions 358 on the right, 12467 on the left
    // 123
    // 4 5
    // 678
    else if ((extra.substr(0, 5) == "(Oa1:"))
    {
        switch (extra[5])
        {
            case '1':
                [[fallthrough]];
            case '2':
                [[fallthrough]];
            case '4':
                note.m_leftOrnament = OrnCommaRaised;
                break;
            case '6':
                [[fallthrough]];
            case '7':
                note.m_leftOrnament = OrnComma;
                break;
            case '3':
                [[fallthrough]];
            case '5':
                note.m_rightOrnament = OrnCommaRaised;
                break;
            case '8':
                note.m_rightOrnament = OrnComma;
                break;
            default:
                break;
        }
    }
    // smiley
    // 123
    // 4 5
    // 678
    else if ((extra.substr(0, 5) == "(Oc1:"))
    {
        switch (extra[5])
        {
            case '7':
                note.m_leftOrnament = OrnSmiley;
                break;
            case '8':
                note.m_rightOrnament = OrnSmiley;
                break;
            default:
                break;
        }
    }
    else if ((extra.substr(0, 2) == "(C"))
    {
        ParseConnector(tabword, lineNo, extra, note);
    }
    else
    {
        LOGGER << lineNo << ": \"" << tabword << " extra \"" << extra << "\" ignored";
    }
}

void ParserTabCode::ParseConnector(std::string_view tabword, int lineNo, std::string_view extra, Note& note)
{
    Connector connector;
    
    size_t idx{2}; // (C
    if (extra[idx] == '-')
    {
        connector.m_endPoint = EndPointStop;
        ++idx;
    }
    else
    {
        connector.m_endPoint = EndPointStart;
    }
    
    // line id
    int lineId{0};
    while (extra[idx] >= '0' && extra[idx] <= '9')
    {
        lineId = lineId * 10 + (extra[idx] - '0');
        ++idx;
    }

    if (extra[idx] == ':')
    {
        ++idx;
    }
    else
    {
        LOGGER << lineNo << ": \"" << tabword << " connector no ':' " << extra << "\" ignored";
        return;
    }
    
    if (connector.m_endPoint == EndPointStart)
    {
        if (extra.size() - idx > 2)
        {
            connector.m_function = FunSlur;
            connector.m_number = m_slurNumber++;
            
            // curve for slur
            if (extra[idx] == '-')
            {
                connector.m_curve = CurveOver;
                ++idx;
            }
            else
            {
                connector.m_curve = CurveUnder;
            }
            
            if (extra[idx] == '6')
            {
                ++idx;
            }
            else
            {
                LOGGER << lineNo << ": \"" << tabword << " connector no '6' \"" << extra << "\" ignored";
                return;
            }
        }
        else
        {
            connector.m_function = FunHold;
            connector.m_number = m_holdNumber++;
        }
    }
    else
    {
        auto it{m_startConnector.find(lineId)};
        if (it != m_startConnector.end())
        {
            connector.m_function = (it->second).m_function;
            connector.m_curve = (it->second).m_curve;
            m_startConnector.erase(it);
        }
        else
        {
            LOGGER << lineNo << ": \"" << tabword << " can't find start " << extra << "\" ignored";
            return;
        }

        if (connector.m_function == FunSlur)
        {
            --m_slurNumber;
        }
        else
        {
            --m_holdNumber;
        }
    }
        
    // position
    if (extra[idx] >= '1' && extra[idx] <= '8')
    {
        connector.m_compass = Pos(extra[idx] - '0');
        ++idx;
    }
    else
    {
        LOGGER << lineNo << ": \"" << tabword << " connector no position \"" << extra << "\" ignored";
        return;
    }
    
    if (extra[idx] != ')')
    {
        LOGGER << lineNo << ": \"" << tabword << " connector no ')' " << extra << "\" ignored";
        return;
    }

    if (connector.m_endPoint == EndPointStart)
    {
        m_startConnector[lineId] = connector;
    }
    
    note.m_connector = connector;
}

Compass ParserTabCode::Pos(int pos)
{
    // 123
    // 4 5
    // 678
    switch (pos)
    {
        case 1:
            return NorthWest;
        case 2:
            return North;
        case 3:
            return NorthEast;
        case 4:
            return West;
        case 5:
            return East;
        case 6:
            return SouthEast;
        case 7:
            return South;
        case 8:
            return SouthWest;
        default:
            break;
    }
    
    return South;
}

void ParserTabCode::ParseTimeSignature(std::string_view tabword, int lineNo, Bar& bar)
{
    if (tabword == "M(C)")
    {
        bar.m_timeSig.m_timeSymbol = TimeSyCommon;
        bar.m_timeSig.m_beats = 4;
        bar.m_timeSig.m_beatType = 4;
    }
    else if (tabword == "M(C/)")
    {
        bar.m_timeSig.m_timeSymbol = TimeSyCut;
        bar.m_timeSig.m_beats = 2;
        bar.m_timeSig.m_beatType = 2;
    }
    else if (tabword.size() == 4 && tabword[2] >= '2' && tabword[2] <= '9')
    {
        // M(3)
        bar.m_timeSig.m_timeSymbol = TimeSySingleNumber;
        bar.m_timeSig.m_beats = tabword[2] - '0';
        bar.m_timeSig.m_beatType = 4;
    }
    else if (tabword.size() == 6 && tabword[2] >= '1' && tabword[2] <= '9' && (tabword[3] == '/' || tabword[3] == ':') && tabword[4] >= '1' && tabword[4] <= '9')
    {
        // M(3/4)
        bar.m_timeSig.m_timeSymbol = TimeSyNormal;
        bar.m_timeSig.m_beats = tabword[2] - '0';
        bar.m_timeSig.m_beatType = tabword[4] - '0';
    }
    else
    {
        LOGGER << lineNo << ": \"" << tabword << "\" unknown time signature";
    }
}

void ParserTabCode::ParseRules(std::string_view rules, Piece& piece, Part& part)
{
    using namespace pugi;
    
    xml_document doc;
    const xml_parse_result result{doc.load_buffer(rules.data(), rules.size())};
    if (!result)
    {
        LOGGER << "Rules parsed with errors."
               << " Description: " << result.description() 
               << " Offset: " << result.offset;
        return;
    }
    
    const xml_node xmlrules{doc.child("rules")};
    if (!xmlrules)
    {
        LOGGER << "Can't find <rules>";
        return;
    }
    
    // staff lines
    part.m_staffLines = xmlrules.child("staff_lines").text().as_int(Course6);
    
    // title
    const std::string title{xmlrules.child("title").text().as_string()};
    if (!title.empty())
    {
        piece.m_title = title;
    }
    
    // notation
    const std::string notation{xmlrules.child("notation").text().as_string()};
    if (notation == "french")
    {
        part.m_tabType = TabFrench;
    }
    else if (notation == "german")
    {
        part.m_tabType = TabGerman;
    }
    else if (notation == "italian")
    {
        part.m_tabType = TabItalian;
    }
    else if (notation == "spanish")
    {
        part.m_tabType = TabSpanish;
    }

    // crotchet_flag
    const std::string crotchetFlag{xmlrules.child("crotchet_flag").text().as_string()};
    if (!crotchetFlag.empty())
    {
        const std::string_view flags{"BWHQESTYZ"};
        const auto p{flags.find(crotchetFlag[0])};
        if (p != std::string_view::npos)
        {
            part.m_crotchetFlag = static_cast<int>(p - 2); // Q is tabCode default = 1 flag
        }
    }
    
    // fretting
    const std::string fretting{xmlrules.child("fretting").text().as_string()};
    if (!fretting.empty())
    {
        ParseFretting(fretting, part);
    }
    
    std::string tuning{xmlrules.child("tuning").text().as_string()};
    
    if (!tuning.empty())
    {
        // all courses specified
        const xml_node xmlpitch{xmlrules.child("pitch")};
        const int pitch{xmlpitch != nullptr ? xmlpitch.text().as_int(67) : 67};
        part.m_tuning.emplace_back(pitch);
    }
    else
    {
        tuning = xmlrules.child("bass_tuning").text().as_string();
        if (!tuning.empty())
        {
            // only diapasons specified.  Default first 6 courses
            const int numCourses{(std::string(xmlrules.child("tuning_named").text().as_string()) == "baroque") ? 13 : 6};
            Pitch::SetTuning(numCourses, part.m_tuning);
            part.m_tuning.resize(Course6); // only first 6 courses default
        }
        else
        {
            return;
        }
    }
    
    for (size_t idx{1}; idx < tuning.size() && tuning[idx] != ')';)
    {
        if (tuning[idx] == ' ')
        {
            ++idx;
            continue;
        }
        
        size_t inc{0};
        part.m_tuning.emplace_back(part.m_tuning.back().Midi() + std::stoi(tuning.substr(idx), &inc));
        idx += inc;
    }
    
    LOGGER << "Source tuning " << Pitch::GetTuning(part.m_tuning);
}

void ParserTabCode::ParseFretting(std::string_view fretting, Part& part)
{
    // <fretting>
    // ((1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1)
    // (1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1)
    // (1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1)
    // (1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1)
    // (1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1)
    // (1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1))
    // </fretting>

    try
    {
        size_t pos{0};
        int fret{1};
        int tuning{0};
    
        // Assume that if a fret is missing on the top course then it is missing on all courses.
        // Not interested in partial frets, treat as whole frets, as they do not affect fret letters.
        for (;;)
        {
            while (pos < fretting.size() && (fretting[pos] == '(' || fretting[pos] == ' ' || fretting[pos] == '\n'))
            {
                ++pos;
            }
        
            if (pos == fretting.size() || fretting[pos] == ')')
            {
                return;
            }
           
            size_t len{0};
            tuning += static_cast<int>(std::stoi(std::string(fretting.substr(pos)), &len));
            pos += len;
            
            while (fret < tuning)
            {
                part.m_missingFrets.push_back(fret);
                LOGGER << "missing fret : " << fret;
                ++fret;
            }
            ++fret;
        }
    }
    catch (...)
    {
        LOGGER << "error fretting";
        part.m_missingFrets.clear();
    }
}

} // namespace luteconv
