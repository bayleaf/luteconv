#ifndef _OPTIONS_H_
#define _OPTIONS_H_

#include "pitch.h"

#include <string>
#include <string_view>
#include <sstream>

namespace luteconv
{

enum Format
{
    FormatUnknown,
    FormatAbc,          // Abctab
    FormatFrt,          // Humdrum **fret
    FormatFt2,          // Fronimo
    FormatFt3,          // Fronimo
    FormatJtxml,        // Fandando
    FormatJtz,          // Fandango zip
    FormatLsml,         // LuteScribe
    FormatLtl,          // Lute Tablature with Ligatures font
    FormatMei,          // Music Encoding Initiative (Tablature Interest Group)
    FormatMid,          // MIDI
    FormatMnx,          // MNX
    FormatMusicxml,     // MusicXML
    FormatMxl,          // MusicXML zip
    FormatTab,          // Tab
    FormatTabCode,      // TabCode
};

enum TabType
{
    TabUnknown,
    TabFrench,          // letters for frets, staff line 6 = course 1
    TabGerman,          // no staff, different symbol for each course/fret
    TabItalian,         // numbers for frets, staff line 1 = course 1
    TabNeapolitan,      // As spanish except open string is 1
    TabSpanish,         // numbers for frets, staff line 6 = course 1.  Aka milan, modern guitar tab
    
    TabCmn,             // Common Music Notation - for melody line in a lute song
};

// TabGerman subtypes for 6th and higher courses
enum TabGermanSubtype
{
    GstUnknown,
    GstABC,             // A B C D ...
    Gst1AB,             // 1+ A B C ...
    Gst123,             // 1- 2- 3- 4- ...
};

class Options
{
public:
    
    /**
     * Constructor
     */
    Options();
    
    /**
     * Destructor
     */
    ~Options() = default;
    
    /**
     * Process options
     * 
     * @param[in] argc count of arguments
     * @param[in] argv arguments
     * @return true => continue, false => exit
     */
    bool ProcessArgs(int argc, char** argv);
    
    /**
     * If not set, set the format from the filenames
     * 
     */
    void SetFormatFilename();
    
    /**
     * Get the tab type as text
     * 
     * @param[in] tabType
     * @return tab type as text
     */
    static const char * GetTabType(TabType tabType);
    
    /**
     * Get the tab subtype as text
     * 
     * @param[in] tabSubtype
     * @return tab subtype as text
     */
    static const char * GetTabGermanSubtype(TabGermanSubtype tabSubtype);
    
    Format m_srcFormat{FormatUnknown};
    Format m_dstFormat{FormatUnknown};
    TabType m_srcTabType{TabUnknown};
    TabType m_dstTabType{TabUnknown};
    TabGermanSubtype m_dstTabGermanSubtype{GstUnknown};
    std::vector<Pitch> m_tuning;
    std::vector<Pitch> m_7tuning;
    std::vector<Pitch> m_dstTuning;
    std::string m_srcFilename;
    std::string m_dstFilename;
    const std::string m_version;
    int m_index{0};
    int m_flags{0};
    int m_wrapThreshold{25};
    int m_transpose{0};
    int m_rhythmScheme{0};
    int m_midiPatch{0};
    double m_midiTempo{60};
    bool m_multi{false};
    bool m_noHeuristics{false};
    bool m_barSupp{false};
    bool m_list{false};
    bool m_intabulate{false};
    
    const int m_wrapDefault{25}; // default for the --wrap option
    
private:
    void PrintHelp(std::string_view allowed);
    static Format GetFormat(std::string_view format);
    static Format GetFormatFilename(std::string_view filename);
    static TabType GetTabType(std::string_view tabType);
    static TabGermanSubtype GetTabGermanSubtype(std::string_view tabSubtype);
};

} // namespace luteconv

#endif // _OPTIONS_H_
