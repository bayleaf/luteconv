#ifndef _PARSERABC_H_
#define _PARSERABC_H_

#include <iostream>
#include <string_view>

#include "parser.h"

namespace luteconv
{

/**
 * Parse AbcTab .abc file
 * 
 * http://www.lautengesellschaft.de/cdmm/
 */
class ParserAbc: public Parser
{
public:

    /**
     * Constructor
    */
    ParserAbc() = default;

    /**
     * Destructor
     */
    ~ParserAbc() override = default;
    
    /**
     * Parse .abc file
     *
     * @param[in] src .abc stream
     * @param[in] options
     * @param[out] piece destination
     */
    void Parse(std::istream& srcFile, const Options& options, Piece& piece) override;
    
private:
    void ParseHeader(std::string_view line, int lineNo, Piece& piece, Part& part);
    void ParseMusic(std::string_view line, int lineNo, Part& part, bool& barIsClear, int& maxLedger);
    static void ParseBarLine(std::string_view line, int lineNo, size_t& idx, Part& part, bool& barIsClear);
    void ParseChord(std::string_view line, int lineNo, size_t& idx, Part& part, bool& fermata, Fingering& fingering, bool& nextHalved,
            bool& nextDotted, bool& nextTriplet, Connector& nextSlur, bool& barIsClear, int& maxLedger);
    static void ParseRhythm(std::string_view line, size_t &idx, NoteType baseNoteType, int addFlags, NoteType& noteType, bool& dotted);
    static void ParseInformation(std::string_view line, int lineNo, size_t& idx, Part& part);
    static void ParseTimeSignature(std::string_view line, int lineNo, size_t idx, TimeSig& timeSig);
    static std::string Iso8859_1ToUtf8(std::string_view in);
    void ParsePseudoComment(std::string_view line, int lineNo, Part& part);
    static void FixLedgers(Part& part, int lwb, int upb);
    
    NoteType m_defaultNoteType{NoteTypeEighth};
    bool m_haveAddFlags{false};
    int m_addFlags{0};
    int m_defaultAddFlags{2};
    TimeSig m_timeSig;
};

} // namespace luteconv

#endif // _PARSERABC_H_
