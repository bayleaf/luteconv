!!!OTL: Anna Magdalena Bach Notebook Menuet II
!!!COM: J.S. Bach
!!!RNB: Converted to Humdrum .frt by luteconv 1.4.9
!!!RDT: 2022/11/25
**recip	**fret
*	*AT:C3
*	*RT:0:5:9:14:19
*M3/4	*M3/4
=1-	=1-
4	|4 - - - |0
8	- - |3 - -
8	- - - |0 -
8	|2 - - |2 -
8	- - - |3 -
=2	=2
4	|4 - - - |0
4	- - |3 - -
4	- - |3 - -
=3	=3
4	|5 - - - |2
8	- - - |3 -
8	- - - - |0
8	- - - - |2
8	- - - - |4
=4	=4
2	|4 - - - |5
==|!	==|!
*-	*-
