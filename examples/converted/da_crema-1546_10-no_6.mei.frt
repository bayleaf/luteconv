!!!OTL: Recercar sexto
!!!COM: Giovanni Maria da Crema
!!!RNB: Converted to Humdrum .frt by luteconv 1.4.9
!!!RDT: 2022/11/21
**recip	**fret
*	*AT:G2
*	*RT:0:5:10:14:19:24
=1-	=1-
4	|0 - |2 - - -
8.	|0 - |2 - - -
16	|2 - - - - -
=2	=2
8	|3 - |2 - - -
8	- |0 - - - -
8	|3 - |2 - - -
8	- |3 - - - -
=3	=3
8	- |2 - |0 - -
8	- |0 - - - -
8	- - - |0 - -
32	- |2 - - - -
32	- |0I - - - -
32	|3 - - - - -
32	|2I - - - - -
=4	=4
8	|0 - - |1 - -
8	- |3 - - - -
8	- |2 - |1 - -
8	|3 - - - - -
=5	=5
8	- |0 - |0 - -
8	- |2 - - - -
8	- |3 |2 - - -
16	- - - |1I - -
16	- |2 - - - -
=6	=6
8	- |0 - |0 - -
8	|3 - |2 - - -
8	|2 - - - - -
8	- - |1 - - -
=7	=7
8.	|0 - |2 - - |0
16	- - - |0 - -
8	- - - |1 - |0
8	- - - |3 - -
=8	=8
8	- - - |1 - |0
8	- - |2 - - -
8	- - - |1 - |0
8	- - - - |1 -
=9	=9
8	- - - - |0 |2
8	- - - |3 - -
8	- - - - - |2I
16	- - - |1 - -
16	- - - |0I - -
==	==
*-	*-
