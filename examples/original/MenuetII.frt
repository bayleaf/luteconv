!! https://www.humdrum.org/Humdrum/representations/fret.rep.html
!! https://www.humdrum.org/Humdrum/guide18.html
!! Example 18.2.
!!!OTL: Anna Magdalena Bach Notebook Menuet II
!!!COM: J.S. Bach
**recip	**kern	**fret
*	*	*AT:G2
*	*	*RT:0,12:5,17:10,22:14,14:19,19:24,24
*M3/4	*	*M3/4
=1	=1	=1
4	E e g	- |4 - - - |0
8	c	- : : |3 : :
8	d	- : : : |0 x
8	D d e	- |2 : : |2 :
8	f	- : : : |3 :
=2	=2	=2
4	E e g	- |4 : : : |0
4	c	- : : |3 : :
4	c	- : : |3 : x
=3	=3	=3
4	F f a	- |5 : : : |2W
8	f	- : : : |3 :
8	g	- : : : : |0
8	a	- : : : : |2
8	b	- : : : : |4
=4	=4	=4
2	E e cc	- |4 : : : |5v
*-	*-	*-
