Luteconv - Convert between lute tablature file formats
======================================================

There is a variety of lute tablature editing and formatting software available, with
a corresponding variety of file formats used to store the lute tablature.  Luteconv
is a command line utility that converts between some of these different formats.
Currently available for Linux and Windows.

The original motivation for this project was so that pieces from the vast library
of lute tablature available in Fronimo .ft3, Fantango .jtz, Tab .tab and TabCode .tc
formats on the net[2,3,4,15,16] could be imported into MuseScore[11] as MusicXML files.
The currently supported file formats are:

Abctab2ps abc
-------------
Abctab2ps[1] is a music and tablature typesetting program which translates an input file
in the abc language into postscript. It is based on Michael Methfessel's program abc2ps.
While abc2ps can only typeset music, abctab2ps is an extension by Christoph Dalitz that
can also handle lute tablature.  Abctab2ps is well documented and its source code is published.

Fandango jtxml, jtz
-------------------
Fandango[3] is a commercial Windows lute tablature editor written by Alain Veylit. Its file format,
jtxml, is a closed, proprietary XML format; jtz is zip compressed jtxml.  Luke Emmet[7] has
reverse engineered the format for his program LuteScribe so that some of the data can be extracted.
I am grateful for his work on this.

Fronimo ft2, ft3
----------------
Fronimo[4] is a commercial Windows lute tablature editor written by Francesco Tribioli. Its file formats,
ft2, ft3, are closed, proprietary binary formats.  Luke Emmet[7] has reverse engineered the formats
for his program LuteScribe so that some the data can be extracted.  I am grateful for his work on this.

Humdrum frt
-----------
Humdrum[6] was originally created by David Huron in the 1980s, and it has been used steadily for decades.
The Humdrum “universe” might be thought of as consisting of two main segments: the notational style
(particularly the kern representation) and the software for analyzing the notated material.
Luteconv supports the fret and recip representations. 

LuteScribe lsml
---------------
LuteScribe[7] is a free (and open source) MS Windows application by Luke Emmet for viewing and
editing musical tabulature for lutes and related historical instruments.

MIDI mid
--------
MIDI[9] (Musical Instrument Digital Interface) is an industry standard music technology protocol
that connects digital musical instruments, computers, tablets, and smartphones from many different
companies. 

MNX mnx
-------
MNX[10] is a new, open standard for representing music notation as machine-readable data.
It is being developed by the W3C.  MNX is a work in progress, and "it's not ready for
implementation". There is as yet no draft specification for tablature so this is a
highly speculative, experimental implementation.  MNX is switching to JSON, watch this space.

Music Encoding Initiative mei
-----------------------------
The Music Encoding Initiative (MEI)[8] is a community-driven effort to define a system for
encoding musical documents in a machine-readable structure. This is an experimental
implementation of some proposals from the Tablature Interest Group[15].

MusicXML musicxml, mxl
----------------------
MusicXML[12] is an music interchange file format and is supported by many music editing programs
e.g. MuseScore, Sibelius, Finale etc.  Its file format, musicxml, is an open,
documented XML format; mxl is a zip compressed musicxml. Although designed primarily for Common
Western Music Notation MusicXML does support lute (and guitar) tablature.  MusicXML is well documented.

Tab tab
-------
Tab[14] is a lute tablature type setting program written by Wayne Cripps.  Its file format, tab,
is a text file designed for user input.  Tab is the lingua franca of tablature file formats and
is often supported as an import format by other lute tablature editing software.  Tab is well
documented and its source code is published.

TabCode tc
----------
TabCode[16] devised by Tim Crawford.  Used by ECOLM - An Electronic Corpus of Lute Music, Goldsmith's University of London.
Where there is a large searchable database of lute pieces.  TabCode is well documented.

Supported source formats
------------------------
abc, frt, ft2, ft3, jtxml, jtz, lsml, mei, mid, mnx, musicxml, mxl, tab and tc.

Supported destination formats
-----------------------------
abc, frt, lsml, mei, mid, mnx, musicxml, mxl, tab and tc.

Supporting proprietary destination formats (jtxml, jtz, ft2, ft3) is not possible as this would require
complete knowledge of their structure and semantics, which is not available.  Whereas they can be used as
source formats as enough information has been gleaned by reverse engineering to give at least the notes,
rhythm and bars.

Converting a file to its own format is supported, although some information may be lost.  However, it can be useful
for converting between different tablature types: french, german, italian, neapolitan, spanish.

Lute Songs
----------
There is limited support for lute songs: one or two melody staff lines with optional lyrics and a single
line of tablature.  Currently the only supported lute song cenvertions are: source formats: mei, musicxml
and tab; destination formats: mei, musicxml and tab. This is work in progress, other lute song convertions
TBA. All other convertions will ignore the melodies and lyrics.
 
Limitations
-----------
* Solo instrument.
* Conversions are "best effort" some data (specific fonts, layout, special symbols) may be lost. 

Platform
--------
luteconv is implemented for Linux (openSUSE) and although it should work on other Linux distributions or
possibly the Windows Subsystem for Linux (WSL) it has not been tested on these platforms.  A Windows
build has been provided by Luke Emmet.  See details below.

Usage
-----
    Usage: luteconv [options ...] [source-file] [destination-file]

    | option                             | function                                    |
    | ------                             | --------                                    |
    | -h --help                          | Show help                                   |
    | -v --version                       | Show version                                |
    | -o --output <destination-file>     | Set destination-file                        |
    | -S --Srctabtype <tabtype>          | Set source tablature type                   |
    | -D --Dsttabtype <tabtype>          | Set destination tablature type              |
    | -G --dstGermanSubtype <gltsubtype> | Set destination German tablature subtype    |
    | -s --srcformat <format>            | Set source format                           |
    | -d --dstformat <format>            | Set destination format                      |
    | -t --tuning <tuning>               | Set tuning for all courses                  |
    | -7 --7tuning <tuning>              | Set tuning from 7th course                  |
    | -T --dstTuning <tuning>            | Set the destination tuning for all courses  |
    | -i --index <index>                 | Set section index                           |
    | -m --multi                         | Multiple sections, output one per file      |
    | -n --noHeuristics                  | Disable heuristics for splitting .tab files |
    | -f --flags <num>                   | Add flags to destination rhythm             |
    | -V --Verbose                       | Set verbose output                          |
    | -w --wrap <num>                    | Set the stave wrap threshold                |
    | -p --transpose <num>               | Transposition in semitones                  |
    | -b --barsupp                       | Suppress initial bar line on each system    |
    | -l --list                          | List sections in the source file            |
    | -I --intabulate                    | Intabulate                                  |
    | -r --rhythmscheme <num>            | Set destination rhythm scheme               |
    | -M --midipatch <num>               | Set the midi patch number                   |
    | -P --miditempo <num>               | Set the midi tempo in beats per minute      |

Options may use long or short syntax: --7tuning=D2 or -7D2

The source-file can be specified as the 1st positional parameter. If no source-file is specified
then standard input is used, in which case the source file format must be specified, --srcformat.

The destination-file can be specified either using the --output option or as the 2nd positional parameter,
this conforms to the GNU Standards for Command Line Interfaces[5]. If no destination-file is specified
then standard output is used, in which case the destination file format must be specified, --dstformat.
 
    format = "abc" | "frt" | "ft2" | "ft3" | "jtxml" | "jtz" | "lsml" | "mei" | "mid" | "mnx" | "musicxml" | "mxl" | "tab" | "tc"
  
if a file format is not specified then the filetype is used.
         
    tabtype = "french" | "german" | "italian" | "neapolitan" | "spanish"

The source tablature type is usually deduced from the source-file.  However, for tab
files it is sometimes necessary to distinguish between french, italian and spanish tablatures.
The default destination tablature type is the source's tablature type.

    gltsubtype = "ABC" | "1AB" | "123"
    
Specifies the representation of 6th and highter courses for German tablature.
            
    tuning = Courses in scientific pitch notation[13], in increasing course number.
  
Luteconv uses the tuning specifed by option --tuning, if given; otherwise 
the tuning specified in the source file, if any; otherwise the
tuning is based on the number of courses used in the piece as follows:
  
      = 8   "G4 D4 A3 F3 C3 G2 F2 D2"  
      <= 10 "G4 D4 A3 F3 C3 G2 F2 Eb2 D2 C2"  
      >= 11 "F4 D4 A3 F3 D3 A2 G2 F2 E2 D2 C2 B1 A1 G1"  

Option --7tuning, if given, will then modify the tuning of the 7th, 8th, ... courses.

Luteconv supports transcription, by transposition of pitch and by having
a different tuning or number of courses for the destination instrument.

The destination tuning may be specified with --dstTuning, defaults to the
source tuning. If the destination tuning is different from the source
tuning then the music is intabulated.

If the --transpose option is given then all pitches are transposed by the
specified number of semitones, positive or negative, and the music is
intabulated.

MIDI .mid may be used a source format, but a lot of the information required
for tablature is missing.  The music has to be intabulated and barring
re-calculated. The default tuning is "G4 D4 A3 F3 C3 G2 F2 Eb2 D2 C2",
otherwise the tuning must be specified.

If any of --intabulate, --dstTuning, --transpose or if MIDI is the source format
then the music is intabulated, which may result in difficult stetches or
unplayable chords. This feature is experimental.
         
MIDI .mid may be used a destination format in order to play the music. Repeats are ignored.
The patch number may be specified with --midipatch.
The tempo may be specified in beats per minute with --miditempo. One beat is a rhythm sign
with no flags.
         
Where the source format allows more than one piece/section per file the --index option
selects the desired piece/section, counting from 0.  Default 0.

The .tab format does not support multiple piece/sections, nevertheless it can be used
with piece/sections separated by blank line and {section title}.  Heuristics are used
to determine these piece/section breaks.  These herustics are also used to determine
the tuning, if not otherwise specified, and to offer limited support for the
diatonic cittern.  Option --noHeuristics will disable this behaviour.

If the --multi option is given then all piece/sections, counting from the index,
are output to separate files.  The destination filename is used as a template with
the index number inserted before the last ".".  E.g. a destination file mytune.tab
will output piece/sections to mytune-0.tab, mytune-1.tab etc.

If the --list option is given then all the piece/sections, counting from the
index, are listed to stdout together with their index numbers.
No conversion is done.

Some lute software encodes rhythm as the number of lute tablature flags, others
encode the note value (whole, half, quarter etc) unfortunately there is
no fixed mapping between the two.  The --flags option adds (or subtracts) flags
from the destination rhythm to adjust this mapping.  Default 0.

Option --wrap, default 25.  Some formats require explicit staff line endings.
Luteconv uses a herustic: count chords, then when the wrap threshold is reached end the
staff at the end of the current bar.

If the  --barsupp is given then initial bar line on each system is suppressed,
unless it's a repeat.  This only affects formats that require explicit staff
line endings.

If the --rhythmscheme is given then the destination rhythm flags are set
according to the scheme number: 0 (default) preserve the source rhythm scheme;
1 only show a rhythm sign if it differs from the previous sign, or if it is
dotted, or the previous sign is dotted; 2 as per 1 but always show the first
rhythm sign in the bar. If either 1 or 2 is used then any beaming is removed.

Examples
--------

Convert tab to musicxml

    luteconv Kapsberger-Gagliarda5a.tab Kapsberger-Gagliarda5a.musicxml

Convert Luis de Milan (spanish tablature) tab to mxl 

	luteconv --Srctabtype=spanish Milan_Fantasia_10.tab Milan_Fantasia_10.mxl
	
Convert 7 course piece with 7th course tuned to D

	luteconv --7tuning=D2 Loath.tab Loath.mxl
	
Convert 2nd piece from a Fandango collection to tab (index counts from 0)

	luteconv --index=1 Willoughby.jtz Fantacy.tab
	
Convert all pieces from a Fandango collection to TabCode (index counts from 0)
Files will be Fantacy-0.tc, Fantacy-1.tc, etc

    luteconv --multi Willoughby.jtz Fantacy.tc
	
Transcribe a piece for the bandora to a 7 course lute by transposing the pitch
by 10 semitones and changing the destination tuning.

    luteconv -p 10 -t A3E3C3G2D2C2G1 -T G4D4A3F3C3G2F2 bandora.tab lute.tab
    
    
Download pre-built binary for Linux
-----------------------------------

Version: 1.5.3, tag: release-1.5.3

* [luteconv-1.5.3-1.x86_64.rpm](http://www.overell.co.uk/luteconv/luteconv-1.5.3-1.x86_64.rpm) SHA-256:03c22bcd215eab8ed524dd104efa60223d945bbfbbc0d5b0f8298bec54effc1b
* [luteconv-1.5.3-1.x86_64.deb](http://www.overell.co.uk/luteconv/luteconv-1.5.3-1.x86_64.deb) SHA-256:56f6818dafbfc035132529318b255b87c1aef9edf1ef7bfcc29d8b2fb99cf583
* [luteconv-1.5.3-1.x86_64.tar.gz](http://www.overell.co.uk/luteconv/luteconv-1.5.3-1.x86_64.tar.gz) SHA-256:a5d7b2bb12b5b42ed7a085e12533d1be330d900e750d92022d91c10eb992a15b

Download pre-built binary for Windows
-------------------------------------

Luke Emmet has provided a build of luteconv for Windows.

[luteconv for Windows](https://www.marmaladefoo.com/pages/luteconv)

Build for Linux
---------------

The implementation is for Linux implemented in C++17 using cmake and tested on openSUSE 15.6, but
I expect any modern Linux or BSD distribution could be used. Luteconv has
been built on Macs by third parties, but omit the "make package" step as Mac
packaging has not been implemented.

In a suitable workspace directory:

    git clone https://bayleaf@bitbucket.org/bayleaf/luteconv.git
    mkdir build
    cd build
    cmake ../luteconv
    make
    make test
    make package

The master branch is used for development.  Releases are tagged release-x.y.z

luteconv has no 3rd party build dependencies.  Tests can be disabled with a cmake option -DBUILD_TESTS=OFF.

The executables will be in build/bin.  The .rpm, .deb and .tar.gz packages will be in the build directory.

Build for Windows
-----------------

Luke Emmet has provided Visual Studio projects files and libraries to build luteconv for Windows.  See the windows directory.

Plugin for MuseScore 3, lute-import
-----------------------------------

For Linux a plugin for MuseScore is installed in /usr/local/share/luteconv/MuseScore3/Plugins/lute-import.qml.
This plugin runs a file dialog to select the lute tablature file to import using luteconv, in any of the
supported source formats.  No luteconv command line parameters can be set using this plugin.
To enable this plugin either copy or symlink it into ~/Documents/MuseScore3/Plugins then in MuseScore, Plugins,
Plugin Manager... reload the plugins and enable lute-import.

TODO
----
1 Other lute tablature file formats to consider:

* [BeierTab](https://www.musico.it/beier-software/beiertab-2/)
* [LilyPond](https://lilypond.org/)
* [TabXML](https://webspace.science.uu.nl/~wieri103/tabxml/)

2 More research into Fronimo ft3 format.

3 More research into Fandango jtxml format.

References
----------
1.  [Abctab2ps](http://www.lautengesellschaft.de/cdmm/)
2.  [Accessible Lute Music](https://wp.lutemusic.org/)
3.  [Fandango](http://fandango.musickshandmade.com/pages/fandango)
4.  [Fronimo](https://sites.google.com/view/fronimo/home)
5.  [GNU](https://www.gnu.org/prep/standards/html_node/Command_002dLine-Interfaces.html)
6.  [Humdrum](https://www.humdrum.org)
7.  [LuteScribe](https://www.orlando-lutes.com/pages/lutescribe)
8.  [MEI](https://music-encoding.org/)
9.  [MIDI](https://www.midi.org/)
10. [MNX](https://w3c.github.io/mnx/docs/)
11. [MuseScore](https://musescore.org/en)
12. [MusicXML](https://w3c.github.io/musicxml/)
13. [Scientific Pitch Notation](https://en.wikipedia.org/wiki/Scientific_pitch_notation)
14. [Tab](https://www.cs.dartmouth.edu/~wbc/lute/AboutTab.html)
15. [Tab Tablature](https://www.cs.dartmouth.edu/~wbc/tab-serv/tab-serv.cgi)
16. [TabCode](http://doc.gold.ac.uk/isms/ecolm/?page=TabCode)
17. [Tablature Interest Group](https://github.com/music-encoding/tablature-ig)
